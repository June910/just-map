## 目录结构

```bash
├── api # 接口请求
│   ├── justMap.ts
│   └── studio.ts
├── assets # 资源文件
│   └── images
├── config 
│   └── index.ts
├── constant.ts
├── core # 
│   ├── index.ts
│   └── util.ts
├── index.ts # 入口文件
├── mapbox # 与mapbox 相关的方法
│   └── index.ts
├── react # react wrapper
│   ├── context
│   ├── index.tsx
│   └── layer
└── utils 
    ├── getRequestInstance.ts
    ├── index.ts
    ├── logger.ts
    └── useJsonData.tsx
```