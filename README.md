# just-map

基于 just 能力核心前端地图库

## 主要功能

- 渲染在 just-studio 中配置的地图样式, 同时支持添加各类专题图层
  - 数据源操作
  - 图层操作
  - 视图操作

## 安装

```bash
npm config set registry=http://registry.m.jd.com
npm install @jd/just-map
```

## 使用示例

更多详细的使用说明,可参考[demo 项目](http://dev-doc.smart-urban.cn/just-map/#/)

## 本地 test 流程

- cd just-map
  - npm link 到全局
  - npm link just-map-test/node_modules/react 在 lib 中 link 一下测试项目中 react
  - cd just map test
  - npm link @jd/just-map
  - npm i mapbox-gl@1.13.0 deck.gl @deck.gl/mapbox @deck.gl/geo-layers @deck.gl/core three@0.109.0 @turf/turf -S
  - npm start

## 发布

- npm run release

## 反馈

https://git.jd.com/web-uc-weapons/just-map/issues
