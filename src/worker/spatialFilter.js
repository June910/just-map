/* eslint-disable no-cond-assign */
function spatailFilter() {
  // self.importScripts('https://unpkg.com/@turf/turf@6.3.0/turf.min.js');
  // self.importScripts('turf.js');
  // import turf from './turf.js';
  // const turf = require('./turf.js');
  // console.log("🚀 ~ file: spatialFilter.js ~ line 5 ~ spatailFilter ~ turf", turf)
  // self.importScripts(turf);
  // function aa(){
  //     console.log('hahaha,I am module from aa function');
  // }
  // wkt 解析 为geojson
  const numberRegexp = /[-+]?([0-9]*\.[0-9]+|[0-9]+)([eE][-+]?[0-9]+)?/;
  const tuples = new RegExp(
    '^' + numberRegexp.source + '(\\s' + numberRegexp.source + '){1,}',
  );
  function wktParse(input) {
    const parts = input.split(';');
    let _ = parts.pop();
    const srid = (parts.shift() || '').split('=').pop();

    let i = 0;

    function $(re) {
      const match = _.substring(i).match(re);
      if (!match) return null;
      i += match[0].length;
      return match[0];
    }

    function crs(obj) {
      if (obj && srid.match(/\d+/)) {
        obj.crs = {
          type: 'name',
          properties: {
            name: `urn:ogc:def:crs:EPSG::${srid}`,
          },
        };
      }

      return obj;
    }

    function white() {
      $(/^\s*/);
    }

    function multicoords() {
      white();
      let depth = 0;
      const rings = [];
      const stack = [rings];
      let pointer = rings;
      let elem;

      while ((elem = $(/^(\()/) || $(/^(\))/) || $(/^(,)/) || $(tuples))) {
        if (elem === '(') {
          stack.push(pointer);
          pointer = [];
          stack[stack.length - 1].push(pointer);
          depth++;
        } else if (elem === ')') {
          // For the case: Polygon(), ...
          if (pointer.length === 0) return null;

          pointer = stack.pop();
          // the stack was empty, input was malformed
          if (!pointer) return null;
          depth--;
          if (depth === 0) break;
        } else if (elem === ',') {
          pointer = [];
          stack[stack.length - 1].push(pointer);
          // eslint-disable-next-line no-restricted-globals
        } else if (!elem.split(/\s/g).some(Number.isNaN)) {
          Array.prototype.push.apply(
            pointer,
            elem.split(/\s/g).map(parseFloat),
          );
        } else {
          return null;
        }
        white();
      }

      if (depth !== 0) return null;

      return rings;
    }

    function coords() {
      const list = [];
      let item;
      let pt;
      while ((pt = $(tuples) || $(/^(,)/))) {
        if (pt === ',') {
          list.push(item);
          item = [];
        } else if (!pt.split(/\s/g).some(Number.isNaN)) {
          if (!item) item = [];
          Array.prototype.push.apply(item, pt.split(/\s/g).map(parseFloat));
        }
        white();
      }

      if (item) list.push(item);
      else return null;

      return list.length ? list : null;
    }

    function point() {
      if (!$(/^(point(\sz)?)/i)) return null;
      white();
      if (!$(/^(\()/)) return null;
      const c = coords();
      if (!c) return null;
      white();
      if (!$(/^(\))/)) return null;
      return {
        type: 'Point',
        coordinates: c[0],
      };
    }

    function multipoint() {
      if (!$(/^(multipoint)/i)) return null;
      white();
      const newCoordsFormat = _.substring(_.indexOf('(') + 1, _.length - 1)
        .replace(/\(/g, '')
        .replace(/\)/g, '');
      _ = `MULTIPOINT (${newCoordsFormat})`;
      const c = multicoords();
      if (!c) return null;
      white();
      return {
        type: 'MultiPoint',
        coordinates: c,
      };
    }

    function multilinestring() {
      if (!$(/^(multilinestring)/i)) return null;
      white();
      const c = multicoords();
      if (!c) return null;
      white();
      return {
        type: 'MultiLineString',
        coordinates: c,
      };
    }

    function linestring() {
      if (!$(/^(linestring(\sz)?)/i)) return null;
      white();
      if (!$(/^(\()/)) return null;
      const c = coords();
      if (!c) return null;
      if (!$(/^(\))/)) return null;
      return {
        type: 'LineString',
        coordinates: c,
      };
    }

    function polygon() {
      if (!$(/^(polygon(\sz)?)/i)) return null;
      white();
      const c = multicoords();
      if (!c) return null;
      return {
        type: 'Polygon',
        coordinates: c,
      };
    }

    function multipolygon() {
      if (!$(/^(multipolygon)/i)) return null;
      white();
      const c = multicoords();
      if (!c) return null;
      return {
        type: 'MultiPolygon',
        coordinates: c,
      };
    }

    function geometrycollection() {
      const geometries = [];
      let geometry;

      if (!$(/^(geometrycollection)/i)) return null;
      white();

      if (!$(/^(\()/)) return null;
      while ((geometry = root())) {
        geometries.push(geometry);
        white();
        $(/^(,)/);
        white();
      }
      if (!$(/^(\))/)) return null;

      return {
        type: 'GeometryCollection',
        geometries,
      };
    }

    function root() {
      return (
        point() ||
        linestring() ||
        polygon() ||
        multipoint() ||
        multilinestring() ||
        multipolygon() ||
        geometrycollection()
      );
    }

    return crs(root());
  }
  function getGeomType(feature) {
    if (feature.type === 'Feature') {
      return feature.geometry.type;
    }
    return feature.type;
  }
  function getGeom(geojson) {
    if (geojson.type === 'Feature') {
      return geojson.geometry;
    }
    return geojson;
  }
  function getCoord(coord) {
    if (!coord) {
      throw new Error('coord is required');
    }
    if (!Array.isArray(coord)) {
      if (
        coord.type === 'Feature' &&
        coord.geometry !== null &&
        coord.geometry.type === 'Point'
      ) {
        return coord.geometry.coordinates;
      }
      if (coord.type === 'Point') {
        return coord.coordinates;
      }
    }
    if (
      Array.isArray(coord) &&
      coord.length >= 2 &&
      !Array.isArray(coord[0]) &&
      !Array.isArray(coord[1])
    ) {
      return coord;
    }
    throw new Error('coord must be GeoJSON Point or an Array of numbers');
  }
  function getCoords(coords) {
    if (Array.isArray(coords)) {
      return coords;
    }
    // Feature
    if (coords.type === 'Feature') {
      if (coords.geometry !== null) {
        return coords.geometry.coordinates;
      }
    } else {
      // Geometry
      if (coords.coordinates) {
        return coords.coordinates;
      }
    }
    throw new Error(
      'coords must be GeoJSON Feature, Geometry Object or an Array',
    );
  }
  function getMidpoint(pair1, pair2) {
    return [(pair1[0] + pair2[0]) / 2, (pair1[1] + pair2[1]) / 2];
  }
  function multiToSingle(feature) {
    if (feature.type === 'Feature') {
      const geometry = feature.geometry;
      const geometryType = geometry.type;
      // @ts-ignore
      const coordinateList = feature.geometry.coordinates;
      return coordinateList.map((coor) => ({
        ...feature,
        geometry: {
          type: geometryType.substr(geometryType.indexOf('i') + 1),
          coordinates: coor,
        },
      }));
    }
    // @ts-ignore
    const coordinateList = feature.coordinates;
    const geometryType = feature.type;
    return coordinateList.map((coor) => ({
      type: geometryType.substr(geometryType.indexOf('i') + 1),
      coordinates: coor,
    }));
  }
  function compareCoords(pair1, pair2) {
    return pair1[0] === pair2[0] && pair1[1] === pair2[1];
  }
  function bbox(geojson) {
    var result = [Infinity, Infinity, -Infinity, -Infinity];
    coordEach(geojson, function (coord) {
      if (result[0] > coord[0]) {
        result[0] = coord[0];
      }
      if (result[1] > coord[1]) {
        result[1] = coord[1];
      }
      if (result[2] < coord[0]) {
        result[2] = coord[0];
      }
      if (result[3] < coord[1]) {
        result[3] = coord[1];
      }
    });
    return result;
  }
  function coordEach(geojson, callback, excludeWrapCoord) {
    // Handles null Geometry -- Skips this GeoJSON
    if (geojson === null) return;
    var j,
      k,
      l,
      geometry,
      stopG,
      coords,
      geometryMaybeCollection,
      wrapShrink = 0,
      coordIndex = 0,
      isGeometryCollection,
      type = geojson.type,
      isFeatureCollection = type === 'FeatureCollection',
      isFeature = type === 'Feature',
      stop = isFeatureCollection ? geojson.features.length : 1;

    // This logic may look a little weird. The reason why it is that way
    // is because it's trying to be fast. GeoJSON supports multiple kinds
    // of objects at its root: FeatureCollection, Features, Geometries.
    // This function has the responsibility of handling all of them, and that
    // means that some of the `for` loops you see below actually just don't apply
    // to certain inputs. For instance, if you give this just a
    // Point geometry, then both loops are short-circuited and all we do
    // is gradually rename the input until it's called 'geometry'.
    //
    // This also aims to allocate as few resources as possible: just a
    // few numbers and booleans, rather than any temporary arrays as would
    // be required with the normalization approach.
    for (var featureIndex = 0; featureIndex < stop; featureIndex++) {
      geometryMaybeCollection = isFeatureCollection
        ? geojson.features[featureIndex].geometry
        : isFeature
        ? geojson.geometry
        : geojson;
      isGeometryCollection = geometryMaybeCollection
        ? geometryMaybeCollection.type === 'GeometryCollection'
        : false;
      stopG = isGeometryCollection
        ? geometryMaybeCollection.geometries.length
        : 1;

      for (var geomIndex = 0; geomIndex < stopG; geomIndex++) {
        var multiFeatureIndex = 0;
        var geometryIndex = 0;
        geometry = isGeometryCollection
          ? geometryMaybeCollection.geometries[geomIndex]
          : geometryMaybeCollection;

        // Handles null Geometry -- Skips this geometry
        if (geometry === null) continue;
        coords = geometry.coordinates;
        var geomType = geometry.type;

        wrapShrink =
          excludeWrapCoord &&
          (geomType === 'Polygon' || geomType === 'MultiPolygon')
            ? 1
            : 0;

        switch (geomType) {
          case null:
            break;
          case 'Point':
            if (
              callback(
                coords,
                coordIndex,
                featureIndex,
                multiFeatureIndex,
                geometryIndex,
              ) === false
            )
              return false;
            coordIndex++;
            multiFeatureIndex++;
            break;
          case 'LineString':
          case 'MultiPoint':
            for (j = 0; j < coords.length; j++) {
              if (
                callback(
                  coords[j],
                  coordIndex,
                  featureIndex,
                  multiFeatureIndex,
                  geometryIndex,
                ) === false
              )
                return false;
              coordIndex++;
              if (geomType === 'MultiPoint') multiFeatureIndex++;
            }
            if (geomType === 'LineString') multiFeatureIndex++;
            break;
          case 'Polygon':
          case 'MultiLineString':
            for (j = 0; j < coords.length; j++) {
              for (k = 0; k < coords[j].length - wrapShrink; k++) {
                if (
                  callback(
                    coords[j][k],
                    coordIndex,
                    featureIndex,
                    multiFeatureIndex,
                    geometryIndex,
                  ) === false
                )
                  return false;
                coordIndex++;
              }
              if (geomType === 'MultiLineString') multiFeatureIndex++;
              if (geomType === 'Polygon') geometryIndex++;
            }
            if (geomType === 'Polygon') multiFeatureIndex++;
            break;
          case 'MultiPolygon':
            for (j = 0; j < coords.length; j++) {
              geometryIndex = 0;
              for (k = 0; k < coords[j].length; k++) {
                for (l = 0; l < coords[j][k].length - wrapShrink; l++) {
                  if (
                    callback(
                      coords[j][k][l],
                      coordIndex,
                      featureIndex,
                      multiFeatureIndex,
                      geometryIndex,
                    ) === false
                  )
                    return false;
                  coordIndex++;
                }
                geometryIndex++;
              }
              multiFeatureIndex++;
            }
            break;
          case 'GeometryCollection':
            for (j = 0; j < geometry.geometries.length; j++)
              if (
                coordEach(
                  geometry.geometries[j],
                  callback,
                  excludeWrapCoord,
                ) === false
              )
                return false;
            break;
          default:
            throw new Error('Unknown Geometry Type');
        }
      }
    }
  }
  function doBBoxOverlap(bbox1, bbox2) {
    if (bbox1[0] > bbox2[0]) {
      return false;
    }
    if (bbox1[2] < bbox2[2]) {
      return false;
    }
    if (bbox1[1] > bbox2[1]) {
      return false;
    }
    if (bbox1[3] < bbox2[3]) {
      return false;
    }
    return true;
  }
  function isPointInMultiPoint(multiPoint, pt) {
    let i;
    let output = false;
    for (i = 0; i < multiPoint.coordinates.length; i++) {
      if (compareCoords(multiPoint.coordinates[i], pt.coordinates)) {
        output = true;
        break;
      }
    }
    return output;
  }
  function booleanPointOnLine(pt, line, options) {
    // eslint-disable-next-line no-void
    if (options === void 0) {
      options = {};
    }
    // Normalize inputs
    var ptCoords = getCoord(pt);
    var lineCoords = getCoords(line);
    // Main
    for (var i = 0; i < lineCoords.length - 1; i++) {
      var ignoreBoundary = false;
      if (options.ignoreEndVertices) {
        if (i === 0) {
          ignoreBoundary = 'start';
        }
        if (i === lineCoords.length - 2) {
          ignoreBoundary = 'end';
        }
        if (i === 0 && i + 1 === lineCoords.length - 1) {
          ignoreBoundary = 'both';
        }
      }
      if (
        isPointOnLineSegment(
          lineCoords[i],
          lineCoords[i + 1],
          ptCoords,
          ignoreBoundary,
        )
      ) {
        return true;
      }
    }
    return false;
  }
  // See http://stackoverflow.com/a/4833823/1979085
  /**
   * @private
   * @param {Position} lineSegmentStart coord pair of start of line
   * @param {Position} lineSegmentEnd coord pair of end of line
   * @param {Position} pt coord pair of point to check
   * @param {boolean|string} excludeBoundary whether the point is allowed to fall on the line ends.
   * If true which end to ignore.
   * @returns {boolean} true/false
   */
  function isPointOnLineSegment(
    lineSegmentStart,
    lineSegmentEnd,
    pt,
    excludeBoundary,
  ) {
    var x = pt[0];
    var y = pt[1];
    var x1 = lineSegmentStart[0];
    var y1 = lineSegmentStart[1];
    var x2 = lineSegmentEnd[0];
    var y2 = lineSegmentEnd[1];
    var dxc = pt[0] - x1;
    var dyc = pt[1] - y1;
    var dxl = x2 - x1;
    var dyl = y2 - y1;
    var cross = dxc * dyl - dyc * dxl;
    if (cross !== 0) {
      return false;
    }
    if (!excludeBoundary) {
      if (Math.abs(dxl) >= Math.abs(dyl)) {
        return dxl > 0 ? x1 <= x && x <= x2 : x2 <= x && x <= x1;
      }
      return dyl > 0 ? y1 <= y && y <= y2 : y2 <= y && y <= y1;
    } else if (excludeBoundary === 'start') {
      if (Math.abs(dxl) >= Math.abs(dyl)) {
        return dxl > 0 ? x1 < x && x <= x2 : x2 <= x && x < x1;
      }
      return dyl > 0 ? y1 < y && y <= y2 : y2 <= y && y < y1;
    } else if (excludeBoundary === 'end') {
      if (Math.abs(dxl) >= Math.abs(dyl)) {
        return dxl > 0 ? x1 <= x && x < x2 : x2 < x && x <= x1;
      }
      return dyl > 0 ? y1 <= y && y < y2 : y2 < y && y <= y1;
    } else if (excludeBoundary === 'both') {
      if (Math.abs(dxl) >= Math.abs(dyl)) {
        return dxl > 0 ? x1 < x && x < x2 : x2 < x && x < x1;
      }
      return dyl > 0 ? y1 < y && y < y2 : y2 < y && y < y1;
    }
    return false;
  }
  function isMultiPointInMultiPoint(multiPoint1, multiPoint2) {
    for (var _i = 0, _a = multiPoint2.coordinates; _i < _a.length; _i++) {
      var coord2 = _a[_i];
      var matchFound = false;
      for (var _b = 0, _c = multiPoint1.coordinates; _b < _c.length; _b++) {
        var coord1 = _c[_b];
        if (compareCoords(coord2, coord1)) {
          matchFound = true;
          break;
        }
      }
      if (!matchFound) {
        return false;
      }
    }
    return true;
  }
  function isMultiPointOnLine(lineString, multiPoint) {
    var haveFoundInteriorPoint = false;
    for (var _i = 0, _a = multiPoint.coordinates; _i < _a.length; _i++) {
      var coord = _a[_i];
      if (booleanPointOnLine(coord, lineString, { ignoreEndVertices: true })) {
        haveFoundInteriorPoint = true;
      }
      if (!booleanPointOnLine(coord, lineString)) {
        return false;
      }
    }
    if (haveFoundInteriorPoint) {
      return true;
    }
    return false;
  }
  function isMultiPointInPoly(polygon, multiPoint) {
    for (var _i = 0, _a = multiPoint.coordinates; _i < _a.length; _i++) {
      var coord = _a[_i];
      if (!booleanPointInPolygon(coord, polygon, { ignoreBoundary: true })) {
        return false;
      }
    }
    return true;
  }
  function isLineOnLine(lineString1, lineString2) {
    var haveFoundInteriorPoint = false;
    for (var _i = 0, _a = lineString2.coordinates; _i < _a.length; _i++) {
      var coords = _a[_i];
      if (
        booleanPointOnLine(
          { type: 'Point', coordinates: coords },
          lineString1,
          {
            ignoreEndVertices: true,
          },
        )
      ) {
        haveFoundInteriorPoint = true;
      }
      if (
        booleanPointOnLine(
          { type: 'Point', coordinates: coords },
          lineString1,
          {
            ignoreEndVertices: false,
          },
        )
      ) {
        return false;
      }
    }
    return haveFoundInteriorPoint;
  }
  function isLineInPoly(polygon, linestring) {
    var output = false;
    var i = 0;
    var polyBbox = bbox(polygon);
    var lineBbox = bbox(linestring);
    if (!doBBoxOverlap(polyBbox, lineBbox)) {
      return false;
    }
    for (i; i < linestring.coordinates.length - 1; i++) {
      var midPoint = getMidpoint(
        linestring.coordinates[i],
        linestring.coordinates[i + 1],
      );
      if (
        booleanPointInPolygon(
          { type: 'Point', coordinates: midPoint },
          polygon,
          {
            ignoreBoundary: true,
          },
        )
      ) {
        output = true;
        break;
      }
    }
    return output;
  }

  function isPolyInPoly(feature1, feature2) {
    // Handle Nulls
    if (feature1.type === 'Feature' && feature1.geometry === null) {
      return false;
    }
    if (feature2.type === 'Feature' && feature2.geometry === null) {
      return false;
    }
    var poly1Bbox = bbox(feature1);
    var poly2Bbox = bbox(feature2);
    if (!doBBoxOverlap(poly1Bbox, poly2Bbox)) {
      return false;
    }
    var coords = getGeom(feature2).coordinates;
    for (var _i = 0, coords_1 = coords; _i < coords_1.length; _i++) {
      var ring = coords_1[_i];
      for (var _a = 0, ring_1 = ring; _a < ring_1.length; _a++) {
        var coord = ring_1[_a];
        if (!booleanPointInPolygon(coord, feature1)) {
          return false;
        }
      }
    }
    return true;
  }
  function inRing(pt, ring, ignoreBoundary) {
    var isInside = false;
    if (
      ring[0][0] === ring[ring.length - 1][0] &&
      ring[0][1] === ring[ring.length - 1][1]
    ) {
      ring = ring.slice(0, ring.length - 1);
    }
    for (var i = 0, j = ring.length - 1; i < ring.length; j = i++) {
      var xi = ring[i][0];
      var yi = ring[i][1];
      var xj = ring[j][0];
      var yj = ring[j][1];
      var onBoundary =
        pt[1] * (xi - xj) + yi * (xj - pt[0]) + yj * (pt[0] - xi) === 0 &&
        (xi - pt[0]) * (xj - pt[0]) <= 0 &&
        (yi - pt[1]) * (yj - pt[1]) <= 0;
      if (onBoundary) {
        return !ignoreBoundary;
      }
      var intersect =
        yi > pt[1] !== yj > pt[1] &&
        pt[0] < ((xj - xi) * (pt[1] - yi)) / (yj - yi) + xi;
      if (intersect) {
        isInside = !isInside;
      }
    }
    return isInside;
  }
  function inBBox(pt, bbox) {
    return (
      bbox[0] <= pt[0] &&
      bbox[1] <= pt[1] &&
      bbox[2] >= pt[0] &&
      bbox[3] >= pt[1]
    );
  }

  function booleanPointInPolygon(point, polygon, options) {
    if (options === void 0) {
      options = {};
    }
    // validation
    if (!point) {
      throw new Error('point is required');
    }
    if (!polygon) {
      throw new Error('polygon is required');
    }
    var pt = getCoord(point);
    var geom = getGeom(polygon);
    var type = geom.type;
    var bbox = polygon.bbox;
    var polys = geom.coordinates;
    // Quick elimination if point is not inside bbox
    if (bbox && inBBox(pt, bbox) === false) {
      return false;
    }
    // normalize to multipolygon
    if (type === 'Polygon') {
      polys = [polys];
    }
    var insidePoly = false;
    for (var i = 0; i < polys.length && !insidePoly; i++) {
      // check if it is in the outer ring first
      if (inRing(pt, polys[i][0], options.ignoreBoundary)) {
        var inHole = false;
        var k = 1;
        // check for the point in any of the holes
        while (k < polys[i].length && !inHole) {
          if (inRing(pt, polys[i][k], !options.ignoreBoundary)) {
            inHole = true;
          }
          k++;
        }
        if (!inHole) {
          insidePoly = true;
        }
      }
    }
    return insidePoly;
  }
  function booleanContains(feature1, feature2) {
    const geom1 = getGeom(feature1);
    const geom2 = getGeom(feature2);
    const type1 = geom1.type;
    const type2 = geom2.type;
    const coords1 = geom1.coordinates;
    const coords2 = geom2.coordinates;
    switch (type1) {
      case 'Point':
        switch (type2) {
          case 'Point':
            return compareCoords(coords1, coords2);
          default:
            throw new Error(`feature2 ${type2} geometry not supported`);
        }
      case 'MultiPoint':
        switch (type2) {
          case 'Point':
            return isPointInMultiPoint(geom1, geom2);
          case 'MultiPoint':
            return isMultiPointInMultiPoint(geom1, geom2);
          default:
            throw new Error(`feature2 ${type2} geometry not supported`);
        }
      case 'LineString':
        switch (type2) {
          case 'Point':
            return booleanPointOnLine(geom2, geom1, {
              ignoreEndVertices: true,
            });
          case 'LineString':
            return isLineOnLine(geom1, geom2);
          case 'MultiPoint':
            return isMultiPointOnLine(geom1, geom2);
          default:
            throw new Error(`feature2 ${type2} geometry not supported`);
        }
      case 'Polygon':
        switch (type2) {
          case 'Point':
            return booleanPointInPolygon(geom2, geom1, {
              ignoreBoundary: true,
            });
          case 'LineString':
            return isLineInPoly(geom1, geom2);
          case 'Polygon':
            return isPolyInPoly(geom1, geom2);
          case 'MultiPoint':
            return isMultiPointInPoly(geom1, geom2);
          default:
            throw new Error(`feature2 ${type2} geometry not supported`);
        }
      default:
        throw new Error(`feature2 ${type1} geometry not supported`);
    }
  }

  function booleanCompleteContains(feature1, feature2) {
    if (getGeomType(feature2).includes('Multi')) {
      const feature2List = multiToSingle(feature2);
      for (let i = 0; i < feature2List.length; i++) {
        if (!booleanContains(feature1, feature2List[i])) {
          return false;
        }
      }
      return true;
    }
    return booleanContains(feature1, feature2);
  }
  function geomEach(geojson, callback) {
    var i,
      j,
      g,
      geometry,
      stopG,
      geometryMaybeCollection,
      isGeometryCollection,
      featureProperties,
      featureBBox,
      featureId,
      featureIndex = 0,
      isFeatureCollection = geojson.type === 'FeatureCollection',
      isFeature = geojson.type === 'Feature',
      stop = isFeatureCollection ? geojson.features.length : 1;

    //     // This logic may look a little weird. The reason why it is that way
    // is because it's trying to be fast. GeoJSON supports multiple kinds
    // of objects at its root: FeatureCollection, Features, Geometries.
    // This function has the responsibility of handling all of them, and that
    // means that some of the `for` loops you see below actually just don't apply
    // to certain inputs. For instance, if you give this just a
    // Point geometry, then both loops are short-circuited and all we do
    // is gradually rename the input until it's called 'geometry'.
    //
    // This also aims to allocate as few resources as possible: just a
    // few numbers and booleans, rather than any temporary arrays as would
    // be required with the normalization approach.
    for (i = 0; i < stop; i++) {
      geometryMaybeCollection = isFeatureCollection
        ? geojson.features[i].geometry
        : isFeature
        ? geojson.geometry
        : geojson;
      featureProperties = isFeatureCollection
        ? geojson.features[i].properties
        : isFeature
        ? geojson.properties
        : {};
      featureBBox = isFeatureCollection
        ? geojson.features[i].bbox
        : isFeature
        ? geojson.bbox
        : undefined;
      featureId = isFeatureCollection
        ? geojson.features[i].id
        : isFeature
        ? geojson.id
        : undefined;
      isGeometryCollection = geometryMaybeCollection
        ? geometryMaybeCollection.type === 'GeometryCollection'
        : false;
      stopG = isGeometryCollection
        ? geometryMaybeCollection.geometries.length
        : 1;

      for (g = 0; g < stopG; g++) {
        geometry = isGeometryCollection
          ? geometryMaybeCollection.geometries[g]
          : geometryMaybeCollection;

        // Handle null Geometry
        if (geometry === null) {
          if (
            callback(
              null,
              featureIndex,
              featureProperties,
              featureBBox,
              featureId,
            ) === false
          )
            return false;
          continue;
        }
        switch (geometry.type) {
          case 'Point':
          case 'LineString':
          case 'MultiPoint':
          case 'Polygon':
          case 'MultiLineString':
          case 'MultiPolygon': {
            if (
              callback(
                geometry,
                featureIndex,
                featureProperties,
                featureBBox,
                featureId,
              ) === false
            )
              return false;
            break;
          }
          case 'GeometryCollection': {
            for (j = 0; j < geometry.geometries.length; j++) {
              if (
                callback(
                  geometry.geometries[j],
                  featureIndex,
                  featureProperties,
                  featureBBox,
                  featureId,
                ) === false
              )
                return false;
            }
            break;
          }
          default:
            throw new Error('Unknown Geometry Type');
        }
      }
      // Only increase `featureIndex` per each feature
      featureIndex++;
    }
  }

  function featureCollection(features, options) {
    if (options === void 0) {
      options = {};
    }
    var fc = { type: 'FeatureCollection' };
    if (options.id) {
      fc.id = options.id;
    }
    if (options.bbox) {
      fc.bbox = options.bbox;
    }
    fc.features = features;
    return fc;
  }

  /**
   * Find a point that intersects LineStrings with two coordinates each
   *
   * @private
   * @param {Feature<LineString>} line1 GeoJSON LineString (Must only contain 2 coordinates)
   * @param {Feature<LineString>} line2 GeoJSON LineString (Must only contain 2 coordinates)
   * @returns {Feature<Point>} intersecting GeoJSON Point
   */

  function booleanDisjoint(feature1, feature2) {
    function multiLineString(coordinates, properties, options) {
      if (options === void 0) {
        options = {};
      }
      var geom = {
        type: 'MultiLineString',
        coordinates: coordinates,
      };
      return feature(geom, properties, options);
    }
    function lineString(coordinates, properties, options) {
      if (options === void 0) {
        options = {};
      }
      if (coordinates.length < 2) {
        throw new Error(
          'coordinates must be an array of two or more positions',
        );
      }
      var geom = {
        type: 'LineString',
        coordinates: coordinates,
      };
      return feature(geom, properties, options);
    }

    function coordsToLine(coords, properties) {
      if (coords.length > 1) {
        return multiLineString(coords, properties);
      }
      return lineString(coords[0], properties);
    }
    function default_1(poly, options) {
      if (options === void 0) {
        options = {};
      }
      var geom = getGeom(poly);
      if (!options.properties && poly.type === 'Feature') {
        options.properties = poly.properties;
      }
      switch (geom.type) {
        case 'Polygon':
          return polygonToLine(geom, options);
        case 'MultiPolygon':
          return multiPolygonToLine(geom, options);
        default:
          throw new Error('invalid poly');
      }
    }
    function isPointOnLine(lineString, pt) {
      for (var i = 0; i < lineString.coordinates.length - 1; i++) {
        if (
          isPointOnLineSegment(
            lineString.coordinates[i],
            lineString.coordinates[i + 1],
            pt.coordinates,
          )
        ) {
          return true;
        }
      }
      return false;
    }
    function isPointOnLineSegment(lineSegmentStart, lineSegmentEnd, pt) {
      var dxc = pt[0] - lineSegmentStart[0];
      var dyc = pt[1] - lineSegmentStart[1];
      var dxl = lineSegmentEnd[0] - lineSegmentStart[0];
      var dyl = lineSegmentEnd[1] - lineSegmentStart[1];
      var cross = dxc * dyl - dyc * dxl;
      if (cross !== 0) {
        return false;
      }
      if (Math.abs(dxl) >= Math.abs(dyl)) {
        if (dxl > 0) {
          return lineSegmentStart[0] <= pt[0] && pt[0] <= lineSegmentEnd[0];
        } else {
          return lineSegmentEnd[0] <= pt[0] && pt[0] <= lineSegmentStart[0];
        }
      } else if (dyl > 0) {
        return lineSegmentStart[1] <= pt[1] && pt[1] <= lineSegmentEnd[1];
      } else {
        return lineSegmentEnd[1] <= pt[1] && pt[1] <= lineSegmentStart[1];
      }
    }
    function isLineOnLine(lineString1, lineString2) {
      var doLinesIntersect = lineIntersect(lineString1, lineString2);
      if (doLinesIntersect.features.length > 0) {
        return true;
      }
      return false;
    }
    function isPolyInPoly(feature1, feature2) {
      for (var _i = 0, _a = feature1.coordinates[0]; _i < _a.length; _i++) {
        var coord1 = _a[_i];
        if (booleanPointInPolygon(coord1, feature2)) {
          return true;
        }
      }
      for (var _b = 0, _c = feature2.coordinates[0]; _b < _c.length; _b++) {
        var coord2 = _c[_b];
        if (booleanPointInPolygon(coord2, feature1)) {
          return true;
        }
      }
      var doLinesIntersect = lineIntersect(
        polygonToLine(feature1),
        polygonToLine(feature2),
      );
      if (doLinesIntersect.features.length > 0) {
        return true;
      }
      return false;
    }

    function polygonToLine(poly, options) {
      if (options === void 0) {
        options = {};
      }
      var geom = getGeom(poly);
      var coords = geom.coordinates;
      var properties = options.properties
        ? options.properties
        : poly.type === 'Feature'
        ? poly.properties
        : {};
      return coordsToLine(coords, properties);
    }

    function multiPolygonToLine(multiPoly, options) {
      if (options === void 0) {
        options = {};
      }
      var geom = getGeom(multiPoly);
      var coords = geom.coordinates;
      var properties = options.properties
        ? options.properties
        : multiPoly.type === 'Feature'
        ? multiPoly.properties
        : {};
      var lines = [];
      coords.forEach(function (coord) {
        lines.push(coordsToLine(coord, properties));
      });
      return featureCollection(lines);
    }
    function feature(geom, properties, options) {
      if (options === void 0) {
        options = {};
      }
      var feat = { type: 'Feature' };
      if (options.id === 0 || options.id) {
        feat.id = options.id;
      }
      if (options.bbox) {
        feat.bbox = options.bbox;
      }
      feat.properties = properties || {};
      feat.geometry = geom;
      return feat;
    }

    function flattenEach(geojson, callback) {
      geomEach(
        geojson,
        function (geometry, featureIndex, properties, bbox, id) {
          // Callback for single geometry
          var type = geometry === null ? null : geometry.type;
          switch (type) {
            case null:
            case 'Point':
            case 'LineString':
            case 'Polygon':
              if (
                callback(
                  feature(geometry, properties, { bbox: bbox, id: id }),
                  featureIndex,
                  0,
                ) === false
              )
                return false;
              return;
          }

          var geomType;

          // Callback for multi-geometry
          switch (type) {
            case 'MultiPoint':
              geomType = 'Point';
              break;
            case 'MultiLineString':
              geomType = 'LineString';
              break;
            case 'MultiPolygon':
              geomType = 'Polygon';
              break;
          }

          for (
            var multiFeatureIndex = 0;
            multiFeatureIndex < geometry.coordinates.length;
            multiFeatureIndex++
          ) {
            var coordinate = geometry.coordinates[multiFeatureIndex];
            var geom = {
              type: geomType,
              coordinates: coordinate,
            };
            if (
              callback(
                feature(geom, properties),
                featureIndex,
                multiFeatureIndex,
              ) === false
            )
              return false;
          }
        },
      );
    }
    function point(coordinates, properties, options) {
      if (options === void 0) {
        options = {};
      }
      if (!coordinates) {
        throw new Error('coordinates is required');
      }
      if (!Array.isArray(coordinates)) {
        throw new Error('coordinates must be an Array');
      }
      if (coordinates.length < 2) {
        throw new Error('coordinates must be at least 2 numbers long');
      }
      if (!isNumber(coordinates[0]) || !isNumber(coordinates[1])) {
        throw new Error('coordinates must contain numbers');
      }
      var geom = {
        type: 'Point',
        coordinates: coordinates,
      };
      return feature(geom, properties, options);
    }
    function intersects(line1, line2) {
      var coords1 = getCoords(line1);
      var coords2 = getCoords(line2);
      if (coords1.length !== 2) {
        throw new Error('<intersects> line1 must only contain 2 coordinates');
      }
      if (coords2.length !== 2) {
        throw new Error('<intersects> line2 must only contain 2 coordinates');
      }
      var x1 = coords1[0][0];
      var y1 = coords1[0][1];
      var x2 = coords1[1][0];
      var y2 = coords1[1][1];
      var x3 = coords2[0][0];
      var y3 = coords2[0][1];
      var x4 = coords2[1][0];
      var y4 = coords2[1][1];
      var denom = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1);
      var numeA = (x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3);
      var numeB = (x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3);
      if (denom === 0) {
        if (numeA === 0 && numeB === 0) {
          return null;
        }
        return null;
      }
      var uA = numeA / denom;
      var uB = numeB / denom;
      if (uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1) {
        var x = x1 + uA * (x2 - x1);
        var y = y1 + uA * (y2 - y1);
        return point([x, y]);
      }
      return null;
    }
    function lineIntersect(line1, line2) {
      var unique = {};
      var results = [];
      // First, normalize geometries to features
      // Then, handle simple 2-vertex segments
      if (line1.type === 'LineString') {
        line1 = feature(line1);
      }
      if (line2.type === 'LineString') {
        line2 = feature(line2);
      }
      if (
        line1.type === 'Feature' &&
        line2.type === 'Feature' &&
        line1.geometry !== null &&
        line2.geometry !== null &&
        line1.geometry.type === 'LineString' &&
        line2.geometry.type === 'LineString' &&
        line1.geometry.coordinates.length === 2 &&
        line2.geometry.coordinates.length === 2
      ) {
        var intersect = intersects(line1, line2);
        if (intersect) {
          results.push(intersect);
        }
        return featureCollection(results);
      }
      // Handles complex GeoJSON Geometries
      // var tree = geojson_rbush_1.default();
      // tree.load(line_segment_1.default(line2));
      // meta_1.featureEach(line_segment_1.default(line1), function (segment) {
      //     meta_1.featureEach(tree.search(segment), function (match) {
      //         var intersect = intersects(segment, match);
      //         if (intersect) {
      //             // prevent duplicate points https://github.com/Turfjs/turf/issues/688
      //             var key = getCoords(intersect).join(",");
      //             if (!unique[key]) {
      //                 unique[key] = true;
      //                 results.push(intersect);
      //             }
      //         }
      //     });
      // });
      return featureCollection(results);
    }
    function isLineInPolyDisjoint(polygon, lineString) {
      for (var _i = 0, _a = lineString.coordinates; _i < _a.length; _i++) {
        var coord = _a[_i];
        if (booleanPointInPolygon(coord, polygon)) {
          return true;
        }
      }
      var doLinesIntersect = lineIntersect(lineString, default_1(polygon));
      if (doLinesIntersect.features.length > 0) {
        return true;
      }
      return false;
    }
    function disjoint(geom1, geom2) {
      switch (geom1.type) {
        case 'Point':
          switch (geom2.type) {
            case 'Point':
              return !compareCoords(geom1.coordinates, geom2.coordinates);
            case 'LineString':
              return !isPointOnLine(geom2, geom1);
            case 'Polygon':
              return booleanPointInPolygon(geom1, geom2);
          }
          /* istanbul ignore next */
          break;
        case 'LineString':
          switch (geom2.type) {
            case 'Point':
              return !isPointOnLine(geom1, geom2);
            case 'LineString':
              return !isLineOnLine(geom1, geom2);
            case 'Polygon':
              return !isLineInPolyDisjoint(geom2, geom1);
          }
          /* istanbul ignore next */
          break;
        case 'Polygon':
          switch (geom2.type) {
            case 'Point':
              return !booleanPointInPolygon(geom2, geom1);
            case 'LineString':
              return !isLineInPolyDisjoint(geom1, geom2);
            case 'Polygon':
              return !isPolyInPoly(geom2, geom1);
          }
      }
      return false;
    }
    var bool = true;
    flattenEach(feature1, function (flatten1) {
      flattenEach(feature2, function (flatten2) {
        if (bool === false) {
          return false;
        }
        bool = disjoint(flatten1.geometry, flatten2.geometry);
      });
    });
    return bool;
  }

  self.onmessage = (e) => {
    const featureIdList = []; // 过滤后的要素ID
    const featuresList = []; // 过滤后的要素
    const { features, spatialFilter, isJustGlobalIdExist } = e.data;
    features.forEach((f) => {
      delete f._vectorTileFeature;
      delete f._tile;
      spatialFilter[1].forEach((geometry) => {
        let geo = geometry;
        if (typeof geometry === 'string') {
          // @ts-ignore
          geo = wktParse(geometry);
        }
        if (f.geometry.type === 'Point') {
          // booleanCrosses 不支持Point
          if (
            booleanCompleteContains(geo, f) &&
            f.properties !== undefined &&
            f.properties !== null
          ) {
            if (isJustGlobalIdExist) {
              featureIdList.push(f.properties.just_global_id);
            } else {
              featuresList.push(f);
            }
          }
        } else {
          switch (spatialFilter[0]) {
            case 'contains':
              if (
                booleanCompleteContains(geo, f) &&
                f.id !== undefined &&
                f.properties !== null
              ) {
                if (isJustGlobalIdExist) {
                  featureIdList.push(f.properties.just_global_id);
                } else {
                  featuresList.push(f);
                }
              }
              break;
            case 'crosses':
              if (
                !booleanDisjoint(f, geo) &&
                f.id !== undefined &&
                f.properties !== null
              ) {
                if (isJustGlobalIdExist) {
                  featureIdList.push(f.properties.just_global_id);
                } else {
                  featuresList.push(f);
                }
              }
              break;
            default:
              break;
          }
        }
      });
    });
    self.postMessage({ featureIdList, featuresList });
  };
}
// export default computedCount;
// 把脚本代码转为string
let code = spatailFilter.toString();
code = code.substring(code.indexOf('{') + 1, code.lastIndexOf('}'));
const blob = new Blob([code], { type: 'application/javascript' });
const workerScript = URL.createObjectURL(blob);
// module.exports = workerScript;
export default workerScript;
