import MapboxDraw, { DrawEventType } from '@mapbox/mapbox-gl-draw';
import dragCircle from './mode/dragCircle';
//@ts-ignore
import DrawRectangle from './mode/DrawRectangle';
import DrawLineString from './mode/draw_line_string';
import DrawPolygon from './mode/draw_polygon';
import { JustMap } from '@/core';
import { log } from '../utils/logger';

export type DrawMode = 'point' | 'line' | 'circle' | 'rectangle' | 'polygon';

interface MapboxDrawModes {
  DRAW_LINE_STRING: 'draw_line_string';
  DRAW_POLYGON: 'draw_polygon';
  DRAW_POINT: 'draw_point';
  DRAW_CIRCLE: 'draw_circle';
  DRAW_RECTANGLE: 'draw_rectangle';
  SIMPLE_SELECT: 'simple_select';
  DIRECT_SELECT: 'direct_select';
  STATIC: 'static';
}
// type MapboxDrawModes = 'draw_point'|'draw_line_string'|'draw_polygon';
const DRAW_MODE_MAPPING: {
  [key in DrawMode]: MapboxDrawModes[keyof MapboxDrawModes];
} = {
  point: 'draw_point',
  line: 'draw_line_string',
  polygon: 'draw_polygon',
  rectangle: 'draw_rectangle',
  circle: 'draw_circle',
};

export class Draw {
  private _draw: MapboxDraw;
  private _map: JustMap;
  constructor(styles?: object[] | undefined, mode?: Object) {
    const options = {
      //@ts-ignore
      modes: Object.assign(
        MapboxDraw.modes,
        {
          draw_rectangle: DrawRectangle,
          draw_circle: dragCircle,
          draw_line_string: DrawLineString,
          draw_polygon: DrawPolygon,
        },
        mode,
      ),
      displayControlsDefault: false,
      userProperties: true,
      styles: styles, //绘图样式
    };
    !styles && delete options.styles;
    //@ts-ignore
    this._draw = new MapboxDraw(options);
  }
  /**
   * 绘图工具添加到地图上
   * @param map JustMap实例
   */
  addTo(map: JustMap) {
    if (this._map) {
      this.remove();
    }
    this._map = map;
    map._addJustControl(this._draw);
    return this;
  }
  /**
   * 切换绘图模式
   * @param mode 绘图模式
   */
  changeMode(mode: DrawMode) {
    const mapboxDrawMode = DRAW_MODE_MAPPING[mode];
    //@ts-ignore draw_circle 和 draw_rectangle是新加的
    this._draw.changeMode(mapboxDrawMode);
    return this;
  }
  /**
   * 卸载绘图工具
   */
  remove() {
    this._map.removeControl(this._draw);
    //@ts-ignore
    this._draw = undefined;
    // DrawInstance.removerInstance();
  }
  /**
   * 要素添加进draw,返回要素ID数组
   * @param geojson 要素
   * @returns 要素Id数组
   */
  add(
    geojson: GeoJSON.Feature | GeoJSON.Geometry | GeoJSON.FeatureCollection,
  ): Array<string> {
    return this._draw.add(geojson);
  }
  /**
   * 获取所有feature
   */
  getAll() {
    this._draw.getAll();
  }
  /**
   * 根据要素Id获取要素
   * @param featureId 要素Id
   * @returns 要素
   */
  get(featureId: string) {
    return this._draw.get(featureId);
  }
  /**
   * 获取选择的要素
   * @returns 要素
   */
  getSelected() {
    return this._draw.getSelected();
  }
  /**
   * 获取选择的要素的Id
   * @returns 要素的Id
   */
  getSelectedIds() {
    return this._draw.getSelectedIds();
  }
  /**
   * 根据要素坐标得到要素Id数组
   * @param point 像素坐标
   * @returns 要素ID数组
   */
  getFeatureIdsAt(point: { x: number; y: number }): Array<string> {
    return this._draw.getFeatureIdsAt(point);
  }

  /**
   * 根据要素ID删除要素
   * @param ids featureId或者featureId数组
   */
  delete(ids: string | Array<string>) {
    this._draw.delete(ids);
    return this;
  }
  /**
   * 删除所有要素
   */
  deleteAll() {
    this._draw.deleteAll();
    return this;
  }

  /**
   * 删除选中的图形或者节点
   * @returns 绘画实例
   */
  trash() {
    this._draw.trash();
    return this;
  }
  /**
   * 绑定事件
   * @param eventType 事件类型
   * @param listener 执行函数
   */
  on(eventType: DrawEventType, listener: Function) {
    //@ts-ignore
    this._map.on(eventType, listener);
  }
  /**
   * 解绑事件
   * @param eventType 事件类型
   * @param listener 执行函数
   */
  off(eventType: DrawEventType, listener: Function) {
    //@ts-ignore
    this._map.off(eventType, listener);
  }
}
// export { Draw };

// 单例模式 重复new Draw 返回同一个
// class DrawInstance {
//   private static _drawInstance: DrawInstance | null;
//   private _draw: MapboxDraw;
//   constructor(styles: Array<object> | undefined) {
//     const options = {
//       //@ts-ignore
//       modes: Object.assign(MapboxDraw.modes, {
//         draw_rectangle: DrawRectangle,
//         draw_circle: dragCircle,
//       }),
//       displayControlsDefault: false,
//       userProperties: true,
//       styles: styles, //绘图样式
//     };
//     !styles && delete options.styles;
//     //@ts-ignore
//     this._draw = new MapboxDraw(options);
//   }
//   static getInstance(styles: Array<object> | undefined): DrawInstance {
//     if (!this._drawInstance) {
//       this._drawInstance = new DrawInstance(styles);
//     }
//     return this._drawInstance;
//   }
//   getDraw() {
//     return this._draw;
//   }
//   static removerInstance() {
//     this._drawInstance = null;
//   }
// }

// 代理模式 统一进行接口改造,当已经移除draw后，再次调用drawde api 接口会提示
const drawHandler = {
  get(target: any, key: any, receiver: any) {
    if (!target._draw && key !== '_draw' && key !== '_styles') {
      return errorHandle;
    }
    return target[key];
  },
};
function errorHandle(args: any) {
  log.warn('draw instance has been removed');
  return;
}
export class DrawProxy {
  constructor(styles?: object[] | undefined, mode?: Object) {
    return new Proxy(new Draw(styles, mode), drawHandler);
  }
}
