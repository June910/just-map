// 返回marker的内容元素
// 1.infomarker 单机开始绘画  绘画过程中 单机继续绘画或者双击结束 + 长度或者面积
// 2.标识marker  点坐标 线长度 面积
export type GeomType =
  | 'Point'
  | 'LineString'
  | 'Polygon'
  | 'StartInfoMarker'
  | 'StartFeatureMarker';
export function createMarkerDom(
  geomType: GeomType,
  value?: [number, number] | number,
) {
  // value是坐标数组 或者长度
  const el = document.createElement('div');
  el.style.color = '#f00';
  el.innerHTML = createMarkerContent(geomType, value);
  return el;
}
export function createMarkerContent(
  geomType: GeomType,
  value?: [number, number] | number,
) {
  let innerHTMLContent = '';
  switch (geomType) {
    case 'Point':
      Array.isArray(value) &&
        (innerHTMLContent = `<div>经度:${value[0].toFixed(
          5,
        )}</div><div>纬度:${value[1].toFixed(5)}</div>`);
      break;
    case 'LineString':
      !Array.isArray(value) &&
        value &&
        (innerHTMLContent =
          value > 1000
            ? `${(value / 1000).toFixed(2)}km`
            : `${value.toFixed(2)}m`);
      break;
    case 'Polygon':
      !Array.isArray(value) &&
        value &&
        (innerHTMLContent =
          value > 100000
            ? `${(value / 100000).toFixed(2)}km<sup>2</sup>`
            : `${value.toFixed(2)}m<sup>2</sup>`);
      break;
    case 'StartFeatureMarker':
      innerHTMLContent = '起点';
      break;
    case 'StartInfoMarker':
      if (!value) {
        innerHTMLContent = '单机确定起点';
      } else {
        innerHTMLContent = '单机继续或者双击结束';
      }
      break;
    default:
      break;
  }
  return innerHTMLContent;
}
