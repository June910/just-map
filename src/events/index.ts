import { log } from 'src/utils/logger';
import { MapEventType, MapLayerEventType } from 'mapbox-gl';

export interface EventData {
  [key: string]: any;
}
export type Listener = (eventData?: EventData) => void;
export type Listeners = { [k: string]: Array<Listener> }; // {'click':[function1(){},function2(){},function2(){}...]}

/**
 * 初始化事件执行顺序: style.load -> source.data =>source.load => load
 * 更新: style.update
 */
export type EventType =
  /**
   * style文件更新的时候触发, 可以用于更新地图样式
   */
  | 'style.update'
  /**
   * style文件下载完成
   */
  | 'style.load'
  /**
   *所有图层加载完成
   */
  | 'load'
  /**
   * 每个just layer的数据加载完成之后触发, 用于更新layer
   */
  | 'source.data'
  /**
   * 每个source加载完触发
   */
  | 'source.load'
  /**
   * scene文件更新的时候触发, 可以用于更新地图样式
   */
  | 'scene.update';
/**
 * 初始化事件执行顺序: style.load -> source.data =>source.load => load
 * 更新: style.update
 */
export type MapboxEventType =
  | keyof Omit<MapEventType, 'load'>
  | keyof MapLayerEventType
  | 'styleimagemissing';

function _addEventListener(
  type: EventType,
  listener: Listener,
  listenerList: Listeners,
) {
  const listenerExists =
    listenerList[type] && listenerList[type].indexOf(listener) !== -1;
  if (!listenerExists) {
    listenerList[type] = listenerList[type] || [];
    listenerList[type].push(listener);
  }
}

function _removeEventListener(
  type: EventType,
  listener: Listener,
  listenerList: Listeners,
) {
  if (listenerList && listenerList[type]) {
    const index = listenerList[type].indexOf(listener);
    if (index !== -1) {
      listenerList[type].splice(index, 1);
    } else {
      log.warn(`${type}事件没有绑定该方法`);
    }
  }
}

class Event {
  _listeners: Listeners = {};
  _oneTimeListeners: Listeners = {};
  private _eventedParent?: Event;
  private _eventedParentData?: EventData;
  over(type: EventType, listener: Listener) {
    _addEventListener(type, listener, this._listeners);
  }
  cancel(type: EventType, listener: Listener) {
    if (!(type in this._listeners)) {
      log.warn(`${type}是无效事件`);
    } else {
      _removeEventListener(type, listener, this._listeners);
    }
  }
  fire(type: EventType, eventData?: EventData) {
    if (!this.listens(type)) {
      return;
    }
    if (type in this._listeners) {
      this._listeners[type].forEach((listener) => {
        listener(eventData);
      });
    }

    const oneTimeListeners =
      this._oneTimeListeners && this._oneTimeListeners[type]
        ? this._oneTimeListeners[type].slice()
        : [];
    // eslint-disable-next-line no-restricted-syntax
    for (const listener of oneTimeListeners) {
      _removeEventListener(type, listener, this._oneTimeListeners);
      listener.call(this, eventData);
    }

    const parent = this._eventedParent;
    if (parent) {
      parent.fire(type, eventData);
    }
  }
  once(type: EventType, listener: Listener) {
    this._oneTimeListeners = this._oneTimeListeners || {};
    _addEventListener(type, listener, this._oneTimeListeners);

    return this;
  }
  setEventParent(parent: Event, data?: EventData) {
    this._eventedParent = parent;
    this._eventedParentData = data;
    return this;
  }
  listens(type: string): boolean | undefined {
    return (
      (this._listeners &&
        this._listeners[type] &&
        this._listeners[type].length > 0) ||
      (this._oneTimeListeners &&
        this._oneTimeListeners[type] &&
        this._oneTimeListeners[type].length > 0) ||
      (this._eventedParent && this._eventedParent.listens(type))
    );
  }
  removeEventAllListeners(type: EventType) {
    if (!(type in this._listeners)) {
      log.warn(`${type}是无效事件`);
    } else {
      const listeners = this._listeners[type];
      listeners.forEach((listener) => {
        _removeEventListener(type, listener, this._listeners);
      });
    }
  }
}
export default Event;
