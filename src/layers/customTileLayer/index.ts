// 通过mapbox提供的customLayer接口 封装切片图层
import mapboxgl from 'mapbox-gl';
import Program from './render/program';
import fragmentSource from './shaders/raster.fragment.js';
import vertexSource from './shaders/raster.vertex.js';
import { baiduUniforms } from './render/program/baidu_raster_program';
import CustomTileSource from '@/sources/customTileSource';
import WebMercatorViewport, { getDistanceScales } from '@math.gl/web-mercator';
//@ts-ignore
import * as vec4 from 'gl-matrix/vec4';
//@ts-ignore
import * as mat4 from 'gl-matrix/mat4';

const VECTOR_TO_POINT_MATRIX = [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0];

const uniformKeys = [
  'u_opacity',
  'u_matrix',
  'u_Sampler',
  'u_unitsPerDegree',
  'u_uintsPerDegree',
  'u_center_lnglat',
  'u_project_center_position',
  'u_zoom',
];
export default class CustomTileLayer {
  map: mapboxgl.Map;
  id: string;
  type: string;
  renderingMode: '2d' | '3d';
  gl: WebGLRenderingContext;
  program: Program;
  source: CustomTileSource;
  opacity: number;
  minZoom: number;
  maxZoom: number;
  viewport: WebMercatorViewport;
  constructor(layer: mapboxgl.AnyLayer, source: CustomTileSource) {
    this.id = layer.id;
    this.type = 'custom';
    this.renderingMode = '2d';
    this.source = source;
    //@ts-ignore
    this.opacity = (layer.paint && layer.paint['raster-opacity']) || 1.0;
    //@ts-ignore
    this.minZoom = layer.minZoom || 0;
    //@ts-ignore
    this.maxZoom = layer.maxZoom || 24;

    this.update = this.update.bind(this);
  }
  // 设置视图参数
  // 参考deckgl的实现方案，解决大比例尺下抖动问题
  // 与中心点相减  去除高位差异
  private _getViewUniforms(gl: WebGLRenderingContext) {
    const zoom = this.map.getZoom();
    const bearing = this.map.getBearing();
    const pitch = this.map.getPitch();
    const center = this.map.getCenter();

    this.viewport = new WebMercatorViewport({
      width: gl.drawingBufferWidth,
      height: gl.drawingBufferHeight,
      longitude: center.lng,
      latitude: center.lat,
      zoom,
      pitch,
      bearing,
    });
    let { viewMatrix, viewProjectionMatrix, projectionMatrix } = this.viewport;

    const { unitsPerDegree, unitsPerDegree2 } = getDistanceScales({
      longitude: center.lng,
      latitude: center.lat,
      highPrecision: true,
    });

    const u_center_LngLat = [Math.fround(center.lng), Math.fround(center.lat)];

    const u_center_Position = this.viewport.projectFlat(u_center_LngLat);
    const u_project_center_Position = vec4.transformMat4(
      [],
      [u_center_Position[0], u_center_Position[1], 0.0, 1.0],
      viewProjectionMatrix,
    );
    if (zoom >= 12) {
      viewProjectionMatrix = mat4.multiply([], projectionMatrix, viewMatrix);
      viewProjectionMatrix = mat4.multiply(
        [],
        viewProjectionMatrix,
        VECTOR_TO_POINT_MATRIX,
      );
    }

    return {
      u_Sampler: 0,
      u_matrix: viewProjectionMatrix,
      u_opacity: this.opacity,
      u_unitsPerDegree: unitsPerDegree,
      u_uintsPerDegree2: unitsPerDegree2,
      u_center_lnglat: u_center_LngLat,
      u_project_center_position: u_project_center_Position,
      u_zoom: zoom,
    };
  }
  //初始化gl资源和注册事件监听
  onAdd(map: mapboxgl.Map, gl: WebGLRenderingContext) {
    this.map = map;
    this.gl = gl;
    this.program = new Program(
      gl,
      { vertexSource, fragmentSource },
      baiduUniforms,
      uniformKeys,
    );
    map.on('move', this.update);
    this.update();
  }
  // 绘制
  render(gl: WebGLRenderingContext, maxtrix: Array<number>) {
    if (this.map.getZoom() < this.minZoom || this.map.getZoom() > this.maxZoom)
      return;
    const showTiles = this.source.showTiles;
    const uniformValues = this._getViewUniforms(gl);
    for (const tile of showTiles) {
      if (!tile.isLoad) continue;
      this.program.draw(gl.TRIANGLES, uniformValues, tile);
    }
  }
  setOpacity(opacity: number) {
    this.opacity = opacity;
    this.map.triggerRepaint();
  }

  //清理事件监听
  onRemove(map: mapboxgl.Map, gl: WebGLRenderingContext) {
    map.off('move', this.update);
  }
  // 渲染更新
  update() {
    const center = this.map.getCenter();
    const zoom = Math.floor(this.map.getZoom());
    const bounds = this.map.getBounds();
    this.source.updateTile(this.gl, bounds, zoom, center, this.map);
  }
}
