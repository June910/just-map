export default `
#ifdef GL_ES
precision mediump float;
#endif
uniform float u_opacity;
uniform sampler2D u_Sampler;
varying vec2 v_texture_pos;
void main(){
  vec4 color = texture2D(u_Sampler, v_texture_pos);
  // vec4 color = vec4(0.5,0.1,0.1,1.0);

  gl_FragColor.rgb = color.rgb;
  gl_FragColor.a = u_opacity;
}
`;
