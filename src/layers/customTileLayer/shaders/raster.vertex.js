export default `
attribute vec2 a_pos;
attribute vec2 a_texture_pos;
varying vec2 v_texture_pos;
uniform mat4 u_matrix;
uniform float u_zoom;
uniform vec3 u_unitsPerDegree;
uniform vec3 u_unitsPerDegree2;
uniform vec2 u_center_lnglat;
uniform vec4 u_project_center_position;

const float TILE_SIZE = 512.0;
const float PI = 3.1415926536;
const float WORLD_SCALE = TILE_SIZE / (PI * 2.0);

vec4 project_offset(vec4 offset){
  float dy = offset.y;
  dy=clamp(dy,-1.,1.);
  vec3 units_per_unit = u_unitsPerDegree + u_unitsPerDegree2 * dy;
  return vec4(offset.xyz * units_per_unit, offset.w);
}

vec2 project_mercator(vec2 lnglat){
  float x = lnglat.x;
  return vec2(radians(x) + PI, PI + log(tan(PI * 0.25 + radians(lnglat.y) * 0.5)));
}

vec4 project_position(vec4 position){
  float X = position.x - u_center_lnglat.x;
  float Y = position.y - u_center_lnglat.y;
  return project_offset(vec4(X,Y,position.z,position.w));
}

void main(){
  v_texture_pos = a_texture_pos;
  if(u_zoom<12.0){
    gl_Position = u_matrix * vec4(project_mercator(a_pos) * WORLD_SCALE, 0.0,1.0);
  }else{
    vec4 project_pos = project_position(vec4(a_pos,0.0,1.0));
    gl_Position = u_matrix * project_pos + u_project_center_position;
  }
}
`;
