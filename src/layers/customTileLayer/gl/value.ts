export interface Value<T> {
  current: T;
  default: T;
  dirty: boolean;
  get(): T;
  setDefault(): void;
  set(value: T): void;
}

class BaseValue<T> implements Value<T> {
  gl: WebGLRenderingContext;
  current: T;
  default: T;
  dirty: boolean;
  constructor(gl: WebGLRenderingContext) {
    this.gl = gl;
    this.default = this.getDefault();
    this.current = this.default;
    this.dirty = false;
  }
  get(): T {
    return this.current;
  }
  set(value: T): void {
    // 子类实现
  }
  getDefault(): T {
    return this.default;
  }
  setDefault(): void {
    this.set(this.default);
  }
}
