import { TypedArray } from './vertex_buffer';

export default class IndexBuffer {
  gl: WebGLRenderingContext;
  buffer?: WebGLBuffer | null;
  data: TypedArray;
  constructor(
    gl: WebGLRenderingContext,
    data: TypedArray,
    dynamicDraw?: boolean,
  ) {
    this.gl = gl;
    this.data = data;
    this.buffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.buffer);
    gl.bufferData(
      gl.ELEMENT_ARRAY_BUFFER,
      data,
      dynamicDraw ? gl.DYNAMIC_DRAW : gl.STATIC_DRAW,
    );
    // gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,null);
  }
  get vertexCount() {
    return this.data.length;
  }
  bind() {
    if (!this.buffer) return;
    this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, this.buffer);
  }
  destroy() {
    if (this.buffer) {
      this.gl.deleteBuffer(this.buffer);
      delete this.buffer;
    }
  }
}
