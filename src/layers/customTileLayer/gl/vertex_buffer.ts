import Program from '../render/program';
import { memberType } from '../util/struct_array';

export type TypedArray = Uint16Array | Float32Array | Uint8Array;

const AttributeType = {
  Int8: 'BYTE',
  Uint8: 'UNSIGNED_BYTE',
  Int16: 'SHORT',
  Uint16: 'UNSIGNED_SHORT',
  Int32: 'INT',
  Uint32: 'UNSIGNED_INT',
  Float32: 'FLOAT',
};
export default class VertexBuffer {
  gl: WebGLRenderingContext;
  dynamicDraw?: boolean;
  attributes: Array<memberType>;
  buffer?: WebGLBuffer | null;
  constructor(
    gl: WebGLRenderingContext,
    attributes: Array<memberType>,
    data: TypedArray,
    dynamicDraw?: boolean,
  ) {
    this.gl = gl;
    this.attributes = attributes;
    this.buffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.buffer);
    gl.bufferData(
      gl.ARRAY_BUFFER,
      data,
      dynamicDraw ? gl.DYNAMIC_DRAW : gl.STATIC_DRAW,
    );
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
  }
  enableAttributes(program: Program) {
    for (let i = 0; i < this.attributes.length; i++) {
      const member = this.attributes[i];
      const attriIndex = program.attributes[member.name];
      this.gl.enableVertexAttribArray(attriIndex);
    }
  }
  bind() {
    //@ts-ignore
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.buffer);
  }
  unbind() {
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, null);
  }
  setVertexAttribPointers(program: Program) {
    for (let i = 0; i < this.attributes.length; i++) {
      const member = this.attributes[i];
      const attriIndex = program.attributes[member.name];
      //@ts-ignore
      this.gl.vertexAttribPointer(
        attriIndex,
        member.components,
        this.gl[AttributeType[member.type]],
        false,
        member.stride,
        member.offset,
      );
    }
  }
  destory() {
    if (this.buffer) {
      this.gl.deleteBuffer(this.buffer);
      delete this.buffer;
    }
  }
}
