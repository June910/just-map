const viewTypes = {
  Int8: Int8Array,
  Uint8: Uint8Array,
  Int16: Int16Array,
  Uint16: Uint16Array,
  Int32: Int32Array,
  Uint32: Uint32Array,
  Float32: Float32Array,
};
export type ViewType = keyof typeof viewTypes;
export interface memberType {
  name: string;
  type: ViewType;
  components: number;
  offset?: number;
  stride?: number;
}

export function createLayout(members: Array<memberType>): Array<memberType> {
  let offset = 0;
  const stride = members.reduce(
    (pre, cur) => pre + cur.components * sizeOf(cur.type),
    0,
  );
  return members.map((m) => {
    const typeSize = sizeOf(m.type);
    const memberOffset = offset;
    const components = m.components || 1;
    offset += typeSize * components;
    return {
      name: m.name,
      type: m.type,
      components,
      offset: memberOffset,
      stride,
    };
  });
}

function sizeOf(type: ViewType): number {
  return viewTypes[type].BYTES_PER_ELEMENT;
}
