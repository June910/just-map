import CustomTileSource from '@/sources/customTileSource';
import Tile from '@/sources/customTileSource/Tile';
import { UniformBindings, UniformLocations } from '../uniform_binding';
import RasterBoundsAttributes from './raster_bounds_attributes';

// 封装 program
export enum EShaderType {
  VS_SHADER,
  FS_SHADER,
}
export enum DrawMode {
  LINE_STRIP = 1,
  LINES = 2,
  TRIANGLES = 3,
}
export default class Program {
  program: WebGLProgram;
  gl: WebGLRenderingContext;
  failedToCreate: boolean;
  // uniform变量
  fixedUniforms: UniformBindings;
  // attribute变量
  attributes: { [_: string]: number } = {};
  constructor(
    gl: WebGLRenderingContext,
    sourceCode: { vertexSource: string; fragmentSource: string },
    fixedUniforms: (
      gl: WebGLRenderingContext,
      locations: UniformLocations,
    ) => UniformBindings,
    uniformKeys: string[],
  ) {
    const { vertexSource, fragmentSource } = sourceCode;
    this.gl = gl;
    // this.fixedUniforms = fixedUniforms;
    this.program = this._createProgram(gl);
    if (!this.program) {
      throw new Error('webgl程序创建失败');
    }
    if (gl.isContextLost()) {
      this.failedToCreate = true;
      return;
    }
    const vertexShader = this._createShader(gl, EShaderType.VS_SHADER);
    const fragmentShader = this._createShader(gl, EShaderType.FS_SHADER);
    if (!this._compileShader(vertexSource, vertexShader)) {
      throw new Error(' WebGL顶点Shader链接不成功! ');
    }
    if (!this._compileShader(fragmentSource, fragmentShader)) {
      throw new Error(' WebGL像素片段Shader链接不成功! ');
    }
    if (!this._linkProgram(this.program, vertexShader, fragmentShader)) {
      throw new Error(' WebGLProgram链接不成功! ');
    }
    this._deleteShader(vertexShader, fragmentShader);
    this._bindUniforms(fixedUniforms, uniformKeys);
    // 获取attribute的position source中的tile要用
    this._getAttribLocation();
  }
  private _createProgram(gl: WebGLRenderingContext): WebGLProgram {
    let program: WebGLProgram | null = gl.createProgram();
    if (program === null) {
      // 直接抛出错误
      throw new Error('WebGLProgram创建失败！');
    }
    return program;
  }

  private _createShader(
    gl: WebGLRenderingContext,
    type: EShaderType,
  ): WebGLShader {
    let shader: WebGLShader | null = null;
    if (type === EShaderType.VS_SHADER) {
      shader = gl.createShader(gl.VERTEX_SHADER);
    } else {
      shader = gl.createShader(gl.FRAGMENT_SHADER);
    }
    if (shader === null) {
      // 如果创建WebGLShader对象失败，我们采取抛错错误的处理方式
      throw new Error('WebGLShader创建失败！');
    }
    return shader;
  }

  private _compileShader(
    // gl: WebGLRenderingContext,
    shaderSource: string,
    shader: WebGLShader,
  ) {
    this.gl.shaderSource(shader, shaderSource);
    this.gl.compileShader(shader);
    // 检查编译错误
    if (this.gl.getShaderParameter(shader, this.gl.COMPILE_STATUS) === false) {
      // 如果编译出现错误，则弹出对话框，了解错误的原因
      console.error(this.gl.getShaderInfoLog(shader));
      // 然后将shader删除掉，防止内存泄漏
      this.gl.deleteShader(shader);
      // 编译错误返回false
      return false;
    }
    // 编译成功返回true
    return true;
  }

  private _linkProgram(
    // gl: WebGLRenderingContext, // 渲染上下文对象
    program: WebGLProgram, // 链接器对象
    vsShader: WebGLShader, // 要链接的顶点着色器
    fsShader: WebGLShader, // 要链接的片段着色器
    beforeProgramLink:
      | ((gl: WebGLRenderingContext, program: WebGLProgram) => void)
      | null = null,
    afterProgramLink:
      | ((gl: WebGLRenderingContext, program: WebGLProgram) => void)
      | null = null,
  ): boolean {
    // 1、使用attachShader方法将顶点和片段着色器与当前的连接器相关联
    this.gl.attachShader(program, vsShader);
    this.gl.attachShader(program, fsShader);

    // 2、在调用linkProgram方法之前，按需触发beforeProgramLink回调函数
    if (beforeProgramLink !== null) {
      beforeProgramLink(this.gl, program);
    }

    // 3、调用linkProgram进行链接操作
    this.gl.linkProgram(program);
    // 4、使用带gl.LINK_STATUS参数的getProgramParameter方法，进行链接状态检查
    if (this.gl.getProgramParameter(program, this.gl.LINK_STATUS) === false) {
      // 4.1 如果链接出错，调用getProgramInfoLog方法将错误信息以弹框方式通知调用者
      console.error(this.gl.getProgramInfoLog(program));
      // 4.2 删除掉相关资源，防止内存泄漏
      this.gl.deleteShader(vsShader);
      this.gl.deleteShader(fsShader);
      this.gl.deleteProgram(program);
      // 4.3 返回链接失败状态
      return false;
    }

    // 5、使用validateProgram进行链接验证
    this.gl.validateProgram(program);
    // 6、使用带gl.VALIDATE_STATUS参数的getProgramParameter方法，进行验证状态检查
    if (
      this.gl.getProgramParameter(program, this.gl.VALIDATE_STATUS) === false
    ) {
      // 6.1 如果验证出错，调用getProgramInfoLog方法将错误信息以弹框方式通知调用者
      console.error(this.gl.getProgramInfoLog(program));
      // 6.2 删除掉相关资源，防止内存泄漏
      this.gl.deleteShader(vsShader);
      this.gl.deleteShader(fsShader);
      this.gl.deleteProgram(program);
      // 6.3 返回链接失败状态
      return false;
    }

    // 7、全部正确，按需调用afterProgramLink回调函数
    if (afterProgramLink !== null) {
      afterProgramLink(this.gl, program);
    }

    // 8、返回链接正确表示
    return true;
  }
  // 绑定uniform变量地址
  private _bindUniforms(
    fixedUniforms: (
      gl: WebGLRenderingContext,
      locations: UniformLocations,
    ) => UniformBindings,
    uniformKeys: string[],
  ) {
    const uniformLocations: { [_: string]: WebGLUniformLocation } = {};
    if (this.program === null) return;
    const program = this.program;
    uniformKeys.forEach((key) => {
      const uniformLocation = this.gl.getUniformLocation(program, key);
      if (uniformLocation) {
        uniformLocations[key] = uniformLocation;
      }
    });
    this.fixedUniforms = fixedUniforms(this.gl, uniformLocations);
  }
  // 获取attribute的location
  private _getAttribLocation() {
    for (let i = 0; i < RasterBoundsAttributes.length; i++) {
      const name = RasterBoundsAttributes[i].name;
      const aPosition = this.gl.getAttribLocation(this.program, name);
      this.attributes[name] = aPosition;
    }
  }
  private _deleteShader(vsShader: WebGLShader, faShader: WebGLShader) {
    this.gl.deleteShader(vsShader);
    this.gl.deleteShader(faShader);
  }
  // 绘图
  draw(drawMode: DrawMode, uniformValues: { [_: string]: any }, tile: Tile) {
    this.gl.useProgram(this.program);
    for (const name in this.fixedUniforms) {
      this.fixedUniforms[name].set(uniformValues[name]);
    }
    this.gl.bindTexture(this.gl.TEXTURE_2D, tile.texture);
    this.gl.activeTexture(this.gl.TEXTURE0);
    this.gl.texParameteri(
      this.gl.TEXTURE_2D,
      this.gl.TEXTURE_MIN_FILTER,
      this.gl.LINEAR,
    );
    this.gl.texParameteri(
      this.gl.TEXTURE_2D,
      this.gl.TEXTURE_WRAP_S,
      this.gl.CLAMP_TO_EDGE,
    );
    this.gl.texParameteri(
      this.gl.TEXTURE_2D,
      this.gl.TEXTURE_WRAP_T,
      this.gl.MIRRORED_REPEAT,
    );
    tile.dataBuffer.bind();
    tile.dataBuffer.setVertexAttribPointers(this);
    tile.dataBuffer.enableAttributes(this);
    tile.dataBuffer.unbind();
    tile.indexBuffer.bind();
    const vertexCount = tile.indexBuffer.vertexCount;
    this.gl.enable(this.gl.BLEND);
    // this.gl.blendFunc(this.gl.SRC_ALPHA, this.gl.ONE_MINUS_SRC_ALPHA);
    this.gl.blendFuncSeparate(
      this.gl.SRC_ALPHA,
      this.gl.ONE_MINUS_SRC_ALPHA,
      this.gl.ONE,
      this.gl.ONE_MINUS_SRC_ALPHA,
    );
    this.gl.drawElements(drawMode, vertexCount, this.gl.UNSIGNED_BYTE, 0);
    // this.gl.drawArrays(this.gl.TRIANGLE_STRIP, 0, 4);
  }
}
