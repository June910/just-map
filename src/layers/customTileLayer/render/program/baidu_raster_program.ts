// 封装uniform的实例化方法
// eg    const uniforms = biaduUnifomrs(gl,uniformlocations)
//       const uniformValues = {'u_opacity':1};
//       uniforms[key].set(uniformValues[key])
import {
  Uniform1f,
  Uniform1i,
  Uniform2f,
  Uniform3f,
  Uniform4f,
  UniformLocations,
  UniformMatrix4f,
} from '../uniform_binding';

export type BaiduUniformsType = {
  u_matrix: UniformMatrix4f;
  u_opacity: Uniform1f;
  u_Sampler: Uniform1i;
  u_unitsPerDegree: Uniform3f;
  u_uintsPerDegree2: Uniform3f;
  u_center_lnglat: Uniform2f;
  u_project_center_position: Uniform4f;
  u_zoom: Uniform1f;
};

const baiduUniforms = (
  gl: WebGLRenderingContext,
  locations: UniformLocations,
): BaiduUniformsType => ({
  u_matrix: new UniformMatrix4f(gl, locations.u_matrix),
  u_opacity: new Uniform1f(gl, locations.u_opacity),
  u_Sampler: new Uniform1i(gl, locations.u_Sampler),
  u_unitsPerDegree: new Uniform3f(gl, locations.u_unitsPerDegree),
  u_uintsPerDegree2: new Uniform3f(gl, locations.u_uintsPerDegree2),
  u_center_lnglat: new Uniform2f(gl, locations.u_center_lnglat),
  u_project_center_position: new Uniform4f(
    gl,
    locations.u_project_center_position,
  ),
  u_zoom: new Uniform1f(gl, locations.u_zoom),
});
const baiduUniformValues = (matrix: Float32Array, opacity: number) => ({
  u_matrix: matrix,
  u_opacity: opacity,
});
export { baiduUniforms, baiduUniformValues };
