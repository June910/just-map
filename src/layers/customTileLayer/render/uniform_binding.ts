// 封装uniform cpu ===》gpu

export type UniformLocations = { [_: string]: WebGLUniformLocation };
class Uniform<T> {
  gl: WebGLRenderingContext;
  location: WebGLUniformLocation;
  current: T;
  constructor(gl: WebGLRenderingContext, location: WebGLUniformLocation) {
    this.gl = gl;
    this.location = location;
  }
  set(v: T): void {}
}

class Uniform1i extends Uniform<number> {
  constructor(gl: WebGLRenderingContext, location: WebGLUniformLocation) {
    super(gl, location);
    this.current = 0;
  }

  set(v: number): void {
    if (this.current !== v) {
      this.current = v;
      this.gl.uniform1i(this.location, v);
    }
  }
}

class Uniform1f extends Uniform<number> {
  constructor(gl: WebGLRenderingContext, location: WebGLUniformLocation) {
    super(gl, location);
    this.current = 0;
  }

  set(v: number): void {
    if (this.current !== v) {
      this.current = v;
      this.gl.uniform1f(this.location, v);
    }
  }
}

class Uniform2f extends Uniform<[number, number]> {
  constructor(gl: WebGLRenderingContext, location: WebGLUniformLocation) {
    super(gl, location);
    this.current = [0, 0];
  }

  set(v: [number, number]): void {
    if (v[0] !== this.current[0] || v[1] !== this.current[1]) {
      this.current = v;
      this.gl.uniform2f(this.location, v[0], v[1]);
    }
  }
}

class Uniform3f extends Uniform<[number, number, number]> {
  constructor(gl: WebGLRenderingContext, location: WebGLUniformLocation) {
    super(gl, location);
    this.current = [0, 0, 0];
  }

  set(v: [number, number, number]): void {
    if (
      v[0] !== this.current[0] ||
      v[1] !== this.current[1] ||
      v[2] !== this.current[2]
    ) {
      this.current = v;
      this.gl.uniform3f(this.location, v[0], v[1], v[2]);
    }
  }
}

class Uniform4f extends Uniform<[number, number, number, number]> {
  constructor(gl: WebGLRenderingContext, location: WebGLUniformLocation) {
    super(gl, location);
    this.current = [0, 0, 0, 0];
  }

  set(v: [number, number, number, number]): void {
    if (
      v[0] !== this.current[0] ||
      v[1] !== this.current[1] ||
      v[2] !== this.current[2] ||
      v[3] !== this.current[3]
    ) {
      this.current = v;
      this.gl.uniform4f(this.location, v[0], v[1], v[2], v[3]);
    }
  }
}
class UniformMatrix4f extends Uniform<Float32Array> {
  constructor(gl: WebGLRenderingContext, location: WebGLUniformLocation) {
    super(gl, location);
    this.current = new Float32Array(16);
  }
  set(v: Float32Array): void {
    if (v[12] !== this.current[12] || v[0] !== this.current[0]) {
      this.current = v;
      this.gl.uniformMatrix4fv(this.location, false, v);
      return;
    }
    for (let i = 1; i < 16; i++) {
      if (v[i] !== this.current[i]) {
        this.current = v;
        this.gl.uniformMatrix4fv(this.location, false, v);
        break;
      }
    }
  }
}
export {
  Uniform1i,
  Uniform1f,
  Uniform2f,
  Uniform3f,
  Uniform4f,
  UniformMatrix4f,
};
export type UniformBindings = { [_: string]: Uniform<any> };
