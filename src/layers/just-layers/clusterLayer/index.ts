import { BaseJustLayer, JustLayer } from '../index';
import { CirclePaint, SymbolPaint, SymbolLayout } from 'mapbox-gl';
import { isJustUrl } from '@/utils';
import Source from '@/sources';

class ClusterLayer extends BaseJustLayer implements JustLayer {
  // map: mapboxgl.Map;
  // layer: any;
  clusterLayerIdList: string[] = []; // 图层id数组
  PaintOrLayoutMapping: {
    [key: string]: [
      string,
      keyof CirclePaint | keyof SymbolPaint | keyof SymbolLayout,
    ];
  } = {}; // justLayer中属性和mapbox原生图层属性对应关系

  initLayer(
    data: GeoJSON.FeatureCollection | undefined,
    beforeId: string | undefined,
  ) {
    const source = this.sourceCache.getSourceCache(
      this.layerSourceId,
    ) as Source;
    const sourceId = source.getSourceId();
    // const originData = source.getOriginData(); // 获取用户传入的原始数据
    // if (typeof originData === 'string' && isJustUrl(originData)) {
    //   newData = { type: 'FeatureCollection', features: data };
    // }
    // this.map.addSource(sourceId, {
    //   type: 'geojson',
    //   data: newData,
    //   ...source.clusterOption,
    // });
    this.addLayers(beforeId, sourceId);
  }
  addLayers(beforeId: string | undefined, sourceId: string) {
    this.clusterLayerIdList = [
      `${this.layer.id}-clusterCircle`,
      `${this.layer.id}-clusterText`,
      `${this.layer.id}-unClusterCircle`,
      `${this.layer.id}-unClusterIcon`,
    ];
    if (this.map.getLayer(`${this.layer.id}-clusterCircle`)) return;

    this.PaintOrLayoutMapping = {
      clusterCircleColor: [this.clusterLayerIdList[0], 'circle-color'],
      clusterCircleRadius: [this.clusterLayerIdList[0], 'circle-radius'],
      clusterTextColor: [this.clusterLayerIdList[1], 'text-color'],
      clusterTextSize: [this.clusterLayerIdList[1], 'text-size'],
      unClusterCircleColor: [this.clusterLayerIdList[2], 'circle-color'],
      unClusterCircleRadius: [this.clusterLayerIdList[2], 'circle-radius'],
      unClusterIconImage: [this.clusterLayerIdList[3], 'icon-image'],
      unClusterIconSize: [this.clusterLayerIdList[3], 'icon-size'],
    };
    const { paint = {}, layout = {}, unClusterIconZoom = 15 } = this.layer;
    const {
      clusterCircleColor = 'rgba(2,103,247,0.5)',
      clusterCircleRadius = 40,
      unClusterCircleColor = 'rgba(2,103,247,0.5)',
      unClusterCircleRadius = 16,
      clusterTextColor = '#FFFFFF',
    } = paint;
    const {
      clusterTextSize = 18,
      unClusterIconImage = '',
      unClusterIconSize = 1,
    } = layout;
    this.map.addLayer(
      {
        id: this.clusterLayerIdList[0],
        type: 'circle',
        source: sourceId,
        filter: ['has', 'point_count'],
        paint: {
          'circle-color': clusterCircleColor,
          'circle-radius': clusterCircleRadius,
        },
      },
      beforeId,
    );
    this.map.addLayer(
      {
        id: this.clusterLayerIdList[1],
        type: 'symbol',
        source: sourceId,
        filter: ['has', 'point_count'],
        paint: {
          'text-color': clusterTextColor,
        },
        layout: {
          'text-field': '{point_count_abbreviated}',
          'text-font': ['literal', ['msyh']],
          'text-size': clusterTextSize,
        },
      },
      beforeId,
    );

    this.map.addLayer(
      {
        id: this.clusterLayerIdList[2],
        type: 'circle',
        source: sourceId,
        filter: ['!', ['has', 'point_count']],
        paint: {
          'circle-color': unClusterCircleColor,
          'circle-radius': unClusterCircleRadius,
          // "circle-stroke-width": 1,
          // "circle-stroke-color": "#fff"
        },
        maxzoom: unClusterIconZoom,
      },
      beforeId,
    );
    unClusterIconImage &&
      this.map.addLayer(
        {
          id: this.clusterLayerIdList[3],
          type: 'symbol',
          source: sourceId,
          filter: ['!', ['has', 'point_count']],
          layout: {
            'icon-image': unClusterIconImage,
            'icon-size': unClusterIconSize,
          },
          minzoom: unClusterIconZoom,
        },
        beforeId,
      );
  }
  remove() {
    this.clusterLayerIdList.forEach((layerId: string) => {
      this.map.getLayer(layerId) && this.map.removeLayer(layerId);
    });
    // const source = this.sourceCache.getSourceCache(this.layerSourceId);
    // const sourceId = source.getSourceId();
    // this.map.removeSource(sourceId);
  }
  move(beforeId: string | undefined) {
    this.clusterLayerIdList.forEach((layerId: string) => {
      this.map.getLayer(layerId) && this.map.moveLayer(layerId, beforeId);
    });
  }
  setProperty(options: any) {
    const { paint, layout } = options;
    paint &&
      Object.keys(paint).forEach((k) => {
        const layerId = this.PaintOrLayoutMapping[k][0];
        const paintKey = this.PaintOrLayoutMapping[k][1];
        this.map.setPaintProperty(layerId, paintKey, paint[k]);
      });
    layout &&
      Object.keys(layout).forEach((k) => {
        if (k === 'visibility') {
          this.clusterLayerIdList.forEach((layerId) => {
            this.map.setLayoutProperty(layerId, 'visibility', layout[k]);
          });
          return;
        }
        const layerId = this.PaintOrLayoutMapping[k][0];
        const layOutKey = this.PaintOrLayoutMapping[k][1];
        this.map.setLayoutProperty(layerId, layOutKey, layout[k]);
      });
  }
  setFilter(filter: any[]) {
    this.map.getLayer(`${this.layer.id}-unClusterCircle`) &&
      this.map.setFilter(`${this.layer.id}-unClusterCircle`, [
        'all',
        ['!', ['has', 'point_count']],
        filter,
      ]);
    this.map.getLayer(`${this.layer.id}-unClusterIcon`) &&
      this.map.setFilter(`${this.layer.id}-unClusterIcon`, [
        'all',
        ['!', ['has', 'point_count']],
        filter,
      ]);

    // this.clusterLayerIdList.forEach((layerId: string) => {
    //   this.map.getLayer(layerId) && this.map.setFilter(layerId, filter);
    // });
  }
  setLayerZoomRange(minZoom: number, maxZoom: number) {
    this.clusterLayerIdList.forEach((layerId: string) => {
      this.map.getLayer(layerId) &&
        this.map.setLayerZoomRange(layerId, minZoom || 0, maxZoom || 24);
    });
  }
}
export default ClusterLayer;
