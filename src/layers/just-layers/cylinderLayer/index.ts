import { BaseJustLayer, JustLayer } from '../index';
import Cylinder from './cylinder/cylinder';
import {
  CylinderLayerOptionType,
  CylinderPoint,
} from './cylinder/Cylinder.types';

export interface CylinderLayerOption
  extends Omit<CylinderLayerOptionType, 'data'> {
  source?: string | any;
}

function transformData(data: GeoJSON.Feature[]): CylinderPoint[] {
  if (!Array.isArray(data)) {
    return [];
  }
  return data
    .filter((item) => item?.geometry?.type === 'Point')
    .map((item) => {
      const obj = {
        // @ts-ignore
        longitude: item.geometry.coordinates?.[0],
        // @ts-ignore
        latitude: item.geometry.coordinates?.[1],
        id: item.id,
      };
      if (item?.properties instanceof Object) {
        Object.assign(obj, item.properties);
      }
      return obj;
    });
}
class CylinderLayer extends BaseJustLayer implements JustLayer {
  // map: mapboxgl.Map;
  // layer: CylinderLayerOption;
  // sourceCache: SourceCache;
  // layerSourceId: string;
  // beforeId:string|undefined
  // constructor(
  //   map: mapboxgl.Map,
  //   layerOption: CylinderLayerOption,
  //   sourceCache: SourceCache,
  // ) {
  //   super();
  //   this.map = map;
  //   this.layer = layerOption;
  //   this.sourceCache = sourceCache;
  //   this.layerSourceId =
  //     typeof this.layer.source === 'string' ? this.layer.source : this.layer.id;
  // }
  // init(beforeId:string|undefined){
  //   if(this.sourceCache.getSourceCache(this.layerSourceId)){
  //     const data = this.sourceCache.getSourceCache(this.layerSourceId).getData();
  //     if(data){
  //       this.initLayer(data,beforeId);
  //       return;
  //     }
  //   // 如果在source缓存中找不到数据,则说明数据加载还未完成，监听source.data方法拿到数据
  //   // 之所以一开始不监听source.data 是因为用户可能先map.addSource 数据下载完成后才map.addLayer
  //     this.sourceCache.getSourceCache(this.layerSourceId).on('source.data',(e)=>{
  //       const data = this.sourceCache.getSourceCache(this.layerSourceId).getData();
  //       this.initLayer(data,beforeId);
  //     });
  //   }
  // }
  initLayer(
    data: GeoJSON.FeatureCollection | undefined,
    beforeId: string | undefined,
  ) {
    if (this.map.getLayer(this.layer.id)) {
      this.map.removeLayer(this.layer.id);
    }
    const featurs = data?.features || [];
    const cylinderData = transformData(featurs);
    const options = Object.assign(
      {},
      this?.layer,
      { ...this?.layer?.paint },
      { ...this?.layer?.layout },
    );
    this.map.addLayer(
      new Cylinder({
        ...options,
        data: cylinderData,
      }),
      beforeId, //TODO 解释见热力图添加
    );
  }
  // add(beforeId:string|undefined){
  //   this.beforeId = beforeId;
  //   this.init(beforeId);
  // }
  // remove() {
  //   if (this.map.getLayer(this.layer.id)) this.map.removeLayer(this.layer.id);
  //   if (this.map.getSource(this.layer.id)) this.map.removeSource(this.layer.id);
  // }
  // move(beforeId: string | undefined) {
  //   this.map.moveLayer(this.layer.id, beforeId);
  // }
  setProperty(options: any) {
    // const { layout } = options;
    // layout &&
    //   Object.keys(layout as Layout).forEach((k) => {
    //     this.map.setLayoutProperty(this.layer.id, k, layout[k]);
    //   });
    // this.layer = { ...this.layer, ...options };
    const { paint, layout, ...others } = options;
    others && Object.assign(this.layer, others);
    if (paint && this.layer.paint) {
      Object.assign(this.layer.paint, paint);
    } else if (paint) {
      this.layer.paint = paint;
    }
    if (layout && this.layer.layout) {
      Object.assign(this.layer.layout, layout);
    } else if (layout) {
      this.layer.layout = layout;
    }
    this.remove();
    this.add(this.beforeId);
    if (this.layer.layout?.visibility === 'none') {
      this.map.setLayoutProperty(this.layer.id, 'visibility', 'none');
      return;
    }
  }
}
export default CylinderLayer;
