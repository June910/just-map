import {
  CylinderLayerType,
  CylinderLayerOptionType,
  CylinderPropsType,
  CylinderBuffersType,
} from './Cylinder.types';
import {
  DEFAULT_COLOR,
  formatData,
  initShader,
  normalizeColor,
  pointsToBuffer,
  Vector3,
} from './util';

export default class CylinderLayer implements CylinderLayerType {
  id: string;
  type: 'custom' = 'custom';
  renderingMode: '3d' = '3d';
  radius: number = 1;
  program: WebGLProgram | null = null;
  color: [number, number, number, number];
  meshData: {
    positions: Float32Array;
    cells: Uint16Array;
    cellsCount: number;
    normals: Float32Array;
  };
  buffers: CylinderBuffersType = {
    verticesBuffer: null,
    cellsBuffer: null,
    normalBuffer: null,
  };

  constructor({
    id,
    valueKey = 'height',
    data,
    color = '#71F7FF',
    radius = 4,
    maxHeight = 1,
  }: CylinderLayerOptionType) {
    this.id = id;
    this.color = normalizeColor(color);

    // 转换为所有的webgl的格式
    const { positions, cells, normals } = formatData({
      data,
      valueKey,
      height: maxHeight,
      radius,
    });
    const UintCells = pointsToBuffer(cells, Uint16Array) as Uint16Array;
    const FloatPositions = pointsToBuffer(
      positions,
      Float32Array,
    ) as Float32Array;
    const normalsFloat = pointsToBuffer(normals, Float32Array) as Float32Array;
    this.meshData = {
      positions: FloatPositions,
      cells: UintCells,
      normals: normalsFloat,
      cellsCount: UintCells.length,
    };
  }

  onAdd(map: mapboxgl.Map, gl: WebGL2RenderingContext) {
    const vertex = `
      attribute vec4 a_Position;
      uniform mat4 u_MvpMatrix;
      attribute vec4 a_Normal;
      uniform vec3 u_LightColor;
      uniform vec4 u_Color;
      uniform vec3 u_LightDirection;
      uniform vec3 u_AmbientLight;
      varying vec4 v_Color;
      void main(){
        gl_Position = u_MvpMatrix * a_Position;
        vec3 normal = normalize(vec3(a_Normal));
        float nDotL = max(dot(u_LightDirection, normal), 0.0);
        vec3 diffuse = u_LightColor * u_Color.rgb * nDotL;
        vec3 ambient = u_AmbientLight * u_Color.rgb;
        v_Color = vec4(diffuse + ambient, u_Color.a);
      }`;
    const fragment = `
      precision mediump float;
      varying vec4 v_Color;
      void main(){
        gl_FragColor = v_Color;
      }`;
    this.program = initShader(gl, vertex, fragment);
    this.buffers = {
      verticesBuffer: gl.createBuffer(),
      cellsBuffer: gl.createBuffer(),
      normalBuffer: gl.createBuffer(),
    };
  }

  render(gl: WebGL2RenderingContext, matrix: number[]) {
    if (!this.program) return;
    gl.useProgram(this.program);

    const u_LightColor = gl.getUniformLocation(this.program, 'u_LightColor');
    const u_LightDirection = gl.getUniformLocation(
      this.program,
      'u_LightDirection',
    );
    gl.uniform3f(u_LightColor, 1.0, 1.0, 1.0);
    const lightDirection = new Vector3([0.5, 3.0, 8.0]);
    lightDirection.normalize();
    gl.uniform3fv(u_LightDirection, lightDirection.elements);

    const u_AmbientLight = gl.getUniformLocation(
      this.program,
      'u_AmbientLight',
    );
    gl.uniform3f(u_AmbientLight, 0.2, 0.2, 0.2);

    // 设置颜色
    const u_Color = gl.getUniformLocation(this.program, 'u_Color');
    gl.uniform4f(u_Color, ...this.color);

    // 设置点
    const { positions, cells, cellsCount, normals } = this.meshData;

    // 点的坐标
    this.setVertexData(
      gl,
      this.buffers.verticesBuffer,
      positions,
      'a_Position',
    );
    // 法向量
    this.setVertexData(gl, this.buffers.normalBuffer, normals, 'a_Normal');

    // 设置索引
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.buffers.cellsBuffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, cells, gl.STATIC_DRAW);

    gl.uniformMatrix4fv(
      gl.getUniformLocation(this.program, 'u_MvpMatrix'),
      false,
      matrix,
    );

    gl.drawElements(gl.TRIANGLES, cellsCount, gl.UNSIGNED_SHORT, 0);
  }

  // 设置顶点数据
  setVertexData(
    gl: WebGL2RenderingContext,
    buffer: WebGLBuffer | null,
    data: any,
    attribute: string,
  ) {
    if (!buffer || !this.program) return;
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);

    const attr = gl.getAttribLocation(this.program, attribute);
    gl.vertexAttribPointer(attr, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(attr);
  }
}
