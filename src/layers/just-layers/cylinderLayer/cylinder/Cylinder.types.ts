import { CustomLayerInterface, Visibility } from 'mapbox-gl';

/**
 * 初始化参数
 */
export interface CylinderLayerOptionType {
  id: string;
  valueKey: string;
  data: CylinderPoint[];
  color: string;
  radius: number;
  maxHeight: number;
}

export interface CylinderBuffersType {
  verticesBuffer: WebGLBuffer | null;
  cellsBuffer: WebGLBuffer | null;
  normalBuffer: WebGLBuffer | null;
}
export interface CylinderLayerType extends CustomLayerInterface {
  color: [number, number, number, number];
  program: WebGLProgram | null;
  meshData: {
    positions: Float32Array;
    cells: Uint16Array;
    cellsCount: number;
    normals: Float32Array;
  };
  buffers: CylinderBuffersType;
}

export interface CylinderPropsType
  extends Omit<CylinderLayerOptionType, 'color'> {
  map?: mapboxgl.Map;
  data: CylinderPoint[];
  color?: string;
  maxzoom?: number;
  minzoom?: number;
  visibility?: Visibility;
  beforeLayerId?: string;
}

export type CylinderPoint = {
  longitude: number;
  latitude: number;
  height?: number;
};
