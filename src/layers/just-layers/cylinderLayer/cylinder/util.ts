/* eslint-disable camelcase */
import mapboxgl from 'mapbox-gl';
import color from 'color';
import { CylinderPoint } from './Cylinder.types';

export const DEFAULT_COLOR: string = '#f00';
const BASE_RADIUS: number = 5e-6;
const BASE_HEIGHT: number = 1e1;

export function initShader(
  gl: WebGL2RenderingContext,
  VSHADER_SOURCE: string,
  FSHADER_SOURCE: string,
): WebGLProgram | null {
  const vertexShader = gl.createShader(gl.VERTEX_SHADER);
  if (!vertexShader) return null;
  gl.shaderSource(vertexShader, VSHADER_SOURCE);
  gl.compileShader(vertexShader);
  const fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);
  if (!fragmentShader) return null;
  gl.shaderSource(fragmentShader, FSHADER_SOURCE);
  gl.compileShader(fragmentShader);

  const program = gl.createProgram();
  if (!program) return null;
  gl.attachShader(program, vertexShader);
  gl.attachShader(program, fragmentShader);
  gl.linkProgram(program);
  return program;
}

export function getZero(val: number) {
  return Math.abs(val) < 1e-15 ? 0 : val;
}

/**
 * 归一化数组
 */
export function normalizeArray(
  arr: Array<number>,
  max: number,
  min: number,
): Array<number> {
  return arr.map((i) => (i - min) / (max - min));
}

// 将颜色字符串转换成webgl识别的颜色
export function normalizeColor(str: string): [number, number, number, number] {
  const colorObj = color(str);
  return normalizeArray(
    colorObj
      .rgb()
      .array()
      // 取前三位，确保是r，g，b
      .slice(0, 3),
    255,
    0,
  ).concat(colorObj.alpha()) as [number, number, number, number];
}

export function formatData({
  data,
  radius = 1,
  valueKey = 'height',
  height = 1,
}: {
  data: CylinderPoint[];
  radius: number;
  valueKey: string;
  height: number;
}) {
  const segments = 4; // 先支持六面柱状体

  const positions = [];
  const cells = [];
  const normals = [];

  // 顶和底的圆
  const length = data.length;
  // 遍历数据 将所有的点和索引一起输出
  // 一个立方体6个面  每个面两个索引 一共十二个索引
  for (let i = 0; i < length; i++) {
    const d = data[i];
    // @ts-ignore
    const value = d[valueKey];
    const c3d = mapboxgl.MercatorCoordinate.fromLngLat(
      [d.longitude, d.latitude],
      value,
    );
    const { x, y, z = 0 } = c3d;
    const h = z * height * BASE_HEIGHT;
    const cap = [];
    // 遍历一个底边
    for (let j = 0; j < segments; j++) {
      const theta = (Math.PI * 2 * j) / segments + Math.PI / 4;
      const p = [
        x + radius * BASE_RADIUS * Math.cos(theta),
        y + radius * BASE_RADIUS * Math.sin(theta),
      ];
      cap.push(p);
    }
    // eslint-disable-next-line no-shadow
    positions.push(...cap.map(([x, y]) => [x, y, 0.0])); // 下面顶点
    normals.push(
      Array(4)
        .fill([0, 0, -1])
        .reduce((acc, cur) => [...acc, ...cur], []),
    ); // 下面的法向量

    // eslint-disable-next-line no-shadow
    positions.push(...cap.map(([x, y]) => [x, y, h])); // 上面顶点
    normals.push(
      Array(4)
        .fill([0, 0, 1])
        .reduce((acc, cur) => [...acc, ...cur], []),
    ); // 上面的法向量

    // 从第0个点开始, 计算每个面的点和 法向量
    for (let m = 0; m < segments; m += 1) {
      const nextM = (m + 1) % segments;
      const basePoint = [cap[m], cap[nextM]];
      positions.push([...basePoint[0], h], [...basePoint[0], 0]);
      positions.push([...basePoint[1], 0], [...basePoint[1], h]);
      const theta = (Math.PI * 2 * m) / segments + Math.PI / 2;
      const normal = [getZero(Math.cos(theta)), getZero(Math.sin(theta)), 0];
      normals.push(
        Array(4)
          .fill(normal)
          .reduce((acc, cur) => [...acc, ...cur], []),
      );
    }
    const offset = i * segments * (segments + 2); // 第n个正方体的顶点排序数
    for (let k = 0; k < segments + 2; k += 1) {
      cells.push([offset + k * 4, offset + 1 + k * 4, offset + 2 + k * 4]);
      cells.push([offset + k * 4, offset + 2 + k * 4, offset + 3 + k * 4]);
    }
  }

  return { positions, cells, normals };
}

export function pointsToBuffer<
  T extends Uint16ArrayConstructor | Float32ArrayConstructor,
>(positions: number[][], Type: T) {
  if (!Array.isArray(positions) || positions.length < 1) return new Type();
  const deminsion = positions[0].length;
  const len = positions.length;
  let idx = 0;
  const buffer = new Type(deminsion * len);
  for (let i = 0; i < len; i++) {
    for (let j = 0; j < deminsion; j++) {
      buffer[idx++] = positions[i][j];
    }
  }
  return buffer;
}

/**
 * Constructor of Vector3
 * If opt_src is specified, new vector is initialized by opt_src.
 * @param opt_src source vector(option)
 */
export class Vector3 {
  elements: Float32Array;
  constructor(opt_src?: number[]) {
    const v = new Float32Array(3);
    if (opt_src && typeof opt_src === 'object') {
      v[0] = opt_src[0];
      v[1] = opt_src[1];
      v[2] = opt_src[2];
    }
    this.elements = v;
  }
  /**
   * Normalize.
   * @return this
   */
  normalize() {
    const v = this.elements;
    const c = v[0];
    const d = v[1];
    const e = v[2];
    let g = Math.sqrt(c * c + d * d + e * e);
    if (g) {
      if (g == 1) return this;
    } else {
      v[0] = 0;
      v[1] = 0;
      v[2] = 0;
      return this;
    }
    g = 1 / g;
    v[0] = c * g;
    v[1] = d * g;
    v[2] = e * g;
    return this;
  }
}
