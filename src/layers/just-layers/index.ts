import SourceCache from '@/sources/source_cache';
import Event from '@/events';
import Source from '@/sources';

abstract class JustLayer {
  abstract add(beforeId: string | undefined): void;
  abstract remove(): void;
  abstract move(beforeId: string | undefined): void;
  abstract setProperty(option: any): void;
}
export { JustLayer };
export type JustLayerType =
  | 'just_heatmap'
  | 'just_cylinder'
  | 'just_trips'
  | 'just_cluster'
  | 'just_solidPolygon'
  | 'just_model';

/**
 * justLayer基类
 */
export class BaseJustLayer extends Event {
  map: mapboxgl.Map;
  layer: any;
  sourceCache: SourceCache;
  layerSourceId: string;
  beforeId: string | undefined;
  baseUrl: string | undefined; //new Map中传入的baseUrl  三维模型需要url地址 统一管理 just://api.justserver.com ===> http://just-test.jd.com

  constructor(
    map: mapboxgl.Map,
    layerOption: any,
    sourceCache: SourceCache,
    baseUrl?: string,
  ) {
    super();
    this.map = map;
    this.layer = layerOption;
    this.sourceCache = sourceCache;
    this.layerSourceId =
      typeof this.layer.source === 'string' ? this.layer.source : this.layer.id;
    this.baseUrl = baseUrl;
  }
  init(beforeId: string | undefined) {
    if (this.sourceCache.getSourceCache(this.layerSourceId)) {
      const data = (
        this.sourceCache.getSourceCache(this.layerSourceId) as Source
      ).getData();
      this.initLayer(data, beforeId);
      this.setLayerZoomRange(this.layer.minzoom, this.layer.maxzoom);
      // if (data) {
      //   this.initLayer(data, beforeId);
      //   return;
      // }
      // 如果在source缓存中找不到数据,则说明数据加载还未完成，监听source.data方法拿到数据
      // 之所以一开始不监听source.data 是因为用户可能先map.addSource 数据下载完成后才map.addLayer
      (this.sourceCache.getSourceCache(this.layerSourceId) as Source).over(
        'source.data',
        (e) => {
          if (e?.sourceId !== this.layerSourceId) return; // 任何一个数据源加载完毕都会触发source.data 只有图层绑定的数据源才执行后续步骤
          const data = (
            this.sourceCache.getSourceCache(this.layerSourceId) as Source
          ).getData();
          this.initLayer(data, beforeId);
        },
      );
    }
  }
  initLayer(
    data: GeoJSON.FeatureCollection | any[] | undefined,
    beforeId: string | undefined,
  ) {
    //  throw new Error('[base just layer]: initLayer require implemnent');
  }

  /**
   * 添加图层
   * @param beforeId beforeId
   */
  add(beforeId: string | undefined) {
    this.beforeId = beforeId;
    this.init(beforeId);
  }
  remove() {
    if (this.map.getLayer(this.layer.id)) this.map.removeLayer(this.layer.id);
    // if (this.map.getSource(this.layer.id)) this.map.removeSource(this.layer.id);
  }
  move(beforeId: string | undefined) {
    this.map.moveLayer(this.layer.id, beforeId);
  }
  setLayerZoomRange(minZoom: number, maxZoom: number) {
    this.map.setLayerZoomRange(this.layer.id, minZoom || 0, maxZoom || 24);
  }
}
