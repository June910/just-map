import { BaseJustLayer, JustLayer } from '../index';
// import Event from '@/events';
// import SourceCache from '@/sources/source_cache';
import { HeatmapPaint, HeatmapLayout } from 'mapbox-gl';
import { isJustUrl } from '@/utils';

class HeatMapLayer extends BaseJustLayer implements JustLayer {
  // map:mapboxgl.Map;
  // layer:mapboxgl.Layer;
  // sourceCache:SourceCache;
  // layerSourceId:string;
  // constructor(map:mapboxgl.Map,layerOption:mapboxgl.Layer,sourceCache:SourceCache){
  //   super();
  //   this.map = map;
  //   this.layer = layerOption;
  //   this.sourceCache = sourceCache;
  //   this.layerSourceId = typeof this.layer.source==='string'?this.layer.source:this.layer.id;
  // }
  // init(beforeId:string|undefined){
  //   if(this.sourceCache.getSourceCache(this.layerSourceId)){
  //     const data = this.sourceCache.getSourceCache(this.layerSourceId).getData();
  //     if(data){
  //       this.initLayer(data,beforeId);
  //       return;
  //     }
  //   // 如果在source缓存中找不到数据,则说明数据加载还未完成，监听source.data方法拿到数据
  //   // 之所以一开始不监听source.data 是因为用户可能先map.addSource 数据下载完成后才map.addLayer
  //     this.sourceCache.getSourceCache(this.layerSourceId).on('source.data',(e)=>{
  //       const data = this.sourceCache.getSourceCache(this.layerSourceId).getData();
  //       this.initLayer(data,beforeId);
  //     });
  //   }
  // }
  initLayer(data: GeoJSON.FeatureCollection, beforeId: string | undefined) {
    // let newData: any = data;
    // const originData = this.sourceCache
    //   .getSourceCache(this.layerSourceId)
    //   .getOriginData(); // 获取用户传入的原始数据
    // if (typeof originData === 'string' && isJustUrl(originData)) {
    //   newData = { type: 'FeatureCollection', features: data };
    // }
    if (this.map.getLayer(this.layer.id)) return;
    this.map.addLayer(
      {
        ...this.layer,
        type: 'heatmap',
        source: this.layerSourceId,
      },
      beforeId,
      //这里的beforeId 由于每个图层source.data触发的时间不一定，先触发的会先执行添加图层，后触发的会后执行添加图层，
      //导致map中可能还不存在beforeId这个图层。所以应该是先按照图层顺序添加进来 然后再更改source（beforeId不存在，不知为何mapboxgl在这里不报错）
    );
  }
  // add(beforeId:string|undefined){
  //   this.init(beforeId);
  // }
  // remove(){
  //   if(this.map.getLayer(this.layer.id)) this.map.removeLayer(this.layer.id);
  //   if(this.map.getSource(this.layer.id)) this.map.removeSource(this.layer.id);
  // }
  // move(beforeId:string|undefined){
  //   this.map.moveLayer(this.layer.id,beforeId);
  // }
  setProperty(options: any) {
    const { paint, layout } = options;
    paint &&
      Object.keys(paint as HeatmapPaint).forEach((k) => {
        this.map.setPaintProperty(this.layer.id, k, paint[k]);
      });
    layout &&
      Object.keys(layout as HeatmapLayout).forEach((k) => {
        this.map.setLayoutProperty(this.layer.id, k, layout[k]);
      });
  }
}
export default HeatMapLayer;
