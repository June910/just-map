import Color from 'color';
import { log } from 'src/utils/logger';

export interface MappingKeysProps {
  customKey: string; // 用户直接使用的key
  layerKey: string; // 传入到图层的key
  expressionEnable: boolean; // 是否可以是表达式
  propertyType: 'paint' | 'layout'; //是layout属性还是paint属性
  isColor?: boolean;
}

const OPERATOR_CHARACTER = ['+', '-', '*', '/'];

function validateGetValueExpression(expression: any) {
  if (!Array.isArray(expression)) {
    return false;
  }
  if (expression.length !== 2 || expression[0] !== 'get') {
    return false;
  }
  return true;
}
function validateCaseValueExpression(expression: any) {
  if (!Array.isArray(expression)) {
    return false;
  }
  if (expression.length < 4 || expression[0] !== 'case') {
    return false;
  }
  return true;
}
function validateOperateValueExpression(expression: any) {
  if (!Array.isArray(expression)) {
    return false;
  }
  if (expression.length !== 3 || !OPERATOR_CHARACTER.includes(expression[0])) {
    return false;
  }
  return true;
}
function hanldeColorTransform(color: any) {
  try {
    const colorArr = Color(color).rgb().array();
    const alpha =
      colorArr[3] === undefined ? 255 : Math.ceil(colorArr[3] * 255);
    return [colorArr[0], colorArr[1], colorArr[2], alpha];
  } catch (error) {
    log.error(error);
    return [0, 0, 0];
  }
  // 支持以下几种颜色
  // "line-color": "#ff0",
  // "line-color": "#ffff00",
  // "line-color": "rgb(255, 255, 0)",
  // "line-color": "rgba(255, 255, 0, 1)",
  // "line-color": "hsl(100, 50%, 50%)",
  // "line-color": "hsla(100, 50%, 50%, 1)",
  // "line-color": "yellow"
}

function getExpression(expression: any, isColor: boolean = false) {
  const propertyValue = expression[1];
  return (d: any) => {
    // 增加对properties的key值存在与否进行判断
    if (d.properties && d.properties[propertyValue] === undefined) {
      throw new Error(`当前数据中,${propertyValue}属性值不存在`);
    }
    return isColor
      ? hanldeColorTransform(d.properties[propertyValue])
      : d.properties[propertyValue];
  };
}
function operateExpression(expression: any) {
  const operateCharacter = expression[0];
  const firstValue = (d: any) =>
    validateGetValueExpression(expression[1])
      ? d.properties[expression[1][1]]
      : expression[1];
  const secondValue = (d: any) =>
    validateGetValueExpression(expression[2])
      ? d.properties[expression[2][1]]
      : expression[2];
  return (d: any) =>
    eval(`${firstValue(d)}${operateCharacter}${secondValue(d)}`);
}
function caseExpression(expression: any, isColor: boolean = false) {
  const filterExpressionLength = expression.length / 2 - 1; //条件表达式个数   表达式长度为8  表达式为索引 1,3,5 表达式个数是3
  return (d: any) => {
    for (let i = 0; i < filterExpressionLength; i++) {
      if (compareExpression(expression[2 * i + 1], d)) {
        return isColor
          ? hanldeColorTransform(expression[2 * i + 2])
          : expression[2 * i + 2];
      }
    }
    return isColor
      ? hanldeColorTransform(expression.slice(-1)[0])
      : expression.slice(-1)[0];
  };
}
function compareExpression(expression: any, d: any) {
  const operateCharacter = expression[0];
  const firstValue = (d1: any) =>
    validateGetValueExpression(expression[1])
      ? d1.properties[expression[1][1]]
      : expression[1];
  const secondValue = (d1: any) =>
    validateGetValueExpression(expression[2])
      ? d1.properties[expression[2][1]]
      : expression[2];

  return eval(`${firstValue(d)}${operateCharacter}${secondValue(d)}`);
}

function transformExpression(expression: any, isColor: boolean = false) {
  if (isExpression(expression)) {
    // 是表达式
    const op = expression[0];
    switch (op) {
      case 'get':
        if (!validateGetValueExpression(expression)) {
          log.error('expression is not valid');
          return;
        }
        return getExpression(expression, isColor);
        break;
      case 'case':
        if (!validateCaseValueExpression(op))
          return caseExpression(expression, isColor);
        break;
      case '-':
      case '+':
      case '*':
      case '/':
        if (!validateOperateValueExpression(expression)) {
          log.error('expression is not valid');
          return;
        }
        return operateExpression(expression);
        break;
      default:
        log.error('expression is not valid');
        break;
    }
    return;
  }
  if (expression === 'visibility') {
    return true;
  }
  if (expression === 'none') {
    return false;
  }
  return isColor ? hanldeColorTransform(expression) : expression;
  // 500
  // ["get","temperature"]===> d=>d.temperature
  // ["-",100,["get","temperature"]] ===>d=>100-d.terperature
  //  ["case",
  //   ["==",["get","temaperature"],100],
  //   value1,
  //   ["==",["get","temaperature"],200],
  //   value2,
  //   ["==",["get","temaperature"],300],
  //   value3,
  //   defaultValue
  //  ]
}
function getVisible(layout: any) {
  return layout && layout.visibility
    ? layout.visibility === 'none'
      ? false
      : true
    : true;
}
function isExpression(expression: any) {
  if (!Array.isArray(expression)) {
    return false;
  }
  if (
    !(
      OPERATOR_CHARACTER.includes(expression[0]) ||
      ['get', 'case'].includes(expression[0])
    )
  ) {
    return false;
  }
  return true;
}
export { transformExpression, getVisible, isExpression };
