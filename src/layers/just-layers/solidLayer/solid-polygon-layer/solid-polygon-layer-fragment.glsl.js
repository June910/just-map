// Copyright (c) 2015 - 2017 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

export default `\
#define SHADER_NAME solid-polygon-layer-fragment-shader
#define IS_SIDE_VERTEX
precision highp float;

varying vec2 topUv;
varying vec4 vColor;
varying float isValid;
varying vec2 vUv;
varying float isSide;
varying vec2 unitPosition;
uniform sampler2D textureImg;
uniform bool isTexture;
varying vec3 worldPosition;
uniform float opacity;
varying vec3 vCameraPosition;
uniform bool extruded;
varying vec3 vNormal;

void main(void) {
  geometry.uv = vUv;
  if (isValid < 0.5) {
    discard;
  }

  vec4 textureColors;
  vec4 resultColors;
  if(isSide==1.0){
    textureColors = texture2D(textureImg, vec2(vUv.y, 1.0-vUv.x));
  }else{
    textureColors = texture2D(textureImg, vec2(topUv.x, topUv.y));
  }

  if(isTexture){
    if (extruded) {
      vec3 lightColor = lighting_getLightColor(textureColors.rgb, vCameraPosition, worldPosition, vNormal);
      resultColors = vec4(lightColor, textureColors.a * opacity);
    } else {
      resultColors = vec4(textureColors.rgb, textureColors.a * opacity);
    }
  }else{
    if (extruded) {
      vec3 lightColor = lighting_getLightColor(vColor.rgb, vCameraPosition, worldPosition, vNormal);
      resultColors = vec4(lightColor, vColor.a * opacity);
    } else {
      resultColors = vec4(vColor.rgb, vColor.a * opacity);
    }
    if(isSide==1.0){
      resultColors = vec4(resultColors.xyz * (vUv.x*0.5+0.5),resultColors.a);
    }
  }
  gl_FragColor = resultColors;
  
    DECKGL_FILTER_COLOR(gl_FragColor, geometry);
}
`;
