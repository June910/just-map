import { HeatmapPaint, HeatmapLayout } from 'mapbox-gl';
//@ts-ignore
import { MapboxLayer } from '@deck.gl/mapbox';
import { BaseJustLayer, JustLayer } from '../index';
import { log } from 'src/utils/logger';
import SolidLayer from './solid-polygon-layer/solid-polygon-layer';
import { clone } from '@/utils';
import {
  transformExpression,
  MappingKeysProps,
  getVisible,
  isExpression,
} from '../util';

const mappingKeys: MappingKeysProps[] = [
  {
    customKey: 'elevation-scale',
    layerKey: 'elevationScale',
    expressionEnable: false,
    propertyType: 'paint',
  },
  {
    customKey: 'elevation',
    layerKey: 'getElevation',
    expressionEnable: true,
    propertyType: 'paint',
  },
  {
    customKey: 'side-color',
    layerKey: 'getSideFillColor',
    expressionEnable: true,
    propertyType: 'paint',
    isColor: true,
  },
  {
    customKey: 'top-color',
    layerKey: 'getTopFillColor',
    expressionEnable: true,
    propertyType: 'paint',
    isColor: true,
  },
  {
    customKey: 'side-pattern',
    layerKey: 'sideImage',
    expressionEnable: false,
    propertyType: 'paint',
  },
  {
    customKey: 'top-pattern',
    layerKey: 'topImage',
    expressionEnable: false,
    propertyType: 'paint',
  },
  {
    customKey: 'material',
    layerKey: 'material',
    expressionEnable: false,
    propertyType: 'paint',
  },
  {
    customKey: 'opacity',
    layerKey: 'opacity',
    expressionEnable: false,
    propertyType: 'paint',
  },
  {
    customKey: 'visibility',
    layerKey: 'visible',
    expressionEnable: false,
    propertyType: 'layout',
  },
];

type polygonCoordinate =
  | [number, number][]
  | [[number, number][], [number, number][]];
interface PolygonData {
  properties: any;
  polygon: polygonCoordinate;
}
function transformData(features: GeoJSON.Feature[]) {
  const featuresList = clone(features);
  if (!Array.isArray(featuresList)) {
    return [];
  }
  const dataList: PolygonData[] = [];
  const polygonFeatures = featuresList.filter(
    (f) => f.geometry.type === 'MultiPolygon' || f.geometry.type === 'Polygon',
  );
  polygonFeatures.forEach((polygonFeature) => {
    const d: PolygonData[] = generatePolygonData(polygonFeature);
    dataList.push(...d);
  });
  return dataList;
}
function generatePolygonData(polygonFeature: GeoJSON.Feature): PolygonData[] {
  const copyPolygonFeature = clone(polygonFeature);
  let d: PolygonData[] = [];
  //@ts-ignore
  const {
    geometry: { type, coordinates },
    properties,
  } = copyPolygonFeature;
  switch (type) {
    case 'Polygon':
      d = [
        {
          polygon: coordinates[0],
          properties,
        },
      ];
      return d;
      break;
    case 'MultiPolygon':
      coordinates.forEach((coor: any, i: number) => {
        d.push({
          polygon: coor[0],
          properties: { ...properties },
        });
      });
      return d;
      break;
    default:
      return [];
      break;
  }
}
function transformStyle(
  paintOrLayout: { [key: string]: any } | undefined,
  propertyType: 'paint' | 'layout',
) {
  const newObj: { [key: string]: any } = {};
  if (!paintOrLayout) return {};
  Object.keys(paintOrLayout).forEach((k) => {
    const mapKeyObj = mappingKeys.find((mK) => mK.customKey === k);
    if (!mapKeyObj) {
      log.error(`property ${k} is not valid`);
      return;
    }
    if (mapKeyObj.propertyType !== propertyType) {
      log.error(`unkonwn property ${k} in ${mapKeyObj.propertyType}`);
      return;
    }
    if (isExpression(paintOrLayout[k]) && !mapKeyObj.expressionEnable) {
      log.error(`property ${k} value can't be an expression`);
      return;
    }
    const isColorValue = mapKeyObj.isColor;
    const newKey = mapKeyObj.layerKey;
    newObj[newKey] = transformExpression(paintOrLayout[k], isColorValue);
  });
  return newObj;
}

class SolidPolygonLayer extends BaseJustLayer implements JustLayer {
  solidPolygonLayer: any = undefined;

  initLayer(data: GeoJSON.FeatureCollection, beforeId: string | undefined) {
    if (this.map.getLayer(this.layer.id)) {
      this.map.removeLayer(this.layer.id);
    }
    const features = data?.features || [];
    const solidData = transformData(features);
    const { paint, layout } = this.layer;
    this.solidPolygonLayer = new MapboxLayer({
      id: this.layer.id,
      type: SolidLayer,
      data: solidData,
      extruded: true,
      getPolygon: (d: any) => d.polygon,
      ...transformStyle(paint, 'paint'),
      ...transformStyle(layout, 'layout'),
    });
    this.map.addLayer(this.solidPolygonLayer, beforeId);
  }

  setProperty(options: any) {
    const { paint, layout } = options;
    const paintExprssion = transformStyle(paint, 'paint');
    const layoutExpression = transformStyle(layout, 'layout');
    paint &&
      Object.keys(paintExprssion as HeatmapPaint).forEach((k) => {
        this.solidPolygonLayer.setProps({
          [k]: paintExprssion[k],
          updateTriggers: {
            [k]: [Math.random()],
          },
        });
      });
    layout &&
      Object.keys(layoutExpression as HeatmapLayout).forEach((k) => {
        if (k === 'visible') {
          this.solidPolygonLayer.setProps({
            visible: getVisible(layout),
          });
          return;
        }
        this.solidPolygonLayer.setProps({
          k: layoutExpression[k],
        });
      });
  }
}
export default SolidPolygonLayer;
