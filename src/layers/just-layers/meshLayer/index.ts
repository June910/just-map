//@ts-ignore
import { SimpleMeshLayer, ScenegraphLayer } from '@deck.gl/mesh-layers';
//@ts-ignore
import { MapboxLayer } from '@deck.gl/mapbox';
//@ts-ignore
import { OBJLoader } from '@loaders.gl/obj';
//@ts-ignore
import { GLTFLoader } from '@loaders.gl/gltf';
import { clone, getJustUrl, isJustSourceUrl } from '@/utils';
import { log } from 'src/utils/logger';
import {
  transformExpression,
  MappingKeysProps,
  getVisible,
  isExpression,
} from '../util';
import { BaseJustLayer, JustLayer } from '../index';
//@ts-ignore
import { registerLoaders } from '@loaders.gl/core';
registerLoaders([OBJLoader, GLTFLoader]);

type MESH_TYPE = 'gltf' | 'obj';

const mappingKeys: MappingKeysProps[] = [
  {
    customKey: 'model-global-scale',
    layerKey: 'sizeScale',
    expressionEnable: false,
    propertyType: 'paint',
  },
  {
    customKey: 'model-color',
    layerKey: 'getColor',
    expressionEnable: true,
    propertyType: 'paint',
    isColor: true,
  },
  {
    customKey: 'model-file',
    layerKey: 'mesh',
    expressionEnable: false,
    propertyType: 'paint',
  },
  {
    customKey: 'model-file',
    layerKey: 'scenegraph',
    expressionEnable: false,
    propertyType: 'paint',
  },
  {
    customKey: 'model-orientation',
    layerKey: 'getOrientation',
    expressionEnable: true,
    propertyType: 'paint',
  },
  {
    customKey: 'model-scale',
    layerKey: 'getScale',
    expressionEnable: true,
    propertyType: 'paint',
  },
  {
    customKey: 'model-translation',
    layerKey: 'getTranslation',
    expressionEnable: false,
    propertyType: 'paint',
  },
  {
    customKey: 'opacity',
    layerKey: 'opacity',
    expressionEnable: false,
    propertyType: 'paint',
  },
  {
    customKey: 'visibility',
    layerKey: 'visible',
    expressionEnable: false,
    propertyType: 'layout',
  },
];
function transformStyle(
  paintOrLayout: { [key: string]: any } | undefined,
  propertyType: 'paint' | 'layout',
  baseUrl?: string,
) {
  const newObj: { [key: string]: any } = {};
  if (!paintOrLayout) return {};
  Object.keys(paintOrLayout).forEach((k) => {
    const mapKeyObj = mappingKeys.find((mK) => mK.customKey === k);
    if (!mapKeyObj) {
      log.error(`property ${k} is not valid`);
      return;
    }
    if (mapKeyObj.propertyType !== propertyType) {
      log.error(`unkonwn property ${k} in ${mapKeyObj.propertyType}`);
      return;
    }
    if (isExpression(paintOrLayout[k]) && !mapKeyObj.expressionEnable) {
      log.error(`property ${k} value can't be an expression`);
      return;
    }
    const isColorValue = mapKeyObj.isColor;
    const newKey = mapKeyObj.layerKey;
    if (newKey === 'mesh') {
      newObj['scenegraph'] = transformExpression(
        isJustSourceUrl(paintOrLayout[k])
          ? `${baseUrl}${getJustUrl(paintOrLayout[k])}`
          : paintOrLayout[k],
        isColorValue,
      );
    }
    newObj[newKey] = transformExpression(paintOrLayout[k], isColorValue);
  });
  return newObj;
}

interface PointData {
  properties: any;
  coordinates: [number, number];
}

function transformData(features: GeoJSON.Feature[]) {
  const featuresList = clone(features);
  if (!Array.isArray(featuresList)) {
    return [];
  }
  const dataList: PointData[] = [];
  const pointFeatures = featuresList.filter(
    (f) => f.geometry.type === 'MultiPoint' || f.geometry.type === 'Point',
  );
  pointFeatures.forEach((pointFeature) => {
    const d: PointData[] = generatePointData(pointFeature);
    dataList.push(...d);
  });
  return dataList;
}
function generatePointData(polygonFeature: GeoJSON.Feature): PointData[] {
  const copyPolygonFeature = clone(polygonFeature);
  let d: PointData[] = [];
  //@ts-ignore
  const {
    geometry: { type, coordinates },
    properties,
  } = copyPolygonFeature;
  switch (type) {
    case 'Point':
      d = [
        {
          coordinates,
          properties,
        },
      ];
      return d;
      break;
    case 'MultiPoint':
      coordinates.forEach((coor: any, i: number) => {
        d.push({
          coordinates: coor,
          properties: { ...properties },
        });
      });
      return d;
      break;
    default:
      return [];
      break;
  }
}

class ModelLayer extends BaseJustLayer implements JustLayer {
  meshLayer: any = undefined;
  initLayer(data: GeoJSON.FeatureCollection, beforeId: string | undefined) {
    const features = data?.features || [];
    const pointData = transformData(features);
    if (this.meshLayer) {
      this.setData(pointData);
      return;
    }
    if (this.map.getLayer(this.layer.id)) {
      this.map.removeLayer(this.layer.id);
    }
    const { paint, layout, metadata } = this.layer;
    const meshType: MESH_TYPE = (metadata && metadata['model-type']) || 'obj';
    // this.meshLayer = new MapboxLayer({
    //   id:this.layer.id,
    //   type:SimpleMeshLayer,
    //   data:pointData,
    //   getPosition:(d:PointData)=>d.coordinates,
    //   ...transformStyle(paint,'paint'),
    //   ...transformStyle(layout,'layout'),
    // });
    this.meshLayer =
      meshType === 'obj'
        ? new MapboxLayer({
            id: this.layer.id,
            type: SimpleMeshLayer,
            data: pointData,
            getPosition: (d: PointData) => d.coordinates,
            pickable: true,
            ...transformStyle(paint, 'paint', this.baseUrl),
            ...transformStyle(layout, 'layout'),
          })
        : new MapboxLayer({
            id: this.layer.id,
            type: ScenegraphLayer,
            data: pointData,
            getPosition: (d: PointData) => d.coordinates,
            _animations: {
              '*': { speed: 1 },
            },
            _lighting: 'pbr',
            pickable: true,
            ...transformStyle(paint, 'paint', this.baseUrl),
            ...transformStyle(layout, 'layout'),
          });
    this.map.addLayer(this.meshLayer, beforeId);
  }
  setProperty(options: any) {
    const { paint, layout } = options;
    const paintExprssion = transformStyle(paint, 'paint', this.baseUrl);
    const layoutExpression = transformStyle(layout, 'layout');
    paint &&
      Object.keys(paintExprssion).forEach((k) => {
        this.meshLayer.setProps({
          [k]: paintExprssion[k],
          updateTriggers: {
            [k]: [Math.random()],
          },
        });
      });
    layout &&
      Object.keys(layoutExpression).forEach((k) => {
        if (k === 'visible') {
          this.meshLayer.setProps({
            visible: getVisible(layout),
          });
          return;
        }
        this.meshLayer.setProps({
          k: layoutExpression[k],
        });
      });
  }
  setData(data: PointData[]) {
    this.meshLayer.setProps({
      data,
    });
  }
}
export default ModelLayer;
