export default `
attribute vec3 instancePosition;
attribute vec3 instancePosition64Low;
attribute vec2 aa;
attribute vec3 instanceStartPoint;
attribute vec3 instanceStartPoint64Low;
attribute vec3 instanceEndPoint;
attribute vec3 instanceEndPoint64Low;
attribute vec3 positions;
varying vec2 unitPosition;
attribute float instanceTimestamps;
attribute float instanceNextTimestamps;
attribute float instanceRadius;
varying float instanceNextTime;
varying float instanceTime;
uniform float currentTime;
varying float timeRadio;
varying float outerRadiusPixels;
uniform float radiusMinPixels; 
uniform float radiusMaxPixels;
attribute float instanceTypes;
varying float instanceType;

void main(){
  unitPosition = positions.xy;
  instanceNextTime = instanceNextTimestamps;
  instanceTime = instanceTimestamps;
  instanceType = instanceTypes;
  timeRadio = 1.0 - (instanceNextTimestamps-currentTime)/(instanceNextTimestamps-instanceTimestamps);
  // timeRadio = 1.0 - (1249.883-currentTime)/(1249.883-1191.0);

  vec3 startCommonPoint = project_position(instanceStartPoint);
  vec3 endCommonPoint = project_position(instanceEndPoint);
  vec3 realCommonPoint = mix(startCommonPoint,endCommonPoint,vec3(timeRadio));
  outerRadiusPixels = clamp(
    project_size_to_pixel(instanceRadius),
    radiusMinPixels, radiusMaxPixels
  );

  vec3 offset = positions * project_pixel_size(outerRadiusPixels);
  // vec3 offset = positions * project_size(50000.0);
  gl_Position = project_common_position_to_clipspace(vec4(realCommonPoint,0.0) + vec4(offset,1.0));


  // vec3 prevPosition = mix(instanceStartPoint, instanceEndPoint, 0.0);
  // vec3 prevPosition64Low = mix(instanceStartPoint64Low, instanceEndPoint64Low, 0.0);
  // gl_Position = project_position_to_clipspace(instanceEndPoint,instanceEndPoint64Low, offset);
  // gl_Position = project_position_to_clipspace(instanceStartPoint,instanceStartPoint64Low, offset);

  // gl_Position = project_position_to_clipspace(vec3(instanceStartPoint,0.0),vec3(instanceStartPoint64Low,0.0), offset);
  // gl_Position = project_position_to_clipspace(prevPosition, prevPosition64Low,offset);

  // gl_Position = project_position_to_clipspace(instancePosition,instancePosition64Low, offset);

}
`;
