// import {Deck} from '@deck.gl/core';
import { MapboxLayer } from '@deck.gl/mapbox';

//import { deck } from 'deck.gl';

import PointMoveLayer from './PointMoveLayer';

const defaultProps = {
  // color: { type: 'color', value: [255, 255, 0] },
  // currentTime: { type: 'number', value: 0, min: 0 },
  // getTimestamps: { type: 'accessor', value: null },
  // getPath: { type: 'accessor', value: (object) => object.path },
  // radiusMinPixels: { type: 'number', min: 0, value: 0 }, //  min point radius in pixels
  // radiusMaxPixels: { type: 'number', min: 0, value: Number.MAX_SAFE_INTEGER }, // max point radius in pixels
};
export default class PointMoveMapboxLayer {
  constructor(props) {
    this.data = props.data;
    this.currentTime = props.currentTime;
    this.id = props.id;
    this.color = props.paint.color;
    this.getRadius = props.paint.getRadius;
    this.radiusMinPixels = props.paint.radiusMinPixels;
    this.radiusMaxPixels = props.paint.radiusMaxPixels;
    this.visible = props.layout.visible;
  }
  extractRgbColorNumber2Array(rgbColorString) {
    const rgb = [];
    if (rgbColorString.includes('rgba')) {
      const newRGB = rgbColorString
        .replace('rgba', '')
        .replace('(', '')
        .replace(')', '')
        .split(',');
      rgb.push(parseInt(newRGB[0]));
      rgb.push(parseInt(newRGB[1]));
      rgb.push(parseInt(newRGB[2]));
    } else {
      const newRGB = rgbColorString
        .replace('rgb', '')
        .replace('(', '')
        .replace(')', '')
        .split(',');
      rgb.push(parseInt(newRGB[0]));
      rgb.push(parseInt(newRGB[1]));
      rgb.push(parseInt(newRGB[2]));
    }
    return rgb;
  }
  init() {
    let myDeckLayer = new MapboxLayer({
      id: `${this.id}_movePointLayer`,
      type: PointMoveLayer,
      data: this.data,
      getPath: (d) => d.path,
      getTimestamps: (d) => {
        let times = d.timestamps;
        return times;
      },
      color: this.extractRgbColorNumber2Array(this.color), //.split(',').map((item) => item.replace(/[^0-9]+/g, '')),
      getRadius: this.getRadius,
      currentTime: this.currentTime,
      radiusMinPixels: this.radiusMinPixels,
      radiusMaxPixels: this.radiusMaxPixels,
      visible: this.visible,
    });
    // let myDeckLayer = new MapboxLayer({
    //   type: IconLayer,
    //   id: 'text-layer',
    //   data: [
    //     {
    //       name: '#San Francisco',
    //       coordinates: [116.832804439986035, 35.403417413372438],
    //     },
    //   ],
    //   pickable: true,
    //   // iconAtlas and iconMapping are required
    //   // getIcon: return a string
    //   iconAtlas:
    //     'https://raw.githubusercontent.com/visgl/deck.gl-data/master/website/icon-atlas.png',
    //   iconMapping: {
    //     marker: { x: 0, y: 0, width: 128, height: 128, mask: true },
    //   },
    //   getIcon: (d) => 'marker',

    //   sizeScale: 15,
    //   getPosition: (d) => d.coordinates,
    //   getSize: (d) => 5,
    //   getColor: (d) => [Math.sqrt(d.exits), 140, 0],
    // });
    return myDeckLayer;
  }
}

PointMoveMapboxLayer.layerName = 'PointMoveMapboxLayer';
PointMoveMapboxLayer.defaultProps = defaultProps;
