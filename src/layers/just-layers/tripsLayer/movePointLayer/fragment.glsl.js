export default `
#ifdef GL_ES
precision highp float;
#endif
uniform vec3 color;
varying float timeRadio;
varying float instanceNextTime;
varying float instanceTime;
varying vec2 unitPosition;
varying float instanceType;
uniform float t;
uniform float currentTime;
void main(){
  float middleRadius = 0.5; //不变
  float outerRadius = middleRadius + (1.0-middleRadius)*t;
  float innerRadius = 0.4+(middleRadius-0.45)*t;
  vec4 outerColor = vec4(1.0,0.63,0.8,1.0-t);
  vec4 middleColor =  vec4(1.0,1.0,1.0,1.0);
  vec4 innerColor = vec4(1.0,0.0,0.0,1.0);

  float radius = distance(unitPosition,vec2(0.0));

  if(timeRadio>=0.0 && timeRadio<=1.0 &&instanceNextTime>instanceTime && instanceNextTime>0.0 && instanceType<3.5){
    if(radius>=outerRadius){
      discard;
    }else{
      gl_FragColor = mix(mix(innerColor,middleColor,smoothstep(innerRadius-0.05,innerRadius,radius)),outerColor,smoothstep(middleRadius-0.05,middleRadius,radius));
    }

  }else{
    discard;
  }
}
`;
