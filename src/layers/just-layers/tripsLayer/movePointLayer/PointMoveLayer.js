// Copyright (c) 2015 - 2017 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import { Model, Geometry } from '@luma.gl/core';
import { Layer, project32, picking } from '@deck.gl/core';
import PathTesselator from './path-tesselator';
import GL from '@luma.gl/constants';
import vs from './vertex.glsl';
import fs from './fragment.glsl';

const defaultProps = {
  color: { type: 'color', value: [255, 255, 0] },
  currentTime: { type: 'number', value: 0, min: 0 },
  getTimestamps: { type: 'accessor', value: null },
  getPath: { type: 'accessor', value: (object) => object.path },
  radiusMinPixels: { type: 'number', min: 0, value: 0 }, //  min point radius in pixels
  radiusMaxPixels: { type: 'number', min: 0, value: Number.MAX_SAFE_INTEGER }, // max point radius in pixels
};
const ATTRIBUTE_TRANSITION = {
  enter: (value, chunk) => {
    return chunk.length ? chunk.subarray(chunk.length - value.length) : value;
  },
};

export default class PointMoveLayer extends Layer {
  getShaders() {
    return super.getShaders({ vs, fs, modules: [project32, picking] });
  }
  get wrapLongitude() {
    return false;
  }
  initializeState(params) {
    const { gl } = this.context;
    // super.initializeState(params);
    const attributeManager = this.getAttributeManager();
    attributeManager.addInstanced({
      positions: {
        size: 3,
        // Start filling buffer from 1 vertex in
        // elementOffset: 120,
        vertexOffset: 0,
        type: GL.DOUBLE,
        fp64: this.use64bitPositions(),
        // transition: ATTRIBUTE_TRANSITION,
        accessor: 'getPath',
        // isIndexed:true,
        // divisor:1,
        update: this.calculatePositions,
        // noAlloc:true,
        shaderAttributes: {
          instanceStartPoint: {
            // elementOffset: 0,
            vertexOffset: 0,
            // size:3,
            // divisor:1
            // size:3
            // size:2
          },
          instanceEndPoint: {
            vertexOffset: 1,
          },
          // instanceLeftPoint: {
          //   vertexOffset: 2,
          // },
          // instanceRightPoint: {
          //   vertexOffset: 3,
          // }
        },
      },
      timestamps: {
        size: 1,
        accessor: 'getTimestamps',
        shaderAttributes: {
          instanceTimestamps: {
            vertexOffset: 0,
          },
          instanceNextTimestamps: {
            vertexOffset: 1,
          },
        },
      },
      instanceRadius: {
        size: 1,
        transition: true,
        accessor: 'getRadius',
        defaultValue: 1,
      },
      instanceTypes: {
        size: 1,
        type: GL.UNSIGNED_BYTE,
        update: this.calculateSegmentTypes,
        noAlloc: true,
      },

      // instancePosition:{
      //   size:3,
      //   type:GL.DOUBLE,
      //   accessor:'getPosition',
      //   transition: true,
      //   fp64: this.use64bitPositions(),
      // }
    });
    // attributeManager.addInstanced({
    //   timestamps: {
    //     size: 1,
    //     accessor: 'getTimestamps',
    //     shaderAttributes: {
    //       instanceTimestamps: {
    //         vertexOffset: 0
    //       },
    //       instanceNextTimestamps: {
    //         vertexOffset: 1
    //       }
    //     }
    //   }
    // });
    this.setState({
      pathTesselator: new PathTesselator({
        fp64: this.use64bitPositions(),
      }),
    });
    // this.setState({
    //   model: this._getModel(gl)
    // });
  }
  updateState({ oldProps, props, changeFlags }) {
    super.updateState({ props, oldProps, changeFlags });

    const attributeManager = this.getAttributeManager();

    const geometryChanged =
      changeFlags.dataChanged ||
      (changeFlags.updateTriggersChanged &&
        (changeFlags.updateTriggersChanged.all ||
          changeFlags.updateTriggersChanged.getPath));

    if (geometryChanged) {
      const { pathTesselator } = this.state;
      const buffers = props.data.attributes || {};
      pathTesselator.updateGeometry({
        data: props.data,
        geometryBuffer: buffers.getPath,
        buffers,
        normalize: !props._pathType,
        // normalize: false,
        loop: props._pathType === 'loop',
        // loop: true,
        getGeometry: props.getPath,
        positionFormat: props.positionFormat,
        wrapLongitude: props.wrapLongitude,
        // TODO - move the flag out of the viewport
        resolution: this.context.viewport.resolution,
        dataChanged: changeFlags.dataChanged,
      });

      this.setState({
        numInstances: pathTesselator.instanceCount,
        startIndices: pathTesselator.vertexStarts,
      });
      if (!changeFlags.dataChanged) {
        // Base `layer.updateState` only invalidates all attributes on data change
        // Cover the rest of the scenarios here
        attributeManager.invalidateAll();
      }
    }

    if (changeFlags.extensionsChanged) {
      const { gl } = this.context;
      if (this.state.model) {
        this.state.model.delete();
      }
      this.setState({ model: this._getModel(gl) });
      attributeManager.invalidateAll();
    }
  }

  _getModel(gl) {
    const positions = [-1, -1, 0, 1, -1, 0, 1, 1, 0, -1, 1, 0];
    return new Model(
      gl,
      Object.assign(this.getShaders(), {
        id: this.props.id,
        geometry: new Geometry({
          drawMode: GL.TRIANGLE_FAN,
          vertexCount: 4,
          attributes: {
            positions: { size: 3, value: new Float32Array(positions) },
          },
        }),
        isInstanced: true,
      }),
    );
    // // prettier-ignore
    // const SEGMENT_INDICES = [
    //   // start corner
    //   0, 1, 2,
    //   // body
    //   1, 4, 2,
    //   1, 3, 4,
    //   // end corner
    //   3, 5, 4
    // ];

    // // [0] position on segment - 0: start, 1: end
    // // [1] side of path - -1: left, 0: center (joint), 1: right
    // // prettier-ignore
    // const SEGMENT_POSITIONS = [
    //   // bevel start corner
    //   0, 0,
    //   // start inner corner
    //   0, -1,
    //   // start outer corner
    //   0, 1,
    //   // end inner corner
    //   1, -1,
    //   // end outer corner
    //   1, 1,
    //   // bevel end corner
    //   1, 0
    // ];

    // return new Model(
    //   gl,
    //   Object.assign({}, this.getShaders(), {
    //     id: this.props.id,
    //     geometry: new Geometry({
    //       drawMode: GL.TRIANGLES,
    //       attributes: {
    //         indices: new Uint16Array(SEGMENT_INDICES),
    //         positions: {value: new Float32Array(SEGMENT_POSITIONS), size: 2}
    //       }
    //     }),
    //     isInstanced: true
    //   })
    // );
  }

  draw(params) {
    const duration = 1000;
    const t = (performance.now() % duration) / duration; // 0-1之间移动
    const { color, currentTime, radiusMinPixels, radiusMaxPixels } = this.props;
    params.uniforms = Object.assign({}, params.uniforms, {
      color,
      currentTime,
      t,
      radiusMinPixels,
      radiusMaxPixels,
    });

    super.draw(params);
  }
  calculatePositions(attribute) {
    const { pathTesselator } = this.state;

    attribute.startIndices = pathTesselator.vertexStarts;
    attribute.value = pathTesselator.get('positions');
  }
  calculateSegmentTypes(attribute) {
    const { pathTesselator } = this.state;

    attribute.startIndices = pathTesselator.vertexStarts;
    attribute.value = pathTesselator.get('segmentTypes');
  }
}

PointMoveLayer.layerName = 'PointMoveLayer';
PointMoveLayer.defaultProps = defaultProps;
