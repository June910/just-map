import { MapboxLayer } from '@deck.gl/mapbox';
import { TripsLayer } from '@deck.gl/geo-layers';

const defaultProps = {
  trailLength: { type: 'number', value: 120, min: 0 },
  currentTime: { type: 'number', value: 0, min: 0 },
  getTimestamps: { type: 'accessor', value: null },
};
export default class TripsMapboxLayer {
  constructor(props) {
    this.data = props.data;
    this.currentTime = props.currentTime;
    this.id = props.id;
    this.getColor = props.paint.getColor;
    this.trailLength = props.paint.trailLength;
    this.opacity = props.paint.opacity;
    this.widthMinPixels = props.paint.widthMinPixels;
    this.rounded = props.paint.rounded;
    this.shadowEnabled = props.paint.shadowEnabled;
    this.visible = props.layout.visible;
  }
  extractRgbColorNumber2Array(rgbColorString) {
    const rgb = [];
    if (rgbColorString.includes('rgba')) {
      const newRGB = rgbColorString
        .replace('rgba', '')
        .replace('(', '')
        .replace(')', '')
        .split(',');
      rgb.push(parseInt(newRGB[0]));
      rgb.push(parseInt(newRGB[1]));
      rgb.push(parseInt(newRGB[2]));
    } else {
      const newRGB = rgbColorString
        .replace('rgb', '')
        .replace('(', '')
        .replace(')', '')
        .split(',');
      rgb.push(parseInt(newRGB[0]));
      rgb.push(parseInt(newRGB[1]));
      rgb.push(parseInt(newRGB[2]));
    }
    return rgb;
  }
  init() {
    let trailLength = this.trailLength;
    let myDeckLayer = new MapboxLayer({
      id: `${this.id}_tripsLayer`,
      type: TripsLayer,
      data: this.data,
      getPath: (d) => d.path,
      getTimestamps: (d) => {
        let times = d.timestamps;
        return times;
      },
      getColor: this.extractRgbColorNumber2Array(this.getColor),
      opacity: this.opacity,
      widthMinPixels: this.widthMinPixels,
      rounded: this.rounded,
      trailLength,
      currentTime: this.currentTime,
      shadowEnabled: this.shadowEnabled,
      visible: this.visible,
      pickable: true,
    });
    return myDeckLayer;
  }
}

TripsMapboxLayer.layerName = 'TripsMapboxLayer';
TripsMapboxLayer.defaultProps = defaultProps;
