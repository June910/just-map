import { BaseJustLayer, JustLayer } from '../index';

import TripsMapboxLayer from './trips-layer/trips-layer-mapbox';
import PointMoveMapboxLayer from './movePointLayer/PointMoveLayer-mapbox';

import { isJustUrl } from '@/utils';
import Source from '@/sources';

class TripsLayer extends BaseJustLayer implements JustLayer {
  _animateId = 0;
  _tripsLayer: any = null;
  _pointLayer: any = null;
  _currentTime = 0;
  _animationSpeed = this.layer.paint.animationSpeed;
  _currentYear = new Date().getFullYear();
  _currentMonth = new Date().toLocaleDateString().split('/')[1];
  _currentDay = new Date().toLocaleDateString().split('/')[2];
  _currentHours = new Date().getHours();
  _currentMin = new Date().getMinutes();
  _isChangingData = false;
  _isRemoved = false;
  _isFirstAddData = false;
  _isInit = true; // 标记是否是初始状态 初始状态添加layerId到source中
  _baseTime = new Date(
    `${this._currentYear}-${this._currentMonth}-${this._currentDay} ${this._currentHours}:${this._currentMin}:00`,
  ).getTime();
  _time = 0;

  /**
   * 计算单条路径的path
   * @param {*} trajSeries
   */
  getSinglePath(trajSeries: any, carId: any) {
    let trajSeriesJson = JSON.parse(trajSeries);
    let pathItem = { id: carId, path: [], timestamps: [], realtimes: [] };
    let path: any = [];
    let tmpTimestamps: any = [];
    let realtimes: any = [];
    if (trajSeriesJson.length > 0) {
      let firstTime = parseFloat(trajSeriesJson[0][0]);
      let lastTime = parseFloat(trajSeriesJson[trajSeriesJson.length - 1][0]);
      trajSeriesJson.map((item: any) => {
        let itemPath = [parseFloat(item[1]), parseFloat(item[2])];
        path.push(itemPath);
        let timestamp: any = parseFloat(item[0]).toString();
        tmpTimestamps.push(timestamp - this._baseTime); //当天、当时、分钟
        realtimes.push(timestamp);
      });
      pathItem.path = path;
      pathItem.timestamps = tmpTimestamps;
      pathItem.realtimes = realtimes;
    }
    return pathItem;
  }

  /**
   * 获取一条路径
   * @param {*} paths
   */
  getPaths(paths: any) {
    let pathsArray: any = [];
    paths?.map((item: any) => {
      if (item !== null) {
        let oneRoute = item;
        let trajSeries = oneRoute.trajSeries;
        let tmpPath = this.getSinglePath(trajSeries, item.id);
        //有坐标才push
        if (tmpPath.path.length > 0) {
          //获取真实时间，进行比较
          pathsArray.push(tmpPath);
        }
      }
    });
    return pathsArray;
  }
  getFirsTime(newData: any) {
    let firstTimeList: any = [];
    let lastTimeList2 = [];
    newData.map((item: any) => {
      if (item !== null) {
        let firstTime = item.realtimes[0];
        let unixTimestamp = new Date(firstTime).toLocaleString();
        firstTimeList.push(firstTime);
        lastTimeList2.push(unixTimestamp);
      }
    });
    let firstTime = firstTimeList.sort()[0];
    if (firstTimeList.length === 0) firstTime = this._baseTime;
    return firstTime;
  }
  initLayer(data: [], beforeId: string | undefined) {
    let newData: any = data;
    const sourceCache = this.sourceCache.getSourceCache(
      this.layerSourceId,
    ) as Source;
    this._isInit && sourceCache.addLayerId(this.layer.id);
    this._isInit = false;
    const originData = sourceCache.getOriginData(); // 获取用户传入的原始数据
    if (typeof originData === 'string' && isJustUrl(originData)) {
      newData = this.getPaths(data);
      if (!this._isFirstAddData) {
        if (this.layer.paint.currentTime !== undefined) {
          //如果有currentTime字段
          this._time = this.layer.paint.currentTime - this._baseTime;
          this._currentTime = this.layer.paint.currentTime - this._baseTime;
        } else {
          //如果没有currentTime字段，就进行计算
          this._time = new Date().getTime() - this._baseTime; //this.getFirsTime(newData) - this._baseTime;
          this._currentTime = this._time; //this.getFirsTime(newData) - this._baseTime;
        }
        if (newData.length > 0) {
          this._isFirstAddData = true;
        }
      }
    } else {
      //静态数据
      if (!this._isFirstAddData) {
        //如果自定义数据
        this._time = this.layer.paint.currentTime || 0;
        this._currentTime = this.layer.paint.currentTime || 0;
        this._isFirstAddData = true;
      }
    }
    //添加轨迹头图层
    if (
      !this.map.getLayer(`${this.layer.id}_movePointLayer`) &&
      !this._isRemoved
    ) {
      this._pointLayer = new PointMoveMapboxLayer({
        ...this.layer,
        currentTime: this._currentTime,
        data: newData,
      }).init();
      this.map.addLayer(this._pointLayer);
    } else {
      (this._pointLayer as any)?.setProps({ data: newData });
    }
    setTimeout(() => {
      //添加轨迹图拖尾图层
      if (
        !this.map.getLayer(`${this.layer.id}_tripsLayer`) &&
        !this._isRemoved
      ) {
        this._tripsLayer = new TripsMapboxLayer({
          ...this.layer,
          currentTime: this._currentTime,
          data: newData,
        }).init();
        this.map.addLayer(this._tripsLayer, `${this.layer.id}_movePointLayer`);
      } else {
        (this._tripsLayer as any)?.setProps({ data: newData });
      }
    }, 5000);

    const animate = () => {
      this._time += this._animationSpeed;
      requestAnimationFrame(animate);
      (this._pointLayer as any)?.setProps({ currentTime: this._time });
      (this._tripsLayer as any)?.setProps({ currentTime: this._time });
    };
    this._animateId = requestAnimationFrame(animate);
  }
  remove() {
    this._isRemoved = true;
    if (this.map.getLayer(`${this.layer.id}_movePointLayer`))
      this.map.removeLayer(`${this.layer.id}_movePointLayer`);
    if (this.map.getLayer(`${this.layer.id}_tripsLayer`))
      this.map.removeLayer(`${this.layer.id}_tripsLayer`);
    this._animateId && cancelAnimationFrame(this._animateId);
    const sourceCache = this.sourceCache.getSourceCache(
      this.layerSourceId,
    ) as Source;
    sourceCache.removeLayerId(this.layer.id);
  }
  move(beforeId: string | undefined) {
    if (this.map.getLayer(`${this.layer.id}_tripsLayer`))
      this.map.moveLayer(`${this.layer.id}_tripsLayer`, beforeId);
    if (this.map.getLayer(`${this.layer.id}_movePointLayer`))
      this.map.moveLayer(
        `${this.layer.id}_movePointLayer`,
        `${this.layer.id}_tripsLayer`,
      );
  }
  extractRgbColorNumber2Array(rgbColorString: string) {
    const rgb = [];
    const newRGB = rgbColorString
      .replace('rgb', '')
      .replace('(', '')
      .replace(')', '')
      .split(',');
    rgb.push(parseInt(newRGB[0]));
    rgb.push(parseInt(newRGB[1]));
    rgb.push(parseInt(newRGB[2]));
    return rgb;
  }
  private _getKeyValues(key: any, value: any) {
    const keyMapping: any = {
      'trail-color': 'getColor',
      'trail-length': 'trailLength',
    };
    // let newKey: string;
    // if (keyMapping.hasOwnProperty(key)) {
    //   newKey = keyMapping[key];
    // } else {
    //   newKey = key;
    // }
    let keyValue: any = {};
    if (key === 'getColor' && value.includes('rgba(')) {
      //将rgba转为rgb
      const tmpValueArray = value.replace('rgba', 'rgb').split(',');
      value = this.extractRgbColorNumber2Array(
        `${tmpValueArray[0]},${tmpValueArray[1]},${tmpValueArray[2]})`,
      );
    }
    keyValue[key] = value;
    return keyValue;
  }
  private _getLayoutKeyValues(key: any, value: any) {
    // let newKey: string;
    // if (keyMapping.hasOwnProperty(key)) {
    //   newKey = keyMapping[key];
    // } else {
    //   newKey = key;
    // }
    if (key === 'visibility' && value === 'none') {
      key = 'visible';
      value = false;
    } else if (key === 'visibility' && value === 'visible') {
      key = 'visible';
      value = true;
    }
    let keyValue: any = {};
    keyValue[key] = value;
    return keyValue;
  }

  setProperty(options: any) {
    const { paint, layout } = options;
    paint &&
      Object.keys(paint).forEach((k) => {
        let tripsLayer = this.map.getLayer(`${this.layer.id}_tripsLayer`);
        tripsLayer &&
          (this._tripsLayer as any).setProps(this._getKeyValues(k, paint[k]));
        let movePointLayer = this.map.getLayer(
          `${this.layer.id}_movePointLayer`,
        );
        movePointLayer &&
          (this._pointLayer as any).setProps(
            this._getLayoutKeyValues(k, paint[k]),
          );
      });
    layout &&
      Object.keys(layout).forEach((k) => {
        let tripsLayer = this.map.getLayer(`${this.layer.id}_tripsLayer`);
        if (tripsLayer) {
          (this._tripsLayer as any).setProps(
            this._getLayoutKeyValues(k, layout[k]),
          );
        }

        let movePointLayer = this.map.getLayer(
          `${this.layer.id}_movePointLayer`,
        );
        if (movePointLayer) {
          (this._pointLayer as any).setProps(
            this._getLayoutKeyValues(k, layout[k]),
          );
        }
      });
  }
}
export default TripsLayer;
