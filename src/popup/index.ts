import { JustMap } from 'src/core/index';
import mapboxgl, { LngLat, Anchor, Offset, LngLatLike } from 'mapbox-gl';
// const wrapMethods = ['addTo'];
interface PopupOptions {
  closeButton?: boolean; //右上角关闭按钮
  closeOnClick?: boolean; //点击地图时是否关闭弹窗
  anchor?: Anchor; //弹窗位置的字符
  offset?: Offset; // 表示弹窗偏移量
  className?: string; //添加到弹窗容器的以空格分隔的CSS类名。
  maxWidth?: string;
}
type PopupEventType = 'open' | 'close';

export class Popup {
  popup: mapboxgl.Popup;
  constructor(options: PopupOptions) {
    this.popup = new mapboxgl.Popup(options);
  }
  /**
   * 在地图上添加弹窗
   * @param map 需要添加弹窗的JustMap地图
   * @returns Popup实例
   */
  addTo(map: JustMap) {
    map.map && this.popup.addTo(map.map);
    return this;
  }
  /**
   * 设置好弹窗的锚的地理位置后使弹窗移动过去。替代了 trackPointer() 的效果。
   * @param lngLat 要设置的弹窗的锚的地理位置。
   * @returns Popup实例
   */
  setLngLat(lngLat: LngLatLike) {
    lngLat && this.popup.setLngLat(lngLat);
    return this;
  }
  /**
   * 将弹窗内容设置为以字符串形式提供的 HTML。
   * @param html 用于代表弹窗 HTML 内容的字符串。
   * @returns Popup实例
   */
  setHTML(html: string): Popup {
    this.popup.setHTML(html);
    return this;
  }
  /**
   * 返回弹窗的锚所在地理位置。
   * @returns 弹窗的锚的地理位置。
   */
  getLngLat(): LngLat {
    return this.popup.getLngLat();
  }
  /**
   * 返回弹窗状态
   * @returns true 弹窗如处于弹出状态  false 弹窗处于关闭状态
   */
  isOpen(): boolean {
    return this.popup.isOpen();
  }
  /**
   * 从添加地图中移除弹窗。
   * @returns Popup实例
   */
  remove(): Popup {
    this.popup.remove();
    return this;
  }
  /**
   * 利用弹窗的锚，借助指针设备，追踪屏幕上光标的位置（在触屏模式下隐藏）。替代了 setLngLat 的效果。 在多数情况下，此处closeOnClick 和 closeButton 应设置为 false 。
   * @returns Popup实例
   */
  trackPointer(): Popup {
    this.popup.trackPointer();
    return this;
  }
  /**
   * 返回Popup的HTML元素。
   * @returns 返回Popup的HTML元素
   */
  getElement(): HTMLElement {
    return this.popup.getElement();
  }
  /**
   * 将弹窗的内容设置为一串文本。
   * @param text  弹窗的文本内容
   */
  setText(text: string) {
    this.popup.setText(text);
    return this;
  }
  /**
   * 返回弹窗的最大宽度。
   * @returns 弹窗的最大宽度
   */
  getMaxWidth(): string {
    return this.popup.getMaxWidth();
  }
  /**
   * 设置弹窗的最大宽度。实质上是在设置 CSS 的 max-width属性。 有效值请参考该链接：https://developer.mozilla.org/en-US/docs/Web/CSS/max-width
   * @param maxWidth 用于表示最大宽度值的字符串。
   * @returns Popup实例
   */
  setMaxWidth(maxWidth: string): Popup {
    this.popup.setMaxWidth(maxWidth);
    return this;
  }
  /**
   * 将弹窗内容设置为以 DOM 节点形式提供的元素。
   * @param htmlNode 用于作为弹窗内容的 DOM 节点。
   * @returns Popup实例
   */
  setDOMContent(htmlNode: Node): Popup {
    this.popup.setDOMContent(htmlNode);
    return this;
  }
  on(eventType: PopupEventType, listener: Function) {
    //@ts-ignore
    this.popup?.on(eventType, listener);
  }
  off(eventType: PopupEventType, listener: Function) {
    //@ts-ignore
    this.popup?.off(eventType, listener);
  }
}
// const handler: ProxyHandler<Popup> = {
//   get(target, key: keyof mapboxgl.Popup, value: any) {
//     if (wrapMethods.includes(key)) {
//       console.log(1111,key);
//       return Reflect.get(target.popup, key, value);
//     }
//     console.log(1112,key,target.popup,value);
//     return (xx:any)=>{
//     console.log("🚀 ~ file: index.ts ~ line 31 ~ return ~ xx", xx,target.popup,key)

//       // Reflect.get(target.popup,key,value)(xx);
//       target.popup[key](xx);
//       return new Proxy(target,handler);
//     };
//   },
// };
// export class ProxyPopup {
//   constructor(options: any) {
//     const popup = new Popup(options);
//     return new Proxy(popup, handler) as Popup;
//   }
// }
