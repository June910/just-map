import axios, { AxiosInstance, AxiosResponse } from 'axios';
import { stringify } from 'qs';
import { log } from './logger';

export interface ResponseHandleOptions {
  quiet?: boolean;
  raw?: boolean;
}

const responseHandle = (options: ResponseHandleOptions = {}) => {
  const { quiet, raw } = options;
  // FIXME:
  // @ts-ignore
  return (response: AxiosResponse<T> = {}): T | Promise<T> => {
    if (raw) return response.data;
    const {
      data: { resultCode: code, data, resultMsg: msg },
    } = response;
    if (code === 200) {
      return data;
    }

    !quiet && log.error(msg || '服务器错误');
    return Promise.reject(response.data);
  };
};

export interface RequestOptionsType {
  cache?: boolean;
  withCredentials?: boolean;
}

export interface AxiosInstanceWithIntercpter extends AxiosInstance {
  quiet?: AxiosInstance;
  raw?: AxiosInstance;
}

const getRequestInstance = (
  baseURL?: string,
  options: RequestOptionsType = {},
) => {
  const { cache = false, withCredentials = false, ...opts } = options;
  const option = {
    baseURL,
    withCredentials,
    paramsSerializer: (params: any) =>
      stringify(params, { arrayFormat: 'repeat' }),
    ...opts,
  };
  const instance: AxiosInstanceWithIntercpter = axios.create(option);
  // 添加请求拦截器
  instance.interceptors.request.use(
    (config) => {
      if (cache) {
        if (!config.params) config.params = {};
        config.params._t = Date.now();
        return config;
      }
      return config;
    },
    (error) => Promise.reject(error),
  );

  instance.interceptors.response.use(responseHandle(), (error) => {
    Promise.reject(error);
  });

  instance.quiet = axios.create(option);
  instance.quiet.interceptors.response.use(responseHandle({ quiet: true }));

  // 原始输出，不需要处理返回值
  instance.raw = axios.create(option);
  instance.raw.interceptors.response.use(responseHandle({ raw: true }));

  return instance;
};

export default getRequestInstance;
