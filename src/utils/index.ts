import { getJsonDataByUrl, GetJSONProps } from 'src/api/studio';
import { v4 as uuidv4 } from 'uuid';
import booleanContains from '@turf/boolean-contains';

type UrlObject = {
  protocol: string;
  authority: string;
  path: string;
  params: Array<string>;
};

function getType(o: any) {
  return Object.prototype.toString.call(o).slice(8, -1);
}
export function isString(value?: any): value is string {
  return getType(value) === 'String';
}
export function uuid() {
  return uuidv4().replace(/-/g, '');
}

export async function getJsonData(
  baseUrl: string,
  sdk: string,
  token: string | undefined,
  data: GetJSONProps,
) {
  return getJsonDataByUrl(baseUrl, sdk, token, data);
}

// 非空数组
export function nonEmptyArray(data: any): boolean {
  return data && Array.isArray(data) && data.length > 0;
}

function firstUpperCase(str: string): string {
  return str.toLowerCase().replace(/( |^)[a-z]/g, (l) => l.toUpperCase());
}

/**
 * 驼峰式变量名
 * @param words
 */
export function generateVarible(...words: string[]): string {
  return words
    .map((word, index) => {
      if (index === 0) return word;
      return firstUpperCase(word);
    })
    .join('');
}

export function getEmptyGeojson(): GeoJSON.FeatureCollection {
  return {
    type: 'FeatureCollection',
    features: [],
  };
}

export function checkUrl(url: any) {
  if (!isString(url)) return false;
  // url= 协议://(ftp的登录信息)[IP|域名](:端口号)(/或?请求参数)
  const strRegex =
    '^((https|http|ftp)://)?' + // (https或http或ftp):// 可有可无
    "(([\\w_!~*'()\\.&=+$%-]+: )?[\\w_!~*'()\\.&=+$%-]+@)?" + // ftp的user@  可有可无
    '(([0-9]{1,3}\\.){3}[0-9]{1,3}' + // IP形式的URL- 3位数字.3位数字.3位数字.3位数字
    '|' + // 允许IP和DOMAIN（域名）
    '(localhost)|' + // 匹配localhost
    "([\\w_!~*'()-]+\\.)*" + // 域名- 至少一个[英文或数字_!~*\'()-]加上.
    '\\w+\\.' + // 一级域名 -英文或数字  加上.
    '[a-zA-Z]{1,6})' + // 顶级域名- 1-6位英文
    '(:[0-9]{1,5})?' + // 端口- :80 ,1-5位数字
    '((/?)|' + // url无参数结尾 - 斜杆或这没有
    "(/[\\w_!~*'()\\.;?:@&=+$,%#-]+)+/?)$"; // 请求参数结尾- 英文或数字和[]内的各种字符
  const re = new RegExp(strRegex);
  return re.test(encodeURI(url));
}

/**
 * 获取图层的下一个index, 不存在则返回undefined
 * @param arr
 * @param predicate
 */
export function getNextBy<T>(
  arr: T[],
  predicate: (p: T, i: number) => boolean,
): T | undefined {
  if (!Array.isArray(arr)) return undefined;
  const index = arr.findIndex(predicate);
  // 没找到
  if (index === -1) {
    return undefined;
  }
  // 最后一个
  if (index === arr.length - 1) {
    return undefined;
  }
  return arr[index + 1];
}

/**
 * Create an object by mapping all the values of an existing object while
 * preserving their keys.
 *
 * @private
 */
export function mapObject(input: any, iterator: any) {
  const output: { [key: string]: any } = {};
  Object.keys(input).forEach((key) => {
    output[key] = iterator.call(input, input[key]);
  });
  return output;
}

/**
 * Deeply clones two objects.
 *
 * @private
 */
export function clone(input: any): any {
  if (Array.isArray(input)) {
    return input.map(clone);
  }
  if (typeof input === 'object' && input) {
    return mapObject(input, clone);
  }
  return input;
}

// 在内部请求url中使用
//TODO 后续要和isJustSourceUrl合并
export function isJustUrl(url: string) {
  return (
    (url.indexOf('http:') !== 0 && url.indexOf('https:') !== 0) ||
    /* !(url.indexOf('/') === 0) && !(url.indexOf('./') === 0) */ url.indexOf(
      'just:',
    ) === 0
  );
}

// 在transformRequest中用这个 因为addSource被拦截了，内部请求
export function isJustSourceUrl(url: string) {
  return url.indexOf('just:') === 0;
}

export function getJustUrl(url: string): string {
  const parseUrlObj = parseUrl(url);
  if (!parseUrlObj) return url;
  const { path, params } = parseUrlObj;
  return params.length > 0 ? `${path}?${params.join('&')}` : `${path}`;
}

function parseUrl(url: string): UrlObject {
  const urlRe = /^(\w+):\/\/([^/?]*)(\/[^?]+)?\??(.+)?/;
  const parts = url.match(urlRe);
  if (!parts) {
    throw new Error('Unable to parse URL object');
  }
  return {
    protocol: parts[1],
    authority: parts[2],
    path: parts[3] || '/',
    params: parts[4] ? parts[4].split('&') : [],
  };
}

export function getGeomType(feature: GeoJSON.Feature | GeoJSON.Geometry) {
  if (feature.type === 'Feature') {
    return feature.geometry.type;
  }
  return feature.type;
}
export function multiToSingle(feature: GeoJSON.Feature | GeoJSON.Geometry) {
  if (feature.type === 'Feature') {
    const geometry = feature.geometry;
    const geometryType = geometry.type;
    // @ts-ignore
    const coordinateList = feature.geometry.coordinates;
    return coordinateList.map((coor: any) => ({
      ...feature,
      geometry: {
        type: geometryType.substr(geometryType.indexOf('i') + 1),
        coordinates: coor,
      },
    }));
  }
  // @ts-ignore
  const coordinateList = feature.coordinates;
  const geometryType = feature.type;
  return coordinateList.map((coor: any) => ({
    type: geometryType.substr(geometryType.indexOf('i') + 1),
    coordinates: coor,
  }));
}

export function booleanCompleteContains(
  feature1: GeoJSON.Feature | GeoJSON.Geometry,
  feature2: GeoJSON.Feature | GeoJSON.Geometry,
) {
  if (getGeomType(feature2).includes('Multi')) {
    const feature2List = multiToSingle(feature2);
    for (let i = 0; i < feature2List.length; i++) {
      // @ts-ignore
      if (!booleanContains(feature1, feature2List[i])) {
        return false;
      }
    }
    return true;
  }
  // @ts-ignore
  return booleanContains(feature1, feature2);
}
export function debounce(func: any, wait: number) {
  let timeout: any;
  return function () {
    // @ts-ignore
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const context = this;
    // eslint-disable-next-line prefer-rest-params
    const args = arguments;

    if (timeout) clearTimeout(timeout);

    // const callNow = !timeout;
    timeout = setTimeout(() => {
      func.apply(context, args);
    }, wait);
    // if (callNow) func.apply(context, args);
  };
}
/**
 * 获取just layer类型
 */
export function getJustMapLayerType(layer: mapboxgl.Layer) {
  if (!layer) return '';
  return layer.metadata && layer.metadata['just:type'];
}

export function valueInEnum(val: string, enumVal: any) {
  // eslint-disable-next-line no-restricted-syntax
  for (const k in enumVal) {
    if (enumVal[k] === val) {
      return true;
    }
  }
  return false;
}
