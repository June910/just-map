/* eslint-disable no-console */
export default class Logger {
  error: { (...data: any[]): void; (message?: any, ...optionalParams: any[]): void; };
  info!: { (...data: any[]): void; (message?: any, ...optionalParams: any[]): void; };
  warn!: { (...data: any[]): void; (message?: any, ...optionalParams: any[]): void; };
  constructor(debug: boolean) {
    ['info', 'warn'].forEach((key) => {
      // @ts-ignore
      this[key] = (...args: any) => (debug ? console[key](...args) : '');
    });
    this.error = console.error;
  }
}

export const log = new Logger(process.env.NODE_ENV !== 'Production');
