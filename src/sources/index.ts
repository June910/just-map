import { fetchData } from 'src/api/justMap';
import { log } from 'src/utils/logger';
import Event from 'src/events';
import { getJustUrl, isJustUrl } from 'src/utils';
import { getJustGisServerData } from 'src/api/justMap';

import { JustSourceData } from '@/core';
import mapboxgl from 'mapbox-gl';

class Source extends Event {
  private _data: string | GeoJSON.GeoJSON | any; // 请求的数据 url地址或者obj
  private _sourceType: 'geojson' | 'trips';
  private _options: {} | any; //查询参数
  private _id: string; // source的id
  private _token: string | undefined;
  private _baseUrl: string | undefined;
  private _reqData: any; // 请求返回的数据
  private _loaded: boolean; // 数据是否加载完成
  private _interval: number | undefined; // 轮询间隔
  private _setInterval: any; //轮询请求实例
  private _map: mapboxgl.Map | undefined;
  clusterOption: any; // 聚合数据源参数
  private _sourceLayerIds: string[] = [];
  constructor(
    id: string,
    source: JustSourceData,
    eventedParent: Event,
    baseUrl: string | undefined,
    token: string | undefined,
    map: mapboxgl.Map | undefined,
  ) {
    super();
    this._map = map;
    this._sourceType = source.type;
    this._data = source.data;
    // @ts-ignore
    this._options = source.options;
    this._id = id;
    this.setEventParent(eventedParent);
    this._baseUrl = baseUrl;
    this._token = token;
    this._reqData = undefined;
    this._loaded = false;
    this._interval = source.interval;
    this.clusterOption = {
      cluster: source.cluster,
      clusterMaxZoom: source.clusterMaxZoom || 14,
      clusterRadius: source.clusterRadius || 50,
    };
    this._interval ? this.requestDataInterval() : this.requestData();
    // source.type === 'just:static'
    //   ? this.requestData()
    //   : this.requestDataInterval();
  }

  requestData() {
    if (typeof this._data == 'string') {
      this._getData(this._data, this._interval)
        .then((res) => {
          if (typeof res === 'string') {
            this._reqData = JSON.parse(res);
          } else {
            this._reqData = res;
          }
          this._handleGeoJSONData();
          this._loaded = true;
          this.fire('source.data', { sourceId: this._id });
        })
        .catch((err) => {
          log.error(err);
          this._loaded = true;
          this.fire('source.data', { sourceId: this._id });
        });
    } else {
      this._reqData = this._data;
      this._handleGeoJSONData();
      this._loaded = true;
      this.fire('source.data', { sourceId: this._id });
    }
  }
  requestDataInterval() {
    this.requestData();
    if (this._sourceLayerIds.length !== 0) {
      //只有layer用到这个source 才会轮询
      this._setInterval = setInterval(() => {
        this.requestData();
      }, this._interval);
    }
  }
  /**
   * 设置数据
   * @param id
   * @param data
   */
  setData(data: string | GeoJSON.GeoJSON) {
    this._data = data;
    this.requestData();
  }

  /**
   * 获取数据
   */
  getData() {
    return this._reqData;
  }
  private _getData(url: string, interval: number | undefined) {
    if (!isJustUrl(url)) {
      return fetchData(url);
    }
    //为了取到数据，间隔5分钟
    let time = new Date().getTime() - 300000;
    let options = JSON.parse(
      JSON.stringify({ ...this._options, earliestTime: interval && time }),
    );
    return getJustGisServerData(
      url.indexOf('just') === 0 ? getJustUrl(url) : url,
      this._baseUrl || '',
      options,
      this._token,
    );
    //如果是gisserver接口
    // if (url.includes('gisserver')) {
    //   return getJustGisServerData(
    //     url,
    //     this._baseUrl || '',
    //     this._options,
    //     this._token,
    //   );
    // } else {
    //   return getJustData(url, this._baseUrl || '', this._token);
    // }
  }
  /**
   * 获取用户传入的data
   */
  getOriginData() {
    return this._data;
  }
  /**
   * 移除source时，暂停轮询，清除定时器
   */
  terminal() {
    if (this._setInterval) {
      clearInterval(this._setInterval);
      this._setInterval = null;
    }
  }

  getLoadStatus() {
    return this._loaded;
  }
  getSourceId() {
    return this._id;
  }
  /**
   * 对geojson数据进行处理 统一给每个feature添加just_global_id
   */
  private _handleGeoJSONData() {
    if (this._sourceType === 'geojson') {
      const features = Array.isArray(this._reqData)
        ? this._reqData
        : this._reqData.features;
      const newFeatures = features.map((f: GeoJSON.Feature) => {
        const uuid = Math.ceil(Math.random() * 1000000);
        const featureProperty = { ...f.properties, just_global_id: uuid };
        return { ...f, properties: featureProperty };
      });
      this._reqData = { type: 'FeatureCollection', features: newFeatures };
      //@ts-ignore
      this._map?.getSource(this._id).setData(this._reqData);
    }
  }

  addLayerId(layerId: string) {
    this._sourceLayerIds.push(layerId);
    if (this._interval && !this._setInterval) {
      // 此时如果数据源是轮询，并且已经不轮询了，则继续轮询
      this.requestDataInterval();
    }
  }
  removeLayerId(layerId: string) {
    const index = this._sourceLayerIds.findIndex((id) => id === layerId);
    if (index === -1) return;
    this._sourceLayerIds.splice(index, 1);
    if (this._sourceLayerIds.length === 0) {
      // 如果当前数据源已经不绑定任何图层了，则终止轮询
      this.terminal();
    }
  }
}
export default Source;
