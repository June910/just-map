import Source from './index';
import CustomTileSource from './customTileSource';

class SourceCache {
  // 缓存所有的source，从而通过source.getData拿到数据
  private static _instance: SourceCache | null = null;
  _cacheSource: { [key: string]: Source | CustomTileSource } = {};

  static create() {
    if (!this._instance) {
      return new SourceCache();
    }
    return this._instance;
  }
  addSourceCache(id: string, source: Source | CustomTileSource) {
    this._cacheSource[id] = source;
  }
  removeCacheSource(id: string) {
    //@ts-ignore
    this._cacheSource[id].terminal && this._cacheSource[id].terminal(); // 如果是实时轨迹 需要清除轮询
    //@ts-ignore
    this._cacheSource[id].clear && this._cacheSource[id].clear(); // 清除customTileSource中的资源
    delete this._cacheSource[id];
  }
  getSourceCache(id: string) {
    return this._cacheSource[id];
  }
  static clear() {
    SourceCache._instance = null;
  }
}
export default SourceCache;
