import { TileNumber, TileBoundCoord, LngLatLike } from '.';
import VertexBuffer from '@/layers/customTileLayer/gl/vertex_buffer';
import IndexBuffer from '@/layers/customTileLayer/gl/index_buffer';
import { createLayout } from '@/layers/customTileLayer/util/struct_array';
import RasterBoundsAttributes from '@/layers/customTileLayer/render/program/raster_bounds_attributes';
import { template } from './util';
import mapboxgl from 'mapbox-gl';

// 瓦片
export default class Tile {
  id: string;
  url: string; // 请求的切片地址
  gl: WebGLRenderingContext;
  dataBuffer: VertexBuffer;
  indexBuffer: IndexBuffer;
  texture: WebGLTexture | null;
  isLoad: boolean = false;
  constructor(
    gl: WebGLRenderingContext,
    id: string,
    xyz: TileNumber,
    tileBoundCoords: TileBoundCoord,
    url: string,
    map: mapboxgl.Map,
    onSuccess: Function,
    onError: Function,
  ) {
    this.id = id;
    this.url = template(url, {
      x: xyz.x,
      y: xyz.y,
      z: xyz.z,
    });
    // 切片的四个角的顶点坐标和纹理坐标
    // const topLeft =  mapboxgl.MercatorCoordinate.fromLngLat(tileBoundCoords.topLeft);
    // const bottomLeft =  mapboxgl.MercatorCoordinate.fromLngLat(tileBoundCoords.bottomLeft);
    // const topRight =  mapboxgl.MercatorCoordinate.fromLngLat(tileBoundCoords.topRight);
    // const bottomRight =  mapboxgl.MercatorCoordinate.fromLngLat(tileBoundCoords.bottomRight);
    const topLeft = tileBoundCoords.topLeft;
    const bottomLeft = tileBoundCoords.bottomLeft;
    const topRight = tileBoundCoords.topRight;
    const bottomRight = tileBoundCoords.bottomRight;
    // const vertexData = new Float32Array([
    //     topLeft.x,topLeft.y,0.0,1.0,
    //     bottomLeft.x,bottomLeft.y,0.0,0.0,
    //     topRight.x,topRight.y,1.0,1.0,
    //     bottomRight.x,bottomRight.y,1.0,0.0,
    // ]);
    const vertexData = new Float32Array([
      topLeft.lng,
      topLeft.lat,
      0.0,
      1.0,
      bottomLeft.lng,
      bottomLeft.lat,
      0.0,
      0.0,
      topRight.lng,
      topRight.lat,
      1.0,
      1.0,
      bottomRight.lng,
      bottomRight.lat,
      1.0,
      0.0,
    ]);
    const indexData = new Uint8Array([0, 1, 2, 2, 1, 3]);
    this.dataBuffer = new VertexBuffer(gl, RasterBoundsAttributes, vertexData);
    this.indexBuffer = new IndexBuffer(gl, indexData);
    const img = new Image();
    img.onload = () => {
      onSuccess(this.id);
      this.texture = gl.createTexture();
      if (!this.texture) return;
      gl.bindTexture(gl.TEXTURE_2D, this.texture);
      // 对纹理进行y轴翻转
      gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, img);
      this.isLoad = true;
      map.triggerRepaint(); //主动让地图重绘
    };
    img.onerror = () => {
      onError(this.id);
    };
    img.crossOrigin = 'anonymous';
    img.src = this.url;
  }
}
