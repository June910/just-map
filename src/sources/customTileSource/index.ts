// 存储瓦片信息,提供给layer
import { JustSourceData, SchemeType } from '@/core';
import {
  lonLatToTileNumbers,
  tileNumbersToLonLat,
  gcj02_To_gps84,
  gps84_To_gcj02,
  bd09_To_gps84,
  gps84_To_bd09,
} from '@/layers/customTileLayer/util/coordConvert';
import Event from 'src/events';
import TransformClassBaidu from '@/layers/customTileLayer/util/transform-class-baidu';
import mapboxgl, { LngLatBounds } from 'mapbox-gl';
import Tile from './Tile';

const MAX_TILE_CACHE_COUNT = 20; // 最大的缓存切片数量

interface CustomTileSourceType extends Omit<JustSourceData, 'data'> {}
export interface TileNumber {
  x: number;
  y: number;
  z: number;
}
export interface TileBoundCoord {
  topLeft: LngLatLike;
  topRight: LngLatLike;
  bottomLeft: LngLatLike;
  bottomRight: LngLatLike;
}
export interface LngLatLike {
  lng: number;
  lat: number;
}

const TILE_SIZE = 512;

export default class CustomTileSource extends Event {
  id: string;
  url: string;
  tileSize: number;
  scheme: SchemeType;
  transformBaidu: TransformClassBaidu;
  cacheTile: { [_: string]: Tile } = {};
  cacheTileCoord: { [_: string]: LngLatLike } = {};
  showTiles: Array<Tile> = [];
  tileCount: number = 0; // 每次update时需要的瓦片数量
  loadedTileIndex: number = 0; // 每次瓦片请求成功或者失败的索引
  load: boolean = false;
  constructor(
    id: string,
    sourceOptions: CustomTileSourceType,
    eventedParent: Event,
  ) {
    super();
    const { tiles, tileSize, scheme } = sourceOptions;
    this.url = tiles ? tiles[0] : '';
    this.tileSize = tileSize || TILE_SIZE;
    this.scheme = scheme || SchemeType.BAIDU;
    this.transformBaidu = new TransformClassBaidu();
    this.setEventParent(eventedParent);
    this.onTileLoadError = this.onTileLoadError.bind(this);
    this.onTileLoadSuccess = this.onTileLoadSuccess.bind(this);
  }

  private _createTileKey(x: number, y: number, z: number) {
    return `${z}/${x}/${y}`;
  }

  //计算两个瓦片编号的距离
  private _tileDistance(tile1: TileNumber, tile2: TileNumber) {
    return Math.sqrt(
      Math.pow(tile1.x - tile2.x, 2) + Math.pow(tile1.y - tile2.y, 2),
    );
  }

  // 瓦片对应的经纬度
  private _addGridCoord(xyz: TileNumber, xPlus: number, yPlus: number) {
    const key = this._createTileKey(xyz.x + xPlus, xyz.y + yPlus, xyz.z);
    if (this.cacheTileCoord[key]) return;
    this.cacheTileCoord[key] =
      this.scheme === SchemeType.BAIDU
        ? bd09_To_gps84(
            this.transformBaidu.pixelToLnglat(
              0,
              0,
              xyz.x + xPlus,
              xyz.y + yPlus,
              xyz.z,
            ),
          )
        : gcj02_To_gps84(
            tileNumbersToLonLat(xyz.x + xPlus, xyz.y + yPlus, xyz.z),
          );
  }
  // 计算当前切片的四个角的经纬度
  private _calculateTileBoundCoord(xyz: TileNumber) {
    return {
      topLeft:
        this.scheme === SchemeType.BAIDU
          ? this.cacheTileCoord[this._createTileKey(xyz.x, xyz.y + 1, xyz.z)]
          : this.cacheTileCoord[this._createTileKey(xyz.x, xyz.y, xyz.z)],
      topRight:
        this.scheme === SchemeType.BAIDU
          ? this.cacheTileCoord[
              this._createTileKey(xyz.x + 1, xyz.y + 1, xyz.z)
            ]
          : this.cacheTileCoord[this._createTileKey(xyz.x + 1, xyz.y, xyz.z)],
      bottomLeft:
        this.scheme === SchemeType.BAIDU
          ? this.cacheTileCoord[this._createTileKey(xyz.x, xyz.y, xyz.z)]
          : this.cacheTileCoord[this._createTileKey(xyz.x, xyz.y + 1, xyz.z)],
      bottomRight:
        this.scheme === SchemeType.BAIDU
          ? this.cacheTileCoord[this._createTileKey(xyz.x + 1, xyz.y, xyz.z)]
          : this.cacheTileCoord[
              this._createTileKey(xyz.x + 1, xyz.y + 1, xyz.z)
            ],
    };
  }
  private _scaleZoom(scale: number) {
    return Math.log(scale) / Math.LN2;
  }
  private _coveringZoomLevel(zoom: number) {
    const z =
      Math.floor(zoom + this._scaleZoom(TILE_SIZE / this.tileSize)) +
      (this.scheme === SchemeType.BAIDU ? 1 : 0);
    return Math.max(0, z);
  }

  // 过滤出需要渲染的瓦片
  coveringTiles(
    bounds: LngLatBounds,
    z: number,
    center: LngLatLike,
  ): Array<TileNumber> {
    const northWest = bounds.getNorthWest();
    const southEast = bounds.getSouthEast();
    const southWest = bounds.getSouthWest();
    const northEast = bounds.getNorthEast();
    const zoom = this._coveringZoomLevel(z);
    let maxTile, minTile, centerTileNumber: TileNumber;
    if (this.scheme === SchemeType.GAODE) {
      const transformNorthWest = gps84_To_gcj02(northWest);
      const transformSouthEast = gps84_To_gcj02(southEast);
      const transformCenter = gps84_To_gcj02(center);
      minTile = lonLatToTileNumbers(
        transformNorthWest.lng,
        transformNorthWest.lat,
        zoom,
      );
      maxTile = lonLatToTileNumbers(
        transformSouthEast.lng,
        transformSouthEast.lat,
        zoom,
      );
      const centerTile = lonLatToTileNumbers(
        transformCenter.lng,
        transformCenter.lat,
        zoom,
      );
      centerTileNumber = {
        x: centerTile[0],
        y: centerTile[1],
        z: zoom,
      };
    } else {
      const transformBDNorthWest = gps84_To_bd09(southWest);
      const transformBDSouthEast = gps84_To_bd09(northEast);
      const transformBDCenter = gps84_To_bd09(center);
      minTile = this.transformBaidu.lnglatToTile(
        transformBDNorthWest.lng,
        transformBDNorthWest.lat,
        zoom,
      );
      maxTile = this.transformBaidu.lnglatToTile(
        transformBDSouthEast.lng,
        transformBDSouthEast.lat,
        zoom,
      );
      const centerBDTile = this.transformBaidu.lnglatToTile(
        transformBDCenter.lng,
        transformBDCenter.lat,
        zoom,
      );
      centerTileNumber = {
        x: centerBDTile[0],
        y: centerBDTile[1],
        z: zoom,
      };
    }
    const currentTileList = [];
    // 四周往外扩张  避免出现边缘缺失情况
    for (let x = minTile[0] - 1; x <= maxTile[0] + 1; x++) {
      for (let y = minTile[1] - 1; y <= maxTile[1] + 1; y++) {
        const xyz = {
          x,
          y,
          z: zoom,
        };
        currentTileList.push(xyz);
        // 瓦片号对应的经纬度
        this._addGridCoord(xyz, 0, 0);
        if (x === maxTile[0] + 1) this._addGridCoord(xyz, 1, 0);
        if (y === maxTile[1] + 1) this._addGridCoord(xyz, 0, 1);
        if (x === maxTile[0] + 1 && y === maxTile[1] + 1)
          this._addGridCoord(xyz, 1, 1);
      }
    }
    currentTileList.sort((a, b) => {
      return (
        this._tileDistance(a, centerTileNumber) -
        this._tileDistance(b, centerTileNumber)
      );
    });
    return currentTileList;
  }

  getLoadStatus() {
    return this.load;
  }
  // 清理缓存和gl资源
  clear() {
    for (let tileKey in this.cacheTile) {
      const tile = this.cacheTile[tileKey];
      tile.dataBuffer.destory();
      tile.indexBuffer.destroy();
    }
    this.showTiles = [];
    this.cacheTile = {};
    this.cacheTileCoord = {};
  }
  onTileLoadSuccess(id: string) {
    this.loadedTileIndex++;
    if (this.loadedTileIndex == this.tileCount) {
      // 说明当前状态下瓦片加载完成
      this.load = true;
      this.fire('source.data', { sourceId: this.id });
    }
  }
  onTileLoadError(id: string) {
    this.loadedTileIndex++;
    if (this.loadedTileIndex == this.tileCount) {
      // 说明当前状态下瓦片加载完成
      this.load = true;
      this.fire('source.data', { sourceId: this.id });
    }
  }

  // 更新瓦片
  updateTile(
    gl: WebGLRenderingContext,
    bounds: LngLatBounds,
    zoom: number,
    center: LngLatLike,
    map: mapboxgl.Map,
  ) {
    this.showTiles = [];
    const currentTiles = this.coveringTiles(bounds, zoom, center);
    this.tileCount = currentTiles.length;
    this.loadedTileIndex = 0;
    this.load = false;
    currentTiles.forEach((t) => {
      const key = this._createTileKey(t.x, t.y, t.z);
      const tileBoundsCoord = this._calculateTileBoundCoord(t);
      if (!this.cacheTile[key]) {
        const tile = new Tile(
          gl,
          key,
          t,
          tileBoundsCoord,
          this.url,
          map,
          this.onTileLoadSuccess,
          this.onTileLoadError,
        );
        this.showTiles.push(tile);
        this.cacheTile[key] = tile;
      } else {
        this.showTiles.push(this.cacheTile[key]);
      }
    });
  }
}
