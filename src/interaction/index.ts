import JustStyle from 'src/mapbox/style';
import { MapLayerEventType } from 'mapbox-gl';
import { log } from 'src/utils/logger';
import _ from 'lodash';
import {
  InteractionEventProps,
  InteractionProps,
  JustMap,
  LayerInteractionOption,
} from '@/core';
export default class InteractionManager {
  map: mapboxgl.Map;
  style: JustStyle;
  justMap: JustMap;
  interactions: { [key: string]: any } = {};
  constructor(map: mapboxgl.Map, style: JustStyle, justMap: JustMap) {
    this.map = map;
    this.style = style;
    this.justMap = justMap;
  }
  /**
   * 初始化所有的interactions
   * @param interactions
   */
  initInteraction(interactions: InteractionProps[]) {
    // interactions=》所有图层的交互
    interactions.forEach((interaction) => {
      const { id: layerId, eventList } = interaction;
      eventList.forEach((e) => {
        const { id: eventId, type, actionOption } = e;
        this.handleInteraction(layerId, eventId, type, actionOption);
      });
    });
  }
  /**
   * 增加交互
   * @param layerId
   * @param eventId
   * @param type
   * @param actionOption
   * @returns
   */
  addInteraction(
    layerId: string,
    eventId: string,
    type: keyof MapLayerEventType | 'load',
    actionOption: LayerInteractionOption,
  ) {
    if (layerId !== 'map') {
      // 地图的交互不需要判断图层是否存在
      // 图层的交互
      const layer = this.style.getLayer(layerId);
      if (!layer) {
        log.error('layer', layerId, 'does not exist');
        return;
      }
    }
    const layerEvent = this.style.getInteraction(layerId, eventId);
    if (layerEvent) {
      log.error('event', eventId, 'has already been exist in layer', layerId);
      return;
    }

    this.style.addInteraction(layerId, eventId, type, actionOption);
    if (type !== 'load') {
      // 如果交互的事件类型是load,说明添加的时候不起作用，而是地图初始化重新渲染才执行
      this.handleInteraction(layerId, eventId, type, actionOption);
    }
  }
  /**
   * 执行具体的交互效果
   * @param layerId
   * @param eventId
   * @param type
   * @param actionOption
   * @returns
   */
  handleInteraction(
    layerId: string,
    eventId: string,
    type: keyof MapLayerEventType | 'load',
    actionOption: LayerInteractionOption,
  ) {
    // 如果是load  是不是可以不区分map上还是layer上
    // 交互事件如果不是load，则map.on绑定回调函数
    // 如果是load 则直接执行回调函数
    const { type: actionType, options } = actionOption;
    if (actionType === 'highlight') {
      this.handleHightLightInteraction(layerId, eventId, type, options);
      return;
    }
    if (actionType === 'popup') {
      this.handlePopupInteraction(layerId, eventId, type, options);
      return;
    }
    if (actionType === 'flyTo') {
      this.handleFlyToInteraction(layerId, eventId, type, options);
    }
  }
  handleHightLightInteraction(
    layerId: string,
    eventId: string,
    type: keyof MapLayerEventType | 'load',
    options: any,
  ) {
    // 执行map.on 找出feature 获取feature.properties.just_global_id
    //    执行:1.改变临时图层样式属性
    //    执行:2.改变临时图层的source
    //    执行:3.改变原始图层filter 选中的要素不显示
    // 只可能是layer 不可能是map   不可能是load
    const layer = options?.actionLayer
      ? this.map
          .getStyle()
          .layers?.find((l) => l.id === `${layerId}-${options.actionLayer}`)
      : this.style.getLayer(layerId);
    if (!layer) return;
    const interactionLayerId = `${layerId}-interaction`;
    this.addTempLayer(layer, interactionLayerId);
    const interactionId = `${layerId}-${eventId}`;
    let globalFeatureId: any = null;
    this.interactions[interactionId] = (e: any) => {
      if (!e) return;
      const bbox = [
        [e.point.x - 5, e.point.y - 5],
        [e.point.x + 5, e.point.y + 5],
      ];
      //@ts-ignore
      const features = this.map?.queryRenderedFeatures(bbox, {
        layers: [layer.id],
      });
      //@ts-ignore
      const interactionFeatures = this.map?.queryRenderedFeatures(bbox, {
        layers: [interactionLayerId],
      });
      if (
        (features && features.length > 0) ||
        (interactionFeatures && interactionFeatures.length > 0)
      ) {
        const selectFeature = features[0] || interactionFeatures[0];
        if (globalFeatureId === selectFeature?.properties?.just_global_id)
          return;
        const { paint, layout } = options;
        paint &&
          Object.keys(paint).forEach((k) => {
            this.map?.setPaintProperty(interactionLayerId, k, paint[k]);
          });
        layout &&
          Object.keys(layout).forEach((k) => {
            this.map?.setPaintProperty(interactionLayerId, k, layout[k]);
          });

        const interactionLayerSourceData = {
          type: 'FeatureCollection',
          features: [selectFeature],
        };
        this.map
          ?.getSource(interactionLayerId)
          //@ts-ignore
          .setData(interactionLayerSourceData);

        // 临时图层 setPaintProperty setLayoutProperty 更改source
        if (
          selectFeature.properties &&
          selectFeature.properties.just_global_id !== undefined
        ) {
          // 执行属性过滤
          //@ts-ignore
          const filter = layer?.filter;
          const newFilter = filter
            ? [
                'all',
                filter,
                [
                  '!=',
                  'just_global_id',
                  selectFeature.properties.just_global_id,
                ],
              ]
            : ['!=', 'just_global_id', selectFeature.properties.just_global_id];
          this.map?.setFilter(layerId, newFilter);
        }
        globalFeatureId = selectFeature?.properties?.just_global_id;
        // feature = selectFeature;
      } else {
        globalFeatureId = null;
        // feature = null;
        //点击图层外面时 1.还原原始图层的过滤 2.清空临时图层的source
        //@ts-ignore
        const filter = layer?.filter;
        this.map?.setFilter(layerId, filter);
        this.map
          ?.getSource(interactionLayerId)
          // @ts-ignore
          .setData({ type: 'FeatureCollection', features: [] });
      }
    };
    if (type == 'load') {
      this.interactions[interactionId]();
      return;
    }
    this.map?.on(type, this.interactions[interactionId]);
  }
  handlePopupInteraction(
    layerId: string,
    eventId: string,
    type: keyof MapLayerEventType | 'load',
    options: any,
  ) {
    const layer = options?.actionLayer
      ? this.map
          .getStyle()
          .layers?.find((l) => l.id === `${layerId}-${options.actionLayer}`)
      : this.style.getLayer(layerId);
    this.justMap.addControl({
      id: 'popupid' + eventId,
      type: 'popup',
      options: {
        ...options,
        layerId: `${layer?.id},${layer?.id}-interaction`,
        lngLat:
          '${ function() { return shareData.map' +
          _.upperFirst(type) +
          'EventData.lngLat } }',
        mapClickEventData:
          '${ function() { return shareData.map' +
          _.upperFirst(type) +
          'EventData } }',
      },
    });
  }
  handleFlyToInteraction(
    layerId: string,
    eventId: string,
    type: keyof MapLayerEventType | 'load',
    options: any,
  ) {
    const interactionId = `${layerId}-${eventId}`;
    this.interactions[interactionId] = (e: any) => {
      if (!e) {
        //load事件  不管是layer还是map  都会自动飞入
        const { center, zoom = 10 } = options;
        this.map?.flyTo({
          center,
          zoom,
        });
      } else {
        // 非load事件  区分是map还是layer
        const { lng, lat } = e.lngLat;
        const { zoom } = options;
        if (layerId === 'map') {
          // 绑定在map上的事件
          this.map?.flyTo({
            center: [lng, lat],
            zoom,
          });
          return;
        }
        // 绑定在layer上
        const bbox = [
          [e.point.x - 5, e.point.y - 5],
          [e.point.x + 5, e.point.y + 5],
        ];
        //@ts-ignore
        const features = this.map?.queryRenderedFeatures(bbox, {
          layers: [layerId],
        });
        if (features && features.length > 0) {
          this.map?.flyTo({
            center: [lng, lat],
            zoom,
          });
        }
      }
    };
    if (type == 'load') {
      this.interactions[interactionId]();
      return;
    }
    this.map?.on(type, this.interactions[interactionId]);
  }
  removeInteraction(layerId: string, eventId: string) {
    const layerEvent = this.style.getInteraction(layerId, eventId);
    if (!layerEvent) {
      log.error('event ', eventId, ' not exist in layer ', layerId);
      return;
    }
    this.style.removeInteraction(layerId, eventId);
    this.handleRemoveInteraction(layerId, eventId, layerEvent);
  }
  handleRemoveInteraction(
    layerId: string,
    eventId: string,
    layerEvent: InteractionEventProps,
  ) {
    const layer = this.style.getLayer(layerId);
    if (!layer) return;
    const interactionLayerId = `${layerId}-interaction`;
    const type = layerEvent?.type;
    const interactionId = `${layerId}-${eventId}`;
    if (this.interactions[interactionId]) {
      this.map?.off(type, this.interactions[interactionId]);
      delete this.interactions[interactionId];

      if (layerEvent.actionOption.type === 'highlight') {
        // 组装删除后的interactions中的
        const layerEventList = this.style.getInteractionList(layerId);
        const layerHighlightEventList = layerEventList?.filter(
          (e) => e.actionOption.type === 'highlight',
        );
        if (layerHighlightEventList && layerHighlightEventList.length === 0) {
          // 只有一个hightLight事件 可以删除临时图层
          if (this.map?.getLayer(interactionLayerId))
            this.map.removeLayer(interactionLayerId);
          if (this.map?.getSource(interactionLayerId))
            this.map.removeSource(interactionLayerId);
          // 还原之前的过滤
          const layer = this.style.getLayer(layerId);
          const filter = layer?.filter;
          this.map?.setFilter(layerId, filter);
          return;
        }
        if (layerHighlightEventList && layerHighlightEventList.length > 0) {
          if (this.map?.getLayer(interactionLayerId))
            this.map.removeLayer(interactionLayerId);
          if (this.map?.getSource(interactionLayerId))
            this.map.removeSource(interactionLayerId);
          this.addTempLayer(layer, interactionLayerId);
          const hightLightStyle =
            this.style.getLayerHightlightInteractionStyle(layerId);
          const { paint, layout } =
            hightLightStyle || ({} as { [k: string]: any });
          paint &&
            Object.keys(paint).forEach((k) => {
              this.map?.setPaintProperty(interactionLayerId, k, paint[k]);
            });
          layout &&
            Object.keys(layout).forEach((k) => {
              this.map?.setPaintProperty(interactionLayerId, k, layout[k]);
            });
        }
      }
      if (layerEvent.actionOption.type === 'popup') {
        this.justMap.removeControl('popupid' + eventId);
      }
    }
  }
  addTempLayer(layer: mapboxgl.Layer, interactionLayerId: string) {
    if (this.map?.getLayer(interactionLayerId))
      this.map.removeLayer(interactionLayerId);
    if (this.map?.getSource(interactionLayerId))
      this.map.removeSource(interactionLayerId);
    const { id: layerId, type, paint, layout } = layer;
    // 高亮
    const layers = this.map?.getStyle().layers as mapboxgl.Layer[];
    const layerIndex = layers?.findIndex((l) => l.id === layerId);
    const afterLayerId =
      layerIndex === undefined ||
      layerIndex === -1 ||
      layerIndex === layers.length - 1
        ? undefined
        : layers[layerIndex + 1].id;

    if (!this.map?.getLayer(interactionLayerId)) {
      this.map?.addLayer(
        {
          id: interactionLayerId,
          //@ts-ignore
          type: type,
          //@ts-ignore
          paint: paint || {},
          layout: layout || {},
          source: {
            type: 'geojson',
            data: { type: 'FeatureCollection', features: [] },
          },
        },
        afterLayerId,
      );
    }
  }
}
