import mapboxgl from 'mapbox-gl';

// export type CONTROL_TYPE = 'scale'|'navigation'|'geolocate'|'attribution'|'fullscreen';
// 通过简单工厂，把所有的control类通过统一的接口暴漏出去，用户只需要传type和对应的options就可以
// 自定义的control在当前文件夹中新建  添加到controlTypes中
// 自定义control的实现遵循mapbox Icontrol 参考http://www.mapbox.cn/mapbox-gl-js/api/#icontrol
export type CONTROL_TYPE =
  | 'scale'
  | 'navigation'
  | 'geolocate'
  | 'attribution'
  | 'fullscreen'
  | 'jscale'
  | 'poi'
  | 'route'
  | 'fence'
  | 'layergroup'
  | 'popup';
// export enum CONTROL_TYPE {
//   SCALE='scale',
//   NAVIGATION='navigation',
//   GEOLOCATE='geolocate',
//   ATTRIBUTION='attribution',
//   FULLSCREEN='fullscreen',
//   JSCALE='jscale'
// };

// const controlTypes = {
//   scale:mapboxgl.ScaleControl,
//   navigation:mapboxgl.NavigationControl,
//   geolocate:mapboxgl.GeolocateControl,
//   attribution:mapboxgl.AttributionControl,
//   fullscreen:mapboxgl.FullscreenControl
// };
export interface MAPBOX_CONTROL {
  type: CONTROL_TYPE;
  MapboxControl: any;
}
export const MAPBOX_CONTROL_TYPE_LIST: MAPBOX_CONTROL[] = [
  {
    type: 'scale',
    MapboxControl: mapboxgl.ScaleControl,
  },
  {
    type: 'navigation',
    MapboxControl: mapboxgl.NavigationControl,
  },
  {
    type: 'geolocate',
    MapboxControl: mapboxgl.GeolocateControl,
  },
  {
    type: 'attribution',
    MapboxControl: mapboxgl.AttributionControl,
  },
  {
    type: 'fullscreen',
    MapboxControl: mapboxgl.FullscreenControl,
  },
];

class Control {
  // private _type:CONTROL_TYPE;
  // private _options:any;

  // constructor(type:CONTROL_TYPE,options:any){
  //   this._type = type;
  //   this._options = options;
  // }
  static Create(type: CONTROL_TYPE, options: any) {
    const controlItem = MAPBOX_CONTROL_TYPE_LIST.find((mc) => mc.type === type);
    if (controlItem) {
      const control: mapboxgl.IControl | mapboxgl.Control =
        new controlItem.MapboxControl(options);
      return control;
    }
  }
}
export default Control;
