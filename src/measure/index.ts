// const measure = new Measure()
// 1.支持测量面积和长度和点坐标
// 2.长度：显示每两个节点之间的长度 可以删除和移动和新增节点 更新长度
// 3.面积：在面积的中心位置显示面积，可以删除和移动节点和新增节点 更新面积

//问题  1.draw和measure以什么形式提供给开发者 new Draw
//问题  2.darw和measure map绑定的事件是否会冲突

//关键实现节点：1.图形画完不能拖动 需要更改mode
//             2.生成距离量算结果标记 需要更改style文件

import { JustMap, LngLatLike } from '../core';
import { DrawProxy, Draw, DrawMode } from '@/draw';
import {
  draw_point,
  draw_polygon,
  draw_line_string,
  DirectSelect,
  SimpleSelect,
} from './mode';
import {
  createMarkerDom,
  createMarkerContent,
  GeomType,
} from './lib/createMarkerDom';
import styles from './styles';
import { Marker } from 'src/marker';
import { Node, SingleLinkList } from './lib/DoubleNode';
import * as turf from '@turf/turf';

type MeasureType = 'length' | 'area' | 'coordinate';
const MEASURE_DRAW_MAPPING: {
  [key in MeasureType]: DrawMode;
} = {
  coordinate: 'point',
  length: 'line',
  area: 'polygon',
};
// const MARKER_OFFSET = [15, -5];
const MARKER_OFFSET: { [key in GeomType]: [number, number] } = {
  Point: [5, -15],
  LineString: [5, 5],
  Polygon: [0, 0],
  StartInfoMarker: [15, -5],
  StartFeatureMarker: [5, -5],
};

// 需要的图层
// 测量图层
// 点图层
// 1.静态点图层
// 2.选中状态下的点图层
//     1、2 是一个图层 通过状态切换
// 3.经纬度坐标 marker或者symbol

// 线图层
// 1、静态线图层
// 2、选中下的线图层
//    1、2 是同一个图层
// 3、选中状态下的节点
// 4、选中状态下的中心点
// 5、节点选中状态下的点

// 面图层
// 1、静态面图层
// 2、选中状态下的面图层
//     1、2 是同一个图层
// 3、选中状态下的节点
// 4、选中状态下的中心点
// 5、节点选中状态下的点

// 1. 一个点有一个marker 显示坐标  要素marker
//{
//    id:
//    Marker:
//}

//{
//    id:
//    Marker:
//}
//{
//    id:
//    Marker:
//}
// 2. 一个线有多个marker 显示一条线之前所有点的累积长度 要素节点marker

// 3. 一个面有一个marker 显示面积  要素marker

// 4. 提示marker  创建开始过程出现 创建完成消失 id:'justmap-measure-info-marker'
// 还没画点时 单机确认
// 画点后  单机继续/双击完成
// 完成后消失

// measure.beforeCreate 处于十字状态 还未开始画
// 如果时点 则更新infoMarker
// 如果时线或者面 1.更新infoMarker  2.当画第一个点时，仍然继续监听这个事件 新建featureMarker
// measure.create  要素绘画完成
export class Measure {
  _measure: Draw;
  _featureMarker: {
    [key: string]: Node | SingleLinkList | null;
  }; //标识marker 点和面是Node 线是DoubleLinkList key标识要素id
  _infoMarker: Node; // 只能同时显示一个这样的marker
  _map: JustMap;
  constructor() {
    //@ts-ignore
    this._measure = new DrawProxy(styles, {
      draw_point,
      draw_polygon,
      draw_line_string,
      direct_select: DirectSelect,
      simple_select: SimpleSelect,
      // new Layer
    });
    this._featureMarker = {};
    this._createMarker = this._createMarker.bind(this);
    this._beforeCreateAction = this._beforeCreateAction.bind(this);
    this._afterCreateAction = this._afterCreateAction.bind(this);
    this._updateMovingMarker = this._updateMovingMarker.bind(this);
    this._createLineVertex = this._createLineVertex.bind(this);
    this._deleteFeature = this._deleteFeature.bind(this);
  }
  addTo(map: JustMap) {
    this._measure.addTo(map);
    this._map = map;
    this._measure.on('draw.delete', this._deleteFeature); // 删除要素
    //@ts-ignore
    this._measure.on('measure.create', this._afterCreateAction); // 画完之后
    //触发条件 由direct_select
    // onMidPoint 点击线的中间节点 新建节点
    // drawVertex 拖拽节点
    // onTrash 删除节点
    //@ts-ignore
    this._measure.on('measure.update', this._updateMovingMarker); // 画完后移动节点
    return this;
  }
  /**
   * 改变测量类型
   * @param type 测量类型
   */
  changeMode(type: MeasureType) {
    //@ts-ignore
    const drawType = MEASURE_DRAW_MAPPING[type];
    this._measure.changeMode(drawType);
    //@ts-ignore
    this._measure.on('measure.beforecreate', this._beforeCreateAction);
    // 触发条件
    // draw_point的onMouseMove
    // draw_line_string 的onMouseMove onClick
    // draw_polygon 的onMouseMove
    //@ts-ignore
    this._measure.on('measure.create.linevertex', this._createLineVertex);
    //@ts-ignore
  }
  /**
   * 删除所有要素
   */
  deleteAll() {
    this._measure.deleteAll();
    this._deleteAllMarkers();
    return this;
  }

  /**
   * 删除选中的图形或者节点
   * @returns 绘画实例
   */
  trash() {
    // 会触发measure.delete事件
    this._measure.trash();
    return this;
  }
  /**
   * 卸载measure功能
   */
  remove() {
    this._measure.remove();
    this._deleteAllMarkers();
    this._measure.off('draw.delete', this._deleteFeature); // 删除要素
    //@ts-ignore
    this._measure.off('measure.create', this._afterCreateAction); // 画完之后
    //@ts-ignore
    this._measure.off('measure.update', this._updateMovingMarker); // 画完后移动节点
    //@ts-ignore
    this._measure = undefined;
  }
  // 画完之前触发的事件
  // 1.鼠标变成十字 开始编辑
  // 2.鼠标点击和移动 编辑中
  private _beforeCreateAction(obj: any) {
    const { state, e } = obj;
    const { currentVertexPosition, line, polygon } = state;

    if (!this._infoMarker) {
      // 创建this._infoMarker
      this._infoMarker = this._createMarker(
        'info-marker',
        e.lngLat,
        'StartInfoMarker',
      );
    } else {
      this._infoMarker.getMarker().setLngLat(e.lngLat).addTo(this._map);
    }
    if (currentVertexPosition > 0) {
      this._infoMarker.getEl().innerHTML =
        createMarkerContent('StartInfoMarker');
      if (line) {
        // line marker
        const { id, coordinates } = line;
        if (!this._featureMarker[id]) {
          this._featureMarker[id] = new SingleLinkList();
        }
        // const node = this._createMarker(id,);
        // this._featureMarker[id].append(node);
      }
    }

    if (currentVertexPosition > 1) {
      // 只有当画出三个点的时候 才能有面积
      // 只有线和面的时候 才更新_infoMarker的位置和内容
      // 鼠标此时已经按下click
      if (polygon) {
        const { id, coordinates } = polygon;
        const polygonCoordinates = JSON.parse(JSON.stringify(coordinates));

        //@ts-ignore
        polygonCoordinates[0].push(polygonCoordinates[0][0]); //合并第一个点 拼装成一个polygon坐标串

        //@ts-ignore
        const center = turf.centerOfMass(turf.polygon(polygonCoordinates));
        //@ts-ignore
        const area = turf.area(turf.polygon(polygonCoordinates));
        if (!this._featureMarker[id]) {
          this._featureMarker[id] = this._createMarker(
            id,
            //@ts-ignore
            center.geometry.coordinates,
            'Polygon',
            area,
          );
        } else {
          // @ts-ignore
          (this._featureMarker[id] as Node).getEl().innerHTML =
            createMarkerContent('Polygon', area);
          (this._featureMarker[id] as Node)
            .getMarker()
            //@ts-ignore
            .setLngLat(center.geometry.coordinates)
            .addTo(this._map);
        }
      }
    }

    // this._infoMarker.setLngLat(e.lngLat).addTo(this._map);
    // if()
  }
  // 画完之后执行的事件
  private _afterCreateAction(e: any) {
    // 创建后 注销掉创建infoMarker的事件
    //@ts-ignore
    this._measure.off('measure.beforecreate', this._beforeCreateAction);
    //@ts-ignore
    this._measure.off('measure.create.linevertex', this._createLineVertex);
    this._infoMarker.getEl().innerHTML = createMarkerContent('StartInfoMarker');
    this._infoMarker.getMarker().remove();

    // 给要素添加标识marker
    // 点和面直接添加要素marker  线添加到最后一个节点上 使用双向链表的数据结构
    // 如果时点，添加featureMarker
    // 如果时线或者面 则不执行任何动作
    const feature = e.features[0];
    const {
      id,
      geometry: { type, coordinates },
    } = feature;
    if (type !== 'Point') return;
    let markerShowValue = coordinates;
    let positionCoordinate = coordinates;
    this._featureMarker[id] = this._createMarker(
      id,
      positionCoordinate,
      type,
      markerShowValue,
    );
  }
  /**
   * draw.delete 触发要素删除事件
   * @param e
   */
  private _deleteFeature(e: any) {
    const { features } = e;
    const { id } = features[0];
    this._deleteMarker(id);
  }
  /**
   * 删除marker
   * @param id 要素id
   * @returns
   */
  private _deleteMarker(id: string) {
    if (!this._featureMarker[id]) return;
    if ((this._featureMarker[id] as SingleLinkList).length !== undefined) {
      //线 单向链表 删除线的marker
      (this._featureMarker[id] as SingleLinkList).changeLineVertex(
        0,
        (currPosition: number) => {
          const node = (this._featureMarker[id] as SingleLinkList).getNode(
            currPosition,
          );
          node && node.getMarker().remove();
        },
      );
    } else {
      // 点和面
      // 删除点或者面marker
      (this._featureMarker[id] as Node).getMarker().remove();
      this._featureMarker[id] = null;
    }
  }
  // 当编辑节点时  拖动节点 添加节点 删除节点
  private _updateMovingMarker(obj: any) {
    const { features, state, action, e } = obj;
    const feature = features[0];
    const {
      id,
      geometry: { type, coordinates },
    } = feature;
    let showPosition;
    let showValue;
    if (type !== 'LineString') {
      // 如果不是线
      if (type === 'Point') {
        showPosition = coordinates;
        showValue = coordinates;
      }
      if (type === 'Polygon') {
        if (coordinates.length == 0) return; //三个节点删除后 不合理的坐标 内部返回的coordinates为[]
        showPosition = turf.centerOfMass(feature).geometry.coordinates;
        showValue = turf.area(feature);
      }
      this._featureMarker[id] &&
        //@ts-ignore
        this._featureMarker[id].getMarker().setLngLat(showPosition);
      this._featureMarker[id] &&
        //@ts-ignore
        (this._featureMarker[id].getEl().innerHTML = createMarkerContent(
          type,
          showValue,
        ));
    } else {
      // 处理线的情况 和点面相比 特殊的地方在于一个线有多个marker
      if (!state) return;
      const index = Number(state.selectedCoordPaths[0] || -1); //获取节点索引
      if (action === 'change_coordinates') {
        //改变节点
        //@ts-ignore
        const node = this._featureMarker[id].getNode(index);
        node.getMarker().setLngLat(e.lngLat);
        //改变这个节点和这个节点之后所有节点的显示的内容
        (this._featureMarker[id] as SingleLinkList).changeLineVertex(
          index,
          (currPosition: number) => {
            const lineCoordinatesToVertex = coordinates.slice(
              0,
              currPosition + 1,
            );
            if (lineCoordinatesToVertex.length < 2) {
              // 考虑移动的是第一个点 内容不发生变化
              return;
            }
            const lengthToVertex =
              turf.length(turf.lineString(lineCoordinatesToVertex)) * 1000;
            const node = (this._featureMarker[id] as SingleLinkList).getNode(
              currPosition,
            );
            node &&
              (node.getEl().innerHTML = createMarkerContent(
                type,
                lengthToVertex,
              ));
          },
        );
        // node.getEl().innerHTML = ''
      }
      if (action === 'add') {
        // 增加节点
        const lineCoordinatesToNewVertex = coordinates.slice(0, index + 1);
        const lengthToNewVertex =
          turf.length(turf.lineString(lineCoordinatesToNewVertex)) * 1000;
        const newNode = this._createMarker(
          id,
          e.lngLat,
          'LineString',
          lengthToNewVertex,
        );
        (this._featureMarker[id] as SingleLinkList).insert(index, newNode);
      }
      if (action === 'delete') {
        if (coordinates.length === 1) return; // 三个节点删除后 不合理的坐标 内部返回的coordinates为[]
        (this._featureMarker[id] as SingleLinkList)
          .getNode(index)
          ?.getMarker()
          .remove();
        (this._featureMarker[id] as SingleLinkList).removeAt(index);

        (this._featureMarker[id] as SingleLinkList).changeLineVertex(
          index,
          (currPosition: number) => {
            const lineCoordinatesToVertex = coordinates.slice(
              0,
              currPosition + 1,
            );
            if (lineCoordinatesToVertex.length < 2) {
              // 考虑删除的是第一个点 内容应该变成起点
              const node = (this._featureMarker[id] as SingleLinkList).getNode(
                currPosition,
              );
              node &&
                (node.getEl().innerHTML =
                  createMarkerContent('StartInfoMarker'));
              return;
            }
            const lengthToVertex =
              turf.length(turf.lineString(lineCoordinatesToVertex)) * 1000;
            const node = (this._featureMarker[id] as SingleLinkList).getNode(
              currPosition,
            );
            node &&
              (node.getEl().innerHTML = createMarkerContent(
                type,
                lengthToVertex,
              ));
          },
        );
      }
    }
  }
  // 新建Marker
  /**
   *
   * @param id marker的id
   * @param position 显示的位置
   * @param type marker显示内容的类型 Point|LineString|Polygon|info
   * @param value marker显示的值 坐标|长度|面积
   * @returns 返回Node类型的marker
   */
  private _createMarker(
    id: string,
    position: LngLatLike,
    type: GeomType,
    value?: [number, number] | number,
  ) {
    const el = createMarkerDom(type, value);
    const marker = new Marker({
      element: el,
      //@ts-ignore
      offset: MARKER_OFFSET[type],
      anchor: 'top-left',
    });
    const node = new Node({
      parentId: id,
      marker: marker,
      el, // 避免重复创建div
    });
    node
      //@ts-ignore
      .getMarker()
      .setLngLat(position)
      .addTo(this._map);
    return node;
  }
  /**
   * 画线的时候 创建线节点
   * @param obj
   */
  private _createLineVertex(obj: any) {
    const { state, e } = obj;
    const { currentVertexPosition, line, polygon } = state;
    if (line) {
      // line marker
      const { id, coordinates } = line;
      // 这里得到的coordinates很奇怪，画第一个点时，coordiantes中有两个，currentVertexPosition=1
      if (!this._featureMarker[id]) {
        this._featureMarker[id] = new SingleLinkList();
      }
      let vertexNode: Node;
      if (currentVertexPosition === 1) {
        vertexNode = this._createMarker(id, e.lngLat, 'StartFeatureMarker');
      } else {
        const lineCoordiantes = Array.from(coordinates) as Array<
          [number, number]
        >;
        lineCoordiantes.pop(); // 去除最后一个
        const length = turf.length(turf.lineString(lineCoordiantes)) * 1000;
        vertexNode = this._createMarker(id, e.lngLat, 'LineString', length);
      }
      (this._featureMarker[id] as SingleLinkList).append(vertexNode);
      // const node = this._createMarker(id,);
      // this._featureMarker[id].append(node);
    }
  }
  /**
   * 删除所有marker
   */
  private _deleteAllMarkers() {
    Object.keys(this._featureMarker).forEach((key) => {
      this._deleteMarker(key);
    });
  }
}
