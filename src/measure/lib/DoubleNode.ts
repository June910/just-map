import { Marker } from '@/marker';

interface NodeProps {
  parentId: string; // 父元素id
  nextNode?: Node | null; // 后一个节点
  marker: Marker; // marker
  el: Element;
}
export class Node {
  parentId: string; // 父元素id
  nextNode?: Node | null; // 前一个节点
  marker: Marker; // marker
  el: Element; // 缓存dom元素 避免更新时不断创建
  constructor(props: NodeProps) {
    const { parentId, nextNode = null, marker, el } = props;
    this.parentId = parentId;
    this.marker = marker;
    this.nextNode = nextNode;
    this.el = el;
  }
  getMarker() {
    return this.marker;
  }
  getEl() {
    return this.el;
  }
}

// 单项链表存储线的节点
export class SingleLinkList {
  length: number;
  head: Node | null;
  tail: Node | null;
  constructor() {
    this.length = 0;
    this.head = null;
    this.tail = null;
  }
  append(node: Node) {
    if (!this.head || !this.tail) {
      this.head = node;
      this.tail = node;
    } else {
      this.tail.nextNode = node;
      this.tail = node;
    }
    this.length++;
  }
  insert(position: number, node: Node) {
    if (position < 0 || position > this.length) return;
    let index = 0;
    let previous;
    let currentNode = this.head;
    if (position === 0) {
      // 从头插入
      if (!this.head) {
        this.head = node;
        this.tail = node;
      } else {
        node.nextNode = currentNode;
        this.head = node;
      }
      node.nextNode = this.head?.nextNode;
      this.head = node;
    } else if (position === this.length) {
      //插入到末尾
      currentNode = this.tail;
      //@ts-ignore
      currentNode.nextNode = node;
      this.tail = node;
    } else {
      while (index++ < position) {
        previous = currentNode;
        //@ts-ignore
        currentNode = currentNode.nextNode;
      }
      node.nextNode = currentNode;
      //@ts-ignore
      if (!previous) return;
      previous.nextNode = node;
    }
    this.length++;
  }
  removeAt(position: number) {
    // debugger;
    let index = 0;
    let currentNode = this.head;
    let previousNode;
    if (!this.tail || !this.head) return; //链表尾空
    if (position < 0 || position >= this.length) return; // 索引错误
    if (position === 0) {
      // 移除第一个
      this.head = currentNode?.nextNode || null;
      if (this.length === 1) {
        this.tail = null;
      }
    } else {
      while (index++ < position) {
        previousNode = currentNode;
        //@ts-ignore
        currentNode = currentNode.nextNode || null;
      }
      //@ts-ignore
      if (!previousNode) return;
      previousNode.nextNode = currentNode?.nextNode || null;
    }
    this.length--;
    return currentNode;
  }
  getHead() {
    return this.head;
  }
  getTail() {
    return this.tail;
  }
  getNode(position: number) {
    if (position < 0 || position >= this.length) return; //索引不对 或者为空
    let currentNode = this.head;
    let index = 0;
    while (index++ < position) {
      currentNode = currentNode?.nextNode || null;
    }
    return currentNode;
  }
  getLength() {
    return this.length;
  }
  changeLineVertex(position: number, changeAction: Function) {
    if (position < 0 || position >= this.length) return; //索引不对 或者为空
    let currentNode = this.getNode(position);
    let index = position;
    while (index < this.length) {
      changeAction(index);
      index++;
    }
    return currentNode;
  }
}
