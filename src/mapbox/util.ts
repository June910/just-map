import { pickBy } from 'lodash';
import mapboxgl from 'mapbox-gl';
import { isJustUrl } from 'src/utils';
import { log } from 'src/utils/logger';
// import { justMapConfigList } from 'src/react/config';
import { JustStyle, JustSource, SchemeType } from 'src/core';
import { mapboxLayerTypeList, mapboxSourceTypeList } from './constant';

/**
 * 获取just layer类型
 */
export function getJustMapLayerType(layer: mapboxgl.Layer) {
  if (!layer) return '';
  return layer.metadata && layer.metadata['just:type'];
}

// export function getJustMapLayerTypeList() {
//   return justMapConfigList.map((cfg) => cfg.type);
// }

// export function isJustMapLayer(layer: mapboxgl.AnyLayer) {
//   const type = getJustMapLayerType(layer);
//   return type && getJustMapLayerTypeList().includes(type);
// }

// 获取自定义的地图图层
// export function getjustMapLayers(mapStyle: mapboxgl.Style) {
//   const { layers } = mapStyle;
//   if (!layers) {
//     log.error('style文件中没有layers!');
//     return [];
//   }
//   const l = layers?.filter(isJustMapLayer);
//   return l;
// }

/**
 * 获取非mapboxgl的图层
 */
export function getNonMapboxLayers(layers: mapboxgl.Layer[]) {
  return layers?.filter((l) => !isMapboxLayer(l)) || [];
}

export function getMapboxLayers(layers: mapboxgl.Layer[]) {
  return layers?.filter((l) => isMapboxLayer(l)) || [];
}

export function isMapboxLayer(layer: mapboxgl.Layer) {
  return layer.type && mapboxLayerTypeList.includes(layer.type);
}

export function isMapboxSource(source: mapboxgl.Source) {
  //@ts-ignore
  return (
    source.type &&
    mapboxSourceTypeList.includes(source.type) &&
    !(source.scheme && Object.values(SchemeType).includes(source.scheme))
  );
}
/**
 * 获取justSources  type:'just'
 * @param style style文件 isMapboxSource
 */

export function getJustSources(style: JustStyle): JustSource {
  if (style) {
    const justSources: { [key: string]: any } = {};
    const sources = style.sources;
    if (!sources) return {};
    Object.keys(sources).forEach((k) => {
      // const type = sources[k].type;
      //@ts-ignore
      if (!isMapboxSource(sources[k])) {
        justSources[k] = sources[k];
      }
    });
    return justSources;
  }
  return {};
}
export function getMapboxSources(style: JustStyle): JustSource {
  if (style) {
    const mapboxSources: { [key: string]: any } = {};
    const sources = style.sources;
    if (!sources) return {};
    Object.keys(sources).forEach((k) => {
      const type = sources[k].type;
      //@ts-ignore
      if (isMapboxSource(sources[k])) {
        mapboxSources[k] = sources[k];
      }
      if (type === 'geojson') {
        //@ts-ignore
        const { options, ...otherSourceConfig } = sources[k];
        mapboxSources[k] = {
          ...otherSourceConfig,
          data: {
            type: 'FeatureCollection',
            features: [],
          },
        };
      }
    });
    return mapboxSources;
  }
  return {};
}

/**
 * 获取groups解构后的style文件 groups变成layers
 */
export function getMapboxStyle(
  originMapStyle: JustStyle | undefined | null,
): mapboxgl.Style {
  if (!originMapStyle) return getBlankMapboxStyle();
  const { groups, sources, controls, interactions, ...others } = originMapStyle;
  let newLayers: mapboxgl.Layer[] = [];
  if (Array.isArray(groups) && groups.length > 0) {
    groups.forEach((g: any) => {
      if (g.metadata && g.metadata['just:isGroup']) {
        g.layers && g.layers.length > 0 && newLayers.push(...g.layers);
      } else {
        newLayers.push(g);
      }
    });
  }
  const mapboxLayers = newLayers
    .filter((l) => isMapboxLayer(l) && !isCusotmTileLayer(l, originMapStyle))
    .reverse();
  const mapboxSources: { [key: string]: any } =
    getMapboxSources(originMapStyle);

  // sources &&
  //   Object.keys(sources).forEach((s) => {
  //     const type = sources[s].type;
  //     if (!type.includes('just')) {
  //       mapboxSources[s] = sources[s];
  //     }
  //   });
  // const mapboxSources = pickBy(sources, (source) =>
  //   mapboxSourceTypeList.includes(source.type),
  // ) as mapboxgl.Sources;
  const newStyle: JustStyle = {
    ...others,
    // @ts-ignore
    layers: mapboxLayers,
    sources: mapboxSources,
  };
  delete newStyle.groups;
  // @ts-ignore
  return newStyle;
}

export function isCusotmTileLayer(
  l: mapboxgl.Layer,
  style: JustStyle | undefined,
) {
  if (!style) return true;
  const layerSource = l.source;
  const sourceId = typeof layerSource === 'string' ? layerSource : l.id;
  //@ts-ignore
  const source =
    typeof layerSource === 'string' ? style.sources[sourceId] : layerSource;
  //@ts-ignore
  if (source?.scheme && Object.values(SchemeType).includes(source.scheme))
    return true;
  return false;
}

/**
 * 获取空的mapbox gl的style
 */
export function getBlankMapboxStyle() {
  return { version: 8 };
}

export function findGroupIndex(layerId: string, style: JustStyle): number {
  const groups = Array.from(style.groups || []);
  let groupIndex: number = -1;
  if (groups.length > 0) {
    for (let i = 0; i < groups.length; i++) {
      const group = groups[i];
      if (group.metadata?.['just:isGroup']) {
        // 图层组
        // @ts-ignore
        const layers = group.layers || [];
        const findIndex = layers.findIndex(
          (l: mapboxgl.AnyLayer) => l.id === layerId,
        );
        if (findIndex >= 0) {
          groupIndex = i;
          break;
        }
      } else {
        // 图层
        if (group.id === layerId) {
          groupIndex = i;
          break;
        }
      }
    }
  }
  return groupIndex;
}
