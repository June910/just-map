// 所有mapbox的source的type
export const mapboxSourceTypeList = [
  'vector',
  'raster',
  'raster-dem',
  // 'geojson',
  'image',
  'video',
];
// 所有的mapbox的layer的type
export const mapboxLayerTypeList = [
  'background',
  'fill',
  'line',
  'symbol',
  'raster',
  'circle',
  'fill-extrusion',
  'heatmap',
  'hillshade',
  'custom',
];
