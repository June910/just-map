import { isString, checkUrl, clone } from 'src/utils';
import { MapLayerEventType } from 'mapbox-gl';
import { log } from 'src/utils/logger';
import {
  JustSourceData,
  SpatialFilter,
  MapboxLayer,
  JustStyle,
  EVENT_TYPE,
  ControlProperty,
  Group,
  LayerInteractionOption,
  InteractionProps,
  StyleType,
} from 'src/core';
import Event from 'src/events';
import mapboxgl from 'mapbox-gl';
import { getFileFromUrl } from '../api/justMap';
import {
  findGroupIndex,
  getJustSources,
  getMapboxLayers,
  getMapboxSources,
  getMapboxStyle,
  getNonMapboxLayers,
} from './util';
import { MAPBOX_CONTROL_TYPE_LIST } from '../control';

export interface AddLayerOptions {
  before?: string | undefined;
  center?: boolean;
}

// export interface JustSourceRaw{}

/**
 * 获取style,和操作style相关的方法
 * style相当于数据存储层
 */
export default class MapStyle extends Event {
  style: JustStyle = { version: 8 };
  constructor(
    style: JustStyle | string,
    styleType: StyleType,
    eventedParent: Event,
  ) {
    super();
    this.setEventParent(eventedParent);
    this.requestStyle(style, styleType);
  }
  async requestStyle(style: JustStyle | string, styleType: StyleType) {
    try {
      const rawStyle = await MapStyle.RequestStyle(style, styleType);
      if (!rawStyle) {
        throw new Error('[style] style is empty');
      }
      this.style = rawStyle;
      this.fire('style.load');
    } catch (error) {
      log.error(error || 'fail to load style');
    }
  }
  /**
   * 根据id获取图层的索引
   * @param layerId
   * @returns 索引值(不存时返回-1)
   */
  getLayerIndex(layerId?: string): number {
    return this.getLayers().findIndex(({ id }) => id === layerId) || -1;
  }
  getLayer(layerId: string) {
    return this.getLayers()?.find(({ id }) => id === layerId);
  }
  /**
   * 获取图层组内所有图层 图层数组内图层的顺序 最后一个在地图中是最上面(reverse)
   * @returns 图层数组
   */
  getLayers(): mapboxgl.Layer[] {
    const layers: mapboxgl.Layer[] = [];
    this.style?.groups?.forEach((g) => {
      if (g.metadata?.['just:isGroup']) {
        const group = g as Group;
        if (group.layers && group.layers.length > 0) {
          layers.push(...group.layers);
        }
      } else {
        layers.push(g);
      }
    });
    layers.reverse();
    return layers;
  }
  getControl(controlId: string) {
    return this.style?.controls?.find(({ id }) => id === controlId);
  }
  addControl(control: ControlProperty) {
    const type = control.type;

    if (!this.style.controls) {
      this.style.controls = [];
    }
    this.style.controls.push(control);
    if (MAPBOX_CONTROL_TYPE_LIST.map((c) => c.type).includes(type)) {
      return;
    }
    this.fire('scene.update', this.style);
  }
  removeControl(id: string) {
    const idx = this.style?.controls?.findIndex((c) => c.id === id);
    if (idx !== undefined && idx !== -1) {
      const type = this.getControl(id)?.type;
      this.style?.controls?.splice(idx, 1);
      if (type && MAPBOX_CONTROL_TYPE_LIST.map((c) => c.type).includes(type)) {
        return;
      }
      this.fire('scene.update', this.style);
    }
  }
  setControlProperty(id: string, options: any, isUpdate?: boolean) {
    if (!this.style || !this.style.controls) return;
    const idx = this.style?.controls?.findIndex((c) => c.id === id);
    if (idx !== undefined && idx !== -1) {
      this.style.controls[idx].options = {
        ...this.style?.controls[idx].options,
        ...options,
      };
      isUpdate !== false && this.fire('scene.update', this.style);
    }
  }

  addLayer(layer: mapboxgl.AnyLayer, beforeId?: string) {
    // const { before } = options;
    if (!Array.isArray(this.style?.groups)) {
      this.style.groups = [];
    }
    const beforeIndex = this.getLayerIndex(beforeId);
    if (beforeIndex > -1) {
      this.style?.groups.splice(beforeIndex, 1, layer);
    } else {
      this.style?.groups.unshift(layer);
    }
    this.fire('style.update');
  }
  removeLayer(id: string) {
    const groups = this.style?.groups;
    if (!groups || groups.length === 0) return;
    const groupIndex = findGroupIndex(id, this.style);
    if (groupIndex >= 0) {
      const groups = this.style.groups;
      const group = groups && groups[groupIndex];
      if (group?.metadata && group?.metadata['just:isGroup']) {
        // @ts-ignore
        const layers: mapboxgl.AnyLayer[] = group.layers;
        const layerIndex = layers.findIndex((l) => l.id === id);
        layers.splice(layerIndex, 1);
      } else {
        this.style.groups?.splice(groupIndex, 1);
      }
    }
    this.fire('style.update');
  }
  updateLayer(layer: mapboxgl.AnyLayer) {
    if (!this.style || !this.style.groups) return;
    const { id: layerId } = layer;
    const groupIndex = findGroupIndex(layerId, this.style);
    if (groupIndex > -1) {
      const groups = this.style.groups;
      const group = groups && groups[groupIndex];
      if (group?.metadata && group?.metadata['just:isGroup']) {
        // 更新组的显示隐藏
        const allVisList = (group as Group).layers?.map(
          (i) => i.layout?.visibility,
        );
        if (!group.layout) {
          group.layout = {};
        }
        // 全部都是不可见
        if (allVisList?.every((i) => i === 'none')) {
          group.layout.visibility = 'none';
        } else {
          group.layout.visibility = 'visible';
        }

        // @ts-ignore
        const layers: mapboxgl.AnyLayer[] = group.layers;
        const layerIndex = layers.findIndex((l) => l.id === layer.id);
        layers.splice(layerIndex, 1, layer);
      } else {
        this.style.groups[groupIndex] = layer;
      }
    }
  }
  addSource(id: string, source: mapboxgl.Source | JustSourceData) {
    if (!this.style) {
      return;
    }
    if (!this.style.sources) {
      this.style.sources = {};
    }
    // FIXME
    // @ts-ignore
    this.style.sources[id] = source;
  }
  removeSource(id: string) {
    const sources = this.style?.sources;
    if (sources) {
      delete sources[id];
    }
  }
  getSource(id: string) {
    if (this.style?.sources) {
      return this.style?.sources[id];
    }
  }
  getNonMapboxSources() {
    const styleNonMapboxSources = this.style ? getJustSources(this.style) : {};
    return { ...styleNonMapboxSources };
  }
  getNonMapboxLayers() {
    const layers = this.getLayers();
    return getNonMapboxLayers(layers);
  }
  getMapboxLayers() {
    const layers = this.getLayers();
    return getMapboxLayers(layers);
  }
  getMapboxSources() {
    const styleNonMapboxSources = this.style
      ? getMapboxSources(this.style)
      : {};
    return { ...styleNonMapboxSources };
  }
  /**
   * 获取map的style文件
   */
  getStyle(): JustStyle | undefined {
    return clone(this.style);
  }
  setStyle(style: JustStyle | string, styleType?: StyleType) {
    this.requestStyle(style, styleType || 'justmap');
  }

  /**
   * 获取原生mapbox的地图图层
   */
  getMapboxStyle(): mapboxgl.Style {
    return getMapboxStyle(this.style);
  }
  /**
   * 获取style
   */
  static async RequestStyle(
    style: JustStyle | mapboxgl.Style | string | undefined,
    styleType: StyleType = 'justmap',
  ): Promise<JustStyle | mapboxgl.Style | undefined> {
    if (!style) {
      log.error('[staic request style] style required');
      return;
    }
    if (!isString(style)) {
      // TODO checkStyle()
      return MapStyle._getJustStyle(style, styleType);
    }
    // if (checkUrl(style)) {
    try {
      const styleReq: JustStyle = await getFileFromUrl(style);
      return MapStyle._getJustStyle(styleReq, styleType);
    } catch (error) {
      log.error('获取style文件失败');
    }
    // } else {
    //   log.error('style文件的url地址不正确');
    // }
  }
  private static _getJustStyle(
    rawStyle: JustStyle | mapboxgl.Style,
    styleType: StyleType = 'justmap',
  ) {
    if (styleType === 'mapbox') {
      // 兼容mapbox原生style文件
      const cloneRawStyle = clone(rawStyle);
      const layers = cloneRawStyle.layers;
      if (!Array.isArray(layers)) {
        log.error('style is not valid');
        return;
      }
      delete cloneRawStyle.layers;
      return { ...cloneRawStyle, groups: layers.reverse() };
    } else {
      return rawStyle;
    }
  }
  static transformGroup2Layer(groups: (mapboxgl.Layer | Group)[] | undefined) {
    const layers: any[] = [];
    if (!Array.isArray(groups)) {
      return layers;
    }
    groups.forEach((g: any) => {
      if (g.metadata?.['just:isGroup'] && Array.isArray(g.layers)) {
        layers.push(...g.layers);
      } else {
        layers.push(g);
      }
    });
    // style中layer顺序和mapbox中正好相反
    return layers.reverse();
  }
  static transformMapboxStyle(justStyle: JustStyle): mapboxgl.Style {
    const style = {
      metadata: justStyle.metadata,
      sources: justStyle.sources,
      bearing: justStyle.bearing,
      center: justStyle.center,
      name: justStyle.name,
      sprite: justStyle.sprite,
      zoom: justStyle.zoom,
      glyphs: justStyle.glyphs,
      pitch: justStyle.pitch,
      light: justStyle.light,
      layers: MapStyle.transformGroup2Layer(justStyle.groups),
      version: 8,
    };
    if (justStyle.transition) {
      Object.assign(style, { transition: justStyle.transition });
    }
    // FIXME 这里的source， 也需要转换
    // @ts-ignore
    return style;
  }
  updateStyle(style: JustStyle) {
    this.style = style;
  }
  setFilter(layerId: string, filter: any[]) {
    const layer = this.getLayer(layerId) as MapboxLayer;
    delete layer.spatialFilter;
    if (layer.filter === filter) {
      return;
    }
    if (filter === null && layer?.filter) {
      delete layer.filter;
    }
    const layerIndex = this.getLayerIndex(layerId);
    if (layerIndex !== undefined && layerIndex !== -1) {
      layer.filter = filter;
      // @ts-ignore
      // this.style?.layers?.splice(layerIndex, 1, layer);
      this.updateLayer(layer);
    }
  }
  setSpatialFilter(layerId: string, spatialFilter: SpatialFilter) {
    const layer = this.getLayer(layerId) as MapboxLayer;
    delete layer.filter;
    if (JSON.stringify(layer.spatialFilter) === JSON.stringify(spatialFilter)) {
      return;
    }
    if (spatialFilter === null && layer?.spatialFilter) {
      delete layer.spatialFilter;
      // @ts-ignore
      this.updateLayer(layer);
      return;
    }
    // @ts-ignore
    this.updateLayer({ ...layer, spatialFilter });

    // const layerIndex = this.style?.layers?.findIndex((l) => l.id === layerId);
    // if (layerIndex !== undefined && layerIndex !== -1) {
    //   layer.spatialFilter = spatialFilter;
    //   // @ts-ignore
    //   this.style?.layers?.splice(layerIndex, 1, layer);
    // }
  }

  setLayerProperty(layerId: string, options: any) {
    const layer = this.getLayer(layerId);
    if (!layer) return;
    layer.paint = {
      ...layer.paint,
      ...options.paint,
    };
    layer.layout = {
      ...layer.layout,
      ...options.layout,
    };
    // @ts-ignore
    this.updateLayer(layer);
    this.fire('style.update');
  }
  // addMapInteraction(eventId: string, type: EVENT_TYPE, options: any) {
  //   if (!this.style) return;
  //   if (!this.style?.interactions) {
  //     this.style.interactions = [];
  //   }
  //   const mapInteraction = this.style?.interactions?.find(
  //     (interaction) => interaction.attachType === 'map',
  //   );
  //   if (mapInteraction) {
  //     const eventIndex = mapInteraction.eventList?.findIndex(
  //       (e) => e.id === eventId,
  //     );
  //     if (eventIndex !== -1) {
  //       mapInteraction.eventList.splice(eventIndex, 1, {
  //         id: eventId,
  //         type,
  //         options,
  //       });
  //     } else {
  //       mapInteraction.eventList.push({ id: eventId, type, options });
  //     }
  //   } else {
  //     this.style?.interactions.push({
  //       id: 'map',
  //       attachType: 'map',
  //       eventList: [{ id: eventId, type, options }],
  //     });
  //   }
  // }
  /**
   * 添加交互
   * @param layerId
   * @param eventId
   * @param type
   * @param actionOption
   * @returns
   */
  addInteraction(
    layerId: string,
    eventId: string,
    type: keyof MapLayerEventType | 'load',
    actionOption: LayerInteractionOption,
  ) {
    let layerInteractions = this.style.interactions;
    if (!layerInteractions) {
      this.style.interactions = [];
      layerInteractions = [];
    }
    let layerInteraction = layerInteractions?.find(
      (interaction) => interaction.id === layerId,
    );
    if (!layerInteraction) {
      this.style.interactions?.push({
        id: layerId,
        eventList: [],
      });
      layerInteraction = {
        id: layerId,
        eventList: [],
      };
    }
    const layerEvent = layerInteraction.eventList.find((e) => e.id === eventId);
    if (layerEvent) {
      // 如果已经存在，就不添加
      return;
    }
    layerInteraction?.eventList.push({
      id: eventId,
      type,
      actionOption,
    });
    const layerInteractionIndex = layerInteractions?.findIndex(
      (interaction) => interaction.id === layerId,
    );
    layerInteractionIndex !== undefined &&
      this.style.interactions?.splice(
        layerInteractionIndex,
        1,
        layerInteraction,
      );
  }
  /**
   * 获取图层的某个交互
   * @param layerId
   * @param eventId
   * @returns
   */
  getInteraction(layerId: string, eventId: string) {
    const layerInteractions = this.style.interactions;
    const layerInteraction = layerInteractions?.find(
      (interaction) => interaction.id === layerId,
    );
    if (!layerInteraction) return;
    const layerEventList = layerInteraction.eventList;
    const layerEvent = layerEventList.find((e) => e.id === eventId);
    if (!layerEvent) return;
    return layerEvent;
  }
  /**
   * 获取图层所有交互
   * @param layerId
   * @param eventId
   * @returns
   */
  getInteractionList(layerId: string) {
    const layerInteractions = this.style.interactions;
    const layerInteraction = layerInteractions?.find(
      (interaction) => interaction.id === layerId,
    );
    if (!layerInteraction) return;
    return layerInteraction.eventList;

    // const layerEvent= layerEventList.find(e=>e.id===eventId);
    // if(!layerEvent) return;
    // return layerEvent;
  }
  /**
   * 移除图层全部交互
   * @param layerId
   * @returns
   */
  // removeLayerInteraction(layerId:string){
  //   const layerInteractions = this.style.interactions;
  //   const layerInteractionIndex  = layerInteractions?.findIndex(interaction=>interaction.id===layerId);
  //   if(!layerInteractionIndex || layerInteractionIndex!==-1) return;
  //   layerInteractions?.splice(layerInteractionIndex,1);
  // }
  /**
   * 移除某个交互
   * @param layerId
   * @param eventId
   * @returns
   */
  removeInteraction(layerId: string, eventId: string) {
    const layerInteractions = clone(
      this.style.interactions,
    ) as InteractionProps[];
    const layerInteractionIndex = layerInteractions?.findIndex(
      (interaction) => interaction.id === layerId,
    );
    if (!(layerInteractionIndex !== undefined && layerInteractionIndex > -1))
      return;
    const layerInteraction =
      layerInteractions && layerInteractions[layerInteractionIndex];
    if (!layerInteraction) return;
    const layerEventList = layerInteractions
      ? layerInteractions[layerInteractionIndex].eventList
      : [];
    const layerEventIndex = layerEventList?.findIndex((e) => e.id === eventId);
    if (layerEventIndex === -1 || layerEventIndex === undefined) return;
    layerEventList?.splice(layerEventIndex, 1);
    const newLayerInteraction = {
      ...layerInteraction,
      eventList: layerEventList,
    };
    layerInteractions?.splice(layerInteractionIndex, 1, newLayerInteraction);
    this.style.interactions = layerInteractions;
  }
  /**
   * 获取当前style文件中图层是hightlight的交互的paint和layout
   * @param layerId
   * @returns
   */
  getLayerHightlightInteractionStyle(layerId: string) {
    const layerInteractions = this.style.interactions;
    const layerInteraction = layerInteractions?.find(
      (interaction) => interaction.id === layerId,
    );
    if (!layerInteraction) return;
    const layerHightLightInteractionList = layerInteraction.eventList.filter(
      (e) => e.actionOption.type === 'highlight',
    );
    const paint = {},
      layout = {};
    layerHightLightInteractionList.forEach((action) => {
      const { paint: layerPaint, layout: layerLayout } =
        action.actionOption.options;
      Object.assign(paint, layerPaint);
      Object.assign(layout, layerLayout);
    });
    return { paint, layout };
  }
  // setLayerProperty(layerId: string, options: any) {
  //   const index = this.style?.layers?.findIndex((l) => l.id === layerId);
  //   if (index !== undefined && index !== -1) {
  //     // @ts-ignore
  //     this.style.layers[index].paint = {
  //       // @ts-ignore
  //       ...this.style.layers[index].paint,
  //       ...options.paint,
  //     };
  //     // @ts-ignore
  //     this.style.layers[index].layout = {
  //       // @ts-ignore
  //       ...this.style.layers[index].layout,
  //       ...options.layout,
  //     };
  //     this.fire('style.update');
  //   }
  // }
  getGroups() {
    return this.style?.groups;
  }
  getGroup(id: string): mapboxgl.Layer | Group | null {
    const groupOrLayer = this.style?.groups?.find((i) => i.id === id);
    if (groupOrLayer?.metadata?.['just:isGroup']) {
      return groupOrLayer;
    }
    return null;
  }
  setGroupProperty(id: string, options: any) {
    const group = this.getGroup(id);
    if (!group) {
      log.warn('group', id, 'not exist');
    }
    Object.assign(group, options);

    this.fire('style.update');
  }
  setLayerZoomRange(layerId: string, minZoom: number, maxZoom: number) {
    const layer = this.getLayer(layerId);
    if (!layer) return;
    layer.minzoom = minZoom;
    layer.maxzoom = maxZoom;
    this.fire('style.update');
  }
}
