// 地理围栏源数据图层
export const FENCE_SOURE_LAYER = 'just_realtime';

export const FENCE_DEFAULT_NORMAL_COLOR = 'rgba(211, 211, 211,0.5)';
export const FENCE_DEFAULT_INVOLVED_COLOR = 'rgba(255,0,0,0.5)';