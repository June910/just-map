import { JustMap } from 'src/core/index';
import mapboxgl, { LngLat, LngLatLike, Anchor, Offset } from 'mapbox-gl';
import { Popup } from '@/popup';
import { Point, PointLike } from 'mapbox-gl';

// const wrapMethods = ['addTo'];
export class Marker {
  marker: mapboxgl.Marker;
  _map: JustMap | undefined;
  _popup: Popup | undefined;
  _offset: Point;
  _draggable: boolean | undefined;
  constructor(options: mapboxgl.MarkerOptions) {
    this.marker = new mapboxgl.Marker(options);
  }
  /**
   * 在地图上添加Marker标记
   * @param map 需要添加Marker标记的JustMap地图
   * @returns marker实例
   */
  addTo(map: JustMap) {
    map.map && this.marker.addTo(map.map);
    this._map = map;
    return this;
  }
  /**
   * 返回Marker标记的锚所在地理位置。
   * @returns Marker标记的锚的地理位置。
   */
  getLngLat(): LngLat {
    return this.marker.getLngLat();
  }
  /**
   * 设置好Marker标记的锚的地理位置后使弹窗移动过去。
   * @param lngLat 要设置的弹窗的锚的地理位置。
   * @returns marker实例
   */
  setLngLat(lngLat: LngLatLike) {
    this.marker.setLngLat(lngLat);
    return this;
  }
  /**
   * 将弹窗内容设置为以字符串形式提供的 HTML。
   * @param html 用于代表弹窗 HTML 内容的字符串。
   * @returns marker实例
   */
  setPopup(popup: Popup): Marker {
    this.marker.setPopup(popup.popup);
    this._popup = popup;
    return this;
  }

  // /**
  //  * 返回拖拽
  //  * @returns true 允许拖拽  false 不允许拖拽
  //  */
  // isDraggable(): boolean {
  //   return this._draggable;
  // }
  /**
   * 从添加地图中移除Marker。
   * @returns Marker实例
   */
  remove(): Marker {
    this.marker.remove();
    return this;
  }

  /**
   * 返回Popup的HTML元素。
   * @returns 返回Popup的HTML元素
   */
  getPopup(): any {
    return this._popup;
  }
  /**
   * 重新打开弹窗
   * @param map 地图实体
   * @param popup 地图弹窗
   * @returns
   */
  togglePopup() {
    if (!this._popup) return this;
    else if (this._popup.isOpen()) this._popup.remove();
    else this._map && this._popup.addTo(this._map);
    return this;
  }
  /**
   * 设置Marker标记的偏移量
   * @param offset
   */
  setOffset(offset: PointLike) {
    this._offset = Point.convert(offset);
    return this;
  }
  convert(a: any) {
    if (a instanceof Point) {
      return a;
    }
    if (Array.isArray(a)) {
      return a[0], a[1];
    }
    return a;
  }

  /**
   * 获取偏移量
   * @returns
   */
  getOffset() {
    return this._offset;
  }
  /**
   * 获取marker标记的HtmlElement
   * @returns 返回element
   */
  getElement() {
    return this.marker.getElement();
  }
  setDraggable(shouldBeDraggable: boolean) {
    this._draggable = !!shouldBeDraggable; // convert possible undefined value to false
    return this;
  }
}
