import { JustMap, JustStyle } from './core';
import Style from './mapbox/style';
import { Popup } from './popup';
import { Marker } from './marker';
import { DrawProxy as Draw } from './draw';

import '../src/css/justmap.css';
import '../src/css/justmap-draw.css';

// import {ProxyPopup as Popup} from './popup';

// import JustMapReact from './react';
// import Control from './control';
// import ControlComponents from './react/controls';

// export default { ProxyJustMap, JustMap, JustMapReact,Control,ControlComponents };
export { JustMap, JustStyle, Style, Popup, Marker, Draw };
