import mapboxgl, { EventData, MapLayerEventType } from 'mapbox-gl';
import _ from 'lodash';
import { log } from '../utils/logger';
import {
  isMapboxLayer,
  getJustSources,
  getMapboxStyle,
  isCusotmTileLayer,
} from '../mapbox/util';
import Source from '../sources';
import Event, { MapboxEventType, EventType } from '../events';
import { CONTROL_TYPE } from '../control';
import SourceCache from '../sources/source_cache';
import { clone, debounce, getJustUrl, isJustSourceUrl } from '../utils';
import SpatialWorker from '../worker/spatialFilter';
import MapStyle from '../mapbox/style';
import InteractionManager from '@/interaction';
import CustomTileLayer from '../layers/customTileLayer';
import CustomTileSource from '../sources/customTileSource';
import { isMapboxSource } from '../mapbox/util';
import Control, { MAPBOX_CONTROL_TYPE_LIST } from '../control';
import {
  HeatMapLayer,
  CylinderLayer,
  clusterLayer,
  TripsLayer,
  SolidPolygonLayer,
  ModelLayer,
} from '../layers';
import { Measure } from '../measure';
import { Draw } from '../draw';

const LAYER_TYPE = {
  just_heatmap: HeatMapLayer,
  just_cylinder: CylinderLayer,
  just_cluster: clusterLayer,
  just_trips: TripsLayer,
  just_solidPolygon: SolidPolygonLayer,
  just_model: ModelLayer,
};
// import { wrapMethods } from './config';
export interface ControlProperty {
  id: string;
  type: CONTROL_TYPE;
  options?: any;
  component?: React.ReactElement;
}
export interface JustMapOptionType {
  map?: mapboxgl.Map;
  baseUrl?: string;
  token?: string;
  style: JustStyle | string;
  styleType?: StyleType;
  mapboxOptions?: Omit<mapboxgl.MapboxOptions, 'style'>;
}
export type JustSourceType = 'trips' | 'geojson';
export enum SchemeType {
  BAIDU = 'baidu',
  GAODE = 'gaode',
}

export interface JustSourceData {
  type: JustSourceType;
  data:
    | GeoJSON.Feature<GeoJSON.Geometry>
    | GeoJSON.FeatureCollection<GeoJSON.Geometry>
    | string
    | any;
  options?: [];
  interval?: number;
  cluster?: boolean;
  clusterMaxZoom?: number;
  clusterRadius?: number;
  tiles?: string[];
  tileSize?: number;
  scheme?: SchemeType;
}
export interface JustSource /*  extends  */ {
  [sourceName: string]: JustSourceData;
}
export type SPATIAL_RELATION = 'crosses' | 'contains';
type RealSpatialFilter = [
  SPATIAL_RELATION,
  GeoJSON.Feature[] | GeoJSON.Geometry[] | string[],
];
export type SpatialFilter = RealSpatialFilter | undefined | null;
export interface MapboxLayer extends mapboxgl.Layer {
  spatialFilter?: SpatialFilter;
}
export type EVENT_TYPE = 'load' | 'click' | 'dbclick' | 'hover';
export type MapboxControlPosition =
  | 'top-right'
  | 'top-left'
  | 'bottom-right'
  | 'bottom-left'
  | undefined;
export interface InteractionProps {
  id: string; // 图层|组件|地图的id  地图的id是map
  // attachType: 'layer' | 'control' | 'map'; // 交互归属类型
  eventList: InteractionEventProps[];
}
export interface InteractionEventProps {
  id: string; // 事件id
  type: keyof MapLayerEventType | 'load'; // 触发事件的类型
  actionOption: LayerInteractionOption; // 事件具体的执行动作
}
export interface Group extends mapboxgl.Layer {
  layers?: mapboxgl.Layer[];
}

export interface JustStyle extends Omit<mapboxgl.Style, 'sources' | 'layers'> {
  sources?: JustSource | mapboxgl.Sources;
  controls?: ControlProperty[];
  interactions?: InteractionProps[];
  groups?: Array<mapboxgl.Layer | Group>;
  minZoom?: number;
  maxZoom?: number;
}

export interface RenderTaskProps {
  id: string;
  task: any;
}
export interface WorkerOutProps {
  featuresList: GeoJSON.Feature[];
  featureIdList: number[];
}
export interface WorkerInProps {
  features: GeoJSON.Feature[];
  spatialFilter: RealSpatialFilter;
}
export interface LayerInteractionOption {
  type: 'highlight' | 'popup' | 'flyTo';
  options?: any;
}
type Listener = (layerId: string, spatialFilter: SpatialFilter) => void;
type Listeners = { [k: string]: Listener };
type Workers = { [k: string]: Worker };
export type StyleType = 'mapbox' | 'justmap';

export type PointLike = [number, number];
export type LngLatLike =
  | { lng: number; lat: number }
  | { lon: number; lat: number }
  | [number, number];

const mapboxEventTypeList: MapboxEventType[] = [
  'click',
  'dblclick',
  'drag',
  'dragend',
  'dragstart',
  'mouseenter',
  'mousedown',
  'mousemove',
  'mouseout',
  'mouseleave',
  'mouseover',
  'mouseup',
  'movestart',
  'moveend',
  'pitch',
  'pitchend',
  'pitchstart',
  'touchcancel',
  'touchend',
  'touchmove',
  'touchstart',
  'zoom',
  'zoomend',
  'zoomstart',
  'wheel',
  'sourcedata',
  'boxzoomcancel',
  'resize',
  'boxzoomend',
  'data',
  'dataloading',
  'move',
  'movestart',
  'render',
  'remove',
  'contextmenu',
  'rotatestart',
  'rotate',
  'rotateend',
  'boxzoomstart',
  'idle',
  'error',
  'webglcontextlost',
  'styledataloading',
  'sourcedataloading',
  'styledata',
  'webglcontextrestored',
  'styleimagemissing',
];

const DRAE_EVENT_TYPE_LIST = [
  'draw.create',
  'draw.delete',
  'draw.update',
  'draw.render',
  'draw.combine',
  'draw.uncombine',
  'draw.modechange',
  'draw.actionable',
  'draw.selectionchange',
];

// type mapboxKey = keyof mapboxgl.Map;
// let a: mapboxKey = '1'
export class JustMap extends Event {
  map: mapboxgl.Map | undefined;
  mapStyle: MapStyle;
  interactionManager: InteractionManager | undefined;
  // TODO 不用react context, 所有请求从sdk走
  private baseUrl: string | undefined;
  private token: string | undefined;
  // private style: JustStyle;
  private justSources: JustSource = {};
  private spatialFilterEventsList: Listeners = {};
  private spatialFilterWorkerList: Workers = {};
  sourceCaches: SourceCache | undefined;
  renderTasks: RenderTaskProps[] = [];
  private preStyle: mapboxgl.Style = { version: 8 }; // setStyle之前上一次的style信息
  private initStatus: boolean = true; // 标记地图第一次加载
  private _justLayers: { [key: string]: any } = {};
  private _mapLoaded: Boolean = false; //地图是否加载完成
  private _draw: Draw | undefined;
  private _measure: Measure | undefined;
  // 用户自定义的control组件
  _controlComponent: any = {};
  // 只能绑定一个交互
  _interaction: { [key: string]: any } = {};
  boxZoom: mapboxgl.BoxZoomHandler | undefined;
  dragPan: mapboxgl.DragPanHandler | undefined;
  scrollZoom: mapboxgl.ScrollZoomHandler | undefined;
  dragRotate: mapboxgl.BoxZoomHandler | undefined;
  keyboard: mapboxgl.KeyboardHandler | undefined;
  doubleClickZoom: mapboxgl.DoubleClickZoomHandler | undefined;
  touchZoomRotate: mapboxgl.TouchZoomRotateHandler | undefined;
  constructor({
    map,
    baseUrl,
    token,
    style,
    styleType = 'justmap',
    mapboxOptions,
  }: JustMapOptionType) {
    super();
    // super(style);
    this.baseUrl = baseUrl;
    this.token = token;
    this.mapStyle = new MapStyle(clone(style), styleType, this);
    this._justMapLoadEvent = this._justMapLoadEvent.bind(this);
    if (map) {
      this.map = map;
      this.sourceCaches = SourceCache.create();
      this.on('style.load', () => {
        this.interactionManager = new InteractionManager(
          map,
          this.mapStyle,
          this,
        );
        this.initStatus && this._justMapLoadTrigger(map); // 初始化进来时触发justMap的load事件 第二次加载不执行
        this._initMap();
        this._initInteractions();
        this.initStatus = false;
      });
    } else {
      const map = new mapboxgl.Map({
        attributionControl: false,
        transformRequest: (url, resourceType) => {
          //兼容两种justUrl情况 just://   /
          if (isJustSourceUrl(url)) {
            return {
              url: `${baseUrl}${getJustUrl(url)}`,
            };
          }
          return {
            url: url,
          };
        },
        ...(mapboxOptions as mapboxgl.MapboxOptions),
        // style: getBlankMapboxStyle(),
      });
      this.map = map;
      this.sourceCaches = SourceCache.create();
      // 如果用纯js写的话,setStyle如何实现底图更换
      this.on('style.load', () => {
        const justStyleObj = this.mapStyle.getStyle();
        if (!justStyleObj) {
          throw new Error('just style文件不存在');
        }
        // 这里要进行just:开头的域名替换，方案有两种：
        // 1.通过transformRequest进行替换
        // 2.style文件转换替换。
        // 这里采用方案1 通过transformRequest方式，避免重新写转换方法。
        map.setStyle(JustMap.getMapbxStyle(justStyleObj));
        justStyleObj.zoom !== undefined &&
          mapboxOptions?.zoom === undefined &&
          map.setZoom(justStyleObj.zoom);
        justStyleObj.center !== undefined &&
          mapboxOptions?.center === undefined &&
          //@ts-ignore
          map.setCenter(justStyleObj.center);
        justStyleObj.pitch !== undefined &&
          mapboxOptions?.pitch === undefined &&
          map.setPitch(justStyleObj.pitch);
        justStyleObj.bearing !== undefined &&
          mapboxOptions?.bearing === undefined &&
          map.setBearing(justStyleObj.bearing);

        map.on('load', () => {
          this._justMapLoadTrigger(map); // 初始化进来时,触发justMap的load
          this._initInteractions();
          this._initMap();
          this.initStatus = false;
        });
        this.interactionManager = new InteractionManager(
          map,
          this.mapStyle,
          this,
        );
        // 第一次加载时 下面不执行
        !this.initStatus && this._initInteractions();
        !this.initStatus && this._initMap();
      });
      // MapStyle.RequestStyle(style).then((justStyleObj: any) => {
      //   if (!justStyleObj) {
      //     throw new Error('just style文件不存在');
      //   }
      //   const map = new mapboxgl.Map({
      //     attributionControl: false,
      //     ...(mapboxOptions as mapboxgl.MapboxOptions),
      //     // style: MapStyle.transformMapboxStyle(justStyleObj), // 这里也有问题
      //     style: JustMap.getMapbxStyle(justStyleObj),
      //   });
      //   this.map = map;
      //   map.on('load', () => {
      //     this._justMapLoadTrigger(map); // 初始化进来时,触发justMap的load
      //     this._initInteractions();
      //   });
      //   // new MapStyle时会触发style.load
      //   this.on('style.load', () => {
      //     // 当用户调用setStyle时,会触发重新渲染
      //     this.interactionManager = new InteractionManager(
      //       map,
      //       this.mapStyle,
      //       this,
      //     );
      //     this._initMap();
      //   });

      //   //   //用纯js写时，通过监听mapbox的map load事件，触发justMap的load事件
      // });
    }
    this.boxZoom = this.map.boxZoom;
    this.scrollZoom = this.map.scrollZoom;
    this.dragRotate = this.map.dragRotate;
    this.keyboard = this.map.keyboard;
    this.doubleClickZoom = this.map.doubleClickZoom;
    this.touchZoomRotate = this.map.touchZoomRotate;
    this.dragPan = this.map.dragPan;
  }

  /**
   * 触发justMap的load事件
   * @param map
   */
  private _justMapLoadTrigger(map: mapboxgl.Map) {
    const justSources = this.mapStyle?.getNonMapboxSources();
    if (Object.keys(justSources).length > 0) {
      this.on('source.data', this._justMapLoadEvent);
    } else {
      this.fire('load', { type: 'load', target: this });
      this._mapLoaded = true;
    }
  }
  private _justMapLoadEvent(e: { sourceId?: string } | undefined) {
    // 当所有的source 的loaded属性为true时,justMap触发load事件
    let justSourceLoaded = true;
    const justSources = this.sourceCaches && this.sourceCaches._cacheSource;
    if (justSources) {
      const sourceKeys = Object.keys(justSources);
      for (let i = 0; i < sourceKeys.length; i++) {
        const source = justSources[sourceKeys[i]];
        if (!source.getLoadStatus()) {
          justSourceLoaded = false;
          break;
        }
      }
      if (justSourceLoaded) {
        // off必须在this.fire('load')之前触发，否则会再次触发load事件
        this.off('source.data', this._justMapLoadEvent);
        this.fire('load', { type: 'load', target: this });
        this._mapLoaded = true;
      }
    }
  }

  /**
   * 初始化交互
   */
  private _initInteractions() {
    const interactions = this.mapStyle?.getStyle()?.interactions;
    if (!interactions || interactions?.length === 0) return;
    this.interactionManager?.initInteraction(interactions);

    // const layerInteractions = interactions.filter(
    //   (item) => item.attachType === 'layer',
    // );
    // const controlInteractions = interactions.filter(
    //   (item) => item.attachType === 'control',
    // );
    // const mapInteractions = interactions.filter(
    //   (item) => item.attachType === 'map',
    // );
    // this._initMapInteraction(mapInteractions);
    // this._initLayerInteraction(layerInteractions);
    // this._initControlInteraction(controlInteractions);
  }
  // private _initMapInteraction(interactions: InteractionProps[]) {
  //   if (interactions.length > 0) {
  //     const loadingEventList = interactions[0].eventList.filter(
  //       (e) => e.type === 'load',
  //     );
  //     loadingEventList.forEach((e) => {
  //       const { actionType, actionContent } = e.options;
  //       // TODO 不同的actionType执行不同的业务逻辑 如何配置
  //       if (actionType === 'flyTo') {
  //         this.map?.flyTo({
  //           center: [
  //             Number(actionContent.split(',')[0]),
  //             Number(actionContent.split(',')[1]),
  //           ],
  //           zoom: 10,
  //         });
  //       }
  //     });
  //   }
  // }
  private _initLayerInteraction(interactions: InteractionProps[]) {
    this.interactionManager?.initInteraction(interactions);
  }

  private _initControlInteraction(interactions: InteractionProps[]) {}

  private _initMap() {
    const mapboxStyle = this.mapStyle?.getMapboxStyle();
    if (!mapboxStyle) {
      return;
    }
    // this._justLayers = {};//清空缓存图层
    // SourceCache.clear();//清空缓存source
    this._initJustRender();
    this._initSpatialFilter();
  }
  //  替换所有source，图层， 交互
  // TODO
  setBaseStyle(style: JustStyle | string, styleType: StyleType = 'justmap') {
    MapStyle.RequestStyle(style).then((res: JustStyle | undefined) => {
      if (res) {
        const preStyle = this.getStyle();
        //移除之前的mapbox原生layers和sources
        this._removeJustResources();
        this._removeMapboxResources();
        const mapboxStyle = (
          styleType === 'mapbox' ? res : getMapboxStyle(res)
        ) as mapboxgl.Style;
        this._addMapboxResources(mapboxStyle); // 添加新的mapbox原生layers和sources
        this._updateMapView(mapboxStyle); // 地图视图(center pitch zoom bearing)更新
        if (preStyle) {
          const controls = preStyle?.controls;
          const newStyle = { ...res, controls };
          this.mapStyle?.setStyle(newStyle); // 更新style文件触发style.load
        }
      }
    });
    // 方案一：
    // preStyle 移除之前的source， layer， 交互
    // this.map?.setStyle();
    // 添加 新的source和layer， 交互
    // 方案二： 拼出新的style， 初始化
    // this.mapStyle.setStyle()
  }

  /**
   * 设置style
   * @param style
   */
  setStyle(style: JustStyle | string, styleType: StyleType = 'justmap') {
    // 不直接执行mapStyle.setStyle 要监听scene.upate并与setBaseMapStyle区分
    MapStyle.RequestStyle(style).then((res: JustStyle | undefined) => {
      if (res) {
        //清除缓存 清除justSource和justLayer
        const mapboxStyle = (
          styleType === 'mapbox' ? res : getMapboxStyle(res)
        ) as mapboxgl.Style;
        this.map?.setStyle(mapboxStyle); // 触发底图更新
        this.mapStyle?.setStyle(res, styleType); // 更新style文件触发style.load
        this.fire('scene.update', this.mapStyle.style); //触发control更新
      }
    });
    // const mapboxStyle = this.mapStyle?.getMapboxStyle();
    // if (mapboxStyle) {
    //   this.preStyle = mapboxStyle;
    // }
  }
  // private _setStyle(map: mapboxgl.Map | undefined) {
  //   if (this.initStatus && map) {
  //     this.initStatus = false;
  //     // 初始化渲染且传入map
  //     return;
  //   }
  //   const mapboxStyle = this.mapStyle?.getMapboxStyle();
  //   if (!mapboxStyle) {
  //     return;
  //   }

  //   const {
  //     layers,
  //     sources,
  //     // @ts-ignore
  //     minZoom,
  //     // @ts-ignore
  //     maxZoom,
  //     zoom,
  //     center,
  //     pitch,
  //     bearing,
  //     ...otherMapboxStyle
  //   } = mapboxStyle;
  //   const { layers: preLayers, sources: preSources } = this.preStyle;
  //   this.map?.setStyle(otherMapboxStyle);
  //   minZoom && this.map?.setMinZoom(minZoom);
  //   maxZoom && this.map?.setMaxZoom(maxZoom);
  //   zoom && this.map?.setZoom(zoom);
  //   pitch && this.map?.setPitch(pitch);
  //   bearing && this.map?.setBearing(bearing);
  //   //@ts-ignore
  //   center && this.map?.setCenter(center);
  //   // const beforeLayerId = this._getBeforeLayerId(preLayers);

  //   this._removeLayers(preLayers);
  //   this._removeSources(preSources);
  //   this._addSources(sources);
  //   // this._addLayers(layers, beforeLayerId);
  //   this._addLayers(layers);
  // }
  /**
   * 清除just相关sources和layers
   */
  private _removeJustResources() {
    const justSources = this.mapStyle?.getNonMapboxSources();
    const layers = this.mapStyle.getNonMapboxLayers();
    layers.forEach((l) => this.removeLayer(l.id));
    Object.keys(justSources).forEach((k) => this.removeSource(k));
  }
  /**
   * 清除mapbox原生的sources和layers
   */
  private _removeMapboxResources() {
    const mapboxSources = this.mapStyle.getMapboxSources();
    const mapboxLayers = this.mapStyle.getMapboxLayers();
    this._removeMapboxLayers(mapboxLayers);
    //@ts-ignore
    this._removeMapboxSources(mapboxSources);
  }
  private _removeMapboxLayers(layers: mapboxgl.Layer[] | undefined) {
    if (layers && layers.length > 0) {
      layers.forEach((l) => {
        this.map?.getLayer(l.id) && this.map?.removeLayer(l.id);
      });
    }
  }
  private _removeMapboxSources(sources: mapboxgl.Sources | undefined) {
    if (!sources) return;
    const sourceKeys = Object.keys(sources);
    if (sourceKeys.length > 0) {
      sourceKeys.forEach((k) => {
        this.map?.getSource(k) && this.map?.removeSource(k);
      });
    }
  }
  private _addMapboxResources(style: mapboxgl.Style) {
    const preLayers = this.getLayers();
    //@ts-ignore
    const beforeLayerId = this._getBeforeLayerId(preLayers);
    const layers = style.layers;
    const sources = style.sources;
    this._addMapboxSources(sources);
    this._addMapboxLayers(layers, beforeLayerId);
  }
  private _addMapboxLayers(
    layers: mapboxgl.AnyLayer[] | undefined,
    beforeLayerId?: string,
  ) {
    if (layers && layers.length > 0) {
      layers.forEach((l) => {
        this.map?.addLayer(l, beforeLayerId);
      });
    }
  }
  private _addMapboxSources(sources: mapboxgl.Sources | undefined) {
    if (!sources) return;
    const sourceKeys = Object.keys(sources);
    if (sourceKeys.length > 0) {
      sourceKeys.forEach((k) => {
        this.map?.addSource(k, sources[k]);
      });
    }
  }
  private _updateMapView(style: mapboxgl.Style) {
    // @ts-ignore
    const { zoom, center, pitch, bearing, minZoom, maxZoom } = style;
    minZoom && this.map?.setMinZoom(minZoom);
    maxZoom && this.map?.setMaxZoom(maxZoom);
    zoom && this.map?.setZoom(zoom);
    pitch && this.map?.setPitch(pitch);
    bearing && this.map?.setBearing(bearing);
    //@ts-ignore
    center && this.map?.setCenter(center);
  }
  private _getBeforeLayerId(preLayers: mapboxgl.AnyLayer[] | undefined) {
    const mapboxLayers = this.map?.getStyle().layers;
    if (!mapboxLayers || (mapboxLayers && mapboxLayers.length === 0))
      return undefined;
    if (!preLayers || (preLayers && preLayers.length === 0)) {
      return mapboxLayers[0].id;
    }
    const lastPreLayerId = preLayers[preLayers.length - 1].id;
    const layerIndex = mapboxLayers.findIndex((l) => l.id === lastPreLayerId);
    if (layerIndex !== -1) {
      if (layerIndex === mapboxLayers.length - 1) {
        return undefined;
      } else {
        return mapboxLayers[layerIndex + 1].id;
      }
    } else {
      return undefined;
    }
  }

  /**
   * 初始化渲染图层
   */
  private _initJustRender() {
    const justSources = this.mapStyle?.getNonMapboxSources();
    // this._updateSource();// TODO 这里为何执行updateSource()
    if (justSources) {
      Object.keys(justSources).forEach((k) => {
        this._addJustSource(k, justSources[k]);
      });
    }
    const layers = this.getLayers();
    const reverseLayers = clone(layers).reverse();
    reverseLayers.forEach((l: mapboxgl.Layer, i: number) => {
      let beforeLayerId: string | undefined;
      beforeLayerId = i === 0 ? undefined : reverseLayers[i - 1].id;
      if (
        beforeLayerId &&
        this.getLayer(beforeLayerId)?.type === 'just_cluster'
      ) {
        // 如果是聚合图，是复合图层，需要把复合图层最上面的图层id暴露出去
        beforeLayerId = `${beforeLayerId}-clusterCircle`;
      }
      //@ts-ignore
      this._addMapLayer(l, beforeLayerId);
      // if (!isMapboxLayer(l) || isCusotmTileLayer(l,this.getStyle())) {

      //   //@ts-ignore
      //   this._addJustLayer(l, beforeLayerId);
      // }
    });
    // todo 添加所有自用图层，
    // 方案1
    // this.on('source.data', (srouceId) => {
    // sourceId => renderLayer
    // 每个layer都是一个class

    //   const renderLayers = getJustLayer.filter(sourceId)
    //   renderLayers.foreach(() => new RenderLaeyr().addTo(map)}
    // })

    // 方案2
    // JustLayer{
    //   on('source.load', (id) => {
    //     if(id === this.soruceId){
    // 如果已经加到了地图上， 就先移除
    //       this.addTo(map1)
    //     }
    //   } )
    // }
  }
  /**
   * style中layer如果存在spatialFilter字段，则执行空间过滤
   */
  private _initSpatialFilter() {
    // 设置空间过滤后，每次移动屏幕或者缩放渲染的要素都会发生变化，所以需要重新触发空间过滤
    this.map?.on(
      'moveend',
      debounce(
        this._handleLayerSpatialList.bind(this, this.spatialFilterEventsList),
        500,
      ),
    );
    // this.on('style.load', () => {
    const style = this.mapStyle?.getMapboxStyle();
    if (!style) {
      return;
    }
    const layers = style.layers;
    layers?.forEach((l: MapboxLayer) => {
      const spatialFilter = l.spatialFilter;
      if (spatialFilter) {
        const layerId = l.id;
        this.setSpatialFilter(layerId, spatialFilter);
      }
    });
    // });
  }

  /**
   * 异步加载数据后渲染just图层
   * @param id
   * @param source
   * @param callback 获取数据后执行的方法  渲染react组件  ||  纯js  addLayer
   */
  private _addJustSource(id: string, source: JustSourceData) {
    const { options, interval, ...otherSourceOptions } = source;
    // 如果是geojson数据，则先添加一个空的geojson数据源
    source.type === 'geojson' &&
      !this.map?.getSource(id) &&
      this.map?.addSource(id, {
        ...otherSourceOptions,
        type: 'geojson',
        data: { type: 'FeatureCollection', features: [] },
      });

    const justSource =
      source.scheme && Object.values(SchemeType).includes(source.scheme)
        ? new CustomTileSource(id, otherSourceOptions, this)
        : new Source(id, source, this, this.baseUrl, this.token, this.map);
    this.sourceCaches?.addSourceCache(id, justSource);
  }
  /**
   * style添加图层
   * @param layer
   * @param beforeId
   * @returns JustMap
   * @example
   *  map.addLayer(layer, { center: true })
   */
  addLayer(layer: mapboxgl.AnyLayer, beforeId?: string) {
    if (this.getLayer(layer.id)) {
      log.error('layer', layer.id, 'is already exist');
      return;
    }
    // custom类型传入的是一个class， 无法写入到style文件
    if (layer.type === 'custom') {
      this.map?.addLayer(layer, beforeId);
      return;
    }
    if (typeof layer.source === 'object') {
      this.addSource(layer.id, layer.source);
      layer.source = layer.id;
    }
    if (
      typeof layer.source === 'string' &&
      !this.mapStyle?.getSource(layer.source)
    ) {
      log.error(layer.source, 'is not exist');
      return;
    }
    this._addMapLayer(layer, beforeId);
    this.mapStyle?.addLayer(layer, beforeId);
    return this;
  }
  /**
   * 获取style图层
   * @param layerId 图层id
   * @returns 图层
   */
  getLayer(layerId: string) {
    const styleLayer = this.mapStyle?.getLayer(layerId);
    return styleLayer ? styleLayer : this.map?.getLayer(layerId);
  }
  // 获取style文件的图层
  getLayers() {
    return this.mapStyle?.getLayers();
  }
  /**
   * 删除图层
   * @param id string
   */
  removeLayer(id: string) {
    const layer = this.getLayer(id);
    if (!layer) {
      log.error('layer', id, 'not exit');
      return;
    }
    if (isMapboxLayer(layer)) {
      this.map?.removeLayer(id);
      //@ts-ignore
      layer.spatialFilter && this.setSpatialFilter(id, undefined);
    } else {
      this._removeJustLayer(id);
    }
    this.mapStyle?.removeLayer(id);
    this.removeAllInteraction(id); // 移除图层交互
  }

  moveLayer(id: string, beforeId?: string) {
    const layer = this.getLayer(id);
    if (!layer) {
      log.error('layer', id, 'not exit');
      return;
    }
    if (isMapboxLayer(layer)) {
      this.map?.moveLayer(id, beforeId);
    } else {
      const justLayer = this._justLayers[id];
      if (justLayer) {
        justLayer.move(beforeId);
      }
    }
    //TODO 更新style信息 如何更改 待定 groups
  }
  /**
   *设置指定样式图层的缩放范围
   * @param layerId 需要应用缩放范围的图层 ID
   * @param minzoom 设置的最小缩放值（0-24）
   * @param maxzoom 设置的最大缩放值（0-24）
   */
  setLayerZoomRange(layerId: string, minzoom: number, maxzoom: number) {
    const layer = this.getLayer(layerId);
    if (!layer) {
      log.error('layer', layerId, 'not exit');
      return;
    }
    if (isMapboxLayer(layer)) {
      this.map?.setLayerZoomRange(layerId, minzoom, maxzoom);
    } else {
      const justLayer = this._justLayers[layerId];
      if (justLayer) {
        justLayer.setLayerZoomRange(layerId, minzoom, maxzoom);
      }
    }
    this.mapStyle?.setLayerZoomRange(layerId, minzoom, maxzoom);
  }

  /**
   * 返回包含地图 <canvas> 标签的 HTML 元素
   *
   */
  getCanvasContainer() {
    return this.map?.getCanvasContainer();
  }
  /**
   *返回地图的 HTML 嵌套元素。
   */
  getContainer() {
    return this.map?.getContainer();
  }
  /**
   * 投影
   * @param point
   */
  project(point: PointLike) {
    return this.map?.project(point);
  }
  /**
   * 设置paint属性
   * @param layerId 图层id
   * @param name 属性名称
   * @param value 属性值
   */
  setPaintProperty(layerId: string, name: string, value: any) {
    const layer = this.getLayer(layerId);
    if (!layer) {
      log.error(`layer ${layerId} is not exist`);
      return;
    }
    if (isMapboxLayer(layer)) {
      this.map?.setPaintProperty(layerId, name, value);
      return;
    }
    log.error(`layer ${layerId} does not support setPaintProperty`);
  }

  /**
   * 设置layout属性
   * @param layerId 图层id
   * @param name 属性名称
   * @param value 属性值
   */
  setLayoutProperty(layerId: string, name: string, value: any) {
    const layer = this.getLayer(layerId);
    if (!layer) {
      log.error(`layer ${layerId} is not exist`);
      return;
    }
    if (isMapboxLayer(layer)) {
      this.map?.setLayoutProperty(layerId, name, value);
      return;
    }
    log.error(`layer ${layerId} does not support setLayoutProperty`);
  }
  /**
   * 更改style中图层信息
   * @param layerId 图层id
   * @param options 图层属性
   */
  setLayerProperty(layerId: string, options: any) {
    const layer = this.getLayer(layerId);
    if (!layer) {
      log.error('layer', layerId, 'not exit');
      return;
    }
    if (isMapboxLayer(layer)) {
      // mapbox图层
      if (options.layout) {
        Object.keys(options.layout).forEach((key) => {
          this.map?.setLayoutProperty(layerId, key, options.layout[key]);
        });
      }
      if (options.paint) {
        const layer = this.getLayer(layerId);
        //@ts-ignore
        const sourceId = layer.source as string;
        const sources = this.getStyle()?.sources;
        const styleSource = sources && sources[sourceId];
        //@ts-ignore
        const scheme = styleSource?.scheme;
        if (Object.values(SchemeType).includes(scheme)) {
          // 是切片图层
          const customTileLayer = this._justLayers[layerId] as CustomTileLayer;
          if (!customTileLayer) return;
          const { 'raster-opacity': rasterOpacity } = options.paint;
          if (rasterOpacity === undefined) return;
          customTileLayer.setOpacity(rasterOpacity);
          return;
        }
        Object.keys(options.paint).forEach((key) => {
          this.map?.setPaintProperty(layerId, key, options.paint[key]);
        });
      }
    } else {
      // 非mapbox图层
      const justLayer = this._justLayers[layerId];
      if (justLayer) {
        justLayer.setProperty(options);
      }
    }

    this.mapStyle?.setLayerProperty(layerId, options);
  }

  /**
   * 地图增加图层
   * @param layer 图层
   * @param options 配置
   */
  private _addMapLayer(layer: mapboxgl.AnyLayer, beforeId?: string) {
    // const { before, center } = options;
    // TODO
    // @ts-ignore 还没加just的layer的说明
    // if (!isMapboxLayer(layer) && center) {
    // this.once('source.load', (e: EventData | undefined) => {
    //   // @ts-ignore ts推断问题
    //   const josn = this.getSource(e?.id)?.getData();
    //   if (!josn) return;
    //   const bounds = bbox(josn);
    //   // @ts-ignore ts推断错误
    //   this.map.fitBounds(bounds);
    // });
    // }  SchemeType
    const sourceId = layer.source as string;
    const sources = this.getStyle()?.sources;
    const styleSource = sources && sources[sourceId];
    //@ts-ignore
    const scheme = styleSource?.scheme;
    if (Object.values(SchemeType).includes(scheme)) {
      const source = this.sourceCaches?.getSourceCache(sourceId);
      if (!source) return;
      if (source instanceof Source) return;
      this._addCustomTileLayer(layer, source, beforeId);
      return;
    }
    if (isMapboxLayer(layer)) {
      this.map?.addLayer(layer, beforeId);
      return;
    }
    this._addJustLayer(layer, beforeId);
  }
  private _addJustLayer(
    layer: mapboxgl.AnyLayer,
    beforeId: string | undefined,
  ) {
    //@ts-ignore
    const justLayer = new LAYER_TYPE[layer.type](
      this.map,
      layer,
      this.sourceCaches,
      this.baseUrl,
    );
    this._justLayers[layer.id] = justLayer;
    this._justLayers[layer.id].add(beforeId);
  }
  private _addCustomTileLayer(
    layer: mapboxgl.AnyLayer,
    source: CustomTileSource,
    beforeId?: string,
  ) {
    const tileLayer = new CustomTileLayer(layer, source);
    this._justLayers[layer.id] = tileLayer;
    //@ts-ignore
    this.map?.addLayer(tileLayer, beforeId);
  }
  /**
   * 移除just图层
   */
  private _removeJustLayer(id: string) {
    const justLayer = this._justLayers[id];
    if (justLayer) {
      justLayer.remove();
    }
    delete this._justLayers[id];
  }
  /**
   * 增加数据源
   * @param id sourceId
   * @param source styleSource
   */
  addSource(id: string, source: mapboxgl.AnySourceData | JustSourceData) {
    if (this.getStyle()?.sources?.[id]) {
      log.error(`source ${id} 已经存在`);
      return;
    }
    this.mapStyle?.addSource(id, source);
    this._addMapSource(id, source);
  }
  /**
   * @param id sourceid
   * @param source source
   */
  private _addMapSource(
    id: string,
    source: mapboxgl.AnySourceData | JustSourceData,
  ) {
    // @ts-ignore
    if (isMapboxSource(source)) {
      // @ts-ignore
      this.map?.addSource(id, source);
    } else {
      // @ts-ignore
      this._addJustSource(id, source);
    }
  }
  /**
   * 获取数据源
   * @param id sourceId
   * @returns source
   */
  getSource(id: string) {
    const style = this.mapStyle?.getStyle();
    if (!style) return;
    const justSource = getJustSources(style);
    // 返回mapbox-gl的source
    if (!(id in justSource)) {
      return this.map?.getSource(id);
    }
    // srouce cache 存储source 实例
    const sourceReq = this.sourceCaches?._cacheSource[id];
    if (!sourceReq) {
      log.warn(`can not find source with id ${id}`);
      return;
    }
    return this.sourceCaches?._cacheSource[id];
  }

  /**
   * 删除style数据源
   * @param id sourceId
   */
  removeSource(id: string) {
    const style = this.mapStyle?.getStyle();
    if (!style) return;
    // just source
    const layers = this.mapStyle.getLayers();
    const layer = layers.find((l: mapboxgl.Layer) => {
      if (typeof l.source === 'string') {
        return l.source === id;
      }
      return l.id === id;
    });
    if (layer) {
      log.error(
        `Source "${id}" cannot be removed while layer "${layer.id}" is using it.`,
      );
      return;
    }
    const justSource = getJustSources(style);
    this.mapStyle?.removeSource(id);
    if (!(id in justSource)) {
      this.map?.removeSource(id);
      return;
    }
    if (justSource[id].type === 'geojson') this.map?.removeSource(id);
    this.sourceCaches?.removeCacheSource(id);
  }

  /**
   * 获取style
   */
  getStyle() {
    return this.mapStyle?.getStyle();
  }

  static getMapbxStyle(style: mapboxgl.Style | JustStyle) {
    return getMapboxStyle(style);
  }

  /**
   * 获取scene
   */
  // getScene() {
  //   return this.mapStyle?.getScene();
  // }
  // setScene(scene: JustStyle | string) {
  //   return this.mapStyle?.setScene(scene);
  // }
  // getMapStyle() {
  //   return this.mapStyle?.getMapStyle();
  // }

  /**
   * 设置属性过滤
   */
  setFilter(layerId: string, filter: any[], options: any = {}) {
    const layer = this.getLayer(layerId);
    if (!layer) {
      log.error('layer', layerId, 'not exit');
      return;
    }
    if (!isMapboxLayer(layer)) {
      const justLayer = this._justLayers[layerId];
      if (justLayer) {
        justLayer.setFilter(filter);
      }
    } else {
      this.setSpatialFilter(layerId, undefined); // 清空空间过滤
      this.map?.setFilter(layerId, filter, options); // 更新地图显示
    }
    this.mapStyle?.setSpatialFilter(layerId, undefined);
    this.mapStyle?.setFilter(layerId, filter); // 更新style文件
  }
  /**
   * 获得属性过滤
   */
  getFilter(layerId: string) {
    const layer = this.getLayer(layerId) as mapboxgl.Layer | undefined;
    if (!layer) {
      log.error('layer', layerId, 'not exit');
      return;
    }
    return layer.filter;
  }
  /**
   * 设置空间过滤setSpatialFilter 支持wkt和geojson  数组
   * 1.server发布的图层 具有just_global_id 唯一id,通过setFilter间接进行空间过滤
   * 2.不是server发布的图层 则通过新建addlayer 间接实现空间过滤
   */
  setSpatialFilter(layerId: string, spatialFilter: SpatialFilter) {
    const layer = this.getLayer(layerId);
    if (!layer) {
      log.error('layer', layerId, 'not exit');
      return;
    }
    if (!isMapboxLayer(layer)) {
      log.error('layer', layerId, 'is not mapbox layer');
      return;
    }
    if (spatialFilter === undefined || spatialFilter === null) {
      if (this.spatialFilterEventsList[layerId]) {
        delete this.spatialFilterEventsList[layerId]; // 删除绑定在layer上的空间过滤函数
        this.spatialFilterWorkerList[layerId].terminate(); // 停止绑定在layer上的空间过滤的worker对象
        delete this.spatialFilterWorkerList[layerId]; // 删除绑定在layer上的空间过滤的worker对象
        this.map?.setFilter(layerId, spatialFilter);
        if (this.map?.getLayer(`spatialFilter_temp_${layerId}`)) {
          this.map.removeLayer(`spatialFilter_temp_${layerId}`);
          this.map.removeSource(`spatialFilter_temp_${layerId}`);
          this.map.setLayoutProperty(layerId, 'visibility', 'visible');
        }
      }
      this.mapStyle?.setSpatialFilter(layerId, spatialFilter);
      return;
    }
    if (spatialFilter[0] !== 'contains' && spatialFilter[0] !== 'crosses') {
      log.error('spatial relation', spatialFilter[0], 'not supported');
      return;
    }

    this._handleSpatialFilter(layerId, spatialFilter);
    this.spatialFilterEventsList[layerId] = () => {
      this._handleSpatialFilter(layerId, spatialFilter);
    };
    this.mapStyle?.setSpatialFilter(layerId, spatialFilter);
  }
  private _handleLayerSpatialList(eventsList: any) {
    Object.keys(eventsList).forEach((k: string) => {
      // @ts-ignore
      this.spatialFilterEventsList[k]();
    });
  }
  /**
   * 支持wkt和GeoJSON.Feature|GeoJSON.Geometry 支持多个
   * @param layerId 图层id
   * @param spatialFilter 空间过滤 ['contains|intersects',[geometry1,geometry2....]]
   */
  private _handleSpatialFilter(
    layerId: string,
    spatialFilter: RealSpatialFilter,
  ) {
    // this.map.setFilter(layerId, undefined);
    const layer = this.getLayer(layerId) as MapboxLayer;
    const sourceId = layer.source as string;
    const sourceLayerId = layer['source-layer'];
    const features = this.map?.querySourceFeatures(sourceId, {
      sourceLayer: sourceLayerId || sourceId,
    });
    if (!(features && features.length)) {
      return;
    }
    // @ts-ignore
    const newFeatures = features.map((f) => ({ ...f, geometry: f.geometry })); // 将features直接传入worker中会丢失geometry信息
    this._handleSpatialWorker(layerId, newFeatures, spatialFilter);
  }
  /**
   * 通过新建图层的方式执行空间过滤
   * @param layerId 图层id
   * @param featuresList 要素列表
   */
  private _handleSpatialFilterByLayer(
    layerId: string,
    featuresList: GeoJSON.Feature[],
  ) {
    const data = {
      type: 'FeatureCollection',
      features: featuresList,
    };
    const tempLayerId = `spatialFilter_temp_${layerId}`;
    if (this.map?.getLayer(tempLayerId)) {
      //  已经有空间过滤临时图层
      // @ts-ignore
      this.map.getSource(tempLayerId).setData(data);
    } else {
      const layers = this.getLayers();
      if (layers && layers.length > 0) {
        let beforeLayerId: string | undefined;
        const layer = clone(this.getLayer(layerId));
        delete layer['source-layer'];
        const layerIndex = layers.findIndex((l) => l.id === layerId);
        if (layerIndex !== -1 && layerIndex !== layers.length - 1) {
          // 图层是最后一个
          beforeLayerId = layers[layerIndex + 1].id;
        }
        const geojsonSource = {
          type: 'geojson',
          data,
        };
        const tempLayer = { ...layer, id: tempLayerId, source: geojsonSource };
        delete tempLayer.spatialFilter;
        this.map?.addLayer(tempLayer, beforeLayerId);
        this.map?.setLayoutProperty(layerId, 'visibility', 'none');
      }
    }
  }
  /**
   * 通过属性过滤的方式执行空间过滤
   * @param layerId 图层id
   * @param featureIdList 要素ID列表
   */
  private _handleSpatialFilterByFilter(layerId: string, featureIdList: any[]) {
    // @ts-ignore
    const toFilter = ['in', 'just_global_id'].concat(Array.from(featureIdList));
    this.map?.setFilter(layerId, toFilter); // 执行过滤
  }
  /**
   * 通过turf执行空间过滤  放在worker中进行
   * @param layerId 图层Id
   * @param features map.querySourceFeatures 获取到的屏幕所有features
   */
  private _handleSpatialWorker(
    layerId: string,
    features: GeoJSON.Feature[],
    spatialFilter: RealSpatialFilter,
  ) {
    const isJustGlobalIdExist =
      features[0].properties &&
      features[0].properties.just_global_id !== undefined; // 判断是否有just_global_id这个唯一key值
    if (!this.spatialFilterWorkerList[layerId]) {
      this.spatialFilterWorkerList[layerId] = new Worker(SpatialWorker);
    }
    this.spatialFilterWorkerList[layerId].postMessage({
      features,
      spatialFilter,
      isJustGlobalIdExist,
    });
    this.spatialFilterWorkerList[layerId].onmessage = (e: any) => {
      const { featureIdList, featuresList } = e.data;
      if (isJustGlobalIdExist) {
        this._handleSpatialFilterByFilter(layerId, featureIdList);
      } else {
        this._handleSpatialFilterByLayer(layerId, featuresList);
      }
    };
  }
  /**
   * 获得空间过滤
   */
  getSpatialFilter(layerId: string) {
    const layer = this.getLayer(layerId) as MapboxLayer | undefined;
    if (!layer) {
      log.error('layer', layerId, 'not exit');
      return;
    }
    return layer.spatialFilter;
  }
  /**
   * @param control 添加control
   * 区分通过mapbox原生方式添加control还是通过react方式添加control
   */
  addControl(control: any, position?: MapboxControlPosition) {
    //包含type属性表示该Control属于自定义
    if (control.type) {
      if (control.component) {
        Object.assign(this._controlComponent, {
          //@ts-ignore
          [control.type]: control.component,
        });
      }
      // 如果没有scene 用户仍然可以增加control
      const { id, type, options } = control;
      if (this.mapStyle?.getControl(id)) {
        log.error('control', id, 'alreald exist');
        return;
      }
      if (MAPBOX_CONTROL_TYPE_LIST.find((mc) => mc.type === type)) {
        const { position } = options || {};
        // @ts-ignore
        const mapboxControl: mapboxgl.IControl | mapboxgl.Control =
          Control.Create(type, options);
        this.map?.addControl(mapboxControl, position);
      }
      this.mapStyle?.addControl(control);
    } else {
      // 通过const control = new mapboxgl.Control()的方式新建的组件
      // 或者通过 按照mapbox标准控件类 onAdd onRemove方式新建的组件
      //添加进来后 不改变style文件
      this._addControl(control, position);
    }

    // this.map.addControl(control);
  }
  // 实现mapbx原生的addControl方法 control中的事件触发通过map实现 onAdd(JustMap)
  _addControl(control: mapboxgl.IControl, position?: MapboxControlPosition) {
    if (position === undefined && control.getDefaultPosition) {
      //@ts-ignore
      position = control.getDefaultPosition();
    }
    if (position === undefined) {
      position = 'top-right';
    }
    if (!control || !control.onAdd) {
      log.error(
        'Invalid argument to map.addControl(). Argument must be a control with onAdd and onRemove methods.',
      );
      return;
    }
    //@ts-ignore 这里map和justmap不一致导致ts检查不通过
    const controlElement = control.onAdd(this.map);
    //@ts-ignore map上维护的私有变量
    this.map?._controls.push(control);
    //@ts-ignore map上维护的私有变量
    const positionContainer = this.map?._controlPositions[position];
    if (position.indexOf('bottom') !== -1) {
      positionContainer.insertBefore(
        controlElement,
        positionContainer.firstChild,
      );
    } else {
      positionContainer.appendChild(controlElement);
    }
    return this;
  }

  // 实现mapbx原生的addControl方法 control中的事件触发通过Justmap实现 onAdd(JustMap)
  // 与_addControl的区别是 new mapboxgl.FullscreenControl 时 内部会用mapbox的map实例的一些私有属性
  // 而这些属性没有暴露在justMap中 导致报错
  // 当draw或者measure这些justmap内部封装的组件  就直接调用_addJustControl方法
  _addJustControl(
    control: mapboxgl.IControl,
    position?: MapboxControlPosition,
  ) {
    if (position === undefined && control.getDefaultPosition) {
      //@ts-ignore
      position = control.getDefaultPosition();
    }
    if (position === undefined) {
      position = 'top-right';
    }
    if (!control || !control.onAdd) {
      log.error(
        'Invalid argument to map.addControl(). Argument must be a control with onAdd and onRemove methods.',
      );
      return;
    }
    //@ts-ignore 这里map和justmap不一致导致ts检查不通过
    const controlElement = control.onAdd(this);
    //@ts-ignore map上维护的私有变量
    this.map?._controls.push(control);
    //@ts-ignore map上维护的私有变量
    const positionContainer = this.map?._controlPositions[position];
    if (position.indexOf('bottom') !== -1) {
      positionContainer.insertBefore(
        controlElement,
        positionContainer.firstChild,
      );
    } else {
      positionContainer.appendChild(controlElement);
    }
    return this;
  }
  /**
   *
   * @param control 移除control
   */
  removeControl(id: string | mapboxgl.IControl | mapboxgl.Control) {
    // 如果没有scene 用户仍然可以control
    if (typeof id !== 'string') {
      this.map?.removeControl(id);
      return;
    }
    if (!this.mapStyle?.getControl(id)) {
      log.error('control', id, 'not exist and can not be removed');
      return;
    }
    const mapboxControls = this.getStyle()?.controls?.filter((c) =>
      MAPBOX_CONTROL_TYPE_LIST.find((mc) => mc.type === c.type),
    );
    const customControlLength = mapboxControls ? mapboxControls.length : 0; // 获取用户自己添加的mapbox原生组件
    // @ts-ignore
    const mapboxControlObjs = this.map._controls; // 获得mapbox的map实例上维护的_controls数组 初始化过程中可能存在AttributonControl和LogoControl
    const initMapboxControlLength =
      mapboxControlObjs.length - customControlLength;
    const mapboxControlIndex = mapboxControls?.findIndex((c) => c.id === id);
    if (mapboxControlIndex !== undefined && mapboxControlIndex !== -1) {
      const mapboxControl =
        mapboxControlObjs[mapboxControlIndex + initMapboxControlLength];
      this.map?.removeControl(mapboxControl);
    }
    this.mapStyle?.removeControl(id);
    // this.map.addControl(control);
  }
  getControl(id: string) {
    return this.mapStyle?.getControl(id);
  }
  setControlProperty(id: string, options: any, isUpdate?: boolean) {
    if (!this.mapStyle?.getControl(id)) {
      log.error('control', id, 'not exist and can not be update');
      return;
    }
    this.mapStyle.setControlProperty(id, options, isUpdate);
  }
  /**
   * 设置地图的interaction 图层id
   */
  // addMapInteraction(eventId: string, type: EVENT_TYPE, options: any) {
  //   this.mapStyle?.addInteraction(eventId, type, options);
  // }
  addControlInteraction(id: string) {}
  addInteraction(
    layerId: string,
    eventId: string,
    type: keyof MapLayerEventType,
    actionOption: LayerInteractionOption,
  ) {
    this.interactionManager?.addInteraction(
      layerId,
      eventId,
      type,
      actionOption,
    );
  }
  /**
   * 移除图层全部交互
   * @param layerId
   * @returns
   */
  removeAllInteraction(layerId: string) {
    const layerInteractionList = this.getStyle()?.interactions;
    if (!layerInteractionList || layerInteractionList.length === 0) return;
    const layerInteraction = layerInteractionList.filter(
      (l) => l.id === layerId,
    );
    if (layerInteraction.length > 0) {
      const eventList = layerInteraction[0].eventList;
      eventList.forEach((l) => {
        this.removeInteraction(layerId, l.id);
      });
    }
  }
  /**
   * 移除图层某个交互
   * @param layerId
   * @param eventId
   * @returns
   */
  removeInteraction(layerId: string, eventId: string) {
    this.interactionManager?.removeInteraction(layerId, eventId);
  }

  getGroups() {
    return this.mapStyle?.getGroups();
  }
  getGroup(id: string) {
    return this.mapStyle?.getGroup(id);
  }
  setGroupProperty(id: string, options: any) {
    const group = this.getGroup(id);
    if (
      group &&
      group?.metadata?.['just:isGroup'] &&
      // @ts-ignore
      Array.isArray(group.layers)
    ) {
      // @ts-ignore
      group.layers.forEach((layer) => {
        this.setLayerProperty(layer.id, options);
      });
    }
    this.mapStyle?.setGroupProperty(id, options);
  }
  /**
   * 给样式添加图像
   * @param id
   * @param image
   * @param options
   */
  addImage(id: string, image: any, options: any) {
    this.map?.addImage(id, image, options);
  }
  /**
   * 判断该图像是否已经被添加。
   * @param id
   */
  hasImage(id: string) {
    return this.map?.hasImage(id);
  }
  /**
   * 使用 Map#addImage 从外部 URL 载入图像
   * @param url
   * @param callback
   */
  loadImage(url: string, callback: Function) {
    //@ts-ignore
    this.map?.loadImage(url, callback);
  }
  /**
   * 从样式中移除图像
   * @param id
   */
  removeImage(id: string) {
    this.map?.removeImage(id);
  }
  /**
   * 监听事件 mapbox原生走原生 justMap走justmap
   * @param type
   * @param listener
   * @returns
   */
  on(type: MapboxEventType | EventType, layerId: any, listener?: any) {
    if (listener === undefined) {
      // @ts-ignore
      if (mapboxEventTypeList.includes(type)) {
        this.map?.on(type, layerId);
        return;
      }
      // @ts-ignore
      this.over(type, layerId);
    } else {
      // @ts-ignore
      this.map?.on(type, layerId, listener);
    }
  }
  off(type: MapboxEventType | EventType, layerId: any, listener?: any) {
    if (listener === undefined) {
      // @ts-ignore
      if (mapboxEventTypeList.includes(type)) {
        this.map?.off(type, layerId);
        return;
      }
      // @ts-ignore
      this.cancel(type, layerId);
    } else {
      // @ts-ignore
      this.map?.off(type, layerId, listener);
    }
  }
  /**
   * 返回一个GeoJSON Feature objects数组，这些对象表示满足查询参数的可见要素。
   * @param geometry [number,number]|[[number,number],[number,number]] 传递一个点或者一个范围
   * @param options layers 可选 只返回这些图层的要素['layerId1','layerId2']; filter 可选 限制查询结果; validtate 可选 filter格式校验;
   */
  queryRenderedFeatures(
    geometry?: PointLike | [PointLike, PointLike],
    options?: Object,
  ) {
    return this.map?.queryRenderedFeatures(geometry, options);
  }
  /**
   * 返回地图的 <canvas> 元素
   * @returns 地图的 <canvas> 元素
   */
  getCanvas() {
    return this.map?.getCanvas();
  }
  /**
   * 不用动态转换的情况下改变中心点、 缩放级别、方位角和倾斜度的任意组合。地图将保留 options 没有指定的当前值
   * @param options 描述转换目标和动态效果的选项
   * @param eventData 该方法触发的事件对象需要添加的其它属性
   * @returns JustMap地图实例
   */
  jumpTo(options: any, eventData: any) {
    this.map?.jumpTo(options, eventData);
    return this;
  }
  /**
   * 使用动态转换，将中心点、缩放级别、方位角和倾斜度组合的原有数值改为新数值。地图将保留 options 没有指定的当前值。
   * @param options 描述转换目标和动态效果的选项
   * @param eventData 该方法触发的事件对象需要添加的其它属性
   * @returns JustMap地图实例
   */
  easeTo(options: any, eventData: any) {
    this.map?.easeTo(options, eventData);
    return this;
  }
  /**
   * 对地图中心、缩放级别、方位角和倾斜度做任意组合改变， 使其沿着一条曲线动态地变化并引发飞行效果。该动态转换能够无缝引入缩放和平移，使用户即使在穿越了很长的距离后也能保持方位角不变
   * @param options 描述转换目标和动态效果的选项
   * @param eventData 该方法触发的事件对象需要添加的其它属性
   * @returns JustMap地图实例
   */
  flyTo(options: any, eventData: any) {
    this.map?.flyTo(options, eventData);
    return this;
  }
  /**
   * 在指定的地理边界内平移和缩放地图，以包含其可见区域。 当地图方位角不为 0 的时候，该函数会将方位角重置为 0。
   * @param bounds 地理范围
   * @param options 动画效果
   * @param eventData 该方法触发的事件对象需要添加的其它属性
   * @returns JustMap地图实例
   */
  fitBounds(bounds: any, options?: any, eventData?: any) {
    this.map?.fitBounds(bounds, options, eventData);
    return this;
  }
  /**
 * 返回一个GeoJSON Feature objects数组，这些对象表示满足查询参数的指定矢量切片或GeoJSON数据源中的要素。

 * @param sourceId 查询矢量切片或GeoJSON数据源的ID
 * @param options sourceLayer filter validate
 * @returns 
 */
  querySourceFeatures(sourceId: string, options?: Object) {
    return this.map?.querySourceFeatures(sourceId, options);
  }
  /**
* 设置要素的状态。状态 对象会与要素的现有状态合并。

* @param feature feature 要素
* @param options 一个键值对集合。其中值应该是有效的 JSON 类型。此方法要求数据集中必须有 feature.id 属性。对于没有 feature ids 的 GeoJSON 数据源， 设置 generateIds 选项（在 GeoJSONSourceSpecification 中）以自动分配
* @returns 
*/
  setFeatureState(feature: mapboxgl.MapboxGeoJSONFeature, options: Object) {
    this.map?.setFeatureState(feature, options);
    return this;
  }
  /**
   * 移除要素状态，将其设置回默认行为。如果只有数据被指定，移除该数据源的所有状态。如果 target.id 也被指定，移除该要素状态的所有键。 如果键也被指定，从该要素状态中移除指定的键。
   * @param target 设置状态位置的标识：可以是一个数据源、一个属性、或者一个属性指定的键
   * @param key （可选） 需要重置的要素状态的键
   * @returns
   */
  removeFeatureState(
    target: mapboxgl.FeatureIdentifier | mapboxgl.MapboxGeoJSONFeature,
    key: string,
  ) {
    this.map?.removeFeatureState(target, key);
    return this;
  }
  /**
* 触发一个显示框的渲染。使用自定义图层时，当图层发生改变，使用此方法去重渲染。 在下一个显示框渲染前多次调用此方法也只会渲染一次。

*/
  triggerRepaint() {
    this.map?.triggerRepaint();
  }
  /**
   * 调整地图大小后立即触发该事件。
   */
  resize() {
    this.map?.resize();
    return this;
  }
  /**
   * 设置倾斜角度
   * @param pitch 需要设置的倾斜度，按照偏离屏幕水平面的度数计算（0-60）。
   * @param eventData 该方法触发的事件对象需要添加的其它属性。
   */
  setPitch(pitch: number, eventData?: Object) {
    this.map?.setPitch(pitch, eventData);
    return this;
  }
  /**
   * @returns 地图当前的倾斜度。
   */
  getPitch() {
    return this.map?.getPitch();
  }
  /**
   *
   * @param center 需要设置的中心点
   * @param eventData 该方法触发的事件对象需要添加的其它属性。
   */
  setCenter(center: LngLatLike, eventData?: Object) {
    this.map?.setCenter(center, eventData);
    return this;
  }
  /**
   *
   * @returns 地图的地理中心点
   */
  getCenter() {
    return this.map?.getCenter();
  }
  /**
   *
   * @param zoom 地图级别
   * @param eventData 该方法触发的事件对象需要添加的其它属性
   */
  setZoom(zoom: number, eventData?: Object) {
    this.map?.setZoom(zoom, eventData);
    return this;
  }
  /**
   *
   * @returns 地图当前的缩放级别
   */
  getZoom() {
    return this.map?.getZoom();
  }
  /**
   *
   * @param bearing 需要设置的方位角
   * @param eventData 该方法触发的事件对象需要添加的其它属性
   */
  setBearing(bearing: number, eventData?: Object) {
    this.map?.setBearing(bearing, eventData);
    return this;
  }
  /**
   *
   * @returns 返回地图当前的方位角。方位角是指南针方向的指向，例如地图方位角为 90° 对应正东指向。
   */
  getBearing() {
    return this.map?.getBearing();
  }
  /**
   * justmap资源加载完成状态
   * @returns boolean
   */
  loaded() {
    return this._mapLoaded;
  }
  addDraw(styles?: object[] | undefined, mode?: Object) {
    if (this._draw) {
      log.error(
        'draw instance and measure instance can not both exist,pls remove measure instance first',
      );
      return;
    }
    if (this._measure) {
      log.error('measure instance has been exist');
      return;
    }
    //@ts-ignore
    this._draw = new Draw(styles, mode).addTo(this);
    return this._draw;
  }
  removeDraw() {
    if (this._draw) {
      //@ts-ignore
      this._draw?.remove();
      // 解绑draw之前绑定的所有事件
      DRAE_EVENT_TYPE_LIST.forEach((eventType) => {
        //@ts-ignore
        this._removeAllListeners(eventType);
      });
      this._draw = undefined;
    }
  }
  addMeasure() {
    if (this._draw) {
      log.error(
        'draw instance and measure instance can not both exist,pls remove draw instance first',
      );
      return;
    }
    if (this._measure) {
      log.error('measure instance has been exist');
      return;
    }
    //@ts-ignore
    this._measure = new Measure().addTo(this);
    return this._measure;
  }
  removeMeasure() {
    if (this._measure) {
      //@ts-ignore
      this._measure?.remove();
      this._measure = undefined;
    }
  }
  /**
   *  解绑一个事件绑定的所有函数
   * @param type 事件类型
   */
  private _removeAllListeners(type: MapboxEventType | EventType) {
    //@ts-ignore
    this.removeEventAllListeners(type);
  }
}

// const handler: ProxyHandler<JustMap> = {
//   get(target, key: keyof mapboxgl.Map & keyof JustMap, value: any) {
//     // if (nonWrapMethods.includes(key)) {
//     // }
//     if (wrapMethods.includes(key)) {
//       return Reflect.get(target, key, value);
//     }
//     // @ts-ignore
//     return Reflect.get(target.map, key, value);
//   },
// };

// export class ProxyJustMap {
//   constructor(options: JustMapOptionType) {
//     const justMap = new JustMap(options);
//     return new Proxy(justMap, handler) as JustMap;
//   }
// }
