import getRequestInstance from 'src/utils/getRequestInstance';

// export interface GetDistinctValuesProps {
//   layerId: string; // 图层ID
//   fieldName: string; // 属性名称
// }
export interface GetJSONProps {
  serviceId: string; // 服务ID
  max: number; // 最大值
  colNames: string; // 指定返回的字段
  // baseUrl:string;
}

const baseAdapter = getRequestInstance();

// /**
//  *获取地图样式
//  */
// export const getMapStyleReq: (id: number) => Promise<string> = (id) =>
//   baseAdapter.get(`map/${id}`);

// // 获取数据源中某个字段的属性值(distinct)
// export const getDistinctValues: (
//   data: GetDistinctValuesProps,
// ) => Promise<any> = (data) => {
//   const { layerId, fieldName } = data;
//   return baseAdapter.post(
//     `/service/${layerId}/distinctValue?attrName=${fieldName}`,
//   );
// };

// // 获取数据源的字段(自发服务)
// export const getLayerField: (id: string) => Promise<string> = (id: string) =>
//   // @ts-ignore
//   baseAdapter.post(`/service/${id}/fieldList`);

// // 获取数据源字段(geoserver)
// export const getLayerSchema: (url: string) => Promise<string> = (url: string) =>
//   // @ts-ignore
//   baseAdapter.raw.post(url);

// 获取geojson数据
export const getJsonDataByUrl: (
  baseUrl: string,
  sdk: string,
  token: string | undefined,
  data: GetJSONProps,
) => Promise<any> = (
  baseUrl: string,
  sdk: string,
  token: string | undefined,
  data: GetJSONProps,
) => {
  const { serviceId, max, colNames } = data;
  const params = colNames ? { max, colNames } : { max };
  return baseAdapter.get(`/service/${serviceId}/geojson`, {
    params,
    baseURL: `${baseUrl}/api/gis-studio`,
    headers: {
      'map-sdk': sdk,
      'map-token': token,
    },
  });
};

// 获取geojson数据
export const getJustData: (
  url: string,
  baseUrl: string,
  token: string | undefined,
) => Promise<any> = (
  url: string,
  baseUrl: string,
  token: string | undefined,
) =>
  baseAdapter.get(url, {
    baseURL: `${baseUrl}/api/gis-studio`,
    // baseURL: `${baseUrl}`,
    headers: {
      'map-sdk': 'true',
      'map-token': token,
    },
  });

export const getFeaturesByClause: (
  id: string,
  query: string,
  baseUrl: string,
  sdk: string,
  token: string,
) => Promise<any> = (
  id: string,
  query: string,
  baseUrl: string,
  sdk: string,
  token: string,
) =>
  // const baseAdapter = getRequestInstance(`${baseUrl}/api/gis-studio`);
  baseAdapter.post(
    `/service/feature/query/${id}`,
    { query },
    {
      baseURL: `${baseUrl}/api/gis-studio`,
      headers: {
        'map-sdk': sdk,
        'map-token': token,
      }
    },
  );
