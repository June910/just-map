import getRequestInstance from 'src/utils/getRequestInstance';
import { AxiosRequestConfig } from 'axios';

const baseAdapter = getRequestInstance();

export interface RealtimeResponseItemType {
  id: string;
  geomType: number;
  name: string;
  geom: string;
}
export interface FetchPOIParams {
  moduleId: string;
  keyword: string;
  baseUrl: string;
  sdk: string;
  token: string;
}
export interface POIItemProps {
  // eslint-disable-next-line camelcase
  formatted_address?: string;
  country?: string;
  province?: string;
  city?: string;
  citycode?: number;
  district?: string;
  adcode?: number;
  location: string;
  score?: number;
  id: string;
  name: string;
  level1?: string;
  level2?: string;
  level3?: string;
}
export interface FetchPOIResultProps {
  count: number;
  from: number;
  size: number;
  geoCodeMsgList: POIItemProps[];
}
export interface RoutePlaneResultProps {
  ids: string;
  length: number;
  route: string;
}
/**
 * 获取所有的layer
 */
// export const getRealtimeData: (
//   url: string,
//   options: AxiosRequestConfig,
// ) => Promise<RealtimeResponseItemType[]> = (url, options) =>
//   // @ts-ignore
//   baseAdapter({
//     url,
//     ...options,
//   });

interface FenceDataProps {
  baseUrl: string;
  sdk: string;
  token: string;
  modelId: string;
  bbox: string;
  maxPointNum: number;
}
// 获取gisserver接口数据
export const getJustGisServerData: (
  url: string,
  baseUrl: string,
  options: {},
  token: string | undefined,
) => Promise<any> = (
  url: string,
  baseUrl: string,
  options: {},
  token: string | undefined,
) =>
  baseAdapter.post(url, options, {
    baseURL: `${baseUrl}`,
    headers: {
      'map-sdk': 'true',
      'map-token': token,
    },
  });
// export const getRealtimeData: (props: FenceDataProps) => Promise<any> = (
//   props: FenceDataProps,
// ) => {
//   const { modelId, bbox, maxPointNum, baseUrl, sdk, token } = props;
//   return baseAdapter.post(
//     '/bm/trajectory/bbox',
//     {
//       modelId,
//       bbox,
//       maxPointNum,
//     },
//     {
//       baseURL: `${baseUrl}/gisserver`,
//       headers: {
//         'map-sdk': sdk,
//         'map-token': token,
//       },
//     },
//   );
// };

export const getFenceData: (
  url: string,
  options: AxiosRequestConfig,
) => Promise<any> = (url, options) =>
  baseAdapter({
    url,
    ...options,
  });
/**
 * 从url地址中获取style文件
 * @param url
 */
export const getFileFromUrl: (url: string) => Promise<any> = (url: string) =>
  // @ts-ignore
  baseAdapter.raw(url);

/**
 * 从url地址中获取数据
 * @param url
 */
export const fetchData: (url: string) => Promise<any> = (url: string) =>
  // @ts-ignore
  baseAdapter.raw(url);

/**
 * @param props poi查询参数
 * @returns 获取poi名称查询结果
 */
export const fetchPOIResult: (
  cur: number, // 这里的cur不是当前页数 是起始索引
  pageSize: number,
  props: FetchPOIParams,
) => Promise<FetchPOIResultProps> = (
  cur: number,
  pageSize: number,
  props: FetchPOIParams,
) => {
  const { moduleId, keyword = '', baseUrl, sdk, token } = props;
  return baseAdapter.post(
    `/bm/poi/keyword/${moduleId}`,
    {},
    {
      baseURL: `${baseUrl}/gisserver`,
      headers: {
        'map-sdk': sdk,
        'map-token': token,
      },
      params: {
        from: cur,
        size: pageSize,
        keyword,
      },
    },
  );
};
interface routePlaneProps {
  baseUrl: string;
  sdk: string;
  token: string;
}
export const routePlan: (
  moduleId: number,
  poiSet: string[],
  props: routePlaneProps,
) => Promise<RoutePlaneResultProps> = (
  moduleId: number,
  poiSet: string[],
  props: routePlaneProps,
) => {
  const { baseUrl, sdk, token } = props;
  return baseAdapter.post(
    `/bm/network/points-route/${moduleId}`,
    {
      poiSet,
    },
    {
      baseURL: `${baseUrl}/gisserver`,
      headers: {
        'map-sdk': sdk,
        'map-token': token,
      },
    },
  );
};
interface ReverseGeocodingProps {
  baseUrl: string;
  sdk: string;
  token: string;
  location: string;
  radius?: string;
}
export const reverseGeocoding: (
  moduleId: string,
  props: ReverseGeocodingProps,
) => Promise<FetchPOIResultProps> = (
  moduleId: string,
  props: ReverseGeocodingProps,
) => {
  const { baseUrl, sdk, token, location, radius } = props;
  return baseAdapter.get(`/bm/poi/reverse_geocoding/${moduleId}`, {
    baseURL: `${baseUrl}/gisserver`,
    headers: {
      'map-sdk': sdk,
      'map-token': token,
    },
    params: {
      location,
      radius,
    },
  });
};

export interface GetFenceDataProps {
  baseUrl: string;
  sdk: string;
  token: string;
}
export const getFenceSourceData: (
  moduleId: string,
  props: GetFenceDataProps,
) => Promise<FeatureProps[]> = (moduleId: string, props: GetFenceDataProps) => {
  const { baseUrl, sdk, token } = props;
  return baseAdapter.get(`/bm/geofence/info/${moduleId}`, {
    baseURL: `${baseUrl}/gisserver`,
    headers: {
      'map-sdk': sdk,
      'map-token': token,
    },
  });
};
export interface FeatureProps {
  uuid: string; // 要素的id
  name: string; // 要素的name字段
  geomWkt: string; // wkt几何数据
  type?: number; // 要素几何类型 1:面   2:线
}
export interface SaveFenceFeatureProps {
  baseUrl: string;
  sdk: string;
  token: string;
  feature: FeatureProps;
}
export const updateFenceFeature: (
  moduleId: string,
  props: SaveFenceFeatureProps,
) => Promise<any> = (moduleId: string, props: SaveFenceFeatureProps) => {
  const { baseUrl, sdk, token, feature } = props;
  return baseAdapter.post(`/bm/geofence/feature/update/${moduleId}`, feature, {
    baseURL: `${baseUrl}/gisserver`,
    headers: {
      'map-sdk': sdk,
      'map-token': token,
    },
  });
};

export const addFenceFeature: (
  moduleId: string,
  props: SaveFenceFeatureProps,
) => Promise<any> = (moduleId: string, props: SaveFenceFeatureProps) => {
  const { baseUrl, sdk, token, feature } = props;
  return baseAdapter.post(`/bm/geofence/feature/add/${moduleId}`, feature, {
    baseURL: `${baseUrl}/gisserver`,
    headers: {
      'map-sdk': sdk,
      'map-token': token,
    },
  });
};
// fenceDataDelete
interface DeleteFenceFeatureProps {
  baseUrl: string;
  sdk: string;
  token: string;
  id: string;
}
export const deleteFenceFeature: (
  moduleId: string,
  props: DeleteFenceFeatureProps,
) => Promise<any> = (moduleId: string, props: DeleteFenceFeatureProps) => {
  const { baseUrl, sdk, token, id } = props;
  return baseAdapter.post(
    `/bm/geofence/feature/delete/${moduleId}/${id}`,
    {},
    {
      baseURL: `${baseUrl}/gisserver`,
      headers: {
        'map-sdk': sdk,
        'map-token': token,
      },
    },
  );
};

interface GetFenceStatusProps {
  baseUrl: string;
  sdk: string;
  token: string;
}
export const getFenceStatus: (
  moduleId: string,
  props: GetFenceStatusProps,
) => Promise<string> = (moduleId: string, props: GetFenceStatusProps) => {
  const { baseUrl, sdk, token } = props;
  return baseAdapter.get(`/bm/geofence/alert-geofence/${moduleId}`, {
    baseURL: `${baseUrl}/gisserver`,
    headers: {
      'map-sdk': sdk,
      'map-token': token,
    },
  });
};
export const getFenceResult: (
  moduleId: string,
  props: GetFenceStatusProps,
) => Promise<{ [key: string]: number }> = (
  moduleId: string,
  props: GetFenceStatusProps,
) => {
  const { baseUrl, sdk, token } = props;
  return baseAdapter.get(`/bm/geofence/statistics-alert-result/${moduleId}`, {
    baseURL: `${baseUrl}/gisserver`,
    headers: {
      'map-sdk': sdk,
      'map-token': token,
    },
  });
};
// getFenceStatus
