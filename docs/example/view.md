---
order: 5
title: 改变视图
---

- 改变视图

```tsx
import React, { useRef, useEffect } from 'react';
import { JustMap, JustSource, Popup } from '@jd/just-map';
import scene from '../../test/style.js';
import clusterData from './cluster.json';
import poiRed from './image/arrow.png';

function App() {
  const mapContainerRef = useRef();
  const layerIdRef = useRef('cluster_uuid');
  const mapRef = useRef();
  useEffect(() => {
    mapRef.current = new JustMap({
      style: scene,
      baseUrl: 'https://just-newrc-beta.jdfmgt.com',
      mapboxOptions: {
        container: mapContainerRef.current,
      },
    });
    mapRef.current.on('load', () => {
      //地图加载完后，才能进行添加图层
    });
  }, []);
  function easeTo() {
    mapRef.current.easeTo({ center: [118, 40], zoom: 9, duration: 2000 });
  }
  function flyTo() {
    mapRef.current.flyTo({ center: [117, 39], zoom: 10, duration: 2000 });
  }
  function jumpTo() {
    mapRef.current.jumpTo({ center: [119, 41], zoom: 11, duration: 2000 });
  }
  function fitBounds() {
    mapRef.current.fitBounds(
      [
        [117, 43],
        [118, 45],
      ],
      {
        padding: { top: 10, bottom: 25, left: 15, right: 5 },
      },
    );
  }

  return (
    <div>
      <div>
        <button onClick={easeTo}>easeTo</button>
        <button onClick={flyTo}>flyTo</button>
        <button onClick={jumpTo}>jumpTo</button>
        <button onClick={fitBounds}>fitBounds</button>
      </div>
      <div
        id="map-container"
        ref={mapContainerRef}
        style={{ height: '500px' }}
      ></div>
    </div>
  );
}

export default App;
```
