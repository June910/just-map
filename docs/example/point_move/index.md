---
order: 5
title: 动态点
---

- 动态点

```tsx
import React, { useRef, useEffect, useState } from 'react';
import { JustMap, JustSource, Popup } from '@jd/just-map';
import pointData from './data.json';
import bearing from '@turf/bearing';
import arrowImg from './arrow.png';
import scene from '../../../test/style.js';

function App() {
  const mapContainerRef = useRef();
  const mapRef = useRef();
  const animationRef = useRef();
  const [time, setTime] = useState(-20); // 设置当前时刻 起始为-20 毫秒
  const lastTimeRef = useRef(0); // 用于设置补充每一帧的补偿
  const SHOW_SPEED_FACTOR = 0.5; // 显示速度快慢 值越大 越快
  const INIT_TIME = '2020-05-24 11:31:00'; //初始时间  其它时间都要减去这个时间得到相对时间
  useEffect(() => {
    mapRef.current = new JustMap({
      style: scene,
      baseUrl: 'https://just-newrc-beta.jdfmgt.com',
      token:
        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJKRC1VUkJBTi1KVVNULUdJUyIsInVzZXJOYW1lIjoidGVzdGdpcyJ9.aPtr-qRrRQKLajqhq_ucAPtva2dhKbYCPwQ5htHF8J4',
      mapboxOptions: {
        container: mapContainerRef.current,
        antialias: true,
        center: [113.341909681876089, 40.081930485946273],
        zoom: 11,
      },
    });
    mapRef.current.on('load', () => {
      //地图加载完后，添加图标  利用icon-rotate转向图标 表示方向
      addImage();
    });
  }, []);

  useEffect(() => {
    //每次时间发生变化执行数据拼接
    time > -20 && getPointGeoJsonDataByTime(time);
  }, [time]);

  // 添加图标
  function addImage() {
    if (!mapRef.current?.hasImage('arrow_img')) {
      mapRef.current?.loadImage(arrowImg, (err: any, image: any) => {
        if (err) throw err;
        mapRef.current?.addImage('arrow_img', image);
      });
    }
  }

  // 组装每一时刻下的点的geojson数据，包含方向和点的位置
  function getPointGeoJsonDataByTime(time) {
    // 传入的time是从0开始的
    const initPointGeojson = {
      type: 'FeatureCollection',
      features: [],
    };
    //{
    // id:''
    // points:[[time,lng,lat],[time,lng,lat],[time,lng,lat]],
    // properties:{
    //number:'xxx',
    //color:'xxx',
    //rotate:xxx, 计算所得
    //}
    //}
    pointData.forEach((trip) => {
      const startPoint = trip.points[0]; // 每一条轨迹的第一个点
      const endPoint = trip.points.slice(-1)[0]; // 每一条轨迹的最后一个点

      if (time > startPoint[0] && time < endPoint[0]) {
        const pointPositionIndex = trip.points.findIndex((p) => p[0] >= time); // 得到当前时刻的点索引
        const curPoint = trip.points[pointPositionIndex - 1];
        const nextPoint = trip.points[pointPositionIndex];
        const curBearing =
          pointPositionIndex === trip.points.length
            ? 0
            : getBearing(curPoint, nextPoint); // TODO 最后一个点的角度应该是和前一个点的角度一致
        const realPoint = getPoint(curPoint, nextPoint, time);
        const feature = {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: realPoint,
          },
          properties: {
            ...trip.properties,
            rotate: curBearing,
          },
        };
        initPointGeojson.features.push(feature);
      }
    });
    mapRef.current.getSource('move_point').setData(initPointGeojson);
  }

  function animate() {
    // 设置补偿
    const d = new Date();
    const time = d.getTime();
    const radio = (time - lastTimeRef.current) / 16.6; // 上一帧和下一帧时间差/16.6  用于设置时间补偿
    // console.log('lastTimeRef.current',lastTimeRef.current,radio);
    const step = lastTimeRef.current !== 0 ? radio : 1;

    setTime((t) => t + step * SHOW_SPEED_FACTOR);
    lastTimeRef.current = time;
    animationRef.current = requestAnimationFrame(animate);
  }

  function addLayer() {
    mapRef.current.addLayer({
      id: 'move_point',
      type: 'symbol',
      source: {
        type: 'geojson',
        data: {
          type: 'FeatureCollection',
          features: [],
        },
      },
      layout: {
        'icon-image': 'arrow_img',
        'icon-rotate': ['get', 'rotate'],
        'icon-allow-overlap': true,
        'icon-size': 0.2,
      },
    });
    animate(); // 开始轮询 每次都执数据源的组装，setData
  }

  function getBearing(curPoint, nextPoint) {
    const bearingValue = bearing(
      [curPoint[1], curPoint[2]],
      [nextPoint[1], nextPoint[2]],
    );
    return bearingValue;
  }
  function getPoint(curPoint, nextPoint, time) {
    const curTime = curPoint[0];
    const curLng = curPoint[1];
    const curLat = curPoint[2];
    const nextTime = nextPoint[0];
    const nextLng = nextPoint[1];
    const nextLat = nextPoint[2];

    if (curTime > time || nextTime < time) return [curLng, curLat];
    const lng =
      curLng + ((time - curTime) / (nextTime - curTime)) * (nextLng - curLng);
    const lat =
      curLat + ((time - curTime) / (nextTime - curTime)) * (nextLat - curLat);
    return [lng, lat];
  }

  function removeLayer() {
    mapRef.current.getLayer('move_point') &&
      mapRef.current.removeLayer('move_point');
    mapRef.current.getSource('move_point') &&
      mapRef.current.removeSource('move_point');
    cancelAnimationFrame(animationRef.current);
  }

  return (
    <div>
      <div>
        <button onClick={addLayer}>添加动态点图层</button>
        <button onClick={removeLayer}>删除动态点图层</button>
      </div>
      <div
        id="map-container"
        ref={mapContainerRef}
        style={{ height: '500px' }}
      ></div>
    </div>
  );
}

export default App;
```
