---
order: 5
title: 数据过滤
---

- 数据过滤 空间过滤和属性过滤只能同时支持一个

```tsx
import React, { useRef, useEffect } from 'react';
import { JustMap, JustSource, Popup } from '@jd/just-map';
import scene from '../../test/style.js';
import clusterData from './cluster.json';
import poiRed from './image/arrow.png';

function App() {
  const mapContainerRef = useRef();
  const layerIdRef = useRef('cluster_uuid');
  const mapRef = useRef();
  useEffect(() => {
    mapRef.current = new JustMap({
      style: scene,
      baseUrl: 'https://portal-just-test.jd.com',
      mapboxOptions: {
        container: mapContainerRef.current,
        center: [116.02364784484371, 39.97025895533943],
        zoom: 12,
      },
    });
    mapRef.current.on('load', () => {
      //地图加载完后，才能进行添加图层
    });
  }, []);

  function setFilter() {
    mapRef.current.setFilter('88756', ['==', 'osm_id', '487743122']);
  }

  function setSpatialFilter() {
    mapRef.current.setSpatialFilter('88756', [
      'crosses',
      [
        {
          id: '29ed0a1341f7c5152dcc06858311d8b1',
          type: 'Feature',
          properties: {},
          geometry: {
            coordinates: [
              [
                [116.05837490216254, 40.0300427888252],
                [116.12840711475235, 39.98867304427432],
                [116.07580741541585, 39.936399164136844],
                [116.00577520282606, 39.97780056315065],
                [116.05837490216254, 40.0300427888252],
              ],
            ],
            type: 'Polygon',
          },
        },
      ],
    ]);
  }

  return (
    <div>
      <div>
        <button onClick={setFilter}>属性过滤</button>
        <button onClick={setSpatialFilter}>空间过滤</button>
      </div>
      <div
        id="map-container"
        ref={mapContainerRef}
        style={{ height: '500px' }}
      ></div>
    </div>
  );
}

export default App;
```
