---
order: 5
title: setFeatureState设置选中状态
---

- setFeatureState 设置选中状态

```tsx
import React, { useRef, useEffect } from 'react';
import mapboxgl from 'mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import { JustMap, JustSource, Popup } from '@jd/just-map';
import scene from '../../test/style.js';
import poiRed from './image/arrow.png';

function App() {
  const mapContainerRef = useRef();
  const layerIdRef = useRef('cluster_uuid');
  const mapRef = useRef();
  let quakeID = null;

  useEffect(() => {
    mapRef.current = new JustMap({
      style: scene,
      baseUrl: 'https://just-newrc-beta.jdfmgt.com',
      mapboxOptions: {
        container: mapContainerRef.current,
        zoom: 3,
      },
    });
    mapRef.current.on('load', () => {
      addLayer();
      mapRef.current.on('mousemove', 'geojson-circle', (event) => {
        mapRef.current.getCanvas().style.cursor = 'pointer';
        if (event.features.length === 0) return;

        if (quakeID) {
          mapRef.current.removeFeatureState({
            source: 'charlieSource',
            id: quakeID,
          });
        }
        quakeID = event.features[0].id;
        mapRef.current.setFeatureState(
          {
            source: 'charlieSource',
            id: quakeID,
          },
          {
            hover: true,
          },
        );
      });
      mapRef.current.on('mouseleave', 'geojson-circle', () => {
        if (quakeID) {
          mapRef.current.setFeatureState(
            {
              source: 'charlieSource',
              id: quakeID,
            },
            {
              hover: false,
            },
          );
        }
        quakeID = null;
        mapRef.current.getCanvas().style.cursor = 'pointer';
      });
    });
  }, []);
  function addLayer() {
    mapRef.current.addSource('charlieSource', {
      type: 'geojson',
      generateId: true, // This ensures that all features have unique IDs
      data: {
        type: 'FeatureColleciton',
        features: [
          {
            type: 'Feature',
            geometry: {
              type: 'Point',
              coordinates: [118.272911, 33.968467],
            },
            properties: {
              name: 'just',
              value: 13,
            },
          },
          {
            type: 'Feature',
            geometry: {
              type: 'Point',
              coordinates: [118.272911, 33.958467],
            },
            properties: {
              name: 'just2',
              value: 10,
            },
          },
        ],
      },
    });

    mapRef.current.addLayer({
      id: 'geojson-circle',
      type: 'circle',
      source: 'charlieSource',
      paint: {
        'circle-color': [
          'case',
          ['boolean', ['feature-state', 'hover'], false],
          [
            'interpolate',
            ['linear'],
            ['get', 'mag'],
            1,
            '#fff7ec',
            1.5,
            '#fee8c8',
            2,
            '#fdd49e',
            2.5,
            '#fdbb84',
            3,
            '#fc8d59',
            3.5,
            '#ef6548',
            4.5,
            '#d7301f',
            6.5,
            '#b30000',
            8.5,
            '#7f0000',
            10.5,
            '#000',
          ],
          '#000',
        ],
        'circle-stroke-color': '#000',
        'circle-stroke-width': 1,
        'circle-radius': [
          'case',
          ['boolean', ['feature-state', 'hover'], false],
          ['interpolate', ['linear'], ['get', 'value'], 1, 8, 10.5, 26],
          15,
        ],
        // 'circle-radius': [
        //   'case',
        //   ['boolean', ['feature-state', 'hover'], false],
        //   ['interpolate', ['linear'], ['get', 'value'], 13, 50,10,10],
        // ],
      },
    });
  }

  return (
    <div>
      <div></div>
      <div
        id="map-container"
        ref={mapContainerRef}
        style={{ height: '500px' }}
      ></div>
    </div>
  );
}

export default App;
```
