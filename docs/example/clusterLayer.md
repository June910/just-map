---
order: 5
title: 添加聚合图
---

- 添加聚合图

```tsx
import React, { useRef, useEffect } from 'react';
import { JustMap, JustSource } from '@jd/just-map';
import scene from '../../test/style.js';
import clusterData from './cluster.json';
import poiRed from './image/arrow.png';

function App() {
  const mapContainerRef = useRef();
  const layerIdRef = useRef('cluster_uuid');
  const mapRef = useRef();
  useEffect(() => {
    mapRef.current = new JustMap({
      style: scene,
      baseUrl: 'https://just-newrc-beta.jdfmgt.com',
      mapboxOptions: {
        container: mapContainerRef.current,
      },
    });
    mapRef.current.on('load', () => {
      if (!mapRef.current?.hasImage('poi_red')) {
        mapRef.current?.loadImage(poiRed, (err: any, image: any) => {
          if (err) throw err;
          mapRef.current?.addImage('poi_red', image);
        });
      }
    });
  }, []);
  function addCylinder() {
    // 聚合图属于复合图层 由四类图层组成
    // 聚合点位图 layerId-clusterCircle
    // 聚合标注图层 layerId-clusterText
    // 非聚合点位图 layerId-unClusterCircle
    // 非聚合icon图 layerId-unClusterIcon
    mapRef.current.addLayer({
      id: layerIdRef.current,
      type: 'just_cluster',
      source: {
        type: 'geojson',
        data: clusterData,
        cluster: true,
        clusterMaxZoom: 14, // Max zoom to cluster points on
        clusterRadius: 50, // Radius of each cluster when clustering points (defaults to 50)
      },
      paint: {
        clusterCircleColor: [
          'step',
          ['get', 'point_count'],
          '#51bbd6',
          100,
          '#f1f075',
          750,
          '#f28cb1',
        ],
        clusterCircleRadius: [
          'step',
          ['get', 'point_count'],
          20,
          100,
          30,
          750,
          40,
        ],
        clusterTextColor: '#FF0',
        clusterTextSize: 15,
      },
      layout: {
        clusterTextSize: 15,
        unClusterIconImage: '0a86d724-8b63-4e84-a534-ca5eed390efb',
      },
      unClusterIconZoom: 10,
    });
  }

  function changeHeight() {
    mapRef.current.setLayerProperty(layerIdRef.current, {
      paint: {
        clusterCircleRadius: 50,
        clusterTextColor: '#F00',
        unClusterCircleColor: '#0F0',
        unClusterCircleRadius: 30,
      },
    });
  }

  function changeColor() {
    mapRef.current.setLayerProperty(layerIdRef.current, {
      layout: {
        clusterTextSize: 5,
      },
    });
  }
  function removeCluster() {
    mapRef.current.removeLayer(layerIdRef.current);
  }
  function addClusterInteraction() {
    const interactionLayerId = 'hightlight-icon';
    mapRef.current?.addLayer({
      id: interactionLayerId,
      //@ts-ignore
      type: 'symbol',
      layout: {
        'icon-image': 'poi_red',
        'icon-size': 0.1,
      },
      filter: ['==', 'id', ''],
      source: {
        type: 'geojson',
        data: clusterData,
      },
    });
    mapRef.current.on('click', (e) => {
      const { x, y } = e.point;
      var bbox = [
        [e.point.x - 5, e.point.y - 5],
        [e.point.x + 5, e.point.y + 5],
      ];
      const features = mapRef.current.queryRenderedFeatures(bbox, {
        layers: [`${layerIdRef.current}-unClusterIcon`],
      });
      if (features && features.length > 0) {
        const selectFeature = features[0];
        const featureId = selectFeature.properties.id;
        mapRef.current.setFilter(interactionLayerId, ['==', 'id', featureId]);
        // mapRef.current.setFilter(layerIdRef.current,['!=', 'id', featureId]);
      } else {
        mapRef.current.setFilter(interactionLayerId, ['==', 'id', '']);
        mapRef.current.setFilter(layerIdRef.current, undefined);
      }
    });
  }

  return (
    <div>
      <div>
        <button onClick={addCylinder}>添加聚合图</button>
        <button onClick={changeHeight}>改变聚合Paint</button>
        <button onClick={changeColor}>改聚合layout</button>
        <button onClick={addClusterInteraction}>增加聚合图交互</button>
        <button onClick={removeCluster}>移除聚合图</button>
      </div>
      <div
        id="map-container"
        ref={mapContainerRef}
        style={{ height: '500px' }}
      ></div>
    </div>
  );
}

export default App;
```
