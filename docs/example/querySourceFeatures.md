---
order: 5
title: 通过Source查询feature
---

- querySourceFeatures 通过 Source 查询 feature

```tsx
import React, { useRef, useEffect } from 'react';
import mapboxgl from 'mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import { JustMap, JustSource, Popup } from '@jd/just-map';
import scene from '../../test/style.js';
// import cylinderData from './Cylinder.json'
import poiRed from './image/arrow.png';

function App() {
  const mapContainerRef = useRef();
  const layerIdRef = useRef('cluster_uuid');
  const mapRef = useRef();
  useEffect(() => {
    mapRef.current = new JustMap({
      style: scene,
      baseUrl: 'https://just-newrc-beta.jdfmgt.com',
      mapboxOptions: {
        container: mapContainerRef.current,
      },
    });
    mapRef.current.on('load', () => {
      addLayer();
    });
  }, []);
  function addLayer() {
    mapRef.current.addSource('charlieSource', {
      type: 'geojson',
      data: {
        type: 'FeatureColleciton',
        features: [
          {
            type: 'Feature',
            geometry: {
              type: 'Point',
              coordinates: [118.272911, 33.968467],
            },
            properties: {
              name: 'just',
            },
          },
        ],
      },
    });

    mapRef.current.addLayer({
      id: 'geojson-circle',
      type: 'circle',
      source: 'charlieSource',
      paint: {
        'circle-color': 'rgba(255,255,0,1)',
        'circle-radius': 50,
      },
    });
  }
  function openPopup() {
    const features = mapRef.current.querySourceFeatures('charlieSource', {
      sourceLayer: 'geojson-circle',
      // i'm confident there is data matching this filter
      filter: ['==', 'name', 'just'],
    });
    if (features.length > 0) {
      const feature = features[0];
      const popup = new Popup()
        .setLngLat(feature.geometry.coordinates)
        .setText(feature.properties.name)
        .addTo(mapRef.current);
    }
  }
  function query() {
    openPopup();
  }

  return (
    <div>
      <div>
        <button onClick={query}>查询</button>
      </div>
      <div
        id="map-container"
        ref={mapContainerRef}
        style={{ height: '500px' }}
      ></div>
    </div>
  );
}

export default App;
```
