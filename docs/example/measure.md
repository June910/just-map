---
order: 7
title: 测量
---

- 支持测量坐标、长度、面积

```tsx
import React, { useRef, useEffect } from 'react';
import { JustMap, Draw } from '@jd/just-map';
import scene from '../../test/style.js';
import '@jd/just-map/dist/index.css';

function App() {
  const mapContainerRef = useRef();
  const layerIdRef = useRef('cluster_uuid');
  const mapRef = useRef();
  const measureRef = useRef();
  const drawRef = useRef();
  useEffect(() => {
    mapRef.current = new JustMap({
      style: scene,
      baseUrl: 'https://just-newrc-beta.jdfmgt.com',
      token:
        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJKRC1VUkJBTi1KVVNULUdJUyIsInVzZXJOYW1lIjoidGVzdGdpcyJ9.aPtr-qRrRQKLajqhq_ucAPtva2dhKbYCPwQ5htHF8J4',
      mapboxOptions: {
        container: mapContainerRef.current,
        antialias: true,
        center: [116.162288432000082, 39.934832644799997],
        zoom: 15,
      },
    });
    mapRef.current.on('load', () => {
      console.log('load');
      //地图加载完后，添加画图功能
      measureRef.current = mapRef.current.addMeasure();
    });
  }, []);
  function measurePoint() {
    measureRef.current.changeMode('coordinate');
  }

  function measureLine() {
    measureRef.current.changeMode('length');
  }
  function measurePolygon() {
    measureRef.current.changeMode('area');
  }
  function deleteSelected() {
    measureRef.current.trash();
  }
  function deleteAll() {
    measureRef.current.deleteAll();
  }

  return (
    <div>
      <div>
        <button onClick={measurePoint}>测量点</button>
        <button onClick={measureLine}>测量线</button>
        <button onClick={measurePolygon}>测量面</button>
        <button onClick={deleteSelected}>删除选择的测量结果</button>
        <button onClick={deleteAll}>删除所有测量结果</button>
      </div>
      <div
        id="map-container"
        ref={mapContainerRef}
        style={{ height: '500px' }}
      ></div>
    </div>
  );
}

export default App;
```
