---
order: 5
title: 实时轨迹
---

- 添加实时轨迹图

```tsx
import React, { useRef, useEffect, useState } from 'react';
import { JustMap, JustSource } from '@jd/just-map';
import scene from '../../test/style.js';
import realtimeData from './RealtimeData.json';
import realtimeData3 from './RealtimeData3.json';

function App({ animationSpeed = 20, loopLength = 1800, visible = true }) {
  const mapContainerRef = useRef();
  const mapRef = useRef();
  useEffect(() => {
    mapRef.current = new JustMap({
      style: scene,
      baseUrl: 'https://just-newrc-beta.jdfmgt.com',
      mapboxOptions: {
        container: mapContainerRef.current,
        center: [116.950141512000073, 35.384570840000151], // [116.950141512000073, 35.384570840000151], // starting position [lng, lat]
        zoom: 8,
      },
      baseUrl: 'http://just-test.jd.com',
      token:
        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJKRC1VUkJBTi1KVVNULUdJUyIsInVzZXJOYW1lIjoianRlc3R1c2VyMyJ9.UvsBjOi2EhF8Z60cHcnaoshCD1p_t0X9oJ6hlqMq7m0',
    });
  }, []);
  function addRealtime() {
    if (mapRef.current.getLayer('realtime_xxx')) {
      mapRef.current.removeLayer('realtime_xxx');
    }
    mapRef.current.addLayer({
      id: 'realtime_xxx',
      type: 'just_trips',
      source: {
        type: 'trips',
        data: '/bm/trajectory/bbox-with-time',
        options: {
          bbox: '116.23509578846489,39.67141691916888,116.480158222554,39.77352932372372',
          maxPointNum: 30,
          modelId: 4448,
        },
        interval: 5000,
      },
      paint: {
        opacity: 1,
        widthMinPixels: 5,
        rounded: true,
        //currentTime: new Date().getTime(), //该参数非必填，如果不提供该字段系统按照第一次获取数据的最早时间
        animationSpeed: 2,
        radiusMinPixels: 5,
        radiusMaxPixels: 100,
        getRadius: 100,
        color: 'rgba(0, 139, 139,1)',
        getColor: 'rgba(0, 139, 139,1)',
        trailLength: 10000,
        shadowEnabled: false,
      },
      layout: {
        visible: true,
      },
    });
  }

  // function changeHeight() {
  //   mapRef.current.setLayerProperty('cylinder_xxx', {
  //     maxHeight: 0.09 * Math.random(),
  //   });
  // }

  function changeColor() {
    mapRef.current.setLayerProperty('realtime_xxx', {
      paint: {
        getColor: 'rgba(106, 90, 205, 1)', //[106, 90, 205],
      },
    });
  }
  function changeTrailLength() {
    mapRef.current.setLayerProperty('realtime_xxx', {
      paint: {
        trailLength: 1000,
      },
    });
  }
  function removeLayer() {
    mapRef.current.removeLayer('realtime_xxx');
  }
  function changeVisible() {
    mapRef.current.setLayerProperty('realtime_xxx', {
      layout: {
        visibility: 'visible',
      },
    });
  }
  function changeVisibleNone() {
    mapRef.current.setLayerProperty('realtime_xxx', {
      layout: {
        visibility: 'none',
      },
    });
  }
  return (
    <div>
      <div>
        <button onClick={addRealtime}>实时轨迹图</button>
        <button onClick={changeColor}>改变轨迹颜色</button>
        <button onClick={changeTrailLength}>改变拖尾长度</button>
        <button onClick={changeVisible}>显示图层</button>
        <button onClick={changeVisibleNone}>隐藏图层</button>

        <button onClick={removeLayer}>移除图层</button>
      </div>
      <div
        id="map-container"
        ref={mapContainerRef}
        style={{ height: '500px' }}
      ></div>
    </div>
  );
}

export default App;
```
