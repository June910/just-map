---
order: 5
title: 添加三维柱状图
---

- 添加柱状图

```tsx
import React, { useRef, useEffect } from 'react';
import { JustMap, JustSource } from '@jd/just-map';
import scene from '../../test/style.js';
import cylinderData from './rawCylinder.json';

function App() {
  const mapContainerRef = useRef();
  const mapRef = useRef();
  useEffect(() => {
    mapRef.current = new JustMap({
      style: scene,
      baseUrl: 'https://just-newrc-beta.jdfmgt.com',
      mapboxOptions: {
        container: mapContainerRef.current,
      },
    });
  }, []);
  function addCylinder() {
    mapRef.current.addLayer({
      id: 'cylinder_xxx',
      type: 'just_cylinder',
      source: {
        type: 'geojson',
        data: cylinderData,
      },
      paint: {
        valueKey: 'height',
        maxHeight: 0.05,
      },
    });
  }

  function changeHeight() {
    mapRef.current.setLayerProperty('cylinder_xxx', {
      paint: {
        maxHeight: 0.09 * Math.random(),
      },
    });
  }

  function changeColor() {
    mapRef.current.setLayerProperty('cylinder_xxx', {
      paint: { color: 'green' },
    });
  }
  function hide() {
    mapRef.current.setLayerProperty('cylinder_xxx', {
      layout: {
        visibility: 'none',
      },
    });
  }
  function show() {
    mapRef.current.setLayerProperty('cylinder_xxx', {
      layout: {
        visibility: 'visible',
      },
    });
  }
  function del() {
    mapRef.current.removeLayer('cylinder_xxx');
  }

  return (
    <div>
      <div>
        <button onClick={addCylinder}>柱状图</button>
        <button onClick={changeHeight}>增加高度</button>
        <button onClick={changeColor}>变成绿色</button>
        <button onClick={hide}>隐藏</button>
        <button onClick={show}>显示</button>
        <button onClick={del}>删除</button>
      </div>
      <div
        id="map-container"
        ref={mapContainerRef}
        style={{ height: '500px' }}
      ></div>
    </div>
  );
}

export default App;
```
