---
order: 5
title: 添加天地图栅格切片底图
---

- 添加天地图

```tsx
import React, { useRef, useEffect } from 'react';
import { JustMap, JustSource, Popup } from '@jd/just-map';
import clusterData from './cluster.json';
import poiRed from './image/arrow.png';

function App() {
  const mapContainerRef = useRef();
  const layerIdRef = useRef('cluster_uuid');
  const mapRef = useRef();
  useEffect(() => {
    /**
     * 获取天地图底图url
     */
    class TiandituTile {
      baseurl = 'http://t0.tianditu.com';
      suffix = '_w/wmts'; // Eg: 'ibo_w/wmts'
      params = {
        tk: '7729245051516d5894f0e6844cc69fe2',
        SERVICE: 'WMTS',
        REQUEST: 'GetTile',
        VERSION: '1.0.0',
        LAYER: '', // Eg: ibo
        STYLE: 'default',
        TILEMATRIXSET: 'w',
        TILEMATRIX: '{z}',
        TILEROW: '{y}',
        TILECOL: '{x}',
        FORMAT: 'tiles',
      };
      url;
      constructor(type, { baseurl, params } = {}) {
        if (!Object.keys(TiandituTile.TYPE).includes(type)) {
          throw new Error(
            `${type} is not one of ${Object.keys(TiandituTile.TYPE).join(',')}`,
          );
        }
        if (baseurl) this.baseurl = baseurl;
        this.params = Object.assign(this.params, params, { LAYER: type });

        if (!this.params.tk) {
          console.error('please make sure that tk is set correctly');
        }
        this.url = `${this.baseurl}/${type}${
          this.suffix
        }?${TiandituTile.stringifyQuery(this.params)}`;
      }
      /**
       * 天地图底图类型
       * 底图和标记可以两两组合
       */
      static TYPE = {
        /**
         * 矢量底图
         */
        vec: 'vec',
        /**
         * 矢量注记
         */
        cva: 'cva',
        /**
         * 影像底图
         */
        img: 'img',
        /**
         * 影像注记
         */
        cia: 'cia',
        /**
         * 地形晕渲
         */
        ter: 'ter',
        /**
         * 地形注记
         */
        cta: 'cta',
        /**
         * 全球境界
         */
        ibo: 'ibo',
        /**
         * 矢量英文注记
         */
        eva: 'eva',
        /**
         * 影像英文注记
         */
        eia: 'eia',
      };
      /**
       * 格式化url的query
       * @param {object} obj
       */
      static stringifyQuery(obj) {
        if (!obj) return '';
        const result = Object.keys(obj).reduce((pre, key) => {
          pre.push(`${key}=${obj[key]}`);
          return pre;
        }, []);
        return `${result.join('&')}`;
      }
    }
    var vecUrl =
      'http://t0.tianditu.com/vec_w/wmts?tk=e90d56e5a09d1767899ad45846b0cefd';
    var cvaUrl =
      'http://t0.tianditu.com/cva_w/wmts?tk=e90d56e5a09d1767899ad45846b0cefd';
    // 天地图 配置---start------------
    const tiandituStyle = {
      // 设置版本号，一定要设置
      version: 8,
      // 添加来源
      sources: {
        tdtVec: {
          //类型为栅格瓦片
          type: 'raster',
          tiles: [
            //请求地址
            vecUrl +
              '&SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=vec&STYLE=default&TILEMATRIXSET=w&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}&FORMAT=tiles',
          ],
          //分辨率
          tileSize: 256,
        },
        tdtCva: {
          type: 'raster',
          tiles: [
            cvaUrl +
              '&SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=cva&STYLE=default&TILEMATRIXSET=w&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}&FORMAT=tiles',
          ],
          tileSize: 256,
        },
      },
      layers: [
        {
          // 图层id，要保证唯一性
          id: 'tdtVec',
          // 图层类型
          type: 'raster',
          // 数据源
          source: 'tdtVec',
          // 图层最小缩放级数
          minzoom: 0,
          // 图层最大缩放级数
          maxzoom: 17,
        },
        {
          id: 'tdtCva',
          type: 'raster',
          source: 'tdtCva',
          minzoom: 0,
          maxzoom: 17,
        },
      ],
    };
    mapRef.current = new JustMap({
      styleType: 'mapbox',
      baseUrl: 'https://just-newrc-beta.jdfmgt.com',
      style: {
        // 设置版本号，一定要设置
        version: 8,
        // 添加来源
        sources: {
          tdtVec: {
            type: 'raster',
            tiles: [new TiandituTile(TiandituTile.TYPE.vec).url],
            // 分辨率
            tileSize: 256,
          },
          tdtCva: {
            type: 'raster',
            tiles: [new TiandituTile(TiandituTile.TYPE.cva).url],
            tileSize: 256,
          },
        },
        layers: [
          {
            // 图层id，要保证唯一性
            id: 'tdtVec',
            // 图层类型
            type: 'raster',
            // 数据源
            source: 'tdtVec',
            // 图层最小缩放级数
            minzoom: 0,
            // 图层最大缩放级数
            maxzoom: 17,
          },
          {
            id: 'tdtCva',
            type: 'raster',
            source: 'tdtCva',
            minzoom: 0,
            maxzoom: 17,
          },
        ],
      },
      mapboxOptions: {
        container: mapContainerRef.current,
      },
    });
    mapRef.current.on('load', () => {
      // //地图加载完后，才能进行添加图层
      // mapRef.current.addLayer({
      //   // 图层id，要保证唯一性
      //   id: 'tdtVec',
      //   // 图层类型
      //   type: 'raster',
      //   // 数据源
      //   source: 'tdtVec',
      //   // 图层最小缩放级数
      //   minzoom: 0,
      //   // 图层最大缩放级数
      //   maxzoom: 17,
      // });
      // mapRef.current.addLayer({
      //   id: 'tdtCva',
      //   type: 'raster',
      //   source: 'tdtCva',
      //   minzoom: 0,
      //   maxzoom: 17,
      // });
    });
  }, []);

  return (
    <div>
      <div
        id="map-container"
        ref={mapContainerRef}
        style={{ height: '500px' }}
      ></div>
    </div>
  );
}

export default App;
```
