---
order: 5
title: 整理替换style样式
---

- 切换 style 文件

```tsx
import React, { useRef, useEffect } from 'react';
import { JustMap, JustSource, Popup } from '@jd/just-map';
import scene from '../../test/style.js';
import rcScene from '../../test/rc.json';
// import cylinderData from './Cylinder.json'
import clusterData from './cluster.json';
import poiRed from './image/arrow.png';

function App() {
  const mapContainerRef = useRef();
  const layerIdRef = useRef('cluster_uuid');
  const mapRef = useRef();
  useEffect(() => {
    mapRef.current = new JustMap({
      style: scene,
      baseUrl: 'https://just-newrc-beta.jdfmgt.com',
      mapboxOptions: {
        container: mapContainerRef.current,
        zoom: 10,
      },
    });
  }, []);
  function setStyle() {
    mapRef.current.setStyle(rcScene);
  }

  return (
    <div>
      <div>
        <button onClick={setStyle}>切换style</button>
      </div>
      <div
        id="map-container"
        ref={mapContainerRef}
        style={{ height: '500px' }}
      ></div>
    </div>
  );
}

export default App;
```
