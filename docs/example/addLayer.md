---
order: 5
title: 添加geojson图层
---

- 添加图层

```tsx
import React, { useRef, useEffect } from 'react';
import { JustMap, JustSource, Popup } from '@jd/just-map';
import scene from '../../test/style.js';
import clusterData from './cluster.json';
import poiRed from './image/arrow.png';

function App() {
  const mapContainerRef = useRef();
  const layerIdRef = useRef('cluster_uuid');
  const mapRef = useRef();
  useEffect(() => {
    mapRef.current = new JustMap({
      style: scene,
      baseUrl: 'https://just-newrc-beta.jdfmgt.com',
      mapboxOptions: {
        container: mapContainerRef.current,
      },
    });
    mapRef.current.on('load', () => {
      //地图加载完后，才能进行添加图层
      //addLayer();
    });
  }, []);
  function addLayer() {
    mapRef.current.addLayer({
      id: 'geojson-circle1',
      type: 'circle',
      source: {
        type: 'geojson',
        data: {
          type: 'FeatureColleciton',
          features: [
            {
              type: 'Feature',
              geometry: {
                type: 'Point',
                coordinates: [117.5, 39],
              },
            },
          ],
        },
      },
      paint: {
        'circle-color': 'rgba(255,255,0,1)',
        'circle-radius': 50,
      },
    });
  }
  function setColor() {
    mapRef.current.setLayerProperty('geojson-circle1', {
      paint: {
        'circle-color': 'rgba(255,0,0,1.0)',
      },
    });
  }

  function setData() {
    mapRef.current.getSource('geojson-circle1').setData({
      type: 'FeatureColleciton',
      features: [
        {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [116, 39],
          },
        },
      ],
    });
  }

  return (
    <div>
      <div>
        <button onClick={addLayer}>添加图层</button>
        <button onClick={setColor}>改变图层颜色</button>
        <button onClick={setData}>改变数据</button>
      </div>
      <div
        id="map-container"
        ref={mapContainerRef}
        style={{ height: '500px' }}
      ></div>
    </div>
  );
}

export default App;
```
