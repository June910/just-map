---
order: 5
title: 图层hover或者click交互
---

- 交互操作

```tsx
import React, { useRef, useEffect } from 'react';
import { JustMap, JustSource } from '@jd/just-map';
import poiRed from './image/arrow.png';
import scene from '../../test/style.js';

function App() {
  const mapContainerRef = useRef();
  const mapRef = useRef();
  useEffect(() => {
    mapRef.current = new JustMap({
      style: scene,
      baseUrl: 'https://just-newrc-beta.jdfmgt.com',
      mapboxOptions: {
        container: mapContainerRef.current,
        zoom: 12,
        center: [116.400819, 39.931093],
      },
    });
  }, []);
  function addClickInteraction1() {
    if (!mapRef.current?.hasImage('poi_red')) {
      mapRef.current?.loadImage(poiRed, (err: any, image: any) => {
        if (err) throw err;
        mapRef.current?.addImage('poi_red', image);
      });
    }
    // Fill-of-green-land
    mapRef.current.addInteraction(
      'external_vector_d680c6c3',
      'event-113',
      'mousemove',
      {
        type: 'highlight',
        options: {
          paint: {
            'fill-extrusion-height': 400,
            'fill-extrusion-pattern': 'poi_red',
          },
        },
      },
    );
  }
  function addClickInteraction2() {
    // Fill-of-green-land
    mapRef.current.addInteraction('88756', 'event-112', 'mousemove', {
      type: 'highlight',
      options: {
        paint: {
          'fill-color': 'rgb(255,0,0)',
        },
      },
    });
  }
  function removeClickInteraction2() {
    mapRef.current.removeInteraction('external_vector_d680c6c3', 'event-113');
  }

  return (
    <div>
      <div>
        <button onClick={addClickInteraction1}>添加建筑物交互</button>
        <button onClick={addClickInteraction2}>添加绿地交互</button>
        <button onClick={removeClickInteraction2}>移除建筑物交互2</button>
      </div>
      <div
        id="map-container"
        ref={mapContainerRef}
        style={{ height: '500px' }}
      ></div>
    </div>
  );
}

export default App;
```
