---
order: 6
title: 绘画
---

- 多种绘图模式

```tsx
import React, { useRef, useEffect } from 'react';
import { JustMap, Draw } from '@jd/just-map';
import scene from '../../test/style.js';
import '@jd/just-map/dist/index.css';

function App() {
  const mapContainerRef = useRef();
  const layerIdRef = useRef('cluster_uuid');
  const mapRef = useRef();
  const drawRef = useRef();
  useEffect(() => {
    mapRef.current = new JustMap({
      style: scene,
      baseUrl: 'https://just-newrc-beta.jdfmgt.com',
      token:
        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJKRC1VUkJBTi1KVVNULUdJUyIsInVzZXJOYW1lIjoidGVzdGdpcyJ9.aPtr-qRrRQKLajqhq_ucAPtva2dhKbYCPwQ5htHF8J4',
      mapboxOptions: {
        container: mapContainerRef.current,
        antialias: true,
        center: [116.162288432000082, 39.934832644799997],
        zoom: 15,
      },
    });
    mapRef.current.on('load', () => {
      console.log('load');
      //地图加载完后，添加画图功能
      drawRef.current = mapRef.current.addDraw();
      drawRef.current.on('draw.create', (e) => {
        console.log('新建图形', e);
      });
      drawRef.current.on('draw.update', (e) => {
        console.log('更新图形', e);
      });
      drawRef.current.on('draw.delete', (e) => {
        console.log('删除图形', e);
      });
    });
  }, []);

  function drawPoint() {
    console.log('draw', drawRef.current);
    drawRef.current.changeMode('point');
  }

  function drawLine() {
    drawRef.current.changeMode('line');
  }
  function drawPolygon() {
    drawRef.current.changeMode('polygon');
  }
  function drawSquare() {
    drawRef.current.changeMode('rectangle');
  }
  function drawCircle() {
    drawRef.current.changeMode('circle');
  }
  function deleteSelectFeature() {
    drawRef.current.trash();
  }
  function deleteAllFeatures() {
    drawRef.current.deleteAll();
  }
  function removeDraw() {
    mapRef.current.removeDraw();
  }

  return (
    <div>
      <div>
        <button onClick={drawPoint}>画点</button>
        <button onClick={drawLine}>画线</button>
        <button onClick={drawPolygon}>画多边形</button>
        <button onClick={drawSquare}>画矩形</button>
        <button onClick={drawCircle}>画圆</button>
        <button onClick={deleteSelectFeature}>删除选中图形</button>
        <button onClick={deleteAllFeatures}>删除所有图形</button>
        <button onClick={removeDraw}>卸载draw功能</button>
      </div>
      <div
        id="map-container"
        ref={mapContainerRef}
        style={{ height: '500px' }}
      ></div>
    </div>
  );
}

export default App;
```
