---
order: 5
title: mapbox原生组件添加和删除
---

- mapbox 原生组件

```tsx
import React, { useRef, useEffect } from 'react';
import mapboxgl from 'mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import { JustMap, JustSource, Popup } from '@jd/just-map';
import scene from '../../test/style.js';
import clusterData from './cluster.json';
import poiRed from './image/arrow.png';

function App() {
  const mapContainerRef = useRef();
  const layerIdRef = useRef('cluster_uuid');
  const mapRef = useRef();
  const mapboxControlRef = useRef();
  useEffect(() => {
    mapRef.current = new JustMap({
      style: scene,
      baseUrl: 'https://just-newrc-beta.jdfmgt.com',
      mapboxOptions: {
        container: mapContainerRef.current,
      },
    });
    mapRef.current.on('load', () => {});
  });

  function addJustMapControl() {
    mapRef.current.addControl({
      id: 'scale-control',
      type: 'scale',
      options: {
        position: 'top-right',
      },
    });
  }

  function deleteJustMapControl() {
    mapRef.current.removeControl('scale-control');
  }

  function addMapboxControl() {
    mapboxControlRef.current = new mapboxgl.FullscreenControl();
    mapRef.current.addControl(mapboxControlRef.current, 'bottom-right');
  }

  function deleteMapboxControl() {
    mapRef.current.removeControl(mapboxControlRef.current);
  }

  return (
    <div>
      <div>
        <button onClick={addJustMapControl}>添加justmap封装好的组件</button>
        <button onClick={deleteJustMapControl}>删除justmap封装好的组件</button>
        <button onClick={addMapboxControl}>添加mapbox原生组件</button>
        <button onClick={deleteMapboxControl}>删除mapbox原生组件</button>
      </div>
      <div
        id="map-container"
        ref={mapContainerRef}
        style={{ height: '500px' }}
      ></div>
    </div>
  );
}

export default App;
```
