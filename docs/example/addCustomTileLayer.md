---
order: 5
title: 添加高德和百度地图
---

- 添加高德和百度地图 进行了坐标纠偏 只支持栅格切片

```tsx
import React, { useRef, useEffect } from 'react';
import { JustMap, JustSource, Popup } from '@jd/just-map';
import scene from '../../test/style.js';

function App() {
  const mapContainerRef = useRef();
  const layerIdRef = useRef('cluster_uuid');
  const mapRef = useRef();
  useEffect(() => {
    mapRef.current = new JustMap({
      style: {
        version: 8.0,
        sources: {
          'gd-source': {
            type: 'raster',
            tiles: [
              'http://webst01.is.autonavi.com/appmaptile?style=6&x={x}&y={y}&z={z}',
            ],
            tileSize: 256,
          },
        },
        layers: [
          {
            id: 'gd-layer',
            type: 'raster',
            source: 'gd-source',
          },
        ],
        center: [116.46706996, 39.99188446],
        zoom: 11,
      },
      baseUrl: 'https://just-newrc-beta.jdfmgt.com',
      styleType: 'mapbox',
      mapboxOptions: {
        container: mapContainerRef.current,
      },
    });
    mapRef.current.on('load', () => {
      console.log('map=>load');
      mapRef.current.addLayer({
        id: 'point-test',
        type: 'circle',
        paint: {
          'circle-color': 'rgba(255,0,0,1.0)',
        },
        source: {
          type: 'geojson',
          data: {
            type: 'FeatureCollection',
            features: [
              {
                type: 'Feature',
                geometry: {
                  type: 'Point',
                  coordinates: [116.46706996, 39.99188446],
                },
              },
            ],
          },
        },
      });
      //地图加载完后，才能进行添加图层
      //addLayer();
    });
  }, []);
  function addNomalLayer() {
    mapRef.current.addLayer({
      id: 'normal-layer',
      type: 'raster',
      source: {
        type: 'raster',
        tiles: [
          'http://webrd01.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=8&x={x}&y={y}&z={z}',
        ],
        tileSize: 256,
      },
      paint: {
        'raster-opacity': 1.0,
      },
    });
  }
  function addGDLayer() {
    mapRef.current.addLayer(
      {
        id: 'GD-layer',
        type: 'raster',
        source: {
          type: 'raster',
          tiles: [
            'http://webrd01.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=8&x={x}&y={y}&z={z}',
          ],
          tileSize: 256,
          scheme: 'gaode',
        },
        paint: {
          'raster-opacity': 1.0,
        },
      },
      'point-test',
    );
  }
  function setGDLayerOpacity() {
    mapRef.current.setLayerProperty('GD-layer', {
      paint: {
        'raster-opacity': 0.5,
      },
    });
  }
  function setGDLayerMove() {
    mapRef.current.moveLayer('GD-layer', 'normal-layer');
  }
  function setGDLayerVisible() {
    const layer = mapRef.current.getLayer('GD-layer');
    if (!layer) return;
    const newVisible =
      layer.layout && layer.layout.visibility === 'none' ? 'visible' : 'none';
    mapRef.current.setLayerProperty('GD-layer', {
      layout: {
        visibility: newVisible,
      },
    });
  }
  function setGDLayerZoom() {
    mapRef.current.setLayerZoomRange('GD-layer', 10, 15);
  }

  function removeGDLayer() {
    mapRef.current.removeLayer('GD-layer');
  }
  function addBDLayer() {
    mapRef.current.addLayer({
      id: 'BD-layer',
      type: 'raster',
      source: {
        type: 'raster',
        tiles: [
          'http://online1.map.bdimg.com/onlinelabel/?qt=tile&x={x}&y={y}&z={z}&styles=pl&scaler=1&p=1',
        ],
        tileSize: 256,
        scheme: 'baidu',
      },
      paint: {
        'raster-opacity': 1.0,
      },
    });
  }
  function removeBDLayer() {
    mapRef.current.removeLayer('BD-layer');
  }

  return (
    <div>
      <div>
        <button onClick={addGDLayer}>添加高德图层</button>
        <button onClick={setGDLayerOpacity}>调整高德地图透明度</button>
        <button onClick={setGDLayerMove}>调整高德地图上下层级</button>
        <button onClick={setGDLayerVisible}>toggle高德地图显隐</button>
        <button onClick={setGDLayerZoom}>设置高德地图缩放等级</button>
        <button onClick={removeGDLayer}>删除高德图层</button>
      </div>
      <div>
        <button onClick={addBDLayer}>添加百度图层</button>
        <button onClick={removeBDLayer}>删除百度图层</button>
      </div>
      <div
        id="map-container"
        ref={mapContainerRef}
        style={{ height: '500px' }}
      ></div>
    </div>
  );
}

export default App;
```
