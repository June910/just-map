---
order: 5
title: 添加mapbox原生聚合图
---

- 添加聚合图

```tsx
import React, { useRef, useEffect } from 'react';
import { JustMap, JustSource } from '@jd/just-map';
import scene from '../../test/style.js';
import clusterData from './cluster.json';
import poiRed from './image/arrow.png';

function App() {
  const mapContainerRef = useRef();
  const layerIdRef = useRef('cluster_uuid');
  const mapRef = useRef();
  useEffect(() => {
    mapRef.current = new JustMap({
      style: scene,
      baseUrl: 'https://just-newrc-beta.jdfmgt.com',
      mapboxOptions: {
        container: mapContainerRef.current,
      },
    });
    mapRef.current.on('load', () => {
      //增加icon
      if (!mapRef.current?.hasImage('poi_red')) {
        mapRef.current?.loadImage(poiRed, (err: any, image: any) => {
          if (err) throw err;
          mapRef.current?.addImage('poi_red', image);
        });
      }
    });
  }, []);
  function addCylinder() {
    mapRef.current.addSource('earthquakes', {
      type: 'geojson',
      // Point to GeoJSON data. This example visualizes all M1.0+ earthquakes
      // from 12/22/15 to 1/21/16 as logged by USGS' Earthquake hazards program.
      data: clusterData,
      cluster: true,
      clusterMaxZoom: 14, // Max zoom to cluster points on
      clusterRadius: 50, // Radius of each cluster when clustering points (defaults to 50)
    });
    // 增加临时icon图层
    mapRef.current.addLayer({
      id: 'hightlight-icon',
      type: 'symbol',
      source: 'earthquakes',
      layout: {
        'icon-image': 'poi_red',
        'icon-size': 0.1,
      },
      filter: ['==', 'id', ''],
    });
    // 增加聚合点图层
    mapRef.current.addLayer({
      id: 'cluster-point',
      type: 'circle',
      source: 'earthquakes',
      filter: ['has', 'point_count'],
      paint: {
        'circle-color': [
          'step',
          ['get', 'point_count'],
          '#51bbd6',
          100,
          '#f1f075',
          750,
          '#f28cb1',
        ],
        'circle-radius': ['step', ['get', 'point_count'], 20, 100, 30, 750, 40],
      },
    });
    // 增加聚合文字图层
    mapRef.current.addLayer({
      id: 'cluster-count',
      type: 'symbol',
      source: 'earthquakes',
      filter: ['has', 'point_count'],
      layout: {
        'text-field': '{point_count_abbreviated}',
        'text-font': ['literal', ['msyh']],
        'text-size': 12,
      },
    });
    // 增加非聚合点图层
    mapRef.current.addLayer({
      id: 'unclustered-point',
      type: 'circle',
      source: 'earthquakes',
      filter: ['!', ['has', 'point_count']],
      paint: {
        'circle-color': '#11b4da',
        'circle-radius': 4,
        'circle-stroke-width': 1,
        'circle-stroke-color': '#fff',
      },
      maxzoom: 14, // 大于14级将不显示
    });
    // 增加非聚合icon图层
    mapRef.current.addLayer({
      id: 'unclustered-icon',
      type: 'symbol',
      source: 'earthquakes',
      filter: ['!', ['has', 'point_count']],
      layout: {
        'icon-image': '0a86d724-8b63-4e84-a534-ca5eed390efb',
        'icon-size': 2,
      },
      minzoom: 14, // 小于14级不显示
    });
    // 聚合点图层添加点击放大交互
    mapRef.current.on('click', 'cluster-point', function (e) {
      var features = mapRef.current.queryRenderedFeatures(e.point, {
        layers: ['cluster-point'],
      });
      var clusterId = features[0].properties.cluster_id;
      console.log(
        '🚀 ~ file: clusterLayer方案2.md ~ line 116 ~ clusterId',
        clusterId,
      );
      mapRef.current
        .getSource('earthquakes')
        .getClusterExpansionZoom(clusterId, function (err, zoom) {
          if (err) return;
          mapRef.current.easeTo({
            center: features[0].geometry.coordinates,
            zoom: zoom,
          });
        });
    });

    // 非聚合点图层增加click变成icon交互
    mapRef.current.on('click', function (e) {
      const features = mapRef.current.queryRenderedFeatures(e.point, {
        layers: ['unclustered-point', 'unclustered-icon'],
      });
      console.log(
        '🚀 ~ file: clusterLayer方案2.md ~ line 137 ~ features',
        features,
      );
      if (features.length > 0) {
        const clusterId = features[0].properties.id;
        mapRef.current.setFilter(
          'hightlight-icon',
          // 'all',
          ['==', 'id', clusterId],
          // ['!has', 'point_count'],
        );
        mapRef.current.setFilter('unclustered-point', [
          'all',
          ['!=', 'id', clusterId],
          ['!has', 'point_count'],
        ]);
        mapRef.current.setFilter('unclustered-icon', [
          'all',
          ['!=', 'id', clusterId],
          ['!has', 'point_count'],
        ]);
      } else {
        mapRef.current.setFilter('hightlight-icon', ['==', 'id', '']);
        mapRef.current.setFilter('unclustered-point', [
          '!',
          ['has', 'point_count'],
        ]);
        mapRef.current.setFilter('unclustered-icon', [
          '!',
          ['has', 'point_count'],
        ]);
      }
    });

    // 增加鼠标交互
    mapRef.current.on('mousemove', function (e) {
      const features = mapRef.current.queryRenderedFeatures(e.point, {
        layers: ['unclustered-point', 'unclustered-icon', 'cluster-point'],
      });
      if (features.length > 0) {
        mapRef.current.getCanvas().style.cursor = 'pointer';
      } else {
        mapRef.current.getCanvas().style.cursor = '';
      }
    });
  }

  function changeHeight() {
    mapRef.current.setLayerProperty(layerIdRef.current, {
      paint: {
        clusterCircleRadius: 50,
        clusterTextColor: '#F00',
        unClusterCircleColor: '#0F0',
        unClusterCircleRadius: 30,
      },
    });
  }

  function changeColor() {
    mapRef.current.setLayerProperty(layerIdRef.current, {
      layout: {
        clusterTextSize: 5,
      },
    });
  }
  function removeCluster() {
    mapRef.current.removeLayer(layerIdRef.current);
  }

  return (
    <div>
      <div>
        <button onClick={addCylinder}>添加聚合图</button>
      </div>
      <div
        id="map-container"
        ref={mapContainerRef}
        style={{ height: '500px' }}
      ></div>
    </div>
  );
}

export default App;
```
