---
order: 5
title: 改变图层样式
---

- setPaintProperty

```tsx
import React, { useRef, useEffect } from 'react';
import { JustMap, JustSource } from '@jd/just-map';
import scene from '../../test/style.js';
import cylinderData from './rawCylinder.json';

function App() {
  const heatMapData = { type: 'FeatureCollection', features: cylinderData };
  const mapContainerRef = useRef();
  const mapRef = useRef();
  useEffect(() => {
    mapRef.current = new JustMap({
      style: scene,
      baseUrl: 'https://just-newrc-beta.jdfmgt.com',
      mapboxOptions: {
        container: mapContainerRef.current,
      },
    });
  }, []);
  function addHeatMap() {
    mapRef.current.addLayer({
      id: 'heatmap_xxx',
      type: 'just_heatmap',
      source: {
        type: 'geojson',
        data: heatMapData,
      },
    });
  }

  function changeColor() {
    mapRef.current.setPaintProperty('heatmap_xxx', 'heatmap-color', [
      'interpolate',
      ['linear'],
      ['heatmap-density'],
      0,
      'rgba(33,102,172,0)',
      0.2,
      'rgb(103,169,207)',
      0.4,
      'rgb(209,229,240)',
      0.6,
      'rgb(253,219,199)',
      0.8,
      'rgb(239,138,98)',
      1,
      'rgb(178,24,43)',
    ]);
  }

  return (
    <div>
      <div>
        <button onClick={addHeatMap}>添加热力图</button>
        <button onClick={changeColor}>改变热力图属性</button>
      </div>
      <div
        id="map-container"
        ref={mapContainerRef}
        style={{ height: '500px' }}
      ></div>
    </div>
  );
}

export default App;
```
