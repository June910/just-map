---
order: 5
title: 动态点效果
---

- triggerRepaint 重新绘制

```tsx
import React, { useRef, useEffect } from 'react';
import mapboxgl from 'mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import { JustMap, JustSource, Popup } from '@jd/just-map';
import scene from '../../test/style.js';
import poiRed from './image/arrow.png';

function App() {
  const mapContainerRef = useRef();
  const layerIdRef = useRef('cluster_uuid');
  const mapRef = useRef();
  let quakeID = null;

  useEffect(() => {
    mapRef.current = new JustMap({
      style: scene,
      baseUrl: 'https://just-newrc-beta.jdfmgt.com',
      mapboxOptions: {
        container: mapContainerRef.current,
      },
    });
    mapRef.current.on('load', () => {
      addImage();
      addLayer();
    });
  }, []);
  function addImage() {
    const size = 200;

    // This implements `StyleImageInterface`
    // to draw a pulsing dot icon on the map.
    const pulsingDot = {
      width: size,
      height: size,
      data: new Uint8Array(size * size * 4),

      // When the layer is added to the map,
      // get the rendering context for the map canvas.
      onAdd: function () {
        const canvas = document.createElement('canvas');
        canvas.width = this.width;
        canvas.height = this.height;
        this.context = canvas.getContext('2d');
      },

      // Call once before every frame where the icon will be used.
      render: function () {
        const duration = 1000;
        const t = (performance.now() % duration) / duration;

        const radius = (size / 2) * 0.3;
        const outerRadius = (size / 2) * 0.7 * t + radius;
        const context = this.context;

        // Draw the outer circle.
        context.clearRect(0, 0, this.width, this.height);
        context.beginPath();
        context.arc(
          this.width / 2,
          this.height / 2,
          outerRadius,
          0,
          Math.PI * 2,
        );
        context.fillStyle = `rgba(255, 200, 200, ${1 - t})`;
        context.fill();

        // Draw the inner circle.
        context.beginPath();
        context.arc(this.width / 2, this.height / 2, radius, 0, Math.PI * 2);
        context.fillStyle = 'rgba(255, 100, 100, 1)';
        context.strokeStyle = 'white';
        context.lineWidth = 2 + 4 * (1 - t);
        context.fill();
        context.stroke();

        // Update this image's data with data from the canvas.
        this.data = context.getImageData(0, 0, this.width, this.height).data;

        // Continuously repaint the map, resulting
        // in the smooth animation of the dot.
        mapRef.current.triggerRepaint();

        // Return `true` to let the map know that the image was updated.
        return true;
      },
    };
    mapRef.current.addImage('pulsing-dot', pulsingDot, { pixelRatio: 2 });
  }

  function addLayer() {
    mapRef.current.addSource('charlieSource', {
      type: 'geojson',
      generateId: true, // This ensures that all features have unique IDs
      data: {
        type: 'FeatureColleciton',
        features: [
          {
            type: 'Feature',
            geometry: {
              type: 'Point',
              coordinates: [118.272911, 33.968467],
            },
            properties: {
              name: 'just',
              value: 13,
            },
          },
          {
            type: 'Feature',
            geometry: {
              type: 'Point',
              coordinates: [118.272911, 33.958467],
            },
            properties: {
              name: 'just2',
              value: 10,
            },
          },
        ],
      },
    });

    mapRef.current.addLayer({
      id: 'geojson-circle',
      type: 'symbol',
      source: 'charlieSource',
      layout: {
        'icon-image': 'pulsing-dot',
      },
    });
  }

  return (
    <div>
      <div></div>
      <div
        id="map-container"
        ref={mapContainerRef}
        style={{ height: '500px' }}
      ></div>
    </div>
  );
}

export default App;
```
