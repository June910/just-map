---
order: 5
title: 3D模型
---

- 三维模型 支持 obj 和 gltf 格式

```tsx
import React, { useRef, useEffect } from 'react';
import { JustMap, JustSource, Popup } from '@jd/just-map';
import pointData from './data.json';
import scene from '../../../test/style.js';

function App() {
  const mapContainerRef = useRef();
  const mapRef = useRef();
  useEffect(() => {
    mapRef.current = new JustMap({
      style: scene,
      baseUrl: 'https://just-newrc-beta.jdfmgt.com',
      token:
        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJKRC1VUkJBTi1KVVNULUdJUyIsInVzZXJOYW1lIjoidGVzdGdpcyJ9.aPtr-qRrRQKLajqhq_ucAPtva2dhKbYCPwQ5htHF8J4',
      mapboxOptions: {
        container: mapContainerRef.current,
        antialias: true,
        center: [118, 39.934832644799997],
        zoom: 15,
      },
    });
    mapRef.current.on('load', () => {
      //地图加载完后，才能进行添加图层
    });
  }, []);
  function addLayer() {
    mapRef.current.addLayer({
      id: 'modellayer-1',
      type: 'just_model',
      source: {
        type: 'geojson',
        data: {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              geometry: {
                type: 'Point',
                coordinates: [118, 39.934832644799997],
              },
              properties: {},
            },
          ],
        },
      },
      metadata: {
        'model-type': 'gltf',
      },
      paint: {
        // 'model-file':'https://raw.githubusercontent.com/visgl/deck.gl-data/master/website/humanoid_quad.obj',
        // 'model-file':'https://raw.githubusercontent.com/KhronosGroup/glTF-Sample-Models/master/2.0/BoxAnimated/glTF-Binary/BoxAnimated.glb',
        // 'model-file':
        //   'http://storage.360buyimg.com/monet-custom/bj_qzf/ludeng.gltf',
        'model-file':
          'https://docs.mapbox.com/mapbox-gl-js/assets/34M_17/34M_17.gltf',
        // 'model-color': '#000',
        'model-global-scale': 20,
      },
      minzoom: 1,
      maxzoom: 24,
    });
    mapRef.current.map.__deck.setProps({
      onClick: (e) => {
        console.log('eee', e);
      },
    });
  }
  function setModelFile() {
    //     {
    //   type: 'Feature',
    //   geometry: {
    //     type: 'Point',
    //     coordinates: [118, 39.934832644799997],
    //   },
    //   properties: {
    //     scale: [1, 2, 1],
    //   },
    // },
    mapRef.current.setLayerProperty('modellayer-1', {
      paint: {
        'model-file':
          'http://storage.360buyimg.com/monet-custom/bj_qzf/ludeng.gltf',
        'model-global-scale': 20,
      },
    });
  }

  function setModelPosition() {
    mapRef.current.getSource('modellayer-1').setData({
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [118.001, 39.934832644799997, 2000],
          },
        },
      ],
    });
  }
  function setModelColor() {
    mapRef.current.setLayerProperty('modellayer-1', {
      paint: {
        'model-color': ['get', 'color'],
      },
    });
  }
  function setModelScale() {
    mapRef.current.setLayerProperty('modellayer-1', {
      paint: {
        // 'model-scale': [1,10,1],
        'model-scale': ['get', 'scale'],
        // 'model-scale':['get','scale']
      },
    });
  }
  function setModelOrientation() {
    mapRef.current.setLayerProperty('modellayer-1', {
      paint: {
        'model-orientation': [0, 0, 90],
        // 'model-orientation': ['get', 'orientation'],
      },
    });
  }
  function setGlobalScale() {
    mapRef.current.setLayerProperty('modellayer-1', {
      paint: {
        'model-global-scale': 20,
      },
    });
  }
  function setOpacity() {
    mapRef.current.setLayerProperty('modellayer-1', {
      paint: {
        opacity: 0.5,
      },
    });
  }
  function setHide() {
    mapRef.current.setLayerProperty('modellayer-1', {
      layout: {
        visibility: 'none',
      },
    });
  }
  function setShow() {
    mapRef.current.setLayerProperty('modellayer-1', {
      layout: {
        visibility: 'visible',
      },
    });
  }
  function removeLayer() {
    mapRef.current.removeLayer('modellayer-1');
  }

  return (
    <div>
      <div>
        <button onClick={addLayer}>添加图层</button>
        <button onClick={setModelFile}>改变模型文件</button>
        <button onClick={setModelPosition}>改变位置</button>
        <button onClick={setModelColor}>改变模型颜色</button>
        <button onClick={setModelScale}>改变模型比例</button>
        <button onClick={setModelOrientation}>改变模型旋转角度</button>
        <button onClick={setGlobalScale}>改变模型整体大小</button>
        <button onClick={setOpacity}>改变透明度为50%</button>
        <button onClick={setHide}>隐藏</button>
        <button onClick={setShow}>显示</button>
        <button onClick={removeLayer}>删除图层</button>
      </div>
      <div
        id="map-container"
        ref={mapContainerRef}
        style={{ height: '500px' }}
      ></div>
    </div>
  );
}

export default App;
```
