---
order: 5
title: 点击图层弹出popup
---

- 点击图层要素弹出 popup

```tsx
import React, { useRef, useEffect } from 'react';
import mapboxgl from 'mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import { JustMap, JustSource, Popup } from '@jd/just-map';
import scene from '../../test/style.js';

function App() {
  const mapContainerRef = useRef();
  const layerIdRef = useRef('cluster_uuid');
  const mapRef = useRef();
  const popupRef = useRef();
  useEffect(() => {
    mapRef.current = new JustMap({
      style: scene,
      baseUrl: 'https://just-newrc-beta.jdfmgt.com',
      mapboxOptions: {
        container: mapContainerRef.current,
      },
    });
    mapRef.current.on('load', () => {
      mapRef.current.addLayer({
        id: 'geojson-circle',
        type: 'circle',
        source: {
          type: 'geojson',
          data: {
            type: 'FeatureColleciton',
            features: [
              {
                type: 'Feature',
                geometry: {
                  type: 'Point',
                  coordinates: [117.5, 39],
                },
                properties: {
                  name: 'just',
                },
              },
            ],
          },
        },
        paint: {
          'circle-color': 'rgba(255,255,0,1)',
          'circle-radius': 50,
        },
      });
      popupRef.current = new Popup();
      popupRef.current.on('open', () => {
        console.log('open');
      });
      popupRef.current.on('close', () => {
        console.log('close');
      });
      // 这里也可以改成mapRef.current.on('click',layerId,(e)=>{....})  这样的话 只有在点击到这个图层时才会触发事件
      mapRef.current.on('click', openPopup);
    });

    // const div = window.document.createElement('div');
    // div.innerHTML = 'Hello, world!';
    // const popup = new Popup()
    //   .setLngLat({ lng: 117, lat: 39 })
    //   .setText(div)
    //   .addTo(mapRef.current);
  }, []);
  function openPopup(e) {
    const { lngLat, point } = e;
    const features = mapRef.current.queryRenderedFeatures(
      [
        [point.x - 5, point.y - 5],
        [point.x + 5, point.y + 5],
      ],
      { layers: ['geojson-circle'] },
    );
    if (features.length > 0) {
      const feature = features[0];
      popupRef.current
        .setLngLat(lngLat)
        .setText(feature.properties.name)
        .addTo(mapRef.current);
    }
  }
  function unbind() {
    mapRef.current.off('click', openPopup);
  }

  return (
    <div>
      <div>
        <button onClick={unbind}>解绑</button>
      </div>
      <div
        id="map-container"
        ref={mapContainerRef}
        style={{ height: '500px' }}
      ></div>
    </div>
  );
}

export default App;
```
