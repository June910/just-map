---
order: 5
title: 添加marker
---

- 添加 marker

```tsx
import React, { useRef, useEffect } from 'react';
import mapboxgl from 'mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import { JustMap, JustSource, Popup, Marker } from '@jd/just-map';
import scene from '../../test/style.js';
import clusterData from './cluster.json';
import poiRed from './image/arrow.png';

function App() {
  const mapContainerRef = useRef();
  const layerIdRef = useRef('cluster_uuid');
  const mapRef = useRef();
  let marker = useRef();
  useEffect(() => {
    mapRef.current = new JustMap({
      style: scene,
      baseUrl: 'https://just-newrc-beta.jdfmgt.com',
      mapboxOptions: {
        container: mapContainerRef.current,
        center: [118.293328, 33.945154],
        zoom: 15,
      },
    });
  }, []);
  function addMarker() {
    marker.current = new Marker({
      draggable: true,
    })
      .setLngLat({ lng: 118.293328, lat: 33.945154 })
      .addTo(mapRef.current);
    // marker.current.setDraggable(true);
  }
  function removeMarker() {
    marker.current.remove();
  }
  function addPopupToMarker() {
    marker.current && marker.current.remove();
    const el = window.document.createElement('div');
    el.innerHTML = '定制图标';
    marker.current = new Marker({ element: el, draggable: true })
      .setLngLat({ lng: 118.293328, lat: 33.945154 })
      .setPopup(
        new Popup({ offset: 25 }) // add popups
          .setHTML(`<h3>弹窗测试</h3><p>弹窗测试</p>`),
      )
      .addTo(mapRef.current);
  }
  function togglePopupToMarker() {
    marker.current && marker.current.remove();
    const popup = new Popup({ offset: 25 }) // add popups
      .setHTML(`<h3>弹窗测试</h3><p>弹窗测试</p>`);
    const el = window.document.createElement('div');
    el.innerHTML = '定制图标';
    marker.current = new Marker(el)
      .setLngLat({ lng: 118.293328, lat: 33.945154 })
      .setPopup(popup)
      .addTo(mapRef.current);
    marker.current.togglePopup();
  }
  function setOffset() {
    if (marker.current === undefined) {
      alert('请先添加marker！');
      return;
    }
    marker.current &&
      marker.current.setOffset({ lng: 118.293328, lat: 33.955154 });
  }
  return (
    <div>
      <div>
        <button onClick={addMarker}>添加marker</button>
        <button onClick={removeMarker}>移除marker</button>
        <button onClick={addPopupToMarker}>Marker绑定Popup</button>
        <button onClick={togglePopupToMarker}>自动打开Popup</button>
        <button onClick={setOffset}>设置偏移</button>
      </div>
      <div
        id="map-container"
        ref={mapContainerRef}
        style={{ height: '500px' }}
      ></div>
    </div>
  );
}

export default App;
```
