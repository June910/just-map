---
order: 4
title: style规格
---

# style

基于 mapbox style 扩展的样式描述文件

## 例子

```json
{
    "version":8.0,
    "metadata":{
        "maputnik:renderer":"mbgljs"
    },
    "sources":{
        "1618307946734":{
            "tiles":[
                "http://portal-just-beta.jd.com/gisserver/tile/getTile/dynamic/3439/{z}/{x}/{y}"
            ],
            "maxzoom":14,
            "type":"vector",
            "minzoom":0
        },
        "1618307123145":{
            "tiles":[
                "http://portal-just-beta.jd.com/gisserver/tile/getTile/dynamic/3479/{z}/{x}/{y}"
            ],
            "maxzoom":14,
            "type":"vector",
            "minzoom":0
        }
    },
    "bearing":0,
    "center":[
        118.27219036902147,
        33.94948234843757
    ],
    "name":"OSM Liberty",
    "sprite":"http://portal-just-beta.jd.com/studio/icon/internal_liujun513/sprite",
    "groups":[
        {
            "id":"group-extrusion",
            "type":"symbol",
            "metadata":{
                "just:name_cn":"2.5D组",
                "just:isGroup":true
            },
            "layers":[
                {
                    "layout":{
                        "visibility":"visible"
                    },
                    "metadata":{
                        "just:service_id":3431,
                        "just:name_cn":"商业场所"
                    },
                    "paint":{
                        "fill-extrusion-height":400,
                        "fill-extrusion-color":"#E7EBF1",
                        "fill-extrusion-opacity":0.8
                    },
                    "id":"external_vector_d680c6c3",
                    "source":"source_name_d680c6c3",
                    "source-layer":"j_b_a_commercial_p_JUST_1617697018074",
                    "type":"fill-extrusion"
                }
            ]
        },
        {
          "id":"external_vector_992df577",
          "type":"symbol",
          "source":"source_name_992df577",
          "source-layer":"j_b_d_county_a",
          "layout":{
              "visibility":"visible",
              "text-field":[
                  "get",
                  "city"
              ],
              "text-font":[
                  "literal",
                  [
                      "msyh"
                  ]
              ],
              "text-anchor":"top",
              "text-offset":[
                  0,
                  1
              ],
              "text-size":12
          },
          "paint":{
              "text-color":"#4F5769",
              "text-halo-color":"#F8F8F8",
              "text-halo-width":1
          },
          "filter":["all",["==","code",8101],["==","width",80]],
          "spatialFilter":[
            "crosses",
            [
              {
                  "id":"63b2a11891e1793c2c2b7aa7f87242b1",
                  "type":"Feature",
                  "geometry":{
                      "coordinates":[
                          [
                              [
                                  118.27625667451952,
                                  33.97146774818761
                              ],
                              [
                                  118.27514376273643,
                                  33.95262224594953
                              ],
                              [
                                  118.29250954529061,
                                  33.951916686631094
                              ],
                              [
                                  118.2936224570737,
                                  33.9707623451599
                              ],
                              [
                                  118.27625667451952,
                                  33.97146774818761
                              ]
                          ]
                      ],
                      "type":"Polygon"
                  }
              }
            ]
          ],
          "metadata":{
              "just:name_cn":"县级标注",
              "just:service_id":3744
          },
          "minzoom":8,
          "maxzoom":11
        }
    ],
    "controls":[
        {
            "id":"poi_1f088326",
            "type":"poi",
            "options":{
                "dataSourceId":2084
            }
        },
        {
            "id":"jscale_a2489d7f",
            "type":"jscale",
            "options":{
                "x":90,
                "y":713
            }
        },
        {
            "id":"layergroup_673210d3",
            "type":"layergroup",
            "options":{
                "x":564,
                "y":50
            }
        }
    ],
    "interactions":[
      {
        "id":"map",
        "attachType":"map",
        "eventList":[
        {
          "id":"load_fdbfd31a",
          "type":"load",
          "options":{
            "name":"交互事件",
            "actionType":"flyTo",
            "actionContent":"117.25,39.11" [] {}
          }
        },
        {
          "id":"load_fdbfd31a",
          "type":"load",
          "options":{
            "name":"交互事件",
            "actionType":"rotate",
            "actionContent":"117.25,39.11" [] {}
          }
        },
        {
          "id":"load_fdbfd31a",
          "type":"click",
          "actionOption":{
            "type":"hightlight",
            "options":{
                "paint":{}
            }
          }
        }
        ]
      }
    ],
    "zoom":13.2166127960162,
    "pitch":55.5,
    "glyphs":"http://just-test.jd.com/studio/font/{fontstack}/{range}.pbf"
}
```

## 字段说明

- version _版本号_
- metadata _style 文件扩展属性_
- zoom _缩放级别_
- bearing _方位角_
- center _中心点_
- pitch _倾斜角_
- sources _数据源_
  - type: mapbox 的 type | 'realtime'| 'xx'
- groups _图层组集合_
  - group _图层组_
    - id _图层组 id_
    - type _图层组类型_
    - metadata _图层组扩展属性_
      - just:isGroup _boolean 是否是图层组_
      - just:name*cn *中文名称\_
    - layers _图层列表_
    - paint 图层组样式
    - layout 图层组布局
    - filter 属性过滤
    - spatialFilter 空间过滤
      - ['crosses'|'contains',[geom1,geom2,geom3]]
  - layer _图层_
    - _id 图层 id_
    - _type 图层类型_
    - _source 数据源 ID_
    - _source-layer 数据源图层名称(适量瓦片数据源需要指定)_
    - _metadata 图层扩展属性_
      - _just:service_id 模型 id 用于数据校验_
      - _just:name_cn: 图层中文名称_
    - paint 图层样式
    - layout 图层布局
- controls _组件列表_
  - id 组件 id
  - type _组件类型 poi|scale|route|fence。。。_
  - options _组件各自的属性_
- interactions _地图交互列表_
  - id 交互 id
  - attchType 交互宿主类型 map|layer|control
  - eventList 事件列表
    - id 事件 id
    - type 事件类型 load|click|dbClick|hover...
    - options 事件交互内容
      - name 交互事件名称
      - actionType 交互类型 flyTo|zoom|rotation...
      - actionContent 交互内容 "117,39"

## 备注

1. 空间过滤的 geometry 可以是 geojson 也可以是 wkt
2. interactions 需要商议(map|layer|control 存放的位置和具体的内容)
