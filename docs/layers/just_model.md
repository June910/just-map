---
order: 0
title: 三维模型
---

## just_model

三维模型,支持格式为：obj、glb、gltf。具体属性见下：

- model-type  
  [metadata] [enum]'obj'|'gltf'.Default to 'obj'.  
  'obj':支持 obj 模型文件  
  'gltf':支持 glb 和 gltf 模型文件
- visibility 显隐  
  [Layout] property. Optional enum. One of 'visible','none'.Defaults to 'visible'  
  'visible':
  the layer is shown.  
  'none':
  the layer is hidden
- model-file 模型文件  
  [Paint] Property.Optional [url].
- model-orientation 模型旋转角度  
  [Paint] Property.三维数组[x,y,z],单位是 degrees.Defaults to [0,0,0].
- model-scale 模型缩放比例  
  [Paint] Property.三维数组[x,y,z],Defaults to [1,1,1].
- model-translation 模型平移距离  
  [Paint] Property.三维数组[x,y,z],单位是米,Defaults to [0,0,0].
- model-color 模型颜色  
  [Paint] Property.Optional [color](/types/#color).默认是模型本身的颜色
- opacity 透明度  
  [Paint] Property. Optional number between 0 and 1 inclusive. Defaults to 1.

  ```js
  map.addLayer({
    id: 'modellayer-1',
    type: 'just_model',
    source: {
      type: 'geojson',
      data: pointData,
    },
    metadata: {
      'model-type': 'gltf',
    },
    paint: {
      'model-file':
        'https://docs.mapbox.com/mapbox-gl-js/assets/34M_17/34M_17.gltf',
    },
  });

  map.setLayerProperty('modellayer-1', {
    paint: {
      // 'model-scale': [1,10,1],
      'model-scale': ['get', 'scale'],
    },
  });
  ```

  **目前仅支持部分简单的表达式**

  - 常数
  - get(获取属性信息) ["get","temperature"]
  - 逻辑计算 ["-",100,["get","temperature"]]
  - case(条件表达)  
     ["case",  
    ["==",["get","temaperature"],100],  
    value1,  
    ["==",["get","temaperature"],200],  
    value2,  
    ["==",["get","temaperature"],300],  
    value3,  
    defaultValue  
     ]
