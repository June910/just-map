---
order: 1
title: 建筑物白膜
---

## just_solidPolygonLayer

对 2.5D 的封装 表现为顶部和侧面均可配置颜色和纹理。配置颜色时，整体表现为简模。具体属性见下：

- visibility 显隐  
  [Layout] property. Optional enum. One of 'visible','none'.Defaults to 'visible'  
  'visible':  
  the layer is shown.  
  'none':  
  the layer is hidden
- elevation 高度  
  [Paint] Property.Optional [number].Defaults to 0.0 支持表达式
- elevation-scale 高度比例  
  [Paint] Property.Optional [number].Defaults to 1.0
- side-color 侧面颜色  
  [Paint] Property.Optional [color](/types/#color).Defaults to "#000000".Disabled by <font color="red">side-pattern</font> 支持表达式.
- top-color 顶部颜色  
  [Paint] Property.Optional [color](/types/#color).Defaults to "#000000".Disabled by <font color="red">top-pattern</font> 支持表达式.
- side-pattern 侧面纹理  
  [Paint] Property. Optional url | img  
  url:图片的链接地址  
  img:img(eg. imprt img from './xxx.png')
- top-pattern 顶部纹理  
  [Paint] Property. Optional url | img  
  url:图片的链接地址  
  img:img(eg. imprt img from './xxx.png')
- opacity 透明度  
  [Paint] Property. Optional number between 0 and 1 inclusive. Defaults to 1.
- material 是否加入光照计算  
   [Paint] Property. Optional [boolean] Defaults to true.
  ```js
  map.addLayer({
    id: 'solidpolygon-1',
    type: 'just_solidPolygon',
    source: {
      type: 'geojson',
      data: buildingData,
    },
    paint: {
      'top-color': '#ffff',
      'side-color': [
        'case',
        ['==', ['get', 'height'], 20],
        'rgb(255,0,0)',
        ['==', ['get', 'height'], 10],
        'rgb(255,0,255)',
        'rgb(255,255,0)',
      ],
      elevation: ['*', 3, ['get', 'height']],
    },
  });
  ```
  **目前仅支持部分简单的表达式**
  - 常数
  - get(获取属性信息) ["get","temperature"]
  - 逻辑计算 ["-",100,["get","temperature"]]
  - case(条件表达)  
     ["case",  
    ["==",["get","temaperature"],100],  
    value1,  
    ["==",["get","temaperature"],200],  
    value2,  
    ["==",["get","temaperature"],300],  
    value3,  
    defaultValue  
     ]
