---
order: 0
title: 栅格切片图层
---

## Raster Layer

栅格切片,对原有的 mapbox 栅格切片图层进行扩展，支持 tms、xyz、gaodd、baidu 四种 scheme 类型，xyz 和 tms 是 mapbox 原生支持的，scheme 默认为 xyz。

- visibility 显隐  
  [Layout] property. Optional enum. One of 'visible','none'.Defaults to 'visible'  
  'visible':
  the layer is shown.  
  'none':
  the layer is hidden

- raster-opacity 透明度  
  [Paint] Property. Optional number between 0 and 1 inclusive. Defaults to 1.

  ```js
  mapRef.current.addLayer({
    id: 'GD-layer',
    type: 'raster',
    source: {
      type: 'raster',
      tiles: [
        'http://webrd01.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=8&x={x}&y={y}&z={z}',
      ],
      tileSize: 256,
      scheme: 'gaode', // tms xyz baidu gaode四种类型
    },
    paint: {
      'raster-opacity': 1.0,
    },
  });

  // 设置透明度
  map.setLayerProperty('GD-layer', {
    paint: {
      // 'model-scale': [1,10,1],
      'raster-opacity': 0.5,
    },
  });

  // 设置显隐
  map.setLayerProperty('GD-layer', {
    layout: {
      visibility: 'none',
    },
  });
  // 图层上下移动
  mapRef.current.moveLayer('GD-layer', 'normal-layer');
  // 分级别显示
  mapRef.current.setLayerZoomRange('GD-layer', 10, 15);
  // 删除图层
  mapRef.current.removeLayer('GD-layer');
  ```

```

```
