---
order: 2
---

## Map

### 初始化

JustMap 对象代表了页面上的地图

**初始化参数**
| 参数| 类型| 说明| 默认值|
| - | - | - | -|
|map?| mapboxgl.Map| mapbox-gl 实例,可外部在实例化后传递给 JustMap；也可在 JustMap 内部实例化 | - |
|baseUrl| string| 后端 server 的域名地址 |
|token?| string| 鉴权 token |
|style|`JustStyle \| string` | TODO |
|styleType|enum(`justmap\|mapbox`) | 加载的 style 文件类型 |justmap
|mapboxOptions?| `Omit<mapboxgl.MapboxOptions, 'style'>` | 去掉 MapboxOptions.style 之后的[其他参数](https://docs.mapbox.com/mapbox-gl-js/api/map/#map-parameters) | `{ attributionControl: false }`

<br/>

```js
const map = new JustMap({
  style: style_obj,
  mapboxOptions: {
    ontainer: 'map',
    center: [-122.420679, 37.772537],
    zoom: 13,
  },
});
map.on('load', () => {
  // 加载数据源和图层
});
```

### 图层

- addLayer(layer: [Layer](/types#layer), beforeId?:string)

  - 向地图中添加图层
  - beforeId:指定在某个图层之下的图层 id,如果为空,则添加的图层在最上面

```js
  map.addLayer(  {
    "id": "water",
    "source": "mapbox-streets",
    "source-layer": "water",
    "type": "fill",
    "paint": {
      "fill-color": "#00ffff"
    })
```

- removeLayer(layerId: string)

  - 删除图层

  ```js
  map.removeLayer('water');
  ```

- setLayerProperty(layerId: string, options: [LayerOptions](/types#layer))

  - 设置图层属性，包括（Paint 或者 Layout） \*_注：setLayerProperty 兼容了 setPaintProperty,setLayoutProperty,三个接口都可以改变图层属性_

- setPaintProperty(layerId: string, name: string, value: any)

  - 设置图层 Paint 属性 \*_注：setLayerProperty 兼容了 setPaintProperty,setLayoutProperty,三个接口都可以改变图层属性_

- setLayoutProperty(layerId: string, name: string, value: any)
  - 设置图层 Layout 属性 \*_注：setLayerProperty 兼容了 setPaintProperty,setLayoutProperty,三个接口都可以改变图层属性_

* moveLayer(layerId: string,beforeId?:string)
  - 调整图层显示层叠顺序
    - id(string)需要移动的图层 ID
    - beforeId(string?)用来插入新图层的现有图层 ID。 如果该参数（argument）被省略，该图层将会被添加到图层数组的末尾。
* getLayers()
  - 获取所有图层
* getLayer(layerId:string)
  - 获取指定图层
* queryRenderedFeatures(geometry?,options?)

  - 数据筛选 具体使用方法见[queryRenderedFeatures](http://www.mapbox.cn/mapbox-gl-js/api/#map#queryrenderedfeatures)

* querySourceFeatures(sourceId: string, options?: Object)
  - 返回一个 GeoJSON Feature objects 数组，这些对象表示满足查询参数的指定矢量切片或 GeoJSON 数据源中的要素
  - 具体使用方法见[querySourceFeatures](https://docs.mapbox.com/mapbox-gl-js/api/map/#map#querysourcefeatures)
  ```js
  const features = map.querySourceFeatures('your-source-id', {
    sourceLayer: 'your-source-layer',
  });
  ```
* setFilter(layerId:string,filter:Array,options={})
  - 设置属性过滤
  - **Parameters**
    - layerId (string) 图层 id
    - filter (Array|null|undefined)筛选器，如果是 null 或者 undefined,则显示全部
    - **options**  
      | 名称| 描述|
      | - | - |
      | options.validate| 是否检查筛选器符合 Mapbox GL 的样式定义。
  ```js
  map.setFilter('my-layer', ['==', 'name', 'USA']);
  ```
* setSpatialFilter(layerId,spatialFilter:Array)
  - 设置空间过滤
  - **Parameters**
    - layerId(string) 图层 id
    - spatialFilter (Array|null|undefined)空间筛选器，如果是 null 或者 undefined,则显示全部
      - ['crosses'|'contains',GeoJSON.Feature[] | GeoJSON.Geometry[] | wkt[]]
  ```js
  map.setSpatialFilter(layerId, ['crosses', [geomety1, geometry2]]);
  ```

### source 数据源

- addSource(id,sourceOption:[sourceOptions](/types#source))

  - 添加数据源

  ```js
  map.addSource('water-source', {
    type: 'vector',
    tiles: [
      'http://a.example.com/tiles/{z}/{x}/{y}.pbf',
      'http://b.example.com/tiles/{z}/{x}/{y}.pbf',
    ],
    maxzoom: 14,
  });
  ```

- removeSource(id:string)
  - 移除数据源
- getSource(id:string)
  - 获取指定数据源
- source.setData(url|geojsonObj) _仅支持 geojson 数据源_
  - 给数据源重新设置数据

### Feature State

- setFeatureState(feature: mapboxgl.MapboxGeoJSONFeature, options: Object)

  - 设置要素的状态。状态 对象会与要素的现有状态合并。

  -具体使用方法见[setFeatureState](https://docs.mapbox.com/mapbox-gl-js/api/map/#map#setFeatureState)

- removeFeatureState(
  target: mapboxgl.FeatureIdentifier | mapboxgl.MapboxGeoJSONFeature,
  key: string,
  )

  - 移除要素状态，将其设置回默认行为。如果只有数据被指定，移除该数据源的所有状态。如果 target.id 也被指定，移除该要素状态的所有键。 如果键也被指定，从该要素状态中移除指定的键。

  -具体使用方法见[removeFeatureState](https://docs.mapbox.com/mapbox-gl-js/api/map/#map#setFeatureState)

### control

- addControl(control: [ControlProperty](/types/#Control))
  - 添加组件

```js
//情况一：添加 mapbox 原生组件,type:'scale' | 'navigation' | 'geolocate' | 'attribution' | 'fullscreen'
map.addControl({
  id: 'scale-control',
  type: 'scale', //mapbox gl 原生scale组件
  options: {
    position: 'top-right',
  },
});

//情况二：添加 mapbox 原生组件,这些组件通过继承IControl接口实现，包括'scale' | 'navigation' | 'geolocate' | 'attribution' | 'fullscreen'|'draw'等第三方开源组件。position包括'top-left' , 'top-right' , 'bottom-left' , and 'bottom-right' . 默认是 'top-right'
map.addControl(new mapboxgl.scale(),position:'top-left')

//情况三： 添加 justmap 自定义组件,控制组件在地图上的任意位置;
map.addControl({
  id: 'scale-control',
  type: 'jscale', //justmap封装的scale组件
  options: {
    x: 'none',
    y: 'none',
    style: {
      bottom: '20px',
      left: '20px',
    },
  },
});
```

- getCanvasContainer()
  - 返回包含地图 canvas 标签的 HTML 元素

```js
const canvasContainer = map.getCanvasContainer();
```

- getContainer()

  - 返回地图的 HTML 嵌套元素

```js
const container = map.getContainer();
```

- getCanvas()

  - 返回地图的 canvas 元素

```js
const canvas = map.getCanvas();
```

- triggerRepaint()
  - 触发一个显示框的渲染。使用自定义图层时，当图层发生改变，使用此方法去重渲染。 在下一个显示框渲染前多次调用此方法也只会渲染一次。

\* just-map-render 相关的 api， 单独使用 just-map 无效

- removeControl(id: `string | mapboxgl.IControl | mapboxgl.Control`)

  - 移除组件

  ```js
  map.removeControl('scale-control');
  ```

- setControlProperty(id: string, options: [ControlProperty](/types/#Control), isUpdate?: boolean)
  - 设置组件的属性 \*_just-map-render 相关的 api， 单独使用 just-map 无效_

### style

- setStyle(setStyle(style: JustStyle | string)

  - 替换整个 style

- setBaseStyle(style: JustStyle)
  - 替换所有 source，图层， 交互

### event

- load justmap 的所有资源加载完毕后触发 触发后才能加载其它数据源和图层
- style.load justmap 的 style 文件下载完成后触发
- source.data 每个数据源加载完成后都会触发 暴漏出的 e 中含有 sourceId
- scene.update 一般在 just-map-render 中使用，用于监听组件的更新
- resize 调整地图大小后立即触发该事件
- 其它 mapbox 地图相关的原生事件，如 mousemove click 等可以参考[mapbox 事件](http://www.mapbox.cn/mapbox-gl-js/api/#map.event:resize)

```js
map.on('load', () => {
  map.addSource(); // 加载数据源
  map.addLayer(); //加载图层
});
map.on('click', (e) => {
  const { lngLat, point } = e;
});
map.on('click', layerId, (e) => {
  // 只有点击图层才会触发
  const { lngLat, point } = e;
});
map.resize();
```

### interaction(交互)

- map.addInteraciton(layerId,eventId,type,actionOption) 增加图层交互(一个图层可以添加多个交互)
  - layerId:[string]图层 id
  - eventId:[string]事件 id
  - type:[enum] 地图事件类型如'click'|'dbclick'|'mousemove...'
  - actionOption:[LayerInteractionOption](/types/#LayerInteractionOption)
  ```js
  map.addInteraction('external_vector_d680c6c3', 'event-113', 'mousemove', {
    type: 'highlight',
    options: {
      paint: {
        'fill-extrusion-height': 400,
        'fill-extrusion-pattern': 'poi_red',
      },
    },
  });
  ```
  \*_注：actionOption 中的 type 类型为 popup 不能在 just-map 中起作用，只能在 just-map-render 用_
- map.removeInteraction(layerId,eventId)移除图层中的某个交互

  - layerId:[string]图层 id
  - eventId:[string]事件 id

### 地图视图

- map.flyTo 动画定位到另外一个视图状态(中心点、缩放级别、方位角、倾斜度)，使其沿着一条曲线动态地变化并引发飞行效果 [使用方法](http://www.mapbox.cn/mapbox-gl-js/api/#map#flyto)
- map.easeTo 动画定位到另外一个视图状态(中心点、缩放级别、方位角、倾斜度) [使用方法](http://www.mapbox.cn/mapbox-gl-js/api/#map#easeto)
- map.jumpTo 瞬间定位到另外一个视图状态(中心点、缩放级别、方位角、倾斜度) [使用方法](http://www.mapbox.cn/mapbox-gl-js/api/#map#jumpto)
- map.fitBounds [使用方法](http://www.mapbox.cn/mapbox-gl-js/api/#map#fitbounds)
- map.setZoom(zoom:number) 设置地图缩放级别
- map.setPitch(pitch:number) 设置地图方位角
- map.setBearing(bearing:number) 设置地图倾斜角
- map.setCenter(center:[LngLatLike](/types#LngLatLike)) 设置地图中心点

### 原生 mapbox-gl

JustMap 只是对 mapbox-gl 做了一层封装。如果 justmap 暴露的方法不满足要求，可以通过`justMap.map`获取实例化后的 mapbox-gl 对象，直接调用 mapbox-gl 的方法。
