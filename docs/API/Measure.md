---
order: 3
---

# Measure

- 测量工具，支持坐标测量、长度测量和面积测量
- 支持图形增加修改删除等编辑功能，编辑同时测量结果同步显示

```js
import { JustMap, Draw } from '@jd/just-map';
const map = new JustMap({
  style: style_obj,
  mapboxOptions: {
    ontainer: 'map',
    center: [117, 37.77],
    zoom: 13,
  },
});
const measure = map.addMeasure();
```

## API

- ### remove()

  移除绘图工具

- ### changeMode(drawType)

  增加一个要素  
  **parameters**

  - drawType 绘画的几何类型[coordiante|length|area]

  **returns**
  measure 实例

- ### deleteAll()

  删除所有要素  
  **returns**  
  measure 实例

- ### trash()

  删除选中的图形或者节点  
  **returns**  
  measure 实例

## 注意事项

1. draw 和 measure 不能共存，如果想用 measure，需要先卸载 add 功能
