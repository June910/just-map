---
order: 3
---

## Popup

```js
import { JustMap, Popup } from '@jd/just-map';
const map = new JustMap({
  style: style_obj,
  mapboxOptions: {
    ontainer: 'map',
    center: [117, 37.77],
    zoom: 13,
  },
});
const popup = new Popup()
  .setLngLat({ lng: 117, lat: 39 })
  .setText(div)
  .addTo(map);
```

popup 的具体使用说明见[popup](http://www.mapbox.cn/mapbox-gl-js/api/#popup)
