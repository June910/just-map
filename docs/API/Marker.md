---
order: 4
---

## Marker

```js
import { JustMap, Popup, Marker } from '@jd/just-map';
const map = new JustMap({
  style: style_obj,
  mapboxOptions: {
    ontainer: 'map',
    center: [117, 37.77],
    zoom: 13,
  },
});
const marker = new Marker().setLngLat({ lng: 117, lat: 39 }).addTo(map);
const markerClick = ({ el, params }) => {
  const { _lngLat } = el.marker; //el.marker 才是mapboxgl 的marker
};
```

marker 的具体使用说明见[marker](http://www.mapbox.cn/mapbox-gl-js/api/#marker)
