---
order: 3
---

# Draw

- 绘图工具，支持点、线、多边形、矩形和圆五中图形的绘制
- 支持图形增加修改删除等编辑功能

## 初始化

addDraw(styles,mode)
| 名称 | 介绍 |默认值|
| --------------------- | ------------------------------------------------------------------------------------------ |----|
| styles[Array<object>]？ | 绘画图形的样式 [参考](https://github.com/mapbox/mapbox-gl-draw/blob/main/docs/EXAMPLES.md) 选填 |
| mode[object]? | 绘图模式 [参考](https://github.com/mapbox/mapbox-gl-draw/blob/main/docs/MODES.md) 选填|

```js
import { JustMap, Draw } from '@jd/just-map';
const map = new JustMap({
  style: style_obj,
  mapboxOptions: {
    ontainer: 'map',
    center: [117, 37.77],
    zoom: 13,
  },
});
const draw = map.addDraw();
```

## API

- ### remove()

  移除绘图工具

- ### changeMode(drawType)

  增加一个要素  
  **parameters**

  - drawType 绘画的几何类型[point|line|circle|rectangle|polygon]

  **returns**
  draw 实例

- ### add(geojson)

  增加一个要素  
  **parameters**

  - geojson 要素 [GeoJSON.FeatureCollection|GeoJSON.Feature|GeoJSON.Geometry]

  **returns**  
  增加的要素的 ID 数组

  **Example**

  ```js
  var feature = { type: 'Point', coordinates: [0, 0] };
  var featureIds = draw.add(feature);
  console.log(featureIds);
  //=> ['some-random-string']

  var feature = {
    id: 'unique-id',
    type: 'Feature',
    properties: {},
    geometry: { type: 'Point', coordinates: [0, 0] },
  };
  var featureIds = draw.add(feature);
  console.log(featureIds);
  //=> ['unique-id']
  ```

- ### get(geojson)

  增加一个要素  
  **parameters**

  - geojson 要素 [GeoJSON.FeatureCollection|GeoJSON.Feature|GeoJSON.Geometry]

  **returns**  
  增加的要素的 ID 数组

  **Example**

  ```js
  var featureIds = draw.add({ type: 'Point', coordinates: [0, 0] });
  var pointId = featureIds[0];
  console.log(draw.get(pointId));
  //=> { type: 'Feature', geometry: { type: 'Point', coordinates: [0, 0] } }
  ```

- ### getAll()

  获取绘画的所有要素
  **returns**  
  绘画的所有要素[Geojson.FeatureCollection]

  **Example**

  ```js
  var featureIds = Draw.getFeatureIdsAt({ x: 20, y: 20 });
  console.log(featureIds);
  //=> ['top-feature-at-20-20', 'another-feature-at-20-20']
  ```

- ### getSelected()

  获取选中的要素  
  **returns**  
  选中的要素[GeoJSON.FeatureCollection]

- ### getSelectedIds()

  获取选中的要素的 Id 数组  
  **returns**  
  选中的要素的 Id 数组[Array<string>]

- ### getFeatureIdsAt(point)

  根据要素坐标得到要素 Id 数组  
  **parameters**

  - point 屏幕坐标 [{x:number,y:number}]

  **returns**  
  要素 ID 数组[Array<string>]

- ### delete(ids)

  根据要素 ID 删除要素  
  **parameters**

  - ids 要素 id [string|Array<string>]

  **Example**

  ```js
  var featureIds = Draw.getFeatureIdsAt({ x: 20, y: 20 });
  console.log(featureIds);
  //=> ['top-feature-at-20-20', 'another-feature-at-20-20']
  ```

  **returns**  
  draw 实例

- ### deleteAll()

  删除所有要素  
  **returns**  
  draw 实例  
  **Example**

  ```js
  draw.add({ type: 'Point', coordinates: [0, 0] });
  draw.deleteAll().getAll();
  // { type: 'FeatureCollection', features: [] }
  ```

- ### trash()

  删除选中的图形或者节点  
  **returns**  
  draw 实例

- ### on(eventType,listener)

  事件绑定  
  **parameters**

  - eventType 事件类型 [draw.create|draw.delete|draw.update|draw.modechange|draw.selectionchange]
  - listenrer 回调函数

- ### off(eventType,listener)
  事件解绑  
  **parameters**
  - eventType 事件类型 [draw.create|draw.delete|draw.update|draw.modechange|draw.selectionchange]
  - listenrer 回调函数

## 事件介绍

- draw.create 新建要素后
- draw.delete 删除要素后
- draw.update 更新图形后
- draw.modechange 模式更改后
- draw.selectionchange 选择图形更改后

## 注意事项

1. draw 和 measure 不能共存，如果想用 draw，需要先卸载 measure 功能
2. draw 添加的事件，在 draw 卸载后会全部删除
