---
order: 3
---

## 类型说明

内部对象的类型， 可以直接从 ts 文件中复制。之后考虑从 ts 导出一份。目前手动维护

### Layer

图层

```js
  {
    "id": "water",
    "source": "mapbox-streets",
    "source-layer": "water",
    "type": "fill",
    "paint": {
      "fill-color": "#00ffff"
    }
```

- **id**
  _Required_ [string]  
  图层的唯一名称
- **type**
  _Required_ [string]  
  图层类型 具有以下几种类型:  
  "fill":面,  
  "line":线,  
  "symbol":符号,  
  "circle":点,  
  "heatmap":热力图,  
  "fill-extrusion":2.5D,  
  "raster":栅格,  
  "hillshade":山体阴影,  
  "background":背景,  
  "just_cylinder":just 封装的三维柱状图,  
  "just_heatmap":just 封装的热力图,  
  "just_cluster":just 封装的聚合图,
  "just_trips":just 封装的实时轨迹图,
  "just_solidPolygonLayer": just 封装的 2.5D 具体参数参考[2.5D](/layers/just_solidPolygonLayer)

- **source**
  _Optional_ [string]  
  图层的数据源名称

- **source-layer**
  _Optional_ [string]  
  数据源中某个图层名称 当数据源为矢量瓦片时，需要指定矢量瓦片数据源的 source-layer

- **minzoom**
  _Optional_ [number] 0-24  
  当前图层显示的地图最小缩放级别，小于这个级别不显示

- **maxzoom**
  _Optional_ [number] 0-24  
  当前图层显示的地图最大缩放级别，大于这个级别不显示

- **filter**
  _Optional_ [expression](https://docs.mapbox.com/mapbox-gl-js/style-spec/expressions/)  
  属性过滤条件

- **spatialFilter**
  _Optional_ ['crosses'|'contains',GeoJSON.Feature[] | GeoJSON.Geometry[] | wkt[]]  
  空间过滤条件

- **paint**
  _Optional_ [paint](http://www.mapbox.cn/mapbox-gl-js/style-spec/)  
  图层 paint 属性

- **layout**
  _Optional_ [layout](http://www.mapbox.cn/mapbox-gl-js/style-spec/)  
  图层 layout 属性

- **metadata**
  _Optional_ [metadata]  
  图层扩展属性：
  - just:service_id: 模型 id studio 中用于数据校验
  - just:name_cn:图层中文名称

### Source

数据源  
常见的几种数据源添加方式

- geojson

```js
{
  "type": "geojson",
  "data": {
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [-77.0323, 38.9131]
        },
        "properties": {
            "title": "Mapbox DC",
            "marker-symbol": "monument"
        }
    }
}
```

- vector

```js
{
  "type": "vector",
  "tiles": [
    "http://a.example.com/tiles/{z}/{x}/{y}.pbf",
    "http://b.example.com/tiles/{z}/{x}/{y}.pbf"
  ],
  "maxzoom": 14
}
```

- raster

```js
{
  "type": "raster",
  "tiles": [
   'http://a.example.com/wms?bbox={bbox-epsg-3857}&format=image/png&service=WMS&version=1.1.1&request=GetMapsrs=EPSG:3857&width=256&height=256&layers=example'
  ],
  "tileSize": 256
}
或者
{
    "type": "raster",
    "url": "mapbox://mapbox.satellite",
    "tileSize": 256
}
```

- **type**
  _Required_ [string]  
  数据源类型 具有以下几种类型:  
  "vector":矢量切片,  
  "raster":栅格切片,  
  "raster-dem":高程[Mapbox Terrain RGB](https://blog.mapbox.com/global-elevation-data-6689f1d0ba65),  
  "geojson":geojson,  
  "image":图片,  
  "video":视频,  
  "raster":栅格,  
  "trips":justmap 封装的实时轨迹数据源

- **url**
  _Optional_ [string]  
  数据源地址 支持 vector raster raster-dem video 几种数据源

- **tiles**
  _Optional_ [array] of [strings]
  多个矢量切片地址数组 支持 vector

- **minzoom**
  _Optional_ [number] Defaults to 0  
  数据源可用的最小缩放级别
- **minzoom**
  _Optional_ [number] Defaults to 22  
  数据源可用的最大缩放级别

- **tileSize**
  _Optional_ [number] Defaults to 512  
  切片大小
- **scheme**
  _Optional_ [enum] "xyz"|"tms".Defaults to "xyz"  
  切片组织方式

- **data**
  _Optional_ [string]  
  geojson 数据源的 url 或者是 geojsonObj

- **cluster**
  _Optional_ [boolean] Defaults to false  
  geojson 数据源是否进行聚合

- **clusterRadius**
  _Optional_ [string] Defaults to 50  
  geojson 数据源的聚合半径

- **clusterMaxZoom**
  _Optional_ [number]  
  geojson 数据源的最大聚合级别

### Control

- id 组件 id
- type \_组件类型
  'scale'
  | 'navigation'
  | 'geolocate'
  | 'attribution'
  | 'fullscreen'是 mapbox 原生组件类型  
  'jscale'
  | 'poi'
  | 'route'
  | 'fence'
  | 'layergroup'
  | 'popup' 是 just 能力封装的组件类型，**只能在 just-map-render 中使用**

- options _组件各自的属性_

### LayerInteractionOption

- type:[enum]'highlight'|'popup'|'flyTo'(高亮|弹框|地图视图飞至中心点)
- options:[any] 交互配置信息

### color

The color type represents a color in the sRGB color space. Colors are written as JSON strings in a variety of permitted formats: HTML-style hex values, rgb, rgba, hsl, and hsla. Predefined HTML colors names, like yellow and blue, are also permitted.

```js
{
    "line-color": "#ff0",
    "line-color": "#ffff00",
    "line-color": "rgb(255, 255, 0)",
    "line-color": "rgba(255, 255, 0, 1)",
    "line-color": "hsl(100, 50%, 50%)",
    "line-color": "hsla(100, 50%, 50%, 1)",
    "line-color": "yellow"
}
```

### LngLatLike

一个 LngLat 对象，或者是表示经度和纬度的两个数字组成的数组， 也可以是一个具有 lng 和 lat 或者 lon 和 lat 属性的对象。

```js
const v1 = [117, 39];
const v2 = { lat: 39, lon: 117 };
const v3 = { lon: 117, lat: 39 };
```
