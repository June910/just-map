---
order: 1
title: 快速上手
nav:
---

## Just Map !

just gis 前端地图引擎

- 基于 mapbox-gl
- 支持 just gis 自有图层
- 兼容 just-map-render

### 安装

```bash
npm i @jd/just-map -S
```

### 快速上手

```tsx
import React, { useRef, useEffect } from 'react';
import { JustMap, JustSource } from '@jd/just-map';
// import scene from '../test/test-portal.json';

// import scene from '../test/标准版.json';
import scene from '../test/style.js';

function App() {
  const mapContainerRef = useRef();

  const mapRef = useRef();

  useEffect(() => {
    mapRef.current = new JustMap({
      baseUrl: 'https://just-newrc-beta.jdfmgt.com',
      token:
        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJKRC1VUkJBTi1KVVNULUdJUyIsInVzZXJOYW1lIjoianRlc3R1c2VyMyJ9.UvsBjOi2EhF8Z60cHcnaoshCD1p_t0X9oJ6hlqMq7m0',
      style: scene,
      // style:
      //   'https://iserver.supermap.io/iserver/services/map-mvt-China/rest/maps/China/tileFeature/vectorstyles.json?type=MapBox_GL&styleonly=true',
      // styleType: 'mapbox',
      mapboxOptions: {
        container: mapContainerRef.current,
        pitch: 60,
        bearing: 60,
        zoom: 9,
        center: [119, 39],
      },
    });
    // mapRef1.current = new JustMap({
    //   baseUrl: 'http://just-test.jd.com',
    //   token:
    //     'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJKRC1VUkJBTi1KVVNULUdJUyIsInVzZXJOYW1lIjoidGVzdGdpcyJ9.aPtr-qRrRQKLajqhq_ucAPtva2dhKbYCPwQ5htHF8J4',
    //   style: scene,
    //   // style:
    //   //   'https://iserver.supermap.io/iserver/services/map-mvt-China/rest/maps/China/tileFeature/vectorstyles.json?type=MapBox_GL&styleonly=true',
    //   // styleType: 'mapbox',
    //   mapboxOptions: {
    //     container: mapContainerRef.current,
    //   },
    // });
    mapRef.current.on('load', () => {
      //geojson数据图层测试
      // mapRef.current.addLayer({
      //   id: 'geojson-circle1',
      //   type: 'circle',
      //   source: {
      //     type: 'geojson',
      //     data: {
      //       type: 'FeatureColleciton',
      //       features: [
      //         {
      //           type: 'Feature',
      //           geometry: {
      //             type: 'Point',
      //             coordinates: [124, 39],
      //           },
      //         },
      //       ],
      //     },
      //   },
      //   paint: {
      //     'circle-color': 'rgba(255,255,0,1)',
      //     'circle-radius': 50,
      //   },
      // });
      // //矢量切片数据图层测试
      // mapRef.current.addLayer({
      //   layout: { visibility: 'visible' },
      //   metadata: { 'just:service_id': '4266', 'just:name_cn': '水系面' },
      //   paint: { 'fill-color': '#D0E1FF', 'fill-outline-color': '#D0E1FF' },
      //   id: 'water-test',
      //   source: '1618306740701',
      //   'source-layer': 'studio_ds_j_b_w_water_p_ITcwHm',
      //   type: 'fill',
      //   minzoom: 10,
      //   maxzoom: 22,
      // });
    });
  }, []);
  return (
    <>
      <div
        id="map-container"
        ref={mapContainerRef}
        style={{ height: '500px' }}
      ></div>
    </>
  );
}

export default App;
```
