## 0.1.0

- render map style from just studio
  - basic mapbox style
  - custom layer configged from just studio
    - heatmap
    - cylinder
    - fence
    - realtime

## 0.3.2

- 增加聚合图
- 增加交互功能
- 增加 popup 功能
- 优化实时轨迹

## 0.3.5

- 原生 popup 暴露
- 拦截 geojson 增加 just_global_id
- 去除 just:static 和 just:realtime 数据源类型,按照数据格式划分数据源类型
- 轮询的数据源和 layer 的 id 绑定 当绑定这个数据源的图层没有时，取消轮询
- 优化 justmap 的 load 事件
- 修复由于 beforeId 导致的 justLayer 添加失效问题

## 0.3.6

- 支持 mapbox 原生 style 文件

## 0.3.7

- 修复 index.d.ts 问题

## 0.3.8

- 支持 mapbox 原生 control map.addControl
- 支持 mapbox 原生 off 事件 map.off

## 0.3.9

- 修复 map.on('load')时 执行 map.addLayer 会再次触发 load 事件
- 增加 fitBounds 方法

## 0.3.10

- 修复多个 map 实例共享一份 style 文件问题

## 0.3.11

- 2.5D 支持精模和纹理
- 添加旋转、缩放、地图位置组件

### 0.3.12

- 支持模型效果
- 修复版本冲突问题

### 0.3.13

- 修复版本冲突

### 0.3.14

- 新增 map 接口
  - setPaintProperty
  - setLayoutProperty
  - setFeatureState
  - removeFeatureState
- 新增 marker
- 增加 mapbox 原生事件
- 修改实时轨迹查询接口时间问题
- 支持精模颜色值有透明度
- 修复数据源是聚合初始化不显示聚合效果问题

### 0.3.15

- 支持 customLayer
- setLayerZoomRange 变更 style 文件
- 支持 setPitch getPitch setBearing getBearing setCenter getCenter setZoom getZoom
- load 事件触发后，通过回调暴露出 map 实例

### 0.3.16

- 支持绘图工具
- 支持测量工具
- 新增视图的交互控制方法 boxZoom、scrollZoom、dragRotate、keyboard、doubleClickZoom、touchZoomRotate、dragPan
- 新增 loaded 方法
- addControl 自实现，自定义组件的事件通过 justmap 监听
- style 文件标准化

### 0.3.17

- 修复添加 mapbox 原生组件的 bug

### 0.3.18

- 增加三维模型对 justUrl 的兼容性
- 修复 getSource setData 的 bug
- 修复 draw measure 事件监听丢失问题(有 bug)

### 0.3.19

- 修复 draw measure 事件监听丢失问题

### 0.3.20

- setFilter 修复属性过滤未清空空间过滤的问题
- 修复精模表达式解析问题

### 0.3.21

- spatialFilter 修复矢量瓦片图层 properties 中没有 just_global_id 情况
- 修复 map 实例传入 justmap 时，数据源有 geojson 原始数据时，load 事件无法暴露情况
