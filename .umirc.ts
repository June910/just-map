import { defineConfig } from 'dumi';
import path from 'path';

const repo = 'just-map';

export default defineConfig({
  title: repo,
  history: { type: 'hash' },
  favicon:
    'https://user-images.githubusercontent.com/9554297/83762004-a0761b00-a6a9-11ea-83b4-9c8ff721d4b8.png',
  logo: 'https://user-images.githubusercontent.com/9554297/83762004-a0761b00-a6a9-11ea-83b4-9c8ff721d4b8.png',
  outputPath: 'docs-dist',
  mode: 'doc',
  hash: true,
  // Because of using GitHub Pages
  // base: `/${repo}/`,
  publicPath: `/${repo}/`,
  // navs: [
  //   null,
  //   {
  //     title: 'GitHub',
  //     path: 'https://github.com/umijs/dumi-template',
  //   },
  // ],
  // more config: https://d.umijs.org/config
  alias: {
    src: '@',
    '@jd/just-map/dist': path.resolve(__dirname, 'dist'),
  },
});
